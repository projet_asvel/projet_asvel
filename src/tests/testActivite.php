<?php
namespace Tests;

use App\Entity\Activite;
use App\Repository\ActiviteRepository;

require_once '../../config/globalConfig.php';
require_once 'inc/head.php';
?>
<body>

<?php

$loRepo = new ActiviteRepository() ;

echo '<h1>Tests des classes de gestion de la table "Activite"</h1>';
$lsNomNouvelleActivité = "ActivitéDeTest" ;
$datas =array (  
    'idActivite'=>Null,
    'nomActivite'=>$lsNomNouvelleActivité,
     );

echo '<h2>Nouvelle entité</h2>';
$loEntity = $loRepo->getEntityByRow($datas);
echo "<b>La nouvelle entité : </b>"; var_dump($loEntity);

echo '<h2>Enregistrer la nouvelle entité</h2>';
$loEntity = $loRepo->sauver($loEntity);
$liIdNouvelleEntité = $loEntity->getIdActivite() ;
$loEntity = $loRepo->getEntityById($liIdNouvelleEntité);
if ($loEntity->getNomActivite() == $datas["nomActivite"]) {
	echo '<span class="OK">Entité correctement insérée</span>' ;
	echo("<br><b>L'Id de la nouvelle entité : </b>");var_dump($liIdNouvelleEntité);
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span>' ;
}

$datas =array (  
    'idActivite'=>Null,
    'nomActivite'=>"Alpinisme",
     );

echo '<h2>Nouvelle entité en double</h2>';
$loEntity = $loRepo->getEntityByRow($datas);
$loEntity = $loRepo->vérifier($loEntity, $tabErr);
if (count($tabErr)!=1){
	echo '<span class="KO">KO : doublon d\'activité incorrectement détecté ('.count($tabErr).'!=1)</span><br>' ;	
} else if ($tabErr[0] != "La valeur de nomActivite n'est pas unique.") {
	echo '<span class="KO">KO : doublon d\'activité incorrectement détecté ("'.$tabErr[0].'"!="La valeur de nomActivite n\'est pas unique."</span><br>' ;	
} else {
	echo '<span class="OK">Le problème d\'unicité a été correctement détecté</span>' ;	
}

echo '<h2>Modification d\'une entité pour créer un doublon</h2>';
$loEntity = $loRepo->getEntityById($liIdNouvelleEntité);
$loEntity->setNomActivite("Alpinisme") ;
// var_dump($loEntity) ;
$loEntity = $loRepo->vérifier($loEntity, $tabErr);
if (count($tabErr)!=1){
	echo '<span class="KO">KO : doublon d\'activité incorrectement détecté ('.count($tabErr).'!=1)</span><br>' ;	
} else if ($tabErr[0] != "La valeur de nomActivite n'est pas unique.") {
	echo '<span class="KO">KO : doublon d\'activité incorrectement détecté ("'.$tabErr[0].'"!="La valeur de nomActivite n\'est pas unique."</span><br>' ;	
} else {
	echo '<span class="OK">Le problème d\'unicité a été correctement détecté</span>' ;	
}

echo '<h2>Effacer la nouvelle entité</h2>';
$rep = $loRepo->deleteEntityId($liIdNouvelleEntité);
if ($rep) {
	$loEntity = $loRepo->getEntityById($liIdNouvelleEntité) ;
}
if ($rep && $loEntity == NULL) {
	echo '<span class="OK">Entité correctement supprimée</span>' ;
} else {
	echo '<span class="KO">KO : échec de la suppression</span><br>' ;
	var_dump($loEntity) ;
}

echo '<h2>Affichage de toutes les entités</h2>';
$Rep = $loRepo->getAll();
var_dump($Rep);
?>
</body>
