<?php
namespace Tests;

use App\Entity\Participer;
use App\Repository\ParticiperRepository;

require_once '../../config/globalConfig.php';
require_once 'inc/head.php';
?>
<body>

<?php

$loRepo = new ParticiperRepository() ;

echo '<h1>Tests des classes de gestion de la table "Participer"</h1>';
$datas =array (  
    'idAdherent'=>1,
    'idSortie'=>1,
    'dateInscription'=>	"2020-02-01 08:00:00+01",		// Attention au format de date! (qui suppose date_default_timezone_set('Europe/Paris'), cf. appConfig.php 
    'montantArrhes'=>1,
    'montantTotal'=>2,
    'commentaireParticipation'=>"commentaireParticipation",
    'idFonction'=>1,				// 1 : participant, 2: Encadrant, 3: co-encadrant
    'idRaisonAnnulation'=>1,
    'idTypeReglementArrhes'=>1,			// 1;'Especes', 2;'Carte Bancaire', 3;'Chèques', 4;'Chèque vacances'
    'idTypeReglementMontantTotal'=>1,	// 1;'Especes', 2;'Carte Bancaire', 3;'Chèques', 4;'Chèque vacances'
     );

// $loEntity = new Participer($datas);
echo '<h2>Nouvelle entité</h2>';
$loEntity = $loRepo->getEntityByRow($datas);
$laIdParticipation = Array("idAdherent"=>$loEntity->getIdAdherent(), "idSortie"=>$loEntity->getIdSortie()) ;
echo "<b>La nouvelle entité : </b>"; var_dump($loEntity);
echo("<br><b>Le tableau de l'Id de la nouvelle entité : </b>");var_dump($laIdParticipation);

echo '<h2>Enregistrer la nouvelle entité</h2>';
$loEntity = $loRepo->sauver($loEntity);
// var_dump($loEntity) ;
$loEntity = $loRepo->getEntityById($laIdParticipation);
if (!$loEntity) {
	echo '<span class="KO">KO : échec de la récupération de l\'entité nouvellement créée</span>' ;	
}
//echo $datas["dateInscription"] . "<br>" ;
//echo $loEntity->getDateInscription() . "<br>" ;
if ($loEntity->getDateInscription() == $datas["dateInscription"]
		&& $loEntity->getMontantArrhes() == $datas["montantArrhes"]
		&& $loEntity->getMontantTotal() == $datas["montantTotal"]
		&& $loEntity->getCommentaireParticipation() == $datas["commentaireParticipation"]
		&& $loEntity->getIdFonction() == $datas["idFonction"]
		&& $loEntity->getIdRaisonAnnulation() == $datas["idRaisonAnnulation"]
		&& $loEntity->getIdTypeReglementArrhes() == $datas["idTypeReglementArrhes"]
		&& $loEntity->getIdTypeReglementMontantTotal() == $datas["idTypeReglementMontantTotal"]
		) {
	echo '<span class="OK">Entité correctement insérée</span>' ;
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span>' ;
	var_dump($loEntity) ;
}

echo '<h2>Modifier la nouvelle entité</h2>';
$datas["commentaireParticipation"] .= "Modifié" ;
$loEntity->setCommentaireParticipation($datas["commentaireParticipation"]) ;
$loEntity = $loRepo->sauver($loEntity);
if ($loEntity->getCommentaireParticipation() == $datas["commentaireParticipation"]) {
	echo '<span class="OK">Entité correctement mise à jour</span>' ;
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span>' ;
}

echo '<h2>Effacer la nouvelle participation (1, 1)</h2>';
$rep = $loRepo->deleteEntityId($laIdParticipation);
//var_dump($rep);
if ($rep) {
	$loEntity = $loRepo->getEntityById($laIdParticipation);
}
if ($rep && $loEntity == NULL) {
	echo '<span class="OK">Entité correctement supprimée</span>' ;
} else {
	echo '<span class="KO">KO : échec de la suppression</span><br>' ;
	var_dump($loEntity) ;
}

echo '<h2>Affichage de toutes les participations de la sortie 1</h2>';
$Rep = $loRepo->getParticipationsBySortie(1);
var_dump($Rep);
?>
