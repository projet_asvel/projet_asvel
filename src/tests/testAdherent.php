<?php
namespace Tests;

use App\Entity\Adherent;
use App\Entity\Role;
use App\Repository\AdherentRepository;

require_once '../../config/globalConfig.php';
require_once 'inc/head.php';
?>
<body>

<?php

$loRepo = new AdherentRepository() ;

echo '<h1>Tests des classes de gestion de la table "Adherent"</h1>';
$lsNomNouvelleAdherent = "AdherentDeTest" ;
$datas =array (  
    'idAdherent'=>Null,
    'nomAdherent'=>$lsNomNouvelleAdherent,
    'prenomAdherent'=>$lsNomNouvelleAdherent,
	'dateNaissanceAdherent'=>'1990-01-01',
	'telAdherent'=>'0607080910',
	'mailAdherent'=>'toto@toto.fr',
	'mdpAdherent'=>'mdp'
     );

echo '<h2>Nouvelle entité</h2>';
$loEntity = $loRepo->getEntityByRow($datas);
echo "<b>La nouvelle entité : </b>"; var_dump($loEntity);

echo '<h2>Vérifier la nouvelle entité</h2>';
if (!$loRepo->vérifier($loEntity, $tabErr)){
	echo '<span class="KO">KO : échec de la vérification</span>' ;
} else {
	echo '<span class="OK">Entité correctement vérifiée</span>' ;
}

echo '<h2>Enregistrer la nouvelle entité</h2>';
$loEntity = $loRepo->sauver($loEntity);
$liIdNouvelleEntité = $loEntity->getIdAdherent() ;
$loEntity = $loRepo->getEntityById($liIdNouvelleEntité);

//echo "<br>" ;
//var_dump($loEntity->getDateNaissanceAdherent() . "<br>") ;
//var_dump($datas["dateNaissanceAdherent"] . "<br>") ;
//var_dump(($loEntity->getDateNaissanceAdherent()==$datas["dateNaissanceAdherent"]) . "<br>") ;
//echo "<br>" ;

if ($loEntity->getNomAdherent() == $datas["nomAdherent"]
		&& $loEntity->getPrenomAdherent() == $datas["prenomAdherent"]
		&& $loEntity->getDateNaissanceAdherent() == $datas["dateNaissanceAdherent"]
		) {
	echo '<span class="OK">Entité correctement insérée</span>' ;
	echo("<br><b>L'Id de la nouvelle entité : </b>");var_dump($liIdNouvelleEntité);
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span>' ;
}

echo '<h2>Modifier la nouvelle entité</h2>';
$datas["nomAdherent"] .= "Modifiée" ;
$loEntity->setNomAdherent($datas["nomAdherent"]) ;
$roles = [new Role(["idRole"=>1]), new Role(["idRole"=>2])] ;
$loEntity->setRoles($roles);
$loEntity = $loRepo->sauver($loEntity);
$loEntity = $loRepo->getEntityById($liIdNouvelleEntité);
if ($loEntity->getNomAdherent() == $datas["nomAdherent"]
		&& count($loEntity->getRoles()) == 2
		&& $loEntity->getRoles()[0]->getIdRole() == 1
		&& $loEntity->getRoles()[1]->getIdRole() == 2
		) {
	echo '<span class="OK">Entité correctement mise à jour</span>' ;
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span>' ;
}

echo '<h2>Changer un rôle à la nouvelle entité</h2>';
$datas["nomAdherent"] .= "Modifiée" ;
$loEntity->setNomAdherent($datas["nomAdherent"]) ;
$roles = [new Role(["idRole"=>3]), new Role(["idRole"=>2])] ;
$loEntity->setRoles($roles);
$loEntity = $loRepo->sauver($loEntity);
$loEntity = $loRepo->getEntityById($liIdNouvelleEntité);

if ($loEntity->getNomAdherent() == $datas["nomAdherent"]
		&& count($loEntity->getRoles()) == 2
		&& $loEntity->getRoles()[1]->getIdRole() == 3
		&& $loEntity->getRoles()[0]->getIdRole() == 2
		) {
	echo '<span class="OK">Entité correctement mise à jour</span>' ;
} else {
	echo '<span class="KO">KO : au moins une propriété de la nouvelle entité n\'est pas conforme</span><br>' ;
	var_dump($loEntity) ;
//	echo $loEntity->getNomAdherent() . "<br>" ;
//	echo $datas["nomAdherent"] . "<br>" ;
//	echo count($loEntity->getRoles()) . "<br>" ;
//	echo $loEntity->getRoles()[1]->getIdRole() . "<br>" ;
//	echo $loEntity->getRoles()[0]->getIdRole() . "<br>" ;
}

echo '<h2>Effacer la nouvelle entité</h2>';
$rep = $loRepo->deleteEntityId($liIdNouvelleEntité);
if ($rep) {
	$loEntity = $loRepo->getEntityById($liIdNouvelleEntité) ;
}
if ($rep && $loEntity == NULL) {
	echo '<span class="OK">Entité correctement supprimée</span>' ;
} else {
	echo '<span class="KO">KO : échec de la suppression</span><br>' ;
	var_dump($loEntity) ;
}

echo '<h2>Affichage de toutes les entités</h2>';
$Rep = $loRepo->getAll();
var_dump($Rep);
?>
</body>
