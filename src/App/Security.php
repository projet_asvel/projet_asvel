<?php

namespace App;

use App\Repository\RoleRepository;

/**
 * Description of Security
 *
 * @author Haris
 */
class Security {

    const ROLE_ADMIN = "Administrateur";
    const ROLE_SECRETAIRE = "Secrétaire";
    const ROLE_ENCADRANT = "Encadrant";
    const ROLE_COMPTABLE = "Comptable";
    const ROLE_ADHERENT = "Adhérent";
    const ADMIN = Array("Administrateur");
    const SECRETAIRE = Array("Secrétaire");
    const ENCADRANT = Array("Encadrant");
    const COMPTABLE = Array("Comptable");
    const ADHERENT = Array("Adhérent");
    const ROLE_TOUS_SAUF_ADHERENT = Array(self::ROLE_ADMIN, self::ROLE_SECRETAIRE, self::ROLE_ENCADRANT, self::ROLE_COMPTABLE);
    const ROLE_TOUS_SAUF_VISITEUR = Array(self::ROLE_ADMIN, self::ROLE_SECRETAIRE, self::ROLE_ENCADRANT, self::ROLE_COMPTABLE, self::ROLE_ADHERENT);
    const ROLE_STAFF = Array(self::ROLE_ENCADRANT, self::ROLE_SECRETAIRE);
    const IDROLE_ADMIN = 1;
    const IDROLE_SECRETAIRE = 2;
    const IDROLE_ENCADRANT = 3;
    const IDROLE_COMPTABLE = 4;
    const IDROLE_ADHERENT = 5;

    public static function hasRole(array $RolesDemandes = null, $idAdherent = null) {
        $ok = FALSE;
        if ($RolesDemandes == null || $RolesDemandes == '') {
            return TRUE;
        }
        if ($idAdherent == null && isset($_SESSION['idadherent'])) {
            $idAdherent = $_SESSION['idadherent'];
        }
        if ($idAdherent !== null) {
            $RoleRep = new RoleRepository;
            $LesRoles = $RoleRep->getRolesByUser($idAdherent);
// LG 20211127 début
            // Puisque la personne a pu se connecter, c'est qu'il est adhérent. Pas besoin de lui donner le rôle d'adherent
            $LesRoles[] = $RoleRep->getEntityById(self::IDROLE_ADHERENT) ;
// LG 20211127 fin                    
            $ok = FALSE;
            ForEach ($LesRoles as $LeRoleDuUser) {
                // Pour chaque rôle de cet user
                foreach ($RolesDemandes as $leRoleDemande) {
                    // Pour chacun des rôles demandés
                    if ($LeRoleDuUser->getNomRole() == $leRoleDemande) {
                        $ok = TRUE;
                        break;
                    }
                }
                if ($ok) {
                    break;
                }
            }
        }

        return $ok;
    }

}
