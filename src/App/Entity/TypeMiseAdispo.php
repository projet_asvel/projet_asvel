<?php

namespace App\Entity;

class TypeMiseAdispo extends \Phaln\AbstractEntity {
    protected $idTypeMiseAdispo;
    protected $nomMiseAdispo;


    function getIdTypeMiseAdispo() {
        return $this->idTypeMiseAdispo;
    }

    function setIdTypeMiseAdispo($idTypeMiseAdispo) {
        $this->idTypeMiseAdispo = $idTypeMiseAdispo;
    }

    function getNomMiseAdispo() {
        return $this->nomMiseAdispo;
    }

    function setNomMiseAdispo($nomMiseAdispo) {
        $this->nomMiseAdispo = $nomMiseAdispo;
    }
}

