<?php

namespace App\Entity;

class TypeMateriel extends \Phaln\AbstractEntity {
    protected $idTypeMateriel;
    protected $nomType;

    function getIdTypeMateriel() {
        return $this->idTypeMateriel;
    }

    function setIdTypeMateriel($idTypeMateriel) {
        $this->idTypeMateriel = $idTypeMateriel;
    }

    function getNomType() {
        return $this->nomType;
    }

    function setNomType($nomType) {
        $this->nomType = $nomType;
    }
}
