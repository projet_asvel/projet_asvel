<?php

namespace App\Entity;



class TypeReglement extends \Phaln\AbstractEntity {
    protected $idTypeReglement;
    protected $nomTypeReglement;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdTypeReglement() {
        return $this->idTypeReglement;
    }

    function getNomTypeReglement() {
        return $this->nomTypeReglement;
    }

    function setIdTypeReglement($idTypeReglement) {
        $this->idTypeReglement = $idTypeReglement;
    }

    function setNomTypeReglement($nomTypeReglement) {
        $this->nomTypeReglement = $nomTypeReglement;
    }

}
