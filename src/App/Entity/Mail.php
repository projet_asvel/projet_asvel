<?php

namespace App\Entity;

class Mail extends \Phaln\AbstractEntity {
    protected $idMail;
    protected $sujetMail;
    protected $contenuMail;
    protected $dateMail;
    protected $idAdherentExpediteur;
    protected $destinatairesVisibles ;
    protected $mailExpediteur ;
	
    // LG 20200323
    public function getDestinataires() {
        $resultSet = NULL;
        $repo = new \App\Repository\AdherentRepository() ;
        $resultSet = $repo->getDestinatairesDuMail($this->getIdMail()) ;
        return $resultSet;
    }

    // LG 20200323
    public function getExpéditeur() {
        $resultSet = NULL;
        $repo = new \App\Repository\AdherentRepository() ;
        $resultSet = $repo->getEntitesById($this->getIdAdherentExpediteur()) ;
        return $resultSet;
    }

}
