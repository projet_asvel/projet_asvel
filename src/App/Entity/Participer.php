<?php

// LG 20200208 récupéré et adapté de YopTeam

namespace App\Entity;

/**
 * Description of Participer
 *
 * @author arnau
 */
class Participer extends \Phaln\AbstractEntity {

    protected $idAdherent_Initial = NULL;
    protected $idSortie_Initial = NULL;
    protected $idAdherent = NULL;
    protected $idSortie = NULL;
    protected $dateInscription = NULL;
    protected $montantArrhes = 0;
    protected $montantTotal = 0;
    protected $commentaireParticipation = '';
    protected $idFonction = NULL;
    protected $idInscripteur = NULL;
    protected $idRaisonAnnulation = NULL;
    protected $idTypeReglementArrhes = NULL;
    protected $idTypeReglementMontantTotal = NULL;
    protected $arrhesRemboursees = NULL;
    protected $validationParticipation;
    protected $nbPlaceVoiture;
    protected $DVA;
    protected $PelleSonde;
    protected $ExcluDuCovoiturage;
    protected $debutant;
    protected $loueMateriel;

    public function hydrate(array $datas = NULL) {
        parent::hydrate($datas);
        $this->idSortie_Initial = $this->idSortie;
        $this->idAdherent_Initial = $this->idAdherent;
    }

    function getInscripteur() {
        $repo = new \App\Repository\AdherentRepository();
        return $repo->getEntitesById($this->getIdInscripteur());
    }

    function getAdherent() {
        $repo = new \App\Repository\AdherentRepository();
        return $repo->getEntitesById($this->getIdAdherent());
    }

}
