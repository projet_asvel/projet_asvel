<?php

namespace App\Entity;

class Photo extends \Phaln\AbstractEntity {
    protected $idPhoto;
    protected $cheminPhoto;
    protected $dateHeurePhoto;
    protected $idSortie;
    protected $idAdherent;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdPhoto() {
        return $this->idPhoto;
    }

    function getCheminPhoto() {
        return $this->cheminPhoto;
    }

    function getDateHeurePhoto() {
        return $this->dateHeurePhoto;
    }

    function getIdSortie() {
        return $this->idSortie;
    }

    function getIdAdherent() {
        return $this->idAdherent;
    }

    function setIdPhoto($idPhoto) {
        $this->idPhoto = $idPhoto;
    }
    
    function setCheminPhoto($cheminPhoto) {
        $this->cheminPhoto = $cheminPhoto;
    }

    function setDateHeurePhoto($dateHeurePhoto) {
        $this->dateHeurePhoto = $dateHeurePhoto;
    }

    function setIdSortie($idSortie) {
        $this->idSortie = $idSortie;
    }

    function setIdAdherent($idAdherent) {
        $this->idAdherent = $idAdherent;
    }

}