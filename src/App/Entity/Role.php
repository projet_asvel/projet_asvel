<?php

namespace App\Entity;



class Role extends \Phaln\AbstractEntity {
    protected $idRole;
    protected $nomRole;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdRole() {
        return $this->idRole;
    }

    function getNomRole() {
        return $this->nomRole;
    }

    function setIdRole($idRole) {
        $this->idRole = $idRole;
    }

    function setNomRole($nomRole) {
        $this->nomRole = $nomRole;
    }

}
