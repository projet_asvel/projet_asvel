<?php

namespace App\Entity;

class Materiel extends \Phaln\AbstractEntity {
    protected $idMateriel;
    protected $Taille;
    protected $Pointure;
    protected $Modele;
    protected $Longitude1;
    protected $Longitude2;
    protected $Latitude1;
    protected $Latitude2;
    protected $dateAchat;
    protected $Commentaire;
    protected $Pays;
    protected $idTypeMateriel;
    protected $idTypeMiseAdispo;
    protected $idAdherent;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdAdherent() {
        return $this->idAdherent;
    }

    function setIdAdherent($idAdherent) {
        $this->idAdherent = $idAdherent;
    }

    function getIdTypeMiseAdispo() {
        return $this->idTypeMiseAdispo;
    }

    function setIdTypeMiseAdispo($idTypeMiseAdispo) {
        $this->idTypeMiseAdispo = $idTypeMiseAdispo;
    }

    function getIdTypeMateriel() {
        return $this->idTypeMateriel;
    }

    function setIdTypeMateriel($idTypeMateriel) {
        $this->idTypeMateriel = $idTypeMateriel;
    }

    function getPays() {
        return $this->Pays;
    }

    function setPays($Pays) {
        $this->Pays = $Pays;
    }

    function getCommentaire() {
        return $this->Commentaire;
    }

    function setCommentaire($Commentaire) {
        $this->Commentaire = $Commentaire;
    }

    function getDateAchat() {
        return $this->dateAchat;
    }

    function setDateAchat($dateAchat) {
        $this->dateAchat = $dateAchat;
    }
    
    function getLatitude2() {
        return $this->Latitude2;
    }

    function setLatitude2($Latitude2) {
        $this->Latitude2 = $Latitude2;
    }

    function getLatitude1() {
        return $this->Latitude1;
    }

    function setLatitude1($Latitude1) {
        $this->Latitude1 = $Latitude1;
    }

    function getLongitude2() {
        return $this->Longitude2;
    }

    function setLongitude2($Longitude2) {
        $this->Longitude2 = $Longitude2;
    }

    function getLongitude1() {
        return $this->Longitude1;
    }

    function setLongitude1($Longitude1) {
        $this->Longitude1 = $Longitude1;
    }

    function getModele() {
        return $this->Modele;
    }

    function setModele($Modele) {
        $this->Modele = $Modele;
    }

    function getIdMateriel() {
        return $this->idMateriel;
    }

    function setIdMateriel($idMateriel) {
        $this->idMateriel = $idMateriel;
    }

    function getPointure() {
        return $this->Pointure;
    }

    function setPointure($Pointure) {
        $this->Pointure = $Pointure;
    }

    function getTaille() {
        return $this->Taille;
    }

    function setTaille($Taille) {
        $this->Taille = $Taille;
    }

}
