<?php

namespace App\Entity;
use App\Repository\ActiviteRepository ;

class Sortie extends \Phaln\AbstractEntity {
    protected $idSortie;
    protected $nomSortie;
    protected $lieuSortie;
    protected $dateDebutSortie;
    protected $heuredepart;
    protected $dateFinSortie;
    protected $denivelleSortie;
    protected $distanceSortie;
    protected $altitudeMaxSortie;
    protected $commentairesSortie;
    protected $tarifSortie;
    protected $coutAutoroute;
    protected $nombreParticipantsMaxSortie;
    protected $nbVoituresSortie;
    protected $idDifficulte;
    protected $idOrganisateur;
    protected $idRaisonAnnulation;
    protected $inscriptionenligne;
    protected $fraisAnnexe ;
    protected $fraisAnnexeDetails ;
    protected $participations;
    protected $activites;
    protected $nomsActivites;

    function getIdDifficulte() {
        return $this->idDifficulte;
    }

    function setIdDifficulte($idDifficulte) {
        $this->idDifficulte = $idDifficulte;
    }

    function getNombreParticipantsMaxSortie() {
        return $this->nombreParticipantsMaxSortie;
    }
    function getNombreParticipantsSortie() {
        throw new Exception("Il faut utiliser getNombreParticipantsMaxSortie");
    }

    function setNombreParticipantsSortie($nombreParticipantsMaxSortie) {
        $this->nombreParticipantsMaxSortie = $nombreParticipantsMaxSortie;
    }

    function getActivites($pbIfNotSet = false) {
        if ($this->activites == null && !$pbIfNotSet) {
                // Il faut déterminer les participations maintenant
                // On a demandé la liste des passeports
                $Repo = new ActiviteRepository();
// echo $this->getIdSortie() ;
                $this->activites = $Repo->getAllBySortie($this->getIdSortie());
        }
        if (is_string($this->activites)) {
                // On a fourni la liste des rôles sous forme d'une chaine de caractères
                $Repo = new ActiviteRepository();
                $laActivites = explode(",", $this->activites) ;
                $this->activites = [] ;
                foreach($laActivites as $liActivite) {
                        if (!$liActivite) continue ;
                        $this->activites[] = $Repo->getEntityById($liActivite);
                }		
        }
        return $this->activites;
    }
    
    function getNomsActivites() {
            $activites = $this->getActivites() ;
            $nomsActivites = "" ;
            foreach($activites AS $activite) {
                    if ($nomsActivites) $nomsActivites .= ", " ;
                    $nomsActivites .= $activite->getNomActivite() ;
            }
            return $nomsActivites;
   }

   function getOrganisateur() {
       $repo = new \App\Repository\AdherentRepository() ;
       return $repo->getEntitesById($this->getIdOrganisateur()) ;
   }
   
// LG 20201028 début   function getTitre() {
   function getTitre($pbCourt = false) {
       if (!$this->idSortie) {
           // Nouvelle sortie
           $titre = "nouvelle" ;
        } else 
// LG 20201028 début
			if ($pbCourt) {
				$dateSortie = strtotime($this->getDateDebutSortie()) ;
				$titre = strtolower($this->getNomsActivites()) . " du " . date('d', $dateSortie) . " " . \App\Lib\Functions::getNomMois(date('m', $dateSortie)) ;
			} else
// LG 20201028 fin		
			{
            $titre = $this->getnomsortie() . ", " 
                    . strtolower($this->getNomsActivites()) 
                    . ", du " . date('d/m/Y',strtotime($this->getDateDebutSortie())) ;
             if ($this->getDateFinSortie() !== $this->getDateDebutSortie()) {
                 $titre .= " au " . date('d/m/Y',strtotime($this->getDateFinSortie())) ;
                 $titre .= " (" . $this->getNbJours() ." jours)" ;
             }
             $organisateur = $this->getOrganisateur() ;
             if ($organisateur) {
                 $titre .= " de " . $organisateur->getNomUsuel() ;
             }
        }
        return $titre ;
   }
   
   function getNbJours() {
        $nbMinutes = strtotime($this->getDateFinSortie()) - strtotime($this->getDateDebutSortie()) ;
        $nbJours = intval($nbMinutes / (24 * 60 * 60) + 1) ;
        return $nbJours ;
   }
   
   function getNbPlacesVoiture() {
       $repo = new \App\Repository\ParticiperRepository() ;
       return $repo->getNbPlacesVoiture($this->idSortie) ;
   }
   
   // LG 20210513
   // Retrouver le statut des inscriptions pour cette sortie
   // $psTexte              : pour retour du texte de statut (ouverte, close, pas encore souverte, ...)
   // $psDétails            : pour retour du commentaire sur le statut
   // valeur de retour      : true si les instcritions sont ouvertes
   function estOuvertePourInscription(&$psTexte, &$psDétails) {
        //  combien de temps au maximum il faut pour que la sortie soit ouverte : jourmax
        //  si sortie de 1j : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_1_J
        //  sinon si : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_2_3_J
        //  sinon : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_SUP_3_J
        // si $joursDiff< jourmax
        // 	si $joursDiff< NB_JOURS_LIMITE_INSCRIPTION_SORTIE jours clos
        // 	 sinon 	 ouvert selon heure et jours
        // sinon
        // pas encore ouvert
       
        $ltMaintenant = getdate();
        $ltSortie = strtotime($this->getDateDebutSortie());

        if ($ltSortie <= $ltMaintenant[0]) {
            // Sortie passée
            $psTexte = "Sortie passée" ;
            $psDétails = "Sortie passée ou en cours" ;
            return false ;
        } else if ($this->getInscriptionenligne()) {
            // L'inscription en ligne est ouverte
            $psTexte = "Inscription ouverte" ;
            $psDétails = "Inscription ouverte" ;
            return true ;
        } else {
            // L'inscription en ligne est fermée
            $psTexte = "Pas encore ouverte ou close" ;
            $psDétails = "Les inscriptions à la sortie sont closes ou pas encore ouvertes" ;
            return false ;            
        }
        
        return false ;
        // Code non testé à mettre en place quand le type d'ouverture automatique de la sortie sera mis en place
        
        $liNoJourDHui = $ltMaintenant['wday'] - 1; //Lundi=0 Dimanche=6
        if ($liNoJourDHui < 0) {
            $liNoJourDHui = 6;
        }
        $JoursNumActuelle = $ltMaintenant['yday'];
        $TimestampActuelle = $ltMaintenant[0]; #timetamps actuelle
        $HeureActuelle = idate("H", $TimestampActuelle) + $ltMaintenant['minutes'] / 60;
        $joursDiff = ceil(($ltSortie - $TimestampActuelle) / 86400);
        $nbJours = $this->getNbJours();

        if ($nbJours == 1) {
            $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_1_J;
        } elseif ($nbJours == 2 || $nbJours == 3) {
            $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_2_3_J;
        } elseif ($nbJours > 3) {
            $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_SUP_3_J;
        }

        if ($joursDiff >= $jourMax) {
            $psTexte = "Inscriptions pas encore ouvertes";
            $psDétails = "Les inscriptions seront ouvertes le mardi à 19h, " . $jourMax . " jours avant la sortie" ;
        } else if (($liNoJourDHui = 2 && $HeureActuelle >= 19)      // Mardi > 19h
                    || ($liNoJourDHui = 3)                          // Mercredi
                    || ($liNoJourDHui = 2 && $HeureActuelle < 19)   // Jeudi < 19h
                ) {
            // On est entre le mardi 19h et le jeudi 19h
            // Les inscriptions sont ouvertes
            $psTexte = "Inscriptions ouvertes";
            $psDétails = "Les inscriptions sont ouvertes" ;
            
        } else {
            // Les inscriptions sont closes
            $psTexte = "Inscriptions closes";
            $psDétails = "Les inscriptions sont closes" ;
        }
       
   }
}
