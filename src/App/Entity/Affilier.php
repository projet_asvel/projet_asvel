<?php

namespace App\Entity;

/**
 * Description of Federation
 *
 * @author Haris
 */
class Affilier extends \Phaln\AbstractEntity {
    protected $idAffiliation ;
    protected $idAdherent ;
    protected $idFederation ;
    protected $dateAffiliation ;
    protected $idTypeLicense ;
    protected $numeroLicense ;
    protected $dateFinAffiliation ;
    
    private $adherent ;
    private $federation ;
    
    // Renvoyer l'entité adhérent de cette affiliation
    function getAdherent($pbIfNotSet = false) {
        if ($this->adherent == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des fédérations
            $Repo = new \App\Repository\AdherentRepository();
            $this->adherent = $Repo->getEntityById($this->getIdAdherent());
        }
        return $this->adherent ;
    }
    
    // Renvoyer l'entité fédération de cette affiliation
    function getFederation($pbIfNotSet = false) {
        if ($this->federation == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des fédérations
            $Repo = new \App\Repository\FederationRepository();
            $this->federation = $Repo->getEntityById($this->getIdFederation());
        }
        return $this->federation ;
    }
    
    function getDateFinAffiliation() {
        if ($this->dateFinAffiliation) {
            return $this->dateFinAffiliation ;
        } else if ($this->getFederation()) {
            return $this->getFederation()->getDateFinAffiliation($this->dateAffiliation) ;
        } else {
            return null ;
        }
    }
}
