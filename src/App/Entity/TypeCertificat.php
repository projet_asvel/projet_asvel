<?php

namespace App\Entity;



class TypeCertificat extends \Phaln\AbstractEntity {
    protected $idTypeCertificat;
    protected $nomTypeCertificat;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdTypeCertificat() {
        return $this->idTypeCertificat;
    }

    function getNomTypeCertificat() {
        return $this->nomTypeCertificat;
    }

    function setIdTypeCertificat($idTypeCertificat) {
        $this->idTypeCertificat = $idTypeCertificat;
    }

    function setNomTypeCertificat($nomTypeCertificat) {
        $this->nomTypeCertificat = $nomTypeCertificat;
    }

}
