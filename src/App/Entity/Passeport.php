<?php

namespace App\Entity;



class Passeport extends \Phaln\AbstractEntity {
    protected $idPasseport;
    protected $nomPasseport;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdPasseport() {
        return $this->idPasseport;
    }

    function getNomPasseport() {
        return $this->nomPasseport;
    }

    function setIdPasseport($idPasseport) {
        $this->idPasseport = $idPasseport;
    }

    function setNomPasseport($nomPasseport) {
        $this->nomPasseport = $nomPasseport;
    }

}
