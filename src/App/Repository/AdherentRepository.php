<?php

namespace App\Repository;

use App\Entity\Adherent;
use App\Repository\RoleRepository;
use \App\Lib\Functions;
// use Phaln\BDD;
use \PDO;

class AdherentRepository extends \Phaln\AbstractRepository {

    protected $table = 'adherent';   // le nom de la table manipulée
    protected $idFieldName = 'idadherent'; // le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\Adherent';  // le nom de la classe mappée
    protected $notFieldProps = ["roles", "passeports", "federations", "affiliations", "tags", "numeroLicense", "nomTypeLicense", "nomFederationLicense", "dateLicense", "dateFinDerniereAffiliation"];  // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    protected $mandatoryProps = ["nomAdherent", "prenomAdherent"];
    protected $checkableProps = [["field" => "dateNaissanceAdherent", "check" => "date"]];
    protected $uniqueProps = [["nomAdherent", "prenomAdherent", "dateNaissanceAdherent"], ["mailAdherent"]];

    public function getEntitesById($id) {
        return $this->getEntityById($id);
    }

    public function getAll($pbInfosSuppl = false, $pbActifs = false) {
        if ($pbActifs) {
            $lsWhere = ' Where (select Max(getDateFinAffiliation(dateFinAffiliation, dateAffiliation, nbJoursValiditeAffiliation)) From Affilier A Inner Join Federation F On A.idFederation = F.idFederation Where IdAdherent = adherent.idAdherent) >= current_date';
        } else {
            $lsWhere = "";
        }
        if ($pbInfosSuppl) {
            // Il faut les colonnes supplémentaires
            $query = 'SELECT adherent.* '
                    . " , to_char((select Max(getDateFinAffiliation(dateFinAffiliation, dateAffiliation, nbJoursValiditeAffiliation)) From Affilier A Inner Join Federation F On A.idFederation = F.idFederation Where IdAdherent = adherent.idAdherent), 'DD-MM-YYYY') As dateFinDerniereAffiliation"
                    . ' FROM adherent '
                    . $lsWhere
                    . ' ORDER BY NomAdherent, PrenomAdherent';
// echo "<br>" . chr(13) . $query . chr(13) . "<br>" ;
        } else {
            $query = 'SELECT adherent.* '
                    . ' FROM adherent '
                    . $lsWhere
                    . ' ORDER BY NomAdherent, PrenomAdherent';
        }
        $reqPrep = $this->db->prepare($query);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }

    // Renvoyer une entité adhérent avec le login (mail) de l'adhérent
    // LG 20200128
    public function getEntityByLogin($psLogin) {
        $reqPrep = $this->db->prepare('SELECT * FROM adherent'
                . " WHERE lower(mailAdherent) = lower(:mail)");
        $reqPrep->bindValue(':mail', $psLogin);
        return $this->getEntityByReqPrep($reqPrep);
    }

    public function getNomOrganisateurBySortie($id) {
        $resultSet = NULL;

        $query = 'SELECT nomadherent FROM adherent,sortie WHERE idsortie = :idFieldName AND sortie.idorganisateur = adherent.idadherent';
        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindValue(':idFieldName', $id);
        $reqPrep->execute();

        while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new Adherent($row);
        }

        return $resultSet;
    }

    public function getAllByRole($idrole) {
        $query = 'SELECT adherent.* '
                . ' FROM adherent '
                . ' INNER JOIN etre ON adherent.idadherent = etre.idadherent'
                . ' WHERE idrole = :idRole'
                . ' ORDER BY NomAdherent, PrenomAdherent';
        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindValue(':idRole', $idrole);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }

    public function sauver(\Phaln\AbstractEntity $entity) {
        // Sauvegarde de l'entité elle-mêem
        $entity = parent::sauver($entity);

        // Sauvegarde des données liées
//return $entity ;
        if ($entity != Null) {
            // L'enregistrement de l'entité elle-même a réussi : il faut maintenant enregistrer les listes
            if ($entity->getRoles(true) != Null) {
                // On a chargé le propriété "rôles" : il faut enregistrer chaque rôle
                $repo = new RoleRepository();
                $repo->sauverRolesAdherent($entity);
            }
            if ($entity->getTags(true) != Null) {
// var_dump($entity->getTags(true)) ;
                // On a chargé le propriété "tags" : il faut enregistrer chaque tag
                $repo = new TagsRepository();
                $repo->sauverTagsAdherent($entity);
            }
            if ($entity->getNumeroLicense() && $entity->getNomFederationLicense()) {
                // Enregistrer les informations de license
                $lsNomFederation = $entity->getNomFederationLicense();
                $repoFede = new FederationRepository();
                $loFederation = $repoFede->getEntityByName($lsNomFederation);
                if ($loFederation) {
                    $liFederation = $loFederation->getIdFederation();
                    $repoTypeLicense = new TypeLicenseRepository();
                    $loTypeLicense = $repoTypeLicense->getEntityByName($entity->getNomTypeLicense(), true);
                    $liTypeLicense = $loTypeLicense->getIdLicense();
                    $data = ["idAdherent" => $entity->getIdAdherent()
                        , "idFederation" => $liFederation
                        , "numeroLicense" => $entity->getNumeroLicense()
                        , "idTypeLicense" => $liTypeLicense
                        , "dateAffiliation" => $entity->getDateLicense()
                    ];
                    $repoAffiliation = new AffilierRepository();
// LG 20200911 début : il faut voir si cette affiliation n'existe pas déja
//					$loAffiliation = new \App\Entity\Affilier($data);
                    $loAffiliation = $repoAffiliation->getAffiliationParCléCandidate($entity->getIdAdherent(), $liFederation, $entity->getDateLicense());
                    if (!$loAffiliation) {
                        // Cette affiliation n'existe pas encore : il faut en créer une nouvelle
                        $loAffiliation = new \App\Entity\Affilier($data);
                    }
// LG 20200911 fin					
                    $repoAffiliation->sauver($loAffiliation);
                }
            }
        }
// var_dump($entity);
        return $entity;
    }

    public function auth(Adherent $adh) {
// LG 20210122 old		$resq = "SELECT * FROM Adherent WHERE mailAdherent = '" . $adh->getMailAdherent() . "'";
        $resq = "SELECT * FROM Adherent WHERE lower(mailAdherent) = lower('" . $adh->getMailAdherent() . "')";
        $sth = $this->db->prepare($resq);
        $res = $sth->execute(array());

        if ($res != false) {
// echo $adh->getMailAdherent() . " PASSE" ;
            $ret = $sth->fetch(\PDO::FETCH_ASSOC);

            $Etre = new RoleRepository();
            $Roles = $Etre->getRolesByUser($ret['idadherent']);
// LG 20210122 old			if ($adh->getMailAdherent() == $ret["mailadherent"] && $this->vérifieMDP($adh->getMdpAdherent(), $ret["mdpadherent"])) {
// echo $adh->getMailAdherent();
            if (strtolower($adh->getMailAdherent()) === strtolower($ret["mailadherent"]) && $this->vérifieMDP($adh->getMdpAdherent(), $ret["mdpadherent"])) {
                $_SESSION['auth_asvel'] = true;
                $_SESSION['mailadherent'] = $adh->getMailAdherent();
                $_SESSION['idadherent'] = $ret['idadherent'];
                $_SESSION['roles'] = $Roles;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function vérifieMDP($mdpSaisi, $mdpBdd) {
// return true ;
        if (MDPCRYPTES) {
            // Le mdp a été généré par password_hash($MDP, PASSWORD_DEFAULT)
            return password_verify($mdpSaisi, $mdpBdd);
        } else {
            return $mdpSaisi == $mdpBdd;
        }
    }

    public function crypteMDP($mdp) {
        if (MDPCRYPTES) {
            return password_hash($mdp, PASSWORD_DEFAULT);
        } else {
            return $mdp;
        }
    }

    /**
     * Retrouver un adhérent dans la base de données, en se basant sur ses données personnelles
     * @param Adherent &$entityCherchée : l'adhérent à retrouver, renvoyé par référence si trouvé (complété avec les données de la BDD)
     * 												, renvoyé tel quel sinon
     * @param Adherent[] &$tabAdherentsDouteux : tableau des entités qui peuvent être confondues avec l'entité cherchée, Null si aucune n'a été trouvée
     * @return true si aucun adhérent douteux n'a été trouvé
     */
// LG 20220729 old    public function retrouveAdherentImporté(Adherent &$entityCherchée, array &$tabAdherentsDouteux) {
    public function retrouveAdherentImporté(Adherent &$entityCherchée, array &$tabAdherentsDouteux, &$pbAdherentNouveau) {
        $entitéTrouvée = null;
        $tabErr = "";

        // Tester la validité de l'entité fournie, sans tenir compte pour l'instant des contraintes d'unicité
        if (!$this->vérifier($entityCherchée, $tabErr, true)) {
            // L'entité est incorrecte
            $tabAdherentsDouteux[] = Array("adherent" => null, "commentaire" => $tabErr);
            return false;
        }

        if ($entityCherchée->getMailAdherent()) {
            // Chercher par les nom, prénom (normalisés) et mail
            $query = "Select * From Adherent"
                    . " Where normalise(nomAdherent) = :nomAdherent"
                    . " And normalise(prenomAdherent) = :prenomAdherent"
                    . " And mailAdherent = :mailAdherent ;";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":prenomAdherent" => Functions::normalise($entityCherchée->getPrenomAdherent()),
                ":mailAdherent" => $entityCherchée->getMailAdherent(),
            ]);
            if ($res && $reqPrep->rowCount() > 0) {
                $entitéTrouvée = new Adherent($reqPrep->fetch(PDO::FETCH_ASSOC));
            } else {
                
            }
        }

        if (!$entitéTrouvée && $entityCherchée->getDateNaissanceAdherent()) {
            // Chercher par les nom, prénom (normalisés) et date de naissance
            $date = $entityCherchée->getDateNaissanceAdherent();
            $query = "Select * From Adherent Where normalise(nomAdherent) = :nomAdherent"
                    . " And normalise(prenomAdherent) = :prenomAdherent"
                    . " And dateNaissanceAdherent = '$date'::Date";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":prenomAdherent" => Functions::normalise($entityCherchée->getPrenomAdherent()),
            ]);
            if ($res && $reqPrep->rowCount() > 0) {
                $entitéTrouvée = new Adherent($reqPrep->fetch(PDO::FETCH_ASSOC));
            }
        }

        if (!$entitéTrouvée && $entityCherchée->getTelAdherent()) {
            // Chercher par les nom, prénom (normalisés) et N° de téléphone
            $query = "Select * From Adherent Where normalise(nomAdherent) = :nomAdherent"
                    . " And normalise(prenomAdherent) = :prenomAdherent"
                    . " And telAdherent = :telAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":prenomAdherent" => Functions::normalise($entityCherchée->getPrenomAdherent()),
                ":telAdherent" => $entityCherchée->getTelAdherent(),
            ]);
            if ($res && $reqPrep->rowCount() > 0) {
                $entitéTrouvée = new Adherent($reqPrep->fetch(PDO::FETCH_ASSOC));
            }
        }

        if (!$entitéTrouvée) {
            // Correspondance exacte non trouvée
            // Chercher si une correspondance approximative est trouvée
            // Chercher par nom-prénom
            $query = "Select * From Adherent Where normalise(nomAdherent) = :nomAdherent"
                    . " And normalise(prenomAdherent) = :prenomAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":prenomAdherent" => Functions::normalise($entityCherchée->getPrenomAdherent()),
            ]);
            if ($res) {
                while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                    $tabAdherentsDouteux[] = Array("adherent" => new Adherent($row), "commentaire" => "Même nom et même prénom mais pas le même email ou pas la même date de naissance");
                }
            }

            // Chercher par nom-dateNaissance
            $query = "Select * From Adherent Where normalise(nomAdherent) = :nomAdherent"
                    . " And dateNaissanceAdherent = :dateNaissanceAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":dateNaissanceAdherent" => $entityCherchée->getDateNaissanceAdherent(),
            ]);
            if ($res) {
                while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                    $tabAdherentsDouteux[] = Array("adherent" => new Adherent($row), "commentaire" => "Mêmes noms et date de naissance : " . $entityCherchée->getDateNaissanceAdherent());
                }
            }

            // Chercher par nom-N° de tel
            $query = "Select * From Adherent Where normalise(nomAdherent) = :nomAdherent"
                    . " And telAdherent = :telAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":nomAdherent" => Functions::normalise($entityCherchée->getNomAdherent()),
                ":telAdherent" => $entityCherchée->getTelAdherent(),
            ]);
            if ($res) {
                while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                    $tabAdherentsDouteux[] = Array("adherent" => new Adherent($row), "commentaire" => "Mêmes noms et N° de téléphone : " . $entityCherchée->getTelAdherent());
                }
            }

            // Chercher par email
            if ($entityCherchée->getMailAdherent()) {
                $query = "Select * From Adherent Where mailAdherent = :mailAdherent";
                $reqPrep = $this->db->prepare($query);
                $res = $reqPrep->execute([":mailAdherent" => $entityCherchée->getMailAdherent()]);
                if ($res) {
                    while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                        $tabAdherentsDouteux[] = Array("adherent" => new Adherent($row), "commentaire" => "Même adresse mail : " . $entityCherchée->getMailAdherent());
                    }
                }
            }

            // Chercher par noTel
            $query = "Select * From Adherent Where telAdherent = :telAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":telAdherent" => $entityCherchée->getTelAdherent()]);
            if ($res) {
                while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                    $tabAdherentsDouteux[] = Array("adherent" => new Adherent($row), "commentaire" => "Même N° de téléphone : " . $entityCherchée->getTelAdherent());
                }
            }
        }

        if ($entitéTrouvée) {
            // On a trouvé une correspondance : c'est un adhérent préexistant à mettre à jour
            $entityCherchée->setIdAdherent($entitéTrouvée->getIdAdherent());
// LG 20200911 début : il ne faut pas écraser le mot de passe
            $entityCherchée->setMdpAdherent($entitéTrouvée->getMdpAdherent());
// LG 20200911 fin
            $pbAdherentNouveau = false ;    // LG 20220729
        }
// LG 20220729 début        
        else {
            $pbAdherentNouveau = true ;
        }
// LG 20220729 fin        

        // Adhérent retrouvé ou non, il faut vérifier l'unicité de son eMail
        if ($entityCherchée->getMailAdherent()) {
            $query = "Select * From Adherent Where mailAdherent = :mailAdherent";
            $reqPrep = $this->db->prepare($query);
            $res = $reqPrep->execute([":mailAdherent" => $entityCherchée->getMailAdherent()]);
            if ($res) {
                while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                    if ($row["idadherent"] != $entityCherchée->getIdAdherent()) {
                        // On a trouvé un adherent avec ce même mail
                        // Mettre le mail à vide et signaler
                        $entityTrouvée = $this->getEntityById($row["idadherent"]) ;
// LG 20220910 début
//                         $lsCommentaire = "Cet adhérent ne pourra pas s'authentifier sur le site car son adresse mail a été mise à vide : 
//                                             l'adhérent " . $entityTrouvée->getNomAdherent() . " " . $entityTrouvée->getPrenomAdherent() . " 
//                                             possède déja cette adresse (" . $entityCherchée->getMailAdherent() . ")" ;
                        $lsCommentaire = "Cet adhérent ne pourra pas s'authentifier sur le site car son adresse mail a été mise à vide : 
                                            l'adhérent " 
                                            . "<a target='_blank' rel='noopener noreferrer' href='info-adherent.php?id=" . $entityTrouvée->getIdAdherent() . "'>"
                                            . $entityTrouvée->getNomAdherent() . " " . $entityTrouvée->getPrenomAdherent() 
                                            . "</a>"
                                            . " possède déja cette adresse (" . $entityCherchée->getMailAdherent() . ")" ;
// LG 20220910 fin
                        $tabAdherentsDouteux[] = Array("adherent" => null, "commentaire" => [$lsCommentaire]);
                        $entityCherchée->setMailAdherent(NULL);
                    }
                }
            }
        }

        // On a retrouvé la personne, sans ambigüité
        // Enregistrer l'entité maintenant
        // Vérifier à nouveau l'entité, cettte fois en tenant compte des contraintes d'unicité
        if (!$this->vérifier($entityCherchée, $tabErr, false)) {
            // L'entité est incorrecte : elle fait doublon avec au moins une autre
            $tabAdherentsDouteux[] = Array("adherent" => null, "commentaire" => $tabErr);
            return false;
        } else {
            // L'entité est correcte et ne fait pas doublon
            $entitéTrouvée = $this->sauver($entityCherchée);

            // Valeurs de retour
            $entityCherchée = $entitéTrouvée ? $entitéTrouvée : $entityCherchée;
            return true;
        }
    }

    public function getDestinatairesDuMail($piIdMail) {
        $query = 'SELECT adherent.* '
                . ' FROM adherent '
                . ' INNER JOIN recevoir ON adherent.idadherent = recevoir.idadherent'
                . ' WHERE idMail = :idMail'
                . ' ORDER BY NomAdherent, PrenomAdherent';
        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindValue(':idMail', $piIdMail);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }
// LG 20200210 fin

    // Procéder à la fusion de deux intervenants
    // LG 20220910
    public function fusionnerAdhérents($poAdherentAConserver, $poAdherentAVirer, &$psMsgErr) {
        $lsSQL = "select dedoublonne(:AdherentAConserver, :AdherentAVirer);" ;
        $reqPrep = $this->db->prepare($lsSQL);

        $psMsgErr = "" ;
        $lbKO = false ;
        try {
            $res = $reqPrep->execute([":AdherentAConserver" => $poAdherentAConserver->getIdAdherent(), ":AdherentAVirer" => $poAdherentAVirer->getIdAdherent()]);
        } catch (\Exception $e) {
            $psMsgErr = $e->getMessage() ;
            $lbKO = true ;
        }
        return !$lbKO ;
    }
}
