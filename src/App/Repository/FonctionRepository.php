<?php

namespace App\Repository;

class FonctionRepository extends \Phaln\AbstractRepository {
    
        protected $mandatoryProps = ["nomfonction"];
        protected $uniqueProps = [["nomfonction"]];
	public function __construct() {
		$this->table = 'fonction';
		$this->classMapped = 'App\Entity\Fonction';
		$this->idFieldName = 'idfonction';

		parent::__construct();
	}

}
