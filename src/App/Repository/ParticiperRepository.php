<?php

namespace App\Repository;

use App\Entity\Participer;
use App\Entity\Sortie;
use App\Repository\FonctionRepository;

class ParticiperRepository extends \Phaln\AbstractRepository {

    protected $table = 'Participer';      // le nom de la table manipulée
    protected $idFieldName = ["idAdherent", "idSortie"]; // le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\Participer';  // le nom de la classe mappée
    protected $notFieldProps = ["idAdherent_Initial", "idSortie_Initial"];       // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    
        public function getParticipationsNonAnnuléeBySortie($idSortie) {
        $SQL = 'SELECT Participer.* FROM Participer'
                . ' WHERE idSortie = :idSortie';
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idSortie', $idSortie);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }
    
    public function getValidationenAttentebySortie($idsortie) {
        $resultSet = [];
        $sQuery = "SELECT COUNT(*) FROM Participer
                 WHERE idSortie = :idsortie
                 AND validationparticipation is NULL  OR validationparticipation = FALSE AND idSortie = :idsortie               
				 GROUP BY dateinscription,idfonction
				 ORDER BY dateinscription,idfonction";
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));

        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet[] = new Participer($row);
        }
        return $resultSet;
    }

    // Renvoyer le nb de participants inscrits à la sortie
    // et par référence le nb de participants validés
    public Function getNbParticipantsBySortie($idsortie, &$ValidésNonAnnulés) {
        $inscrits = 0 ;
        $sQuery = "SELECT COUNT(*) nb FROM Participer
                 WHERE idSortie = :idsortie 
					And idRaisonAnnulation Is Null";
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));
        if ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $inscrits = $row["nb"] ;
        }
        
        $ValidésNonAnnulés = 0 ;
// LG 20201011 début
//        $sQuery = "SELECT COUNT(*) nb FROM Participer
//                 WHERE idSortie = :idsortie
//                    And Coalesce(validationParticipation, false)";
        $sQuery = "SELECT COUNT(*) nb FROM Participer
                 WHERE idSortie = :idsortie
                    And Coalesce(validationParticipation, false)
					And Coalesce(idRaisonAnnulation, 0) = 0";
// LG 20201011 fin
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));
        if ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $ValidésNonAnnulés = $row["nb"] ;
        }
        
        return $inscrits ;
    }
    
    // Renvoyer le nb de participants inscrits à la sortie
    // et par référence le nb de participants validés
    public Function getNbPlacesVoiture($idsortie) {
        $nbPlaces = 0 ;
        $sQuery = "SELECT SUM(Coalesce(nbPlaceVoiture, 0)) nb FROM Participer
                 WHERE idSortie = :idsortie
					AND Coalesce(validationParticipation, false)
					AND idRaisonAnnulation Is Null";
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));
        if ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $nbPlaces = $row["nb"] ;
        }
        
        return $nbPlaces ;
    }
//        public function getValidationenAttentebySortie($idSortie) {
//        $SQL = 'SELECT validationparticipation FROM participer '
//                . 'WHERE validationparticipation = FALSE AND participer.idsortie = :idSortie AND idraisonannulation is NULL';
//        $reqPrep = $this->db->prepare($SQL);
//        $reqPrep->bindValue(':idSortie', $idSortie);
//        $resultSet = $this->getAllByReqPrep($reqPrep);
//        return $resultSet;
//    }
    

    public function getParticipationByAdherentAndSortie($idAdherent, $idSortie) {
        $SQL = 'SELECT * FROM participer '
                . 'WHERE participer.idadherent = :idAdherent AND participer.idsortie = :idSortie';
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idAdherent', $idAdherent);
        $reqPrep->bindValue(':idSortie', $idSortie);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    public function getParticipationsBySortie($idSortie) {
        $SQL = 'SELECT Participer.* FROM Participer'
                . ' WHERE idSortie = :idSortie';
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idSortie', $idSortie);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    public function getAllCoEncadrantsByIdSortie($idSortie) {
// LG 20201011 old        $SQL = 'SELECT Participer.* FROM Participer WHERE idSortie = :idSortie AND participer.idfonction = 3';
        $SQL = 'SELECT Participer.* FROM Participer '
				. 'WHERE idSortie = :idSortie '
					. 'AND participer.idfonction = 3'
					. ' AND Coalesce(idRaisonAnnulation, 0) = 0 ;';
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idSortie', $idSortie);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    public function getAllAdherentByIdSortie($idsortie) {
        $resultSet = [];
        $sQuery = "SELECT *"
                . " FROM participer "
                . " WHERE participer.idsortie = :idsortie "
                . " ORDER BY case when idfonction=3 then 2 when idFonction=2 then 1 else 3 end, dateinscription ;";
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));

        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet[] = new Participer($row);
        }

        return $resultSet;
    }

    // compte le nombre de participant, en fonction de leurs date d'inscription.
    public function CompteParticipant($idsortie) {
        $resultSet = [];
        $sQuery = "SELECT COUNT(*) FROM Participer
                 WHERE idSortie = :idsortie
				 GROUP BY dateinscription
				 ORDER BY dateinscription";
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idsortie' => $idsortie
        ));

        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet[] = new Participer($row);
        }

        return $resultSet;
    }

    public function deleteEntity($idAdherent, $idSortie) {
        $sth = $this->db->prepare('DELETE FROM participer'
                . ' WHERE idadherent = :idadherent AND idsortie = :idsortie');

        $resultat = $sth->execute(array(':idadherent' => $idAdherent, ':idsortie' => $idSortie));
// var_dump($resultat);
        return $resultat;
    }

    public function setInscritAdherentToSortie($idAdherent, $idSortie) {
        $sth = $this->db->prepare('INSERT INTO participer(idadherent, idsortie) VALUES(:idadherent, :idsortie)');

        $resultat = $sth->execute(array(':idadherent' => $idAdherent, ':idsortie' => $idSortie));

        return $resultat;
    }

    // Renvoyer un objet vers la dernière sortie faite par l'adhérent demandé
    // LG 20210117
    public function getDernièreSortieAdhérent($piIidAdherent, $piNbJoursEnArrière) {
        $ldDateLimite = date("Y-m-d", time() - $piNbJoursEnArrière * 24 * 60 * 60) ;
        $sQuery = "SELECT S.* 
                FROM Participer P, Sortie S
                WHERE S.idSortie = P.idSortie
                    AND coalesce(dateFinSortie, dateDebutSortie) < current_date
                    AND coalesce(dateFinSortie, dateDebutSortie) > :dateLimite
                    AND idadherent = :idAdherent
                    AND S.idRaisonAnnulation Is Null                    
                    AND P.idRaisonAnnulation Is Null
                    AND P.validationParticipation
                ORDER BY coalesce(dateFinSortie, dateDebutSortie) DESC
                LIMIT 1
		;" ;
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idAdherent' => $piIidAdherent, 
            'dateLimite' => $ldDateLimite
        ));

        $loSortie = NULL ;
        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $loSortie = new Sortie($row);
        }

        return $loSortie;
    }
    // Renvoyer le nombre de sorties faites par l'adhérent demandé
    // LG 20210117
    public function getNbDernièresSortiesAdhérent($piIidAdherent, $piNbJoursEnArrière) {
        $ldDateLimite = date("Y-m-d", time() - $piNbJoursEnArrière * 24 * 60 * 60) ;
        $sQuery = "SELECT Count(S.*) iNb
                FROM Participer P, Sortie S
                WHERE S.idSortie = P.idSortie
                    AND coalesce(dateFinSortie, dateDebutSortie) < current_date
                    AND coalesce(dateFinSortie, dateDebutSortie) > :dateLimite
                    AND idadherent = :idAdherent
                    AND S.idRaisonAnnulation Is Null                    
                    AND P.idRaisonAnnulation Is Null
                    AND P.validationParticipation
		;" ;
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idAdherent' => $piIidAdherent, 
            'dateLimite' => $ldDateLimite
        ));
        $nb = 0 ;
        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $nb = $row["inb"];
        }

        return intval($nb);
    }
    
    // Renvoyer un texte récapitulatif des sorties refusées à un adhérent
    // LG 20210117
    public function getInfosSortiesRefusées($piIidAdherent, $piNbJoursEnArrière) {
        $ldDateLimite = date("Y-m-d", time() - $piNbJoursEnArrière * 24 * 60 * 60) ;
//if ($piIidAdherent == 131) {
//    echo $ldDateLimite ;
//    echo $toto ;
//}
        $sQuery = "SELECT Count(*) iNbRefus, current_date - Min(coalesce(dateFinSortie, dateDebutSortie)) iNbJoursDepuisPremierRefus 
                FROM Participer P, Sortie S
                WHERE S.idSortie = P.idSortie
                    AND coalesce(dateFinSortie, dateDebutSortie) < current_date
                    AND coalesce(dateFinSortie, dateDebutSortie) > :dateLimite
                    AND idadherent = :idAdherent
                    AND S.idRaisonAnnulation Is Null                    
                    AND P.idRaisonAnnulation Is Null
                    AND NOT P.validationParticipation
		;" ;
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idAdherent' => $piIidAdherent, 
            'dateLimite' => $ldDateLimite
        ));

         while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            if ($row["inbrefus"]) {
                $lsInfos = $row["inbrefus"] . " refus" ;
                if ($row["inbrefus"] == "1") {
                    $lsInfos .= ", il y a " . $row["inbjoursdepuispremierrefus"] . " jours" ;
                } else {
                    $lsInfos .= ", le 1er il y a " . $row["inbjoursdepuispremierrefus"] . " jours" ;                    
                }
            } else {
                 $lsInfos = "aucune" ;
            }
        }
        return $lsInfos ;
    }
    
    // Renvoyer  un objet vers la prochaine sortie demandée par l'adhérent demandé
    // LG 20210117
    function getProchaineDemandeDeParticipation($piIidAdherent) {
        $sQuery = "SELECT S.*, P.validationParticipation, P.idFonction, P.debutant, P.loueMateriel
                FROM Participer P, Sortie S
                WHERE S.idSortie = P.idSortie
                    AND dateDebutSortie >= current_date
                    AND idadherent = :idAdherent
                    AND S.idRaisonAnnulation Is Null
                ORDER BY dateDebutSortie ASC
                LIMIT 1
		;" ;
        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idAdherent' => $piIidAdherent, 
        ));

        $loSortie = NULL ;
        $loRepoFonction = new FonctionRepository();
       while ($row = $req->fetch(\PDO::FETCH_ASSOC)) {
            $loSortie = new Sortie($row);
            $loFonction = $loRepoFonction->getEntityById($row["idfonction"]);
            if ($loFonction->estEncadrant()) {
                $loSortie->nomFonction = $loFonction->getNomFonction() ;
            } else {
                $loSortie->nomFonction = "" ;
            }
            $loSortie->validationParticipation = $row["validationparticipation"] ;
            $loSortie->debutant = $row["debutant"] ;
            $loSortie->loueMateriel = $row["louemateriel"] ;           
        }

        return $loSortie;
    }

}
