<?php

namespace App\Repository;

use App\Entity\Photo;
use Phaln\BDD;
use PDO;

class PhotoRepository extends \Phaln\AbstractRepository 
{

    protected $table = 'photo';
    protected $classMapped = \App\Entity\Photo::class;
    protected $idFieldName = 'idPhoto';


    public function getBySorties($id)
    {
        $req = $this->db->query('SELECT * FROM photo where idsortie = '.$id.'');

        $getAll = null;
        while ($row = $req->fetch(PDO::FETCH_ASSOC))
        {
           $getAll[$row['idphoto']] = new Photo (['idphoto' => $row['idphoto'], 'cheminphoto' => $row['cheminphoto'], 'dateheurephoto' => $row['dateheurephoto'], 'idsortie' => $row['idsortie'],'idadherent' => $row['idadherent']]);
        }
        
        return $getAll;
    }

}
