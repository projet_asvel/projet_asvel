<?php
namespace App\Repository;

use App\Entity\Activite;
// LG 20200208 déac use Phaln\BDD;

class ActiviteRepository extends \Phaln\AbstractRepository
{
// LG 20200208 début
/*
    public function __construct() {

        $this->table = 'activite';
        $this->db = BDD::getConnexion();
        if(!$this->db)
            throw new RepositoryException('Pb db dans PersonneRepository::_construct()');

    }
*/
	protected $table = 'activite';					// le nom de la table manipulée
	protected $classMapped = 'App\Entity\Activite';	// le nom de la classe mappée
	protected $idFieldName = 'idactivite';			// le nom du champ clé primaire. id par défaut.
	protected $notFieldProps = [];					// tableau des propriétés qui ne sont pas des champs de la table sous-jacente
	protected $uniqueProps = [["nomActivite"]];	// Tableau des combinaisons de champs qui doivent être uniques (ex. [["nomActivite"]], [["nomAdherent", "prenomAdherent", "dateNaissanceAdherent", "mailAdherent"], ["mailAdherent"]])
// LG 20200208 fin
	
	public function createEntite($activite){


        $bindParam = array(
            'nomActivite' => $activite->getNomActivite(),
        );

        // Nouvelle entité
        $query = 'INSERT INTO activite'
            . ' (nomActivite)'
            . ' VALUES (:nomActivite)';

        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindParam(':nomActivite', $bindParam['nomActivite'], \PDO::PARAM_STR);
        $reqPrep->execute();


    }

    public function AVIRER_UpdateEntite($activite){
        echo "L'activite a bien été modifié";
    }

// LG 20200208 old    public function deleteEntite($idActivite){
    public function AVIRER_deleteEntite($idActivite){

        $concernerRepositories = new ConcernerRepositories();
        $concernerRepositories->deleteAllEntite($idActivite);

        $sth = $this->db->prepare('DELETE FROM activite'
            . ' WHERE idactivite = :idactivite');

        $resultat = $sth->execute(array(':idactivite' => $idActivite));
    }

    public function getEntitesById($id) {
        $sth = $this->db->prepare('SELECT * FROM activite'
            . ' WHERE idactivite = :idactivite');

        $resultat = $sth->execute(array(':idactivite' => $id));


        if($resultat) {
            while($row = $sth->fetch(\PDO::FETCH_ASSOC))
            {
                return new Activite($row);
            }
        }else {
            return new Activite();
        }
    }

    public function getAll() {
        $query = 'SELECT * FROM Activite Order by NomActivite';
        $reqPrep = $this->db->prepare($query);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }

    public function getAllBySortie($piIdSortie){
        $SQL = 'SELECT A.* FROM Activite A, Concerner C'
                . ' WHERE C.idSortie = :idSortie AND C.idActivite = A.idActivite' ;
// dump_var($SQL, DUMP, "Requête de sélection") ;
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idSortie', $piIdSortie);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

// LG 20200416 inutilisé
//    public function getAllEntites(array $data = NULL) {
//        $resultSet = array();
//        $query = 'SELECT * FROM Activite Order by NomActivite';
//
//        $rqtResult = $this->db->query($query);
//        if($rqtResult)
//        {
//            while($row = $rqtResult->fetch(\PDO::FETCH_ASSOC))
//            {
//                //$resultSet[] = new Activite($row);
//                $act = new Activite($row);
//                //echo "ererr";
//                array_push($resultSet, new Activite($row));
//            }
//            //var_dump($resultSet);
//            return $resultSet;
//        }
//    }

    public function sauverActivitesSortie(\App\Entity\Sortie $entity) {
		$laActivités = $entity->getActivites(true) ;
		if ($laActivités == Null) {
			return true ;
		}

		$lbOK = true ;
		try {
			// Démarrer une transaction car notre action demande plusieurs requêtes. Si l'une échoue, tout doit être annulé
			$this->db->beginTransaction();
			
			// Sauver chacun des rôles de la liste et construire la liste des rôles présents
			$lsLstActivités = "";
			foreach ($laActivités as $activité){
				// Ajouter ce rôle à la liste
				if ($lsLstActivités) $lsLstActivités .= "," ;
				$lsLstActivités .= $activité->getIdActivite() ;

				// Enregistrer ce rôle
				$query = "Select Count(*) as iCnt From Concerner Where idSortie = :idSortie And idActivite = :idActivite ;" ;
dump_var($query, DUMP, "Requête de test pour savoir s'il faut insérer") ;
				$reqPrep = $this->db->prepare($query);
				$reqPrep->bindValue(':idSortie', $entity->getIdSortie());
				$reqPrep->bindValue(':idActivite', $activité->getIdActivite());
				if ($reqPrep->execute()) {
					$nouveau = ($reqPrep->fetch(\PDO::FETCH_NUM)[0] == 0) ;
					if ($nouveau) {
						// Il faut insérer ce nouveau rôle pour cet adhérent
						$query = "Insert Into Concerner (idSortie, idActivite) Values (:idSortie, :idActivite) ;" ;
dump_var($query, DUMP, "Requête d'insertion") ;
						$reqPrep = $this->db->prepare($query);
						$reqPrep->bindValue(':idSortie', $entity->getIdSortie());
						$reqPrep->bindValue(':idActivite', $activité->getIdActivite());
						$reqPrep->execute();
					}
				} else {
					dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
					throw new \Phaln\Exceptions\RepositoryException('Pb db dans ActivitéRepository::sauverActivitésAdherent(): ' . $reqPrep->errorInfo()[2]);
				}
			}

			// Effacer tous les rôles de l'adhérent qui ne sont pas dans la liste
			$query = "Delete From Concerner Where idSortie = :idSortie" ;
			if ($lsLstActivités) {
				// Cet adhérent a des rôles
				$query .= " And Not idActivite In (" . $lsLstActivités . ")" ;				
			}
			$query .= ";" ;
			$reqPrep = $this->db->prepare($query);
			$reqPrep->bindValue(':idSortie', $entity->getIdSortie());
dump_var($entity->getIdSortie(), DUMP, "Requête de nettoyage") ;
dump_var($query, DUMP, "Requête de nettoyage") ;
			if (!$reqPrep->execute()) {
				dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
				throw new \Phaln\Exceptions\RepositoryException('Pb db dans ActivitéRepository::sauverActivitésAdherent(): ' . $reqPrep->errorInfo()[2]);
			}

			// Terminer la transaction en validant
			$this->db->commit();
			
		} catch (\Exception $e) {
			// Terminer la transaction en annulant
			$this->db->rollBack();
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans ActivitéRepository::sauverActivitésAdherent(): ' . $e->getMessage());
		}
		
		return $lbOK ;
    }

}