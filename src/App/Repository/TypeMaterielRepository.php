<?php

namespace App\Repository;
use Phaln\AbstractRepository;
use App\Entity\TypeMateriel;
use PDO;


class TypeMaterielRepository extends AbstractRepository
{
    // public function __construct() 
    // {
    //     // parent::__construct();
        
    //     parent::__construct(); // Appel du constructeur de Phaln\AbstractRepository
    //     $this->table = 'typemateriel'; // Nom de la table à mapper avec l’entité
    //     $this->classMapped = 'Entities\TypeMateriel'; // Nom de l’entité à mapper avec la table
    //     $this->idFieldName = 'idTypeMateriel'; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)

    // }

    protected $table = 'typemateriel';
    protected $classMapped = \App\Entity\TypeMateriel::class;
    protected $idFieldName = 'idTypeMateriel';
}


