<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;
use \App\Lib\Functions;
/**
 * Description of AutreaffiliationRepository
 *
 * @author Haris
 */
class AutreaffiliationRepository extends \Phaln\AbstractRepository{
   	protected $table = 'autreaffiliation';					// le nom de la table manipulée
	protected $classMapped = 'App\Entity\Autreaffiliation';	// le nom de la classe mappée
	protected $idFieldName = 'idautreaffiliation';			// le nom du champ clé primaire. id par défaut.
	protected $notFieldProps = [];	
        protected $uniqueProps = [["NomAutreaffiliation"]];
}
