<?php

namespace App\Repository;

use App\Entity\Mail;

class MailRepository extends \Phaln\AbstractRepository {

    protected $table = 'Mail';  // le nom de la table manipulée
    protected $classMapped = 'App\Entity\Mail'; // le nom de la classe mappée
    protected $idFieldName = 'idMail';   // le nom du champ clé primaire. id par défaut.
    protected $notFieldProps = []; // tableau des propriétés qui ne sont pas des champs de la table sous-jacente

    public function __construct() {
        parent::__construct();
    }

    public function getAllMailByIdAdherent($idAdherent) {
        $dsn = "mysql:host=localhost;dbname=asvel";
        $db = new \PDO($dsn, "root", "");

        $req = "SELECT * FROM recevoir WHERE idAdherent = :idAdherent";

        $reqPrep = $db->prepare($req);

        $rqtResult = $reqPrep->execute(array(
            'idAdherent' => $idAdherent));

        if ($rqtResult) {
            while ($row = $rqtResult->fetch(\PDO::FETCH_ASSOC)) {
                $resultSet[] = new $this->classMapped($row);
            }
        }

        return $resultSet;
    }

    public function getAllMailByIdAdherentExpediteur($idAdherent) {

        $req = "SELECT * FROM mail WHERE idAdherentExpediteur = :idAdherentExpediteur";
        $reqPrep = $this->db->prepare($req);
        $rqtResult = $reqPrep->execute(array('idAdherentExpediteur' => $idAdherent));

        if ($rqtResult) {
            while ($row = $rqtResult->fetch(\PDO::FETCH_ASSOC)) {
                $resultSet[] = new $this->classMapped($row);
            }
        }

        return $resultSet;
    }
    
    /**
     * Enregistrer l'entité fournie : surcharge de la méthode de base
     * Code selon Sonny Chaprier
     * @param \Phaln\AbstractEntity $entity
     * @param \Array $paDestinataires : tableau des Ids des adhérents destinataires
     * @return \Phaln\AbstractEntity : l'entité enregistrée (avec l'Id renseignée en cas d'insertion et de BDD postgreSQL)
     *								   ou null si échec
     * LG 20210513
      */
    public function sauver(\Phaln\AbstractEntity $entity, $paDestinataires = null) {
        $loEntitéSauvée = parent::sauver($entity) ;
        
        if (!$loEntitéSauvée) {
            // La sauvegarde basique a échoué : rien de plus à faire
        } else if ($paDestinataires) {
            // On a fourni la liste des destinataires : l'enregistrer maintenant
            
            // Effacer les destinataires de ce mail
            $req = "DELETE FROM recevoir WHERE idMail = :idMail";
            $reqPrep = $this->db->prepare($req);
            $rqtResult = $reqPrep->execute(array('idMail' => $loEntitéSauvée->getIdMail()));
            
            // Insérer les destinataires du mail
            $req = "" ;
            foreach ($paDestinataires as $liIdAdherent) {
                if ($req) $req .= ", " ;
                $req .= "(" . $liIdAdherent . ",:idMail)" ;
            }
            $req = "INSERT INTO recevoir (idAdherent, idMail) VALUES " . $req . ";" ;
            $reqPrep = $this->db->prepare($req);
            $rqtResult = $reqPrep->execute(array('idMail' => $loEntitéSauvée->getIdMail()));

        }
        
        return $loEntitéSauvée ;
    }

}
