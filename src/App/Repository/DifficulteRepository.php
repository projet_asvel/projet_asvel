<?php
namespace App\Repository;

class DifficulteRepository extends \Phaln\AbstractRepository {
    protected $table = 'difficulte';
    protected $classMapped = 'App\Entity\Difficulte';
    protected $idFieldName = 'idDifficulte';
    protected $mandatoryProps = ["NomDifficulte"];
    protected $uniqueProps = [["NomDifficulte"]];
    
    public function getNomDifficulteBySortie ($id){         
        $resultSet = NULL;

        $query = 'SELECT nomdifficulte FROM difficulte,sortie WHERE idsortie = :idFieldName AND difficulte.iddifficulte = sortie.iddifficulte';
        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindValue(':idFieldName', $id);
        $reqPrep->execute();
        
        while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \App\Entity\Difficulte($row);
        }

        return $resultSet;
    }

}