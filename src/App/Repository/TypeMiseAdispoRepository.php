<?php

// namespace App\Repositories;
// use Phaln\AbstractRepository;

// class TypeMiseAdispoRepository extends AbstractRepository
// {
//     public function __construct() 
//     {
//         // parent::__construct();
        
//         parent::__construct(); // Appel du constructeur de Phaln\AbstractRepository
//         $this->table = 'typemiseadispo'; // Nom de la table à mapper avec l’entité
//         $this->classMapped = 'Entities\TypeMiseAdispo'; // Nom de l’entité à mapper avec la table
//         $this->idFieldName = 'idTypeMiseAdispo'; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)

//     }
// }

namespace App\Repository;
use Phaln\AbstractRepository;
use App\Entity\TypeMiseAdispo;
use PDO;

class TypeMiseAdispoRepository extends AbstractRepository
{
    protected $table = 'typemiseadispo';
    protected $classMapped = \App\Entity\TypeMiseAdispo::class;
    protected $idFieldName = 'idTypeMiseAdispo';

}


