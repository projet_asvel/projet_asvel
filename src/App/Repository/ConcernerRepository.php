<?php

namespace App\Repository;
use App\Entity\Activite;
use App\Entity\Concerner;

use App\Entity\Sortie;
use Phaln\BDD;


class ConcernerRepository
{



    protected $db = NULL;	    // la base de donnée (PDO ou mysqli)
    protected $table = '';	    // le nom de la table manipulée
    protected $idFieldName = 'id';  // le nom du champ clé primaire. id par défaut.

    public function __construct(array $data = NULL) {

        $this->table = 'asvel';
        $this->db = BDD::getConnexion();
        if(!$this->db)
            throw new RepositoryException('Pb db dans PersonneRepository::_construct()');

    }

    public function getAllSortieByIdActivite($idActivite) {

        $resultSet = [];
        $sth = $this->db->prepare('');



        $sQuery = "SELECT sortie.idsortie, idactivite , nomsortie FROM concerner  LEFT JOIN sortie ON sortie.idsortie = concerner.idsortie WHERE idactivite = :idActivite";


        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            'idActivite' => $idActivite
        ));

        //$arraySortie = $req->fetchAll();

        while($row = $req->fetch(\PDO::FETCH_ASSOC))
        {
            $resultSet[] = new Sortie($row);
        }

        return $resultSet;





    }

    public function getAllActivitesByIdSortie($idSortie) {

        $resultSet = [];
        //$sth = $this->db->prepare('');



        $sQuery = "SELECT  concerner.idactivite , nomactivite FROM concerner LEFT JOIN activite ON activite.idactivite = concerner.idactivite WHERE concerner.idsortie = :idsortie";


        $req = $this->db->prepare($sQuery);
        $req->execute(array(
            ':idsortie' => $idSortie
        ));

        //$arraySortie = $req->fetchAll();

        while($row = $req->fetch(\PDO::FETCH_ASSOC))
        {
            $resultSet[] = new Activite($row);
        }

        return $resultSet;





    }

    public function deleteAllEntite($idActivite){
        $sth = $this->db->prepare('DELETE FROM concerner'
            . ' WHERE idactivite = :idactivite');

        $resultat = $sth->execute(array(':idactivite' => $idActivite));
    }

    public function setActiviteToSortie($idActivite, $idSortie){
        $sth = $this->db->prepare('INSERT INTO concerner VALUES(:idSortie, :idActivite)');

        $resultat = $sth->execute(array(':idActivite' => $idActivite, ':idSortie' => $idSortie));
    }



    public function sauver(\Phaln\AbstractEntity $entity)
    {

    }

}