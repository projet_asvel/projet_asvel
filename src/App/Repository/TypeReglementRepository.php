<?php

namespace App\Repository;

use App\Entity\TypeReglement;
use Phaln\BDD;
use \PDO;

class TypeReglementRepository extends \Phaln\AbstractRepository {

//     protected $db = NULL;								// la base de donnée (PDO ou mysqli)
    protected $table = 'typereglement';						// le nom de la table manipulée
    protected $idFieldName = 'idTypeReglement';				// le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\TypeReglement';		// le nom de la classe mappée
}