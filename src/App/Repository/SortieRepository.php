<?php

namespace App\Repository;

use App\Entity\Sortie;
use Phaln\BDD;

class SortieRepository extends \Phaln\AbstractRepository {

    protected $table = 'Sortie';  // le nom de la table manipulée
    protected $idFieldName = 'idSortie'; // le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\Sortie';  // le nom de la classe mappée
    protected $notFieldProps = ["participations", "activites", "nomsActivites"];  // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    protected $mandatoryProps = ["nomsortie", "idorganisateur"];
// LG 20201206 déac : l'unicité sur date, heure, lieu, empêche la création lors de l'alaboration du programme, quand on ne connait pas le lieu ni l'heure     protected $uniqueProps = [["nomsortie"], ["dateDebutSortie", "heuredepart", "lieuSortie"]];
    protected $uniqueProps = [["nomsortie"]];

    /**
     * Renvoie la liste des entités correspondant aux sorties, passées ou à venir
     * @param $sortiePassees boolean        : $sortiePassees true pour les sorties passées
     * @param $typeActivite                 : le type d'activité à filtrer
     */
// LG 20211106 début
//    public function getAllFiltré($sortiePassees, $typeActivite) {
//        if ($sortiePassees) {
//            $query = 'SELECT sortie.* FROM sortie, concerner WHERE sortie.idsortie=concerner.idsortie AND concerner.idactivite = ' . $typeActivite . ' AND datedebutsortie < NOW();';
//        } else {
//            $query = 'SELECT sortie.* FROM sortie, concerner WHERE sortie.idsortie=concerner.idsortie AND concerner.idactivite = ' . $typeActivite . ' AND datedebutsortie >= NOW();';
//        }
    public function getAllFiltré($piFiltreDate, $typeActivite = null, $piAdherent = null) {
        $lsWhere = "" ;
        if ($piFiltreDate == FILTRESORTIE_TOUTESDATES) {
        } else if ($piFiltreDate == FILTRESORTIE_DATESPASSEES) {
            $lsWhere .= " AND datedebutsortie < NOW()" ;
        } else if ($piFiltreDate == FILTRESORTIE_DATESAVENIR) {
            $lsWhere .= " AND datedebutsortie > NOW()" ;
        }
        if ($piAdherent) {
            $lsWhere .= " AND sortie.idSortie In (Select idSortie From Participer Where idAdherent = " . $piAdherent .")" ;
        }
        $query = 'SELECT sortie.* FROM sortie, concerner'
                . ' WHERE sortie.idsortie=concerner.idsortie'
                    . ' AND concerner.idactivite = ' . ($typeActivite?$typeActivite:"concerner.idactivite")
                    . $lsWhere 
                . ' ;';
// LG 20211106 fin
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }
    
    /**
     * Renvoie la liste des entités correspondant aux sorties, passées ou à venir
     * @param int $piNb       : le nombre de sorties à venir (dft = 5)
     */
    public function getProchainesSorties($piNb = 4) {
//        $query = 'SELECT * FROM Sortie Where dateDebutSortie > current_date ORDER BY datedebutsortie LIMIT ' . $piNb ;
        $query = 'SELECT * FROM Sortie Where dateDebutSortie > current_date And Coalesce(idRaisonAnnulation, 0) = 0 ORDER BY datedebutsortie LIMIT ' . $piNb ;
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }
    
    /**
     * Renvoie la liste des sorties pour l'édhérent fourni
     * @param int $piIdAdherent     : l'adhérent pour lequel on veut avoir les sorties
     */
    public function getAllByAdherent($piIdAdherent) {
        $query = 'SELECT S.* '
                . ' FROM Sortie S'
                . ' JOIN Participer P On P.idSortie = S.idSortie '
                . ' WHERE P.idAdherent = ' . $piIdAdherent
                . ' ORDER BY datedebutsortie' ;
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    /**
     * Ajoute à un tableau de données correspondant à une entité les éléments provenant d"entités liées
     * @param type $row tableau associatif des propriétés de l'entité
     * @param $aSubEntities : tableau des sous-entités à charger
     * @return tableau associatif des propriétés de l'entité, plus les données des entités liées demandées
     */
    public function addSubEntities($row, $aSubEntities = null) {
        if (in_array("participations", $aSubEntities)) {
            // On a demandé la liste des participations à cette séance
            $Repo = new ParticiperRepository();
            $participations = $Repo->getParticipationsBySortie($row['idsortie']);
            $Table = array("participations" => $participations);
            $row = array_merge($row, $Table);
        }
        return $row;
    }

    public function sauver(\Phaln\AbstractEntity $entity) {
        $entity = parent::sauver($entity);
        if ($entity != Null) {
            // L'enregistrement de l'entité elle-même a réussi : il faut maintenant enregistrer les listes
            if ($entity->getActivites(true) != Null) {
                // On a chargé le propriété "rôles" : il faut enregistrer chaque rôle
                $repo = new ActiviteRepository();
                $repo->sauverActivitesSortie($entity);
            }
        }
        return $entity;
    }

// LG 20200208 fin
}
