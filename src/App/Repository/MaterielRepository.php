<?php

namespace App\Repository;
use Phaln\AbstractRepository;
use App\Entity\Materiel;
use PDO;

class MaterielRepository extends AbstractRepository
{
    // public function __construct() 
    // {
    //     // parent::__construct();
        
    //     parent::__construct(); // Appel du constructeur de Phaln\AbstractRepository
    //     $this->table = 'materiel'; // Nom de la table à mapper avec l’entité
    //     $this->classMapped = 'Entities\Materiel'; // Nom de l’entité à mapper avec la table
    //     $this->idFieldName = 'idMateriel'; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)

    // }
    protected $table = 'materiel';
    protected $classMapped = \App\Entity\Materiel::class;
    protected $idFieldName = 'idMateriel';

    

    public function getAllByAdherent($idAdherent)
    {
        $req = $this->db->query('SELECT * FROM materiel where idadherent = '.$idAdherent.'');

        $getAll = null;
        while ($row = $req->fetch(PDO::FETCH_ASSOC))
        {
           $getAll[$row['idmateriel']] = new Materiel (['idMateriel' => $row['idmateriel'], 'Taille' => $row['taille'], 'Pointure' => $row['pointure'], 'Modele' => $row['modele'],'Longitude1' => $row['longitude1'], 'Longitude2' => $row['longitude2'],'Latitude1' => $row['latitude1'], 'Latitude2' => $row['latitude2'],'dateAchat' => $row['dateachat'], 'Commentaire' => $row['commentaire'],'Pays' => $row['pays'], 'idTypeMateriel' => $row['idtypemateriel'],'idTypeMiseAdispo' => $row['idtypemiseadispo'], 'idAdherent' => $row['idadherent']]);
        }
        
        return $getAll;
    }

    public function getAllMaterielDispo()
    {
        $req = $this->db->query('SELECT * FROM materiel where idtypemiseadispo != \'2\'');

        $getAll = null;
        while ($row = $req->fetch(PDO::FETCH_ASSOC))
        {
           $getAll[$row['idmateriel']] = new Materiel (['idMateriel' => $row['idmateriel'], 'Taille' => $row['taille'], 'Pointure' => $row['pointure'], 'Modele' => $row['modele'],'Longitude1' => $row['longitude1'], 'Longitude2' => $row['longitude2'],'Latitude1' => $row['latitude1'], 'Latitude2' => $row['latitude2'],'dateAchat' => $row['dateachat'], 'Commentaire' => $row['commentaire'],'Pays' => $row['pays'], 'idTypeMateriel' => $row['idtypemateriel'],'idTypeMiseAdispo' => $row['idtypemiseadispo'], 'idAdherent' => $row['idadherent']]);
        }
        
        return $getAll;
    }

    public function getTypeMat($idType)
    {
        $req = $this->db->query('SELECT nomtype FROM typemateriel where idtypemateriel = '.$idType.'');

        $row = $req->fetch(PDO::FETCH_ASSOC);
        
        return $row['nomtype'];
    }

    public function getTypeDispo($idType)
    {
        $req = $this->db->query('SELECT nommiseadispo FROM typemiseadispo where idtypemiseadispo = '.$idType.'');

        $row = $req->fetch(PDO::FETCH_ASSOC);
        
        return $row['nommiseadispo'];
    }
}


