<?php

namespace App\Repository;

class AffilierRepository extends \Phaln\AbstractRepository {
    protected $table = 'Affilier';     // le nom de la table manipulée
    protected $idFieldName = "idAffiliation" ;  // le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\Affilier';  // le nom de la classe mappée
    protected $uniqueProps = [["idAdherent", "idFederation", "dateAffiliation"]];
    protected $notFieldProps = [];  // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    
    public function getAllByAdherent($idAdherent) {
        $SQL = 'With cte1 AS (Select Distinct idFederation, idAdherent '
                            . ' From affilier '
                            . ' WHERE idAdherent = :idAdherent)'
                . ', cte2 AS (Select A.idFederation, A.idAdherent, max(A.dateAffiliation) dateAffiliation '
                            . ' From affilier A, cte1 '
                            . ' Where A.idFederation = cte1.idFederation'
                            . ' And A.idAdherent = cte1.idAdherent'
                            . ' Group by A.idFederation, A.idAdherent)'
                . ' SELECT A.* FROM affilier A, cte2'
                . ' WHERE A.idFederation = cte2.idFederation And A.dateAffiliation = cte2.dateAffiliation And A.idAdherent = cte2.idAdherent '
                . ' ORDER BY A.dateAffiliation Desc' ;
        $reqPrep = $this->db->prepare($SQL);
        $reqPrep->bindValue(':idAdherent', $idAdherent);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

	// LG 20201011
	 // Récupère la dernière affiliation d'un adhérent
	 public function getDernièreAffiliationAdherent($idAdherent) {
		 $lsWhere = "idAffiliation = (Select idAffiliation From Affilier Where idAdherent = " . $idAdherent . " And dateAffiliation = (select Max(dateAffiliation) From Affilier Where IdAdherent = " . $idAdherent .") Limit 1)" ;
		 return $this->getEntityByWhere($lsWhere) ;
	 }

	 // LG 20200911
	 // Récupère l'entité affiliation correspondant à un adhérent, une fédération, une date d'affiliation (qui est clé unique/candicate sur la table Affilier)
	 public function getAffiliationParCléCandidate($idAdherent, $idFederation, $dateAffiliation) {
		 $lsWhere = "idAdherent = " . $idAdherent ." And idFederation =  " . $idFederation . " And dateAffiliation = '" . $dateAffiliation . "'" ;
// var_dump($lsWhere) ;
		 return $this->getEntityByWhere($lsWhere) ;
	 }
	

}
