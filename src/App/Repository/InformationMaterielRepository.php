<?php

namespace App\Repositories;
use Phaln\AbstractRepository;

class InformationMaterielRepository extends AbstractRepository
{
    public function __construct() 
    {
        // parent::__construct();
        
        parent::__construct(); // Appel du constructeur de Phaln\AbstractRepository
        $this->table = 'informationmateriel'; // Nom de la table à mapper avec l’entité
        $this->classMapped = 'Entities\InformationMateriel'; // Nom de l’entité à mapper avec la table
        $this->idFieldName = 'idInfoMateriel'; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)

    }
}


