<?php

namespace App\Repositories;
use Phaln\AbstractRepository;

class DecrireRepository extends AbstractRepository
{
    public function __construct() 
    {
        // parent::__construct();
        
        parent::__construct(); // Appel du constructeur de Phaln\AbstractRepository
        $this->table = 'decrire'; // Nom de la table à mapper avec l’entité
        $this->classMapped = 'Entities\Decrire'; // Nom de l’entité à mapper avec la table
        $this->idFieldName = ["idTypeMateriel", "idInfoMateriel"]; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)

    }
}


