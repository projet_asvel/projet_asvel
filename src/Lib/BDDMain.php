<?php
namespace Lib;

use Lib\Exceptions\BDDException;
use Lib\Exceptions\RepositoryException;
use Lib\ExceptionsManager;
use mysqli;
use mysqli_sql_exception;
use PDO;
use PDOException;
use TypeError;

abstract class BDDMain
{
    private static $_bdd;
    private static $connexion;
    private static $connexionError;

    protected static function connect($data){
        self::$connexionError = null;
        $typedb = $data['type'];
        try{
            switch($typedb){
                case 'pdo':
                    self::$connexion = new PDO($data['bdd'].':host='.$data['host'].';port='.$data['port'].';dbname='.$data['database'].';', $data['user'], $data['password'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
                    break;
                case 'mysqli':
                    self::$connexion = new mysqli($data['host'],$data['user'],$data['password'],$data['database'],$data['port']);
                    mysqli_report(MYSQLI_REPORT_STRICT);	// MYSQLI_REPORT_STRICT MYSQLI_REPORT_ALL
                    break;
                default:
                    self::$connexionError = "Le type de connexion à la base de données est incorrect.";
            }
        }catch(TypeError $e) {
            self::$connexionError = "Une erreur s'est produite lors de l'instanciation de la base de données.";
            ExceptionsManager::addException(new BDDException(self::$connexionError));
            return false;
        }catch(mysqli_sql_exception $e){
            self::$connexionError = "Une erreur s'est produite sur la base de données '".$data['database']."'<br>".utf8_encode($e->getMessage());
            ExceptionsManager::addException(new BDDException(self::$connexionError));
            return false;
        }catch(PDOException $e){
            self::$connexionError = "Une erreur s'est produite sur la base de données '".$data['database']."'<br>".utf8_encode($e->getMessage());
            ExceptionsManager::addException(new BDDException(self::$connexionError));
            return false;
        }
        if($typedb == "pdo") {
            self::$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        self::get();
        return true;
    }
    
    public static function init($login)
    {
        $class = get_called_class();
        self::$_bdd = new $class($login);
        return self::$_bdd;
    }
    
    public static function get()
    {
        if(self::$connexionError == null){
            if(self::getType() == "mysqli") {
                self::$connexion->set_charset("utf8");
            }
            get_called_class()::setDB(self::$connexion);
            return self::$connexion;
        }
    }
	
	public static function createBuilder($type){
        return new BDDBuilder($type);
    }

    protected static function setDB($db){
        if(get_class($db) != "PDO" && get_class($db) != "mysqli"){
            ExceptionsManager::addException(new BDDException('Le type de BDD est incorrect (got \''.get_class($db).'\')'));
            return;
        }
        get_called_class()::$db = $db;
    }

    public static function getType(){
        if(!isset(get_called_class()::$db)){
            return get_class(self::$connexion);
        }else{
            return get_class(get_called_class()::$db);
        }
    }

    public static function getDB(){
        return get_called_class()::$db;
    }

    private static function ping(){
        $dbClass = get_called_class()::$db;
        switch(get_class($dbClass)){
            case 'mysqli':
                return !$dbClass->ping();
            case 'PDO':
                try {
                    $dbClass->query('SELECT 1');
                    return true;
                } catch (PDOException $e) {
                    return false;
                }
            default:
                throw new BDDException("Le type de connexion à la base de données est incorrect.");
        }
    }

    public static function query($req){
        $dbClass = get_called_class()::$db;
        if(!isset($dbClass)){
            return false;
        }
        if(!self::ping()){
            self::connect(get_called_class()::$con);
        }
        try {
            $res = $dbClass->query($req);
        } catch (PDOException $e) {
            ExceptionsManager::addException(new BDDException($e->getMessage()));
            return false;
        }
        if(!$res){
            ExceptionsManager::addException(new BDDException($dbClass->error));
        }
        return $res;
    }

    public static function lastInsert(){
        $dbClass = get_called_class()::$db;
        return $dbClass->insert_id;
    }
}