<?php
namespace App\Lib;

final class Functions {

	// Selon https://www.php.net/strtr
	static public function normalise($string, $trim = true, $toUpper = true) {
		$table = array(
			'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
			'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
			'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
			'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
			'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
			'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
			'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
			'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
// LG 20220730 début
            '-'=>' ','\' '=>'','"'=>' ',      // tiret, quote et double-quote
// LG 20220730 fin
		);
		if ($trim) {
			array_push($table, [' '=>'']) ;
		}
// var_dump($string) ;
		$normalized = @strtr($string, $table);      // LG 20200429 : @ permet d'éviter le notice selon les versions de PHP (cf. prod.)
		if ($trim) {
			$normalized = str_replace(" ", "", $normalized) ;
		}
		if ($toUpper) {
			$normalized = strtoupper($normalized) ;
		}
		return $normalized ;
	}
	
	// LG 20200210 : repris de CodeGears
	static public function getPath($call=true, $diff=0){
		if(!$call){
			return '';
		}
		$count = substr_count($_SERVER["REQUEST_URI"], "/") - 1 + $diff;
		$ret = $count - substr_count(URL_BASE, "/");
		$final = "";
		for($i=1;$i<=$ret;$i++){
			$final .= "../";
		}
		return $final;
	}

	/**
	 * détermine si la chaine passée en paramètre représente une date valide
	 * LG 20200216
	 * @param string $value     : date à vérifier
         * @param strung valueISO   : pour retour de la date au format ISO
	 * @return bool
	 */
	static public function string_is_date($value, &$valueISO = null){
		if (preg_match("/([0-9]{2})[-\/]([0-9]{2})[-\/]([0-9]{4})/", $value, $matches)) {
			// MM/DD/YYYY
			if (!checkdate($matches[2], $matches[1], $matches[3])) {
				return false ;
			}
                        $valueISO = $matches[3]."/".$matches[2]."/".$matches[1] ;
		} else if (preg_match("/([0-9]{4})[-\/]([0-9]{2})[-\/]([0-9]{2})/", $value, $matches)) {
			// YYY/MM/DD
			if (!checkdate($matches[2], $matches[3], $matches[1])) {
				return false ;
			}
                        $valueISO = $value ;
		} else {
			return false ;
		}
		return true ;
	}

	
	static public function chaine_aleatoire($nb_car = 10, $chaine = 'AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjklmwxcvbn123456789') {
		$nb_lettres = strlen($chaine) - 1;
		$generation = '';
		for ($i = 0; $i < $nb_car; $i++) {
			$pos = mt_rand(0, $nb_lettres);
			$car = $chaine[$pos];
			$generation .= $car;
		}
		return $generation;
	}

	static function getNomMois($piMois) {
		if ($piMois == 1) return "janvier" ;
		if ($piMois == 2) return "février" ;
		if ($piMois == 3) return "mars" ;
		if ($piMois == 4) return "avril" ;
		if ($piMois == 5) return "mai" ;
		if ($piMois == 6) return "juin" ;
		if ($piMois == 7) return "juillet" ;
		if ($piMois == 8) return "août" ;
		if ($piMois == 9) return "septembre" ;
		if ($piMois == 10) return "octobre" ;
		if ($piMois == 11) return "novembre" ;
		if ($piMois == 12) return "décembre" ;
		return "inconnu" ;
	}
}