<?php
namespace Lib;

use Lib\Exceptions\NotFoundException;
use ReflectionClass;

class ExceptionsManager
{
    private static $ExceptionList = array();
    private static $adminCheck = null;

    public static function addException($e){
        if (!$e instanceof \Exception&& !$e instanceof \Error) {
            self::addException(new \InvalidArgumentException('The exception must be a Exception or a Error object.'));
        }
        try {
            $reflect = new ReflectionClass($e);
            $class = $reflect->getShortName();
            if(!isset(self::$ExceptionList[$class])){
                self::$ExceptionList[$class] = array();
            }
            self::$ExceptionList[$class][] = array(
                'message' => $e->getMessage(),
                'stacktrace' => str_replace("\n", '<br>', $e->getTraceAsString())
            );
        } catch (\ReflectionException $e) {
            self::addException(new NotFoundException("La classe '$e' n'est pas valide ou introuvable."));
        }

        $file = fopen("log/error.log","a+");
        $time = date('Y-m-d H:i:s');
        $data = "[$time] [ERROR] : ".$e->getMessage()."\n\t".str_replace("\n", "\n\t", $e->getTraceAsString())."\n";
        fwrite($file, $data);
        fclose($file);
    }

    public static function getIndex(){
        $index = array();
        foreach (self::$ExceptionList as $exception => $value){
            $index[] = $exception;
        }
        return $index;
    }

    public static function getValuesByIndex($index){
        return self::$ExceptionList[$index];
    }

    public static function getExceptions(){
        return self::$ExceptionList;
    }

    public static function setAdminCheck($check){
        if(is_callable($check)){
            self::$adminCheck = $check;
        }
    }

    public static function checkAdmin(){
        return self::$adminCheck->__invoke();
    }
}