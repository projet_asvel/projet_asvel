<?php
namespace Lib;

use \lib\ExceptionsManager;
use \lib\Exceptions\RepositoryException;

class BDDBuilder
{
    private $table;
    private $values = [];
    private $type;

    private $idField;
    private $id;

    public function __construct($type)
    {
        if(isset($type) && gettype($type) == "string"){
            $this->type = $type;
        }else{
            ExceptionsManager::addException(new RepositoryException('Type manquant ou incorrect pour le BDDBuilder'));
        }
    }

    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    public function setId($field, $id){
        $this->idField = $field;
        $this->id = $id;
        return $this;
    }

    public function addValue($index, $value){
        $this->values[$index] = $value;
        return $this;
    }

    public function removeValue($index){
        unset($this->values[$index]);
        return $this;
    }

    public function getInsertQuery(){
        $ind = "";
        $val = "";
        $basic = "INSERT INTO ".$this->table;
        foreach($this->values as $index => $insert){
            $ind .= "$index,";
            if(gettype($insert) == "string"){
                $val .= "'$insert',";
            }else{
                $val .= "$insert,";
            }
        }
        $ind = substr($ind, 0, -1);
        $val = substr($val, 0, -1);
        return "$basic ($ind) VALUES ($val);";
    }

    public function getUpdateQuery(){
        $sql = "";
        $basic = "UPDATE ".$this->table." SET";
        $where = "WHERE ".$this->idField."=".$this->id;
        foreach($this->values as $index => $insert){
            $sql .= "$index=";
            if(gettype($insert) == "string"){
                $sql .= "'$insert', ";
            }else{
                $sql .= "$insert, ";
            }
        }
        $sql = substr($sql, 0, -2);
        return "$basic $sql $where;";
    }

    public function getQuery(){
        return $this->type=="update"?$this->getUpdateQuery():$this->getInsertQuery();
    }

    public function exec(){
        return BDD::query($this->getQuery());
    }
}