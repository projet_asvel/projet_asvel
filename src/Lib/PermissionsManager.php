<?php
namespace Lib;

use App\Repository\RoleRepository;

class PermissionsManager
{
    private $permsList;
    
    public function __construct(array $list) {
        $this->permsList = $list;
    }
    
    public function HasAccessToController($controller, $func){
        if(!RoleRepository::isConnected()){
            if($this->permsList[ROLE_GUEST] == "*") {
                return true;
            }else{
                if(in_array($controller, array_keys($this->permsList[ROLE_GUEST]))){
                    if($this->permsList[ROLE_GUEST][$controller] == "*"){
                        return true;
                    }
                    foreach($this->permsList[ROLE_GUEST][$controller] as $fun){
                        if($fun == $func){
                            return true;
                        }
                    }
                    return false;
                }
                return false;
            }
        }elseif(RoleRepository::isConnected()){
            $roles = RoleRepository::getRoleByAdherent($_SESSION['User']->getIdAdherent());
            foreach($roles as $id => $role){
                if($this->permsList[$role->getIdRole()] == "*"){
                    return true;
                }
                if(in_array($controller, array_keys($this->permsList[$role->getIdRole()]))){
                    if($this->permsList[$role->getIdRole()][$controller] == "*"){
                        return true;
                    }
                    foreach($this->permsList[$role->getIdRole()][$controller] as $fun){
                        if($fun == $func){
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }
}