<?php

namespace Lib;

require '../../vendor/autoload.php';
use \Mailjet\Resources;
// Voir https://dev.mailjet.com/email/guides/getting-started/ et https://dev.mailjet.com/email/guides/send-api-v31/

final class olitsMailer {

	public static $_infoMail;	  // Rempli par appConfig.php

	/** Envoyer un mail avec MailJet
	 * $to		: liste d'adresses sous forme de liste avec séparateur virgule : <mail1>[:<nom1>][[,<mail2>[:<nom2>][...]]
	 * $subject
	 * $body
	 */
	public static function EnvoyerMail($psTo, $psSubject, $psBody, $psSender="", $pbDestinatairesVisibles=true) {
        
        $mj = new \Mailjet\Client(self::$_infoMail["Username"], self::$_infoMail["Password"],true,['version' => 'v3.1']);
        $lsSenderMail = $psSender?$psSender:self::$_infoMail["Sender"] ;

        $laGlobals = [] ;
        $laGlobals["From"] = array("Email"=>$lsSenderMail, "Name"=>$lsSenderMail) ;
        $laGlobals['Subject'] = $psSubject ;
        $laGlobals['TextPart'] = $psBody ;  // Body du message en txt
// LG 20220909 old        $laGlobals['HTMLPart'] = $psBody ;  // Body du message en HTML
        $laGlobals['HTMLPart'] = str_replace("\n", "<br/>\n", $psBody) ;  // Body du message en HTML

        $laMessages = [] ;

		$laTo = explode(",", $psTo) ;
		foreach($laTo as $lsTo) {
            
			if (strpos($lsTo, ":")) {
				// Un nom de destinataire a été fourni en plus de l'adresse
                $lsDestMail = explode(":", $lsTo)[0] ;
                $lsDestName = explode(":", $lsTo)[1] ;
                $laDest = ["Email"=>$lsDestMail, "Name"=>$lsDestName] ;
			} else {
				// Seule l'adresse du destinataire a été fournie
                $lsDestMail = $lsTo ;
                $laDest = ["Email"=>$lsDestMail, "Name"=>$lsDestMail] ;
			}

            // Parcourir les destinataires
            if ($pbDestinatairesVisibles) {
                // Les destinataires peuvent voir les autres destinataires
                // On construit une seule liste de destinataires
                if (count($laMessages) == 0) {
                    $laMessages[] = [] ;
                }
                if (!isset($laMessages[0]["To"])) {
                    $laMessages[0]["To"] = [] ;
                }
                array_push($laMessages[0]["To"], $laDest) ;

            } else {
                // Chaque destinataire doit avoir son propre message
                // Ajouter un message pour chacun
                $laMessage = [] ;
                $laMessage['To'] = [$laDest] ;
                $laMessages[] = $laMessage ;
            }


		}

        $laData = [
            'Globals' => $laGlobals ,
            'Messages' => $laMessages
            ];

        $response = $mj->post(Resources::$Email, ['body' => $laData]);
        $lbSent = $response->success() ;

		if (!$lbSent) {
            // Echec de l'envoi
// var_dump($response->getData()) ;
// LG 20220909 début
//			$lsLog = date('Y-m-d H:i:s') . " : "  . $response->getData() . "\n" ;
// echo json_encode($response->getData()) ;
            $laInfosErrors = $response->getData() ;
			$lsLog = date('Y-m-d H:i:s') . " : "  . json_encode($laInfosErrors) . "\n" ;
			$lsFileName = "../../logs/EnvoisMailKO.log" ;
// LG 20220909 fin
			file_put_contents($lsFileName, $lsLog, FILE_APPEND) ;
// LG 20220909 début
//			echo "Erreur du serveur de mail : " . $response->getData();
			// return false ;
            if (isset($laInfosErrors["Messages"])) {
                // Cas de MailJet ?
                $laMessages = $laInfosErrors["Messages"] ;
                $loRepoAdherents = new \App\Repository\AdherentRepository() ;
                $lsLstErr = "" ;
                $lsLstTo_OK = "" ;
                foreach($laMessages AS $laMessage) {
                    if ($laMessage["Status"] == "success") {
                        // Ce message est envoyé : faire la liste des destinataires OK
                        foreach($laMessage["To"] As $laTo) {
                            $lsMail = $laTo["Email"] ;
                            if ($loAdherent = $loRepoAdherents->getEntityByLogin($lsMail)) {
                                // Un adhérent avec ectte adresse mail a été trouvé : inscrire son nom
                                $lsMail = $loAdherent->getNomUsuel() ;
                            }
                            
                            $lsLstTo_OK .= $lsMail . "<br/>" ;
                        }
                    } else {
                        // Ce message n'a pas pu être envoyé : faire la liste des erreurs
                        foreach($laMessage["Errors"] As $laErr) {
                            $lsLstErr .= $laErr["ErrorMessage"] . "<br/>" ;
                        }
                    }
                }
                $lsMsgErr = $lsLstErr ;
                if ($lsLstTo_OK) {
                    $lsMsgErr .= "<br/><span style= \"background-color: green;\">Certains messages ont été bien envoyés : <br/>" . $lsLstTo_OK . "</span>" ;
                }
            } else {
                $lsMsgErr = json_encode($laInfosErrors) ;
            }
//			echo "Erreur du serveur de mail : " . $lsMsgErr;
            return $lsMsgErr;
// LG 20220909 fin
		} else {
			return true;
		}

    }

}
