<?php
namespace Lib;

use Lib\Twig\RoleExtension;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Lib\Twig\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

abstract class Controller
{
    protected $twig;

    function __construct()
    {
        $loader = new FilesystemLoader(TWIG_PATH);
        $this->twig = new Environment($loader, [
            'debug' => $_SESSION['env'] == "dev"
        ]);
        $this->twig ->addGlobal('session', $_SESSION);

        $this->twig->addExtension(new RoleExtension());
        if($_SESSION['env'] == "dev") {
            $this->twig->addExtension(new DebugExtension());
        }
    }

    abstract function DefaultAction();

    protected function render($tpl, array $arr = []){
        try {
            $arr['host'] = getHost();
            if($_SESSION['env'] == "dev"){
                $arr['exceptionEnable'] = true;
                $arr['access'] = ExceptionsManager::checkAdmin();
                $arr['exceptions'] = ExceptionsManager::getExceptions();
                $arr['critical'] = false;
            }
            return $this->twig->render($tpl, $arr);
        } catch (LoaderError $e) {
            ExceptionsManager::addException($e);
            return $e->getMessage();
        } catch (RuntimeError $e) {
            ExceptionsManager::addException($e);
            return $e->getMessage();
        } catch (SyntaxError $e) {
            ExceptionsManager::addException($e);
            return $e->getMessage();
        }
    }

    public function Error($critical=false){
        echo $this->render('base.twig', array(
            'access' => ExceptionsManager::checkAdmin(),
            'exceptions' => ExceptionsManager::getExceptions(),
            'critical' => $critical
        ));
    }
}
