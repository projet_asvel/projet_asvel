<?php
namespace Lib;

class BDD extends BDDMain
{
    protected static $db;
    protected static $con;

    public function __construct(array $login=null)
    {
        if(isset($login)){
            self::connect($login);
            self::$con = $login;
        }
    }
}