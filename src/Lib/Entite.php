<?php
namespace Lib;

use Lib\Exceptions\EntityException;

/**
 * Classe abstraite Entite ancapsulant les assesseurs en lecture et écriture
 * par défaut aux attributs d'une classe.
 * N'a de sens que pour les classes de la couche modèle qui ne contiennent
 * que des attributs et leurs assesseurs.
 *
 * @author Philippe
 */
abstract class Entite implements \JsonSerializable {
	/**
	 * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
	 * Elle appelle les mutateurs nécessaires.
	 * @param array $datas le tableau associatif des attributs à affecter
	 * @return $this l'objet courant
	 */
	public function hydrate(array $datas=NULL) 
	{
	    //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	    $attrib = get_class_vars(get_class($this));

	    // Appelle le mutateur des attributs existant dans le tableau $datas
	    foreach($attrib as $key=>$val)
	    {
            if(isset($datas[$key]))
            {
                $mutateur = 'set'.$key;
                $this->$mutateur($datas[$key]);
            }
	    }

	    return $this;
	}

	/**
	 * Mutateur simple d'accès en écriture à un attribut.
	 * Peut être surchargé.
	 * @param type $attribut le nom de l'attribut à modifier
	 * @param type $valeur la nouvelle valeur de l'attribut
	 * @return Entite
	 */
	protected function set($attribut, $valeur)
	{
		$this->$attribut = $valeur;
		return $this;
	}
	
	/**
	 * Lecteur simple d'accés en lecture à un attribut
	 * Peut être surchargé.
	 * @param type $attribut le nom de l'attribut
	 * @return type la valeur de l'attribut
	 */
	protected function get($attribut)
	{
		return $this->$attribut;
	}

    /**
     * Méthode "magique" interceptant l'appel de méthode et cherchant a exécuter
     * les assesseurs.
     * @param type $methode nom complet de la méthode
     * @param type $attribValeur la valeur passée en paramètre à la méthode
     * @return Entite
     * @throws Exceptions\EntityException
     */
	public function __call($methode, $attribValeur)
	{
	    $attribName = NULL;

	    //  Le préfixe, normalement get ou set
	    $prefix = substr($methode, 0, 3);   

	    //  Le suffixe, normalement un nom d'attribut
	    $suffix = substr($methode, 3);

	    //  Le nombre de valeur à affecter à l'attribut, 
	    //  normalement 0 pour un get et 1 pour un set
	    $cattrs = count($attribValeur);

	    //  Vérification de l'existence de l'attribut correspondant au suffixe.
	    //  La première lettre du suffixe peut être en majuscule ou non.
	    if(property_exists($this, $suffix))
		    $attribName = $suffix;
	    else if(property_exists($this, lcfirst($suffix)))	
		    $attribName = lcfirst($suffix);

	    //  Il existe bien un attribut correspondant au suffixe
	    if($attribName != NULL)
	    {
            if($prefix == 'set' && $cattrs == 1)
                return $this->set ($attribName, $attribValeur[0]);
            if($prefix == 'get' && $cattrs == 0)
                return $this->get($attribName);
	    }
	    else
	        ExceptionsManager::addException(new EntityException("La méthode $methode n'existe pas..."));
	}
	
	/**
	 * La classe implémente l'interface JsonSerializable ce qui permet 
	 * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
	 * @return array
	 */
	public function jsonSerialize()
	{
	    $array = array();

	    //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	    $attrib = get_class_vars(get_class($this));

	    // Associe la clé du nom de l'attribut à la valeur de cet attribut
	    foreach($attrib as $key=>$val)
	    {
		    $array[$key] = $this->get($key);
	    }
	    return $array;
	}

	/**
	 * Pour affichage simple, style csv, d'un objet
	 * @return string une string composée des valeurs des attributs séparés par un ';'
	 */
	public function __toString()
	{
	    $string = "";

	    //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	    $attrib = get_class_vars(get_class($this));

	    // Ajoute la valeur de chaque attribut à la $string
	    foreach($attrib as $key=>$val)
	    {
		    $string .= $this->get($key) .';';
	    }

	    return $string;
	}
	
	public function __toArray()
	{
	    return $this->jsonSerialize();
	}
}