<?php
namespace Lib\Exceptions;

use Lib\ExceptionsManager;
use Throwable;

/**
 * Description of BDDException
 *
 * @author phaln
 */
class NotFoundException extends \Exception {
}
