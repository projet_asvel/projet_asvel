<?php

//  Le titre à donner aux pages du site
define('TITRE_APP', "Planning sorties ASVEL - Powered by Limoog");
define('DESCRIPION_APP', "Visualisez les plannings des sorties montagne de l'ASVEL Ski Montagne et inscrivez-vous en ligne");
define('VERSION_APP', 1.6);

// La durée d'une session (en secondes)
define('SESSION_LENGTH', 1800);

// Le tarif des sorties
define('FRAIS_KILOMETRIQUES', 0.3);
define('TARIF_SORTIE_ALPI_1J', 6);
define('TARIF_SORTIE_ALPI_2J', 15);
define('TARIF_SORTIE_ALPI_3J', 15);
define('TARIF_SORTIE_ESCALADE_1J', 6);
define('TARIF_SORTIE_ESCALADE_2J', 15);
define('TARIF_SORTIE_ESCALADE_3J', 15);
define('TARIF_SORTIE_AUTRE_1J', 3);
define('TARIF_SORTIE_AUTRE_2J', 8);
define('TARIF_SORTIE_AUTRE_3J', 13);
define('TARIF_SORTIE_SKIPISTE_1J', "-");
define('TARIF_SORTIE_SKIPISTE_2J', 8);
define('TARIF_SORTIE_SKIPISTE_3J', 13);

define('TARIF_LOCATION_DVA', 6);

// Les types de règlement (cf. script SQL data_base.sql)
define('TYPEREGELEMENT_ESPECES', 1);
define('TYPEREGELEMENT_CHEQUE', 3);
define('TYPEREGELEMENT_CHEQUEVACANCES', 4);

// L'avance à l'inscription
define('NB_JOURS_AVANCE_INSCRIPTION_SORTIE_1_J', 5);
define('NB_JOURS_AVANCE_INSCRIPTION_SORTIE_2_3_J', 5+7);
define('NB_JOURS_AVANCE_INSCRIPTION_SORTIE_SUP_3_J', 8 * 7);

// HEURE OUVERTURE 
define('HEURE_OUVERTURE_INSCRIPTION_SORTIE',18);
define('HEURE_FERMETURE_INSCRIPTION_SORTIE',19.5);

define('NB_JOURS_LIMITE_INSCRIPTION_SORTIE', 4);

// Signature par défaut des mails
// LG 20221003 old define('SIGNATURE_MAIL', chr(13) . chr(13) . "Dans le cadre de la prévention de la COVID19, merci de penser à vous munir d'un masque et de gel hydro-alcoolique.");
define('SIGNATURE_MAIL', chr(13) . chr(13) . "");

// LG 20211106 début
define('FILTRESORTIE_TOUTESDATES', 0);
define('FILTRESORTIE_DATESAVENIR', 1);
define('FILTRESORTIE_DATESPASSEES', 2);
// LG 20211106 fin

session_start();
if(isset($_SESSION['timeout'])) {
//    if(time() - $_SESSION['timeout'] > 1800) {
    if(time() - $_SESSION['timeout'] > SESSION_LENGTH) {
        // Inactivité de + de 30 minutes (1800 secondes = 30 min)
        // Fermer la session
        session_destroy();
        session_start();
    }
}
$_SESSION['timeout'] = time() ;

// Constantes pour les chemins.
// Les pages étant dans public/pages, il faut remonter 2 fois.
define('TEMPLATE_PATH', '../../src/Templates');
define('CLASS_DIR', '../../src/,../../vendor/,../../vendor/phaln/');

//  Autoload pour compatibilité Linux (pb des séparateurs d'espace de nom...)
spl_autoload_register(function ($className) {
    $find = false;
    $bases = explode(',', CLASS_DIR);
    $extension = '.php';
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    foreach ($bases as $base) {
	$fullName = $base . $className . $extension;
	if (file_exists($fullName)) {
	    require_once($fullName);
	    $find = true;
	}
	if($find) break;
    }
    return $find;
});

require_once 'appConfig.php';
require_once '../../src/Lib/Functions.php';		// LG 20200212

//  Nécessaire pour éviter le "bug" du json_encode qui travaille différement
//  avec les float (pb de précision) à partir de PHP7.1 
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set('serialize_precision', -1);
}

//  Active ou pas l'affichage de debug et les var_dump
if(!defined('DUMP_EXCEPTIONS'))
    define('DUMP_EXCEPTIONS', TRUE);
if (DUMP_EXCEPTIONS) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
} 
else {
    ini_set('display_errors','Off');  
}

//  FonctionRepositories à utiliser de préférence à var_dump() lorsque XDebug n'est pas configuré sur la configuration
//  Attention, pour voir les affichages, il faut:
//	* soit basculer la constante DUMP à TRUE... 
//	* soit passer TRUE en deuxième paramètre.
//  Vous pouvez passer en 3ème paramètre une chaine de caractères pour commenter le dump.
//  Dans ce cas, vous DEVEZ donner une valeur pour le deuxième paramètre (soit DUMP, soit TRUE).
function dump_var($var, $dump=DUMP, $msg=null)
{
    if($dump) {
		if($msg)
			echo"<p><strong>$msg</strong></p>";
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
    }
}
