<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', FALSE);

//  Basculer à TRUE pour activer les affichages des messages d'erreur
define('DUMP_EXCEPTIONS', TRUE);

//  Basculer à TRUE pour activer le cryptage des mdp (LG 20191222)
define('MDPCRYPTES', TRUE);

//  L'url de votre site, sera utile dans les pages en cas de déplacement du site...
define('URL_BASE', "http://localhost/ASVEL2/");

// Pour être compatible avec la BDD (cf. testParticiper.php)
date_default_timezone_set('Europe/Paris');

//  Vos informations de connexion à la BDD
$infoBdd = array(
		'interface' => 'pdo',	    // "pdo" ou "mysqli"
		'type'   => 'pgsql',	    //  mysql ou pgsql
		'host'   => 'localhost',
		'port'	 => '5432',
		'dbname' => 'ASVEL',
		'user'   => 'postgres',
		'pass'   => 'root',
	);

//  Pour la connexion à la BDD en cas d'utilisation de la librairie Phaln
Phaln\BDD::$infoBdd = $infoBdd;

// Pour les envois de mail
$infoMail = array(
    'SMTPDebug'=>DUMP?3:0,		// Niveau de débogage : : 1 = Erreurs et messages, 2 = messages seulement
    'SMTPAuth'=>'true',			// Authentification SMTP active
	'SMTPSecure'=>'STARTTLS',	// Gmail REQUIERT Le transfert securise
    'Host'=>'smtp.monFournisseur.com',
    'Port' => '587',
    'Username' => 'asvel.skimontagne@monFournisseur.com',
    'Password' => 'mdp',
    'Sender'    => 'contact@asvelskimontagne.fr',
);
Lib\olitsMailer::$_infoMail = $infoMail;
