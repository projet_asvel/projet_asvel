/*
-- Trouver les doublons
select normalise(nomAdherent), normalise(prenomAdherent), count(*) from adherent Group by 1, 2 Having Count(*) > 1 Order by 1 ;
-- Trouver les Ids des doublons
select * from adherent where (normalise(nomAdherent), normalise(prenomAdherent)) In (('PETTENUZZO', 'STEPHANE'), ('PALISSE', 'FRANCOISE')) ;
commit ;
*/

-- LG 20220730 début
/*
-- Eliminer les doublons (l'adhérent d'Id 349 récupère tout ce qui vient de 599)
Rollback ;
Begin ;
Update Participer set idAdherent = 349 Where idAdherent = 599 ;
Update Participer set idInscripteur = 349 Where idInscripteur = 599 ;

Delete from Affilier where (idAdherent, idFederation, dateaffiliation) In (Select 349, idFederation, dateAffiliation From Affilier Where idAdherent = 599) ;
Update Affilier set idAdherent = 349 Where idAdherent = 599 ;

Delete from Avoir where (idAdherent, idTag) In (Select 349, idTag From Avoir Where idAdherent = 599) ;
Update Avoir set idAdherent = 349 Where idAdherent = 599 ;

Update Recevoir set idAdherent = 349 Where idAdherent = 599 ;
Update Obtenir set idAdherent = 349 Where idAdherent = 599 ;

Delete from Etre where (idAdherent, idRole) In (Select 349, idRole From Etre Where idAdherent = 599) ;
Update Etre set idAdherent = 349 Where idAdherent = 599 ;

Delete from Adherent Where idAdherent = 599 ;
*/

Create or replace function dedoublonne(piAdherentAConserver integer, piAdherentAVirer integer) returns boolean as $$
begin
	Update Participer set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;
	Update Participer set idInscripteur = piAdherentAConserver Where idInscripteur = piAdherentAVirer ;

	Delete from Affilier where (idAdherent, idFederation, dateaffiliation) In (Select piAdherentAConserver, idFederation, dateAffiliation From Affilier Where idAdherent = piAdherentAVirer) ;
	Update Affilier set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Avoir where (idAdherent, idTag) In (Select piAdherentAConserver, idTag From Avoir Where idAdherent = piAdherentAVirer) ;
	Update Avoir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Update Recevoir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;
	Update Obtenir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Etre where (idAdherent, idRole) In (Select piAdherentAConserver, idRole From Etre Where idAdherent = piAdherentAVirer) ;
	Update Etre set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Adherent Where idAdherent = piAdherentAVirer ;

	return true ;
End ; $$ language plpgsql ;

select normalise(nomAdherent), normalise(prenomAdherent), * 
	from adherent 
	where (normalise(nomAdherent), normalise(prenomAdherent)) In (select normalise(nomAdherent), normalise(prenomAdherent) from adherent Group by 1, 2 Having Count(*) > 1 Order by 1)
	Order by 1, 2 ;

-- LG 20220730 fin

-- Dédoublonnage au 20220730
-- select dedoublonne(776, 769) ;
-- select dedoublonne(724, 533) ;
-- select dedoublonne(457, 732) ;
-- select dedoublonne(733, 458) ;
-- select dedoublonne(690, 780) ;
-- select dedoublonne(554, 527) ;
-- select dedoublonne(757, 716) ;
-- select dedoublonne(638, 631) ;
-- select dedoublonne(699, 586) ;
-- select dedoublonne(388, 616) ;
-- select dedoublonne(487, 747) ;
-- select dedoublonne(690, 780) ;
-- select dedoublonne(646, 751) ;
-- select dedoublonne(647, 628) ;
select dedoublonne(647, 752) ;
