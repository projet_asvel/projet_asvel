ALTER TABLE Federation
    ADD COLUMN nbJoursValiditeAffiliation integer Default -1;
Update Federation set nbJoursValiditeAffiliation = -1 Where nomFederation = 'FFME' ;
Update Federation set nbJoursValiditeAffiliation = -2 Where nomFederation = 'FFS' ;

insert into federation (nomFederation, nbJoursValiditeAffiliation) VALUES 
    ('Découverte journée', 1)
    , ('Découverte WE', 2)
    , ('Carte membre', -1)
    , ('Invité', 2)
    , ('Club partenaire', -2) ;

ALTER TABLE Affilier
    ADD COLUMN idAffiliation Serial Not Null ;
ALTER TABLE Affilier 
    Drop Constraint Affilier_pkey ;
ALTER TABLE Affilier 
    Add CONSTRAINT Affilier_pkey PRIMARY KEY (idAffiliation) ;
ALTER TABLE Affilier 
    Add CONSTRAINT Affilier_Unique_ad_Fed_Date UNIQUE (idAdherent, idFederation, dateAffiliation) ;
ALTER TABLE Affilier
    ADD COLUMN dateFinAffiliation Date Null Default Null ;

Alter Table Participer Add Column idInscripteur Integer constraint FK_Participer_Inscripteur references Adherent(idAdherent) ;
