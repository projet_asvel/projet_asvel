-- Selon https://stackoverflow.com/questions/13596638/function-to-remove-accents-in-postgresql
CREATE OR REPLACE FUNCTION normalise(psText VarChar, pbTrim Boolean Default true, pbToUpper Boolean Default true) 
  RETURNS character varying AS 
$BODY$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	if pbTrim Then
		select translate(psText, ' ', '') into psText; 
	End If ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
        select translate(v_str, 'Ç', 'C') into v_str; 
        select translate(v_str, 'Ç', 'C') into v_str; 
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
		select translate(v_str, 'ç', 'c') into v_str; 
	end if ;
return v_str; 
end;$BODY$ 
LANGUAGE 'plpgsql' VOLATILE; 

-- ******************************************************************************************************************************
Drop Function If exists Encadrants() CASCADE ;
Create Or Replace FUNCTION MiseAJourParticipationEncadrants() RETURNS trigger as $MiseajourEncadrant$ 
DECLARE
ancienencadrant int;
BEGIN
	if TG_OP = 'UPDATE' then
		UPDATE Participer 
			SET idfonction = 1
			WHERE Participer.idadherent = OLD.idorganisateur
				AND Participer.idsortie = New.idSortie ;
	End If ;
	UPDATE Participer 
		SET idfonction = 2
                    , validationParticipation = true
		WHERE idadherent = New.idorganisateur
			AND idsortie = New.idSortie
		Returning idadherent into ancienencadrant ;
	if ancienencadrant is null then
		INSERT INTO Participer (idsortie,idadherent,idfonction, validationParticipation) values (New.idSortie, New.idorganisateur, 2, true);
	end if;
	RETURN New;
END;
$MiseajourEncadrant$
LANGUAGE PLPGSQL;	

Drop Trigger If exists MiseajourEncadrant ON Sortie ;
CREATE Trigger MiseajourEncadrant
AFTER INSERT OR UPDATE ON Sortie For Each Row
execute procedure MiseAJourParticipationEncadrants() ;

-- ******************************************************************************************************************************
CREATE OR REPLACE FUNCTION DateDerniereInscriptionAdherent()  RETURNS TRIGGER AS $BODY$ 
BEGIN
        UPDATE adherent 
            SET dateDerniereInscriptionAdherent = (SELECT MAX(dateaffiliation) FROM affilier WHERE new.idAdherent= affilier.idAdherent)
			Where new.idAdherent= adherent.idAdherent;
    RETURN new;
END;
$BODY$ 
LANGUAGE plpgsql ;


Drop Trigger If exists UpdateInscriptionAdherent on affilier;
CREATE TRIGGER UpdateInscriptionAdherent 
AFTER INSERT OR UPDATE 
ON affilier 
FOR EACH ROW 
EXECUTE PROCEDURE 
DateDerniereInscriptionAdherent();

-- ******************************************************************************************************************************
-- Selon https://stackoverflow.com/questions/13596638/function-to-remove-accents-in-postgresql
CREATE OR REPLACE FUNCTION normalise(psText VarChar, pbTrim Boolean Default true, pbToUpper Boolean Default true) 
  RETURNS character varying AS 
$BODY$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
	select translate(v_str, '-', '') into v_str; 		-- LG 20220730 : tiret
	select translate(v_str, '''"', '  ') into v_str; 	-- LG 20220730 : quotes et double-quotes
    select translate(v_str, 'Ç', 'C') into v_str;  	    -- LG 20220730
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
-- LG 20220730 old		select translate(v_str, 'Çç', 'Cc') into v_str; 
		select translate(v_str, 'ç', 'c') into v_str; 
	end if ;
	if pbTrim Then
		select translate(v_str, ' ', '') into v_str; 
	End If ;
return v_str; 
end;$BODY$ 
LANGUAGE 'plpgsql' VOLATILE; 
---------------------------------------------------------------------------------------------------------------------------------------------------------
-- LG 20201011
-- Récupérer une date de fin d'affiliation
Create Or Replace Function getDateFinAffiliation(pdFinAffiliation Date, pdDebutAffiliation Date, pnNbJoursValiditeAffiliation Integer) returns date As $getDateFinAffiliation$
Begin
	if not pdFinAffiliation is Null Then
		return pdFinAffiliation ;
	end if ;

	if pnNbJoursValiditeAffiliation = -1 Then
		-- "1 an à partir du 1er septembre"
		if (To_Char(pdDebutAffiliation, 'MM'))::Integer >= 9 Then
			-- Inscription à partir de septembre : fin l'année suivante
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer + 1 || '/08/31', 'YYYY/MM/DD') ;
		else
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer || '/08/31', 'YYYY/MM/DD') ;
		end if ;

	elseif pnNbJoursValiditeAffiliation = -2 Then
		-- "1 an à partir du 1er novembre"
		if (To_Char(pdDebutAffiliation, 'MM'))::Integer >= 11 Then
			-- Inscription à partir de septembre : fin l'année suivante
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer + 1 || '/10/31', 'YYYY/MM/DD') ;
		else
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer || '/10/31', 'YYYY/MM/DD') ;
		end if ;
		
	else
		-- Validité un certain nb de jours
		return pdDebutAffiliation + pnNbJoursValiditeAffiliation ;
	end if ;

End ; $getDateFinAffiliation$ Language plpgsql immutable ;

-- Pour tester
--select getDateFinAffiliation(null, current_date, 2) ;