Drop function if exists StatistiquesParAdherent(date, Date, Integer, Boolean) ;
Create Or Replace function StatistiquesParAdherent(pdDebut date, pdFin Date, piIdActivite Integer Default Null, pbEncadre Boolean default false) 
    returns Table(/*idAdherent integer, */prenomAdherent varchar, nomAdherent varChar, nomActivite varChar
                    , nbInscriptionsTotales bigInt, nbInscriptionsAnnulées bigInt, nbInscriptionsSortiesAnnulées bigInt, nbParticipations bigInt, nbJoursParticipations bigInt, lstSorties Text)
    As $Statistiques$
Declare
	lsSQL Text ;
Begin
	lsSQL = $SQL$
        With cteSelActi As (
            Select A.idActivite, A.nomActivite
                From Activite A
				Where A.idActivite = Coalesce(%L, A.idActivite)
            )
			, cteNomActi As (
				Select distinct Case When Count(*) > 1 Then 'Toutes' Else String_agg(nomActivite, '')::varchar End nomActivite
					From cteSelActi
			)
			, cteSorties As (
                select S.*, S.idRaisonAnnulation notnull As lSortieAnnulée
                    from sortie S
                    Where dateDebutSortie between %L AND %L
                    Order by dateDebutSortie
            )
            , cteSortiesActivite As (
                select distinct S.*
                    from cteSorties S
                    Inner Join Concerner C On C.idSortie = S.idSortie
                    Inner join cteSelActi A On A.idActivite = C.idActivite
                    Order by dateDebutSortie
            )
            , cteSortiesActiviteFaites As (
                select S.*
                    from cteSortiesActivite S
                    Where not lSortieAnnulée
                    Order by dateDebutSortie
            )
-- select * from cteSortiesActiviteFaites
            , cteParticipations As (
                Select P.*, S.lSortieAnnulée, P.idRaisonAnnulation notnull As lParticipationAnnulée--, P.idFonction = 2 As lEncadre
                    From Participer P
                    Inner Join cteSorties S On S.idSortie = P.idSortie
                    where case when %L Then P.idFonction = 2 Else P.idFonction != 2 End
            )
-- select * from cteParticipations
            , cteParticipants As (
                Select distinct A.idAdherent, A.prenomAdherent, A.nomAdherent
                    From cteParticipations P
                    Inner Join cteSortiesActiviteFaites S On S.idSortie = P.idSortie
                    Inner Join Adherent A On A.idAdherent = P.idAdherent
                    Order by 2, 3
            )
-- Select * from cteParticipants
            , cteNbInscriptionsTotales As (
                select  A.idAdherent, Count(*) nbInscriptionsTotales
                    from cteParticipants A
                    Inner Join cteParticipations P On P.idAdherent = A.idAdherent
                    Inner Join cteSortiesActivite S On S.idSortie = P.idSortie
                    Group by 1
            )
            , cteNbInscriptionsAnnulées As (
                select  A.idAdherent, Count(*) nbInscriptionsAnnulées
                    from cteParticipants A
                    Inner Join cteParticipations P On P.idAdherent = A.idAdherent
                    Inner Join cteSortiesActivite S On S.idSortie = P.idSortie
                    Where lParticipationAnnulée
                    Group by 1
            )
            , cteNbInscriptionsSortiesAnnulées As (
                select  A.idAdherent, Count(*) nbInscriptionsSortiesAnnulées
                    from cteParticipants A
                    Inner Join cteParticipations P On P.idAdherent = A.idAdherent
                    Inner Join cteSortiesActivite S On S.idSortie = P.idSortie
                    Where S.lSortieAnnulée And Not lParticipationAnnulée
                    Group by 1
            )
            , cteNbParticipations As (
                select A.idAdherent
                        , Count(*) nbParticipations
                        , string_agg(To_Char(S.dateDebutSortie, 'DD/MM') || ' ' || Coalesce(S.lieuSortie, 'non précisé'), ', ' order by S.dateDebutSortie) lstSorties
                        , sum(coalesce(S.dateFinSortie, s.dateDebutSortie) - s.dateDebutSortie + 1) nbJoursParticipations
                    from cteParticipants A
                    Inner Join cteParticipations P On P.idAdherent = A.idAdherent
                    Inner Join cteSortiesActivite S On S.idSortie = P.idSortie
                    Where Not S.lSortieAnnulée And Not lParticipationAnnulée
                    Group by 1
            )
			select  /*P.idAdherent, */P.prenomAdherent, P.nomAdherent, A.nomActivite
					, Coalesce(PT.nbInscriptionsTotales, 0) nbInscriptionsTotales, Coalesce(PAn.nbInscriptionsAnnulées, 0) nbInscriptionsAnnulées
					, Coalesce(PSAn.nbInscriptionsSortiesAnnulées, 0) nbInscriptionsSortiesAnnulées
					, Coalesce(PAr.nbParticipations, 0) nbParticipations, Coalesce(PAr.nbJoursParticipations, 0) nbJoursParticipations, PAr.lstSorties
                from cteParticipants P
                Cross Join cteNomActi A
                Left Join cteNbInscriptionsTotales PT On PT.idAdherent = P.idAdherent
                Left Join cteNbInscriptionsAnnulées PAn On PAn.idAdherent = P.idAdherent
                Left Join cteNbInscriptionsSortiesAnnulées PSAn On PSAn.idAdherent = P.idAdherent
                Left Join cteNbParticipations PAr On PAr.idAdherent = P.idAdherent
                Order by nbJoursParticipations Desc, nbParticipations Desc, 2, 3 ;
	
	$SQL$ ;
	lsSQL = format(lsSQL, piIdActivite, pdDebut, pdFin, pbEncadre) ;
	
    return query execute lsSQL ;


end ; $Statistiques$ language plpgsql ;

select * from StatistiquesParAdherent(to_date('2021/09/01', 'YYYY/MM/DD'), to_date('2022/08/31', 'YYYY/MM/DD'), 1, true) ;


--------------------------------------------------------------------------------------------------------------------
Create Or Replace function StatistiquesParSortie(pdDebut date, pdFin Date, piIdActivite Integer Default Null) 
    returns Table(nomActivite varChar, dateDebutSortie date, nbJoursSortie Integer, Encadrant text, nomSortie varChar, Annulee varChar, lieuSortie varChar, NbParticipants bigInt, participants text) As $Statistiques$
Declare
	lsSQL Text ;
Begin
    
    -- Nb d'adhérents par sortie
	lsSQL = $SQL$
			Select A.nomActivite
					, S.dateDebutSortie
                    , (coalesce(S.dateFinSortie, dateDebutSortie) - S.dateDebutSortie + 1) As nbJoursSortie
					, P_Encadr.Encadrant
					, S.nomSortie
					, case when S.idRaisonAnnulation notNull then 'Annulée'::varChar Else ''::varChar End As Annulée
					, S.LieuSortie
					, P_Particip.NbParticipants, P_Particip.Participants
				from sortie S
				Inner Join Concerner C On C.idSortie = S.idSortie
				Inner join Activite A On A.idActivite = C.idActivite
                -- Endadrants
				Left Join (
					Select idSortie, string_Agg(prenomAdherent, ',') Encadrant
						From Participer P Inner Join Adherent A On A.idAdherent = P.idAdherent
						Where idFonction = 2
						Group by idSortie
					) P_Encadr ON P_Encadr.idSortie = S.idSortie
                -- Participants
				Left Join (
					Select idSortie, Count(*) NbParticipants, string_Agg(prenomAdherent || ' ' || nomAdherent, ',' Order by prenomAdherent || ' ' || nomAdherent) Participants
						From Participer P Inner Join Adherent A On A.idAdherent = P.idAdherent
						Where idFonction != 2
						Group by idSortie
					) P_Particip ON P_Particip.idSortie = S.idSortie
				where dateDebutSortie between %L AND %L
				    And A.idActivite = Coalesce(%L, A.idActivite)
				Order by A.nomActivite, S.dateDebutSortie
		$SQL$ ;
	lsSQL = format(lsSQL, pdDebut, pdFin, piIdActivite) ;
	Execute lsSQL ;
	
    return query execute lsSQL ;

end ; $Statistiques$ language plpgsql ;

select * from StatistiquesParSortie(to_date('2021/09/01', 'YYYY/MM/DD'), to_date('2022/08/31', 'YYYY/MM/DD'), 1) ;

--------------------------------------------------------------------------------------------------------------------
Create Or Replace function StatistiquesCSV(pdDebut date, pdFin Date, piIdActivite Integer Default Null, psRepDest Text default null) 
    returns Table (description Text, fichierCSV Text) As $Statistiques$
Declare
    lsFich Text ;
	lsSQL Text ;
    lcActivites cursor for select * from Activite A Where A.idActivite = Coalesce(piIdActivite, A.idActivite) ;
    loActivite record ;
Begin
    psRepDest = coalesce(psRepDest, '/home/luc/partage_win10/Poubelle') ;
    if not right(psRepDest, 1) = '/' then
        psRepDest = psRepDest || '/' ;
    end if ;
    
    -- Nb d'adhérents par sortie, activité par activité
    for loActivite in lcActivites Loop
        lsFich = format('%sNbAdherentsParSortie_%s.csv', psRepDest, loActivite.nomActivite) ;
        lsSQL = $SQL$
            Copy (
                select * from StatistiquesParSortie(%L, %L, %L)
                ) to %L with CSV DELIMITER ';' HEADER ;
            $SQL$ ;
        lsSQL = format(lsSQL, pdDebut, pdFin, loActivite.idActivite, lsFich) ;
        Execute lsSQL ;
        Return query select 'Nombre d''adhérents par sortie ' || loActivite.nomActivite, lsFich ;
	end loop ;

	-- Statistiques des encadrements
    lsFich = format('%sNbSortiesParEncadrant.csv', psRepDest) ;
	lsSQL = $SQL$
        Copy (
        	select prenomAdherent Encadrant, nbInscriptionsTotales nbSortiesPrévues, nbInscriptionsSortiesAnnulées nbSortiesAnnulees, nbParticipations nbSortiesFaites, nbJoursParticipations, lstSorties
                from StatistiquesParAdherent(%L, %L, NULL, true)
                Order by 1
                ) to %L with CSV DELIMITER ';' HEADER ;
            $SQL$ ;
	lsSQL = format(lsSQL, pdDebut, pdFin, psRepDest || 'NbSortiesParEncadrant.csv') ;
	Execute lsSQL ;
    Return query select 'Statistiques des encadrements', lsFich ;

	-- Statistiques des participations, activité par activité
    for loActivite in lcActivites Loop
        lsFich = format('%sNbParticipationsParAdherent_%s.csv', psRepDest, loActivite.nomActivite) ;
        lsSQL = $SQL$
            Copy (
                select * from StatistiquesParAdherent(%L, %L, %L, false)
                    ) to %L with CSV DELIMITER ';' HEADER ;
                $SQL$ ;
        lsSQL = format(lsSQL, pdDebut, pdFin, loActivite.idActivite, lsFich) ;
        Execute lsSQL ;
        Return query select 'Statistiques des participations ' || loActivite.nomActivite, lsFich ;
	end loop ;

end ; $Statistiques$ language plpgsql ;

Select * From StatistiquesCSV(to_date('2021/09/01', 'YYYY/MM/DD'), to_date('2022/08/31', 'YYYY/MM/DD'), null, '/home/luc/partage_win10/Poubelle') ;