-- select *  from adherent where nomAdherent = 'SIMONT'
-- delete from adherent where idAdherent = 218
-- select nomadherent, prenomadherent, datenaissanceadherent, count(*) from adherent Group by nomadherent, prenomadherent, datenaissanceadherent Having count(*) > 1
-- delete from activite Where nomActivite = 'ActivitéDeTest'
-- delete from sortie Where nomSortie = 'consectetuer mauris'
-- delete from concerner where idSortie in (select idSortie From sortie Where nomSortie In ('Cras eu', 'nisl elementum', 'Duis mi', 'consectetuer mauris'))
-- delete from participer where idSortie in (select idSortie From sortie Where nomSortie In ('Cras eu', 'nisl elementum', 'Duis mi', 'consectetuer mauris'))
-- delete from sortie where idSortie in (select idSortie From sortie Where nomSortie In ('Cras eu', 'nisl elementum', 'Duis mi', 'consectetuer mauris'))
-- Select nomSortie, Count(*) from sortie Group by  nomSortie Having Count(*) > 1

-- Correctif à la structure de la BDD, si on ne veut pas recréer l'ensemble
-- Les correctrifs définis ici doivent aussi être faits dans "structure.sql*
ALTER TABLE public.tags
    RENAME TO tag;

ALTER TABLE public.typelicense
    RENAME nomlicense TO "nomTypeLicense";

ALTER TABLE public.affilier
    RENAME typelicense TO "idTypeLicense";

ALTER TABLE public.typelicense
    RENAME idlicense TO "idTypeLicense";

ALTER TABLE public.sortie
    RENAME nombreparticipantssortie TO "nombreParticipantsMaxSortie";

 ALTER TABLE public.participer
     ADD COLUMN nbplacevoiture integer;

Drop Table If Exists Autreaffiliation Cascade;  
CREATE TABLE Autreaffiliation (
        idAutreaffiliation serial NOT NULL,
        nomAutreaffiliation varchar(50),
        CONSTRAINT  Autreaffiliation_pkey PRIMARY KEY (idAutreaffiliation)       
);
INSERT INTO public.autreaffiliation(
	nomautreaffiliation)
	VALUES ('License découverte');

INSERT INTO public.autreaffiliation(
	nomautreaffiliation)
	VALUES ('Carte membre');

INSERT INTO public.autreaffiliation(
	nomautreaffiliation)
	VALUES ('Invité');

INSERT INTO public.autreaffiliation(
	nomautreaffiliation)
	VALUES ('membre d''un club partenaire');

ALTER TABLE public.participer
    ADD COLUMN "dva" boolean;

ALTER TABLE public.participer
    ADD COLUMN "pellesonde" boolean;

ALTER TABLE public.sortie
    ADD COLUMN inscriptionenligne boolean;

ALTER TABLE participer
    ADD COLUMN validationparticipation boolean;

ALTER TABLE Mail ADD COLUMN dateMail TIMESTAMP Default CURRENT_TIMESTAMP;
ALTER TABLE public.sortie
    ADD COLUMN heuredepart character varying(5);
ALTER TABLE Sortie
ADD UNIQUE (datedebutsortie, heuredepart, lieusortie);

ALTER TABLE public.adherent
    ALTER COLUMN telAdherent Type Varchar(20);
ALTER TABLE public.adherent
    ALTER COLUMN telContact Type Varchar(20);
ALTER TABLE public.adherent
    ADD COLUMN idautreaffiliation integer;
ALTER TABLE Adherent
    ADD UNIQUE (nomadherent, prenomadherent, datenaissanceadherent);
ALTER TABLE public.adherent
    ALTER COLUMN mailAdherent DROP NOT NULL;
ALTER TABLE public.adherent
    ALTER COLUMN telAdherent DROP NOT NULL;

ALTER TABLE public.materiel
    ALTER COLUMN tarifmateriel DROP NOT NULL;

ALTER TABLE public.sortie
    ALTER COLUMN lieuSortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN distancesortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN denivellesortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN altitudeMaxSortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN tarifSortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN nombreParticipantsMaxSortie DROP NOT NULL;
ALTER TABLE public.sortie
    ALTER COLUMN idDifficulte DROP NOT NULL;





ALTER TABLE Fonction
ADD CONSTRAINT UC_Fonction UNIQUE (nomFonction);
ALTER TABLE Activite
ADD CONSTRAINT UC_Activite UNIQUE (nomActivite);
ALTER TABLE RaisonAnnulation
ADD CONSTRAINT UC_RaisonAnnulation UNIQUE (libelleAnnulation);
ALTER TABLE Difficulte
ADD CONSTRAINT UC_Difficulte UNIQUE (nomDifficulte);
ALTER TABLE Sortie
ADD CONSTRAINT UC_Sortie UNIQUE (nomSortie);
ALTER TABLE TypeLicense
ADD CONSTRAINT UC_TypeLicense UNIQUE (nomTypeLicense);
ALTER TABLE Federation
ADD CONSTRAINT UC_Federation UNIQUE (nomFederation);
ALTER TABLE TypeCertificat
ADD CONSTRAINT UC_TypeCertificat UNIQUE (nomTypeCertificat);
ALTER TABLE Tags
ADD CONSTRAINT UC_Tag UNIQUE (nomTag);
ALTER TABLE Passeport
ADD CONSTRAINT UC_Passeport UNIQUE (nomPasseport);
ALTER TABLE Role
ADD CONSTRAINT UC_nomRole UNIQUE (nomRole);

ALTER TABLE public.etre DROP CONSTRAINT fk_etre_adherent_1;
ALTER TABLE public.etre
  ADD CONSTRAINT fk_etre_adherent_1 FOREIGN KEY (idadherent)
      REFERENCES public.adherent (idadherent) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE Cascade;

ALTER TABLE public.etre DROP CONSTRAINT fk_etre_role_1;
ALTER TABLE public.etre
  ADD CONSTRAINT fk_etre_role_1 FOREIGN KEY (idrole)
      REFERENCES public.role (idrole) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE Cascade;

Alter Table Adherent alter column mdpAdherent Drop Not Null ;
Alter table adherent alter column sexeadherent type varchar(1);
update adherent set sexeadherent = 'H' where sexeadherent = '1';
update adherent set sexeadherent = 'F' where sexeadherent = '0';
ALTER TABLE public.adherent
    ALTER COLUMN datenaissanceadherent DROP NOT NULL;


--ALTER TABLE participer ALTER COLUMN montanttotal DROP not null;
--ALTER TABLE participer ALTER COLUMN montantarrhes DROP not null;
ALTER TABLE participer ALTER COLUMN montanttotal set default 0 ;
ALTER TABLE participer ALTER COLUMN montantarrhes set default 0 ;

CREATE OR REPLACE FUNCTION DateDerniereInscriptionAdherent()  RETURNS TRIGGER AS $BODY$ 
BEGIN
        UPDATE adherent 
            SET dateDerniereInscriptionAdherent = (SELECT MAX(dateaffiliation) FROM affilier WHERE new.idAdherent= affilier.idAdherent)
			Where new.idAdherent= adherent.idAdherent;
    RETURN new;
END;
$BODY$ 
LANGUAGE plpgsql;

Drop Trigger if exists UpdateInscriptionAdherent ON affilier ;
CREATE TRIGGER UpdateInscriptionAdherent 
AFTER INSERT OR UPDATE 
ON affilier 
FOR EACH ROW 
EXECUTE PROCEDURE DateDerniereInscriptionAdherent();


CREATE OR REPLACE FUNCTION Encadrants() RETURNS trigger as $MiseajourEncadrant$ 
DECLARE
    ancienencadrant int;
BEGIN
    UPDATE Participer 
                    SET idfonction = 1
                    WHERE Participer.idadherent = OLD.idorganisateur
                                    AND Participer.idsortie = New.idSortie;
    UPDATE Participer 
            SET idfonction = 2
            WHERE Participer.idadherent = NEW.idorganisateur
                    AND Participer.idsortie = New.idSortie
                            Returning idadherent into ancienencadrant ;
    if ancienencadrant is null then
        INSERT INTO Participer (idsortie,idadherent,idfonction) values (New.idSortie, New.idorganisateur, 2);
    end if;
    RETURN New;
END;
$MiseajourEncadrant$
LANGUAGE PLPGSQL;	

Drop Trigger If Exists MiseajourEncadrant ON Sortie ;
CREATE Trigger MiseajourEncadrant
AFTER
UPDATE
ON Sortie FOR EACH ROW
execute procedure Encadrants() ;

-- Selon https://stackoverflow.com/questions/13596638/function-to-remove-accents-in-postgresql
CREATE OR REPLACE FUNCTION normalise(psText VarChar, pbTrim Boolean Default true, pbToUpper Boolean Default true) 
  RETURNS character varying AS 
$BODY$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	if pbTrim Then
		select translate(psText, ' ', '') into psText; 
	End If ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
		select translate(v_str, 'Çç', 'Cc') into v_str; 
	end if ;
return v_str; 
end;$BODY$ 
LANGUAGE 'plpgsql' VOLATILE; 

