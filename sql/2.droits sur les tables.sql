-- Obsolète : plutôt créer les tables avec le bon rôle avec la commande set role de 1.structure.sql
-- Attribuer les droits sur les tables à l'user de la BDD
-- ToDo : changer le nom de l'user selon celui qu'on a défini
Obsolète : plutôt créer les tables avec le bon rôle avec la commande set role de 1.structure.sql

GRANT ALL ON TABLE public.activite TO "OLITS";
GRANT ALL ON TABLE public.adherent TO "OLITS";
GRANT ALL ON TABLE public.affilier TO "OLITS";
GRANT ALL ON TABLE public.autreaffiliation TO "OLITS";
GRANT ALL ON TABLE public.avoir TO "OLITS";
GRANT ALL ON TABLE public.concerner TO "OLITS";
GRANT ALL ON TABLE public.difficulte TO "OLITS";
GRANT ALL ON TABLE public.etre TO "OLITS";
GRANT ALL ON TABLE public.federation TO "OLITS";
GRANT ALL ON TABLE public.fonction TO "OLITS";
GRANT ALL ON TABLE public.louer TO "OLITS";
GRANT ALL ON TABLE public.mail TO "OLITS";
GRANT ALL ON TABLE public.materiel TO "OLITS";
GRANT ALL ON TABLE public.obtenir TO "OLITS";
GRANT ALL ON TABLE public.participer TO "OLITS";
GRANT ALL ON TABLE public.passeport TO "OLITS";
GRANT ALL ON TABLE public.photo TO "OLITS";
GRANT ALL ON TABLE public.raisonannulation TO "OLITS";
GRANT ALL ON TABLE public.recevoir TO "OLITS";
GRANT ALL ON TABLE public.role TO "OLITS";
GRANT ALL ON TABLE public.sortie TO "OLITS";
GRANT ALL ON TABLE public.tag TO "OLITS";
GRANT ALL ON TABLE public.typecertificat TO "OLITS";
GRANT ALL ON TABLE public.typelicense TO "OLITS";
GRANT ALL ON TABLE public.typereglement TO "OLITS";
GRANT ALL ON TABLE public.recevoir TO "OLITS";

