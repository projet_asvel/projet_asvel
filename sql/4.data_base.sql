-- Données de base : à générer sur le serveur de production

insert into activite (nomactivite)  VALUES ('Ski de randonnée');
insert into activite (nomactivite)  VALUES ('Ski de Fond');
insert into activite (nomactivite)  VALUES ('Ski Alpin');
insert into activite (nomactivite)  VALUES ('Alpinisme');
insert into activite (nomactivite)  VALUES ('Randonnée');
insert into activite (nomactivite)  VALUES ('Randonnée raquettes');
insert into activite (nomactivite)  VALUES ('Via ferrata');
insert into activite (nomactivite)  VALUES ('Canyoning');
insert into activite (nomactivite)  VALUES ('Escalade');

-- Voir les constantes TYPEREGELEMENT_ dans \config\globalConfig.php
insert into typereglement (idTypeReglement, nomtypereglement) VALUES (1, 'Espèces');
-- insert into typereglement (idTypeReglement, nomtypereglement) VALUES (2, 'Carte Bancaire');
insert into typereglement (idTypeReglement, nomtypereglement) VALUES (3, 'Chèque');
insert into typereglement (idTypeReglement, nomtypereglement) VALUES (4, 'Chèque vacances');

insert into role (nomrole) VALUES ('Administrateur');
insert into role (nomrole) VALUES ('Secrétaire');
insert into role (nomrole) VALUES ('Encadrant');
insert into role (nomrole) VALUES ('Comptable');
insert into role (nomrole) VALUES ('Adhérent');

insert into fonction (idFonction, nomfonction) VALUES (1, 'Participant');
insert into fonction (idFonction, nomfonction) VALUES (2, 'Encadrant');
insert into fonction (idFonction, nomfonction) VALUES (3, 'Co-encadrant');

insert into tag (nomtag) VALUES ('SAE');
insert into tag (nomtag) VALUES ('SAE jeune');
insert into tag (nomtag) VALUES ('Ski alpin');
insert into tag (nomtag) VALUES ('Alpi');
insert into tag (nomtag) VALUES ('Ski rando');

insert into difficulte (nomdifficulte) VALUES ('*');
insert into difficulte (nomdifficulte) VALUES ('**');
insert into difficulte (nomdifficulte) VALUES ('***');
insert into difficulte (nomdifficulte) VALUES ('****');
insert into difficulte (nomdifficulte) VALUES ('F');
insert into difficulte (nomdifficulte) VALUES ('PD');
insert into difficulte (nomdifficulte) VALUES ('AD');
insert into difficulte (nomdifficulte) VALUES ('D');

insert into typecertificat (nomtypecertificat) VALUES ('Loisir');
insert into typecertificat (nomtypecertificat) VALUES ('Compétition');

insert into raisonannulation (libelleannulation) VALUES ('Météo');
insert into raisonannulation (libelleannulation) VALUES ('Encadrant indisponible');
insert into raisonannulation (libelleannulation) VALUES ('Manque de participants');
insert into raisonannulation (libelleannulation) VALUES ('Pas de voiture');

insert into federation (nomFederation, nbJoursValiditeAffiliation) VALUES ('FFME', -1), ('FFS', -2);

insert into federation (nomFederation, nbJoursValiditeAffiliation) VALUES 
    ('License découverte journée', 1)
    , ('License découverte WE', 2)
    , ('Carte membre', -1)
    , ('Invité', 2)
    , ('Membre d un club partenaire', -2) ;

insert into TypeLicense(nomTypeLicense) VALUES('Loisir');
insert into TypeLicense(nomTypeLicense) VALUES('Compétition');	
