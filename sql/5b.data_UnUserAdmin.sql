-- Pour préparation de la base de production
-- Créer un seul user, avec droits d'admin et de secrétaire
-- login : admin@asvel.net, mdp = 123 (hashé)

Insert Into Adherent (idAdherent,nomadherent,prenomadherent,datenaissanceadherent,adresseadherent,mailadherent,teladherent,cpadherent,villeadherent,paysadherent,nationaliteadherent,mdpadherent,commentaireadherent,datederniereinscriptionadherent)
Values (-1, 'Administrateur','deBase','1901-01-01','245, cours Emile Zola','admin@asvel.net','0102030405','69100','Villeurbanne','France','Français','$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2','Administrateur pour la configuration de l''application','1901-01-01') ;

INSERT INTO etre (idAdherent,idRole) VALUES (-1,1), (-1,2) ;
