Alter Table Participer Add Column debutant Boolean Default false ;
Alter Table Participer Add Column loueMateriel Boolean Default false ;

-- Ajouter les droits "Adherent" à tous les adhérents
Insert Into Etre (idAdherent, idRole) 
Select A.idAdherent, 5
	From Adherent A
	where not A.idAdherent In (
		select A.idAdherent
			from Adherent A, etre E
			Where A.idAdherent = E.idAdherent
				AND E.idRole = 5
		) ;

-- Ajouter les droits "Adherent" lors de la création d'un adhérent
Create Or Replace function fTrigAdherentAI() returns trigger as $$
Declare
	liNb BigInt ;
Begin
	With cte AS (
		Insert Into Etre (idAdherent, idRole) 
			Select A.idAdherent, 5
				From Adherent A
				where A.idAdherent = new.idAdherent
					And not A.idAdherent In (
						select A.idAdherent
							from etre E
							Where E.idAdherent = new.idAdherent
								AND E.idRole = 5
						)
				returning idAdherent
				)
			Select Count(*) Into liNb From cte ;
		return new ;
End ; $$ Language plpgSQL ;
Drop Trigger If exists trigAdherentAI On Adherent ;
Create Trigger trigAdherentAI
	After Insert On Adherent
	for each row
	Execute Procedure fTrigAdherentAI() ;