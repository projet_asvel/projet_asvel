-- ******************************************************************************************************************************
-- Selon https://stackoverflow.com/questions/13596638/function-to-remove-accents-in-postgresql
CREATE OR REPLACE FUNCTION normalise(psText VarChar, pbTrim Boolean Default true, pbToUpper Boolean Default true) 
  RETURNS character varying AS 
$BODY$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
	select translate(v_str, '-', '') into v_str; 		-- LG 20220730 : tiret
	select translate(v_str, '''"', '  ') into v_str; 	-- LG 20220730 : quotes et double-quotes
    select translate(v_str, 'Ç', 'C') into v_str;  	    -- LG 20220730
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
-- LG 20220730 old		select translate(v_str, 'Çç', 'Cc') into v_str; 
		select translate(v_str, 'ç', 'c') into v_str; 
	end if ;
	if pbTrim Then
		select translate(v_str, ' ', '') into v_str; 
	End If ;
return v_str; 
end;$BODY$ 
LANGUAGE 'plpgsql' VOLATILE; 