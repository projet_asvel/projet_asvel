-- Exécuter une fois cette commande, si pas connecté en tant que le bon utilisateur
set role "CHANGER POUR INDIQUER LE NOM DU ROLE POSTGRES QUE VOUS SOHAITEZ RENDRE PROPRIETAIRE DE CES OBJETS" ;

Drop Table If Exists Autreaffiliation Cascade;  
CREATE TABLE Autreaffiliation (
        idAutreaffiliation serial NOT NULL,
        nomAutreaffiliation varchar(50),
        CONSTRAINT  Autreaffiliation_pkey PRIMARY KEY (idAutreaffiliation)       
);

Drop Table If Exists Activite Cascade ;
CREATE TABLE Activite (
	idActivite serial NOT NULL,
	nomActivite varchar(50) ,
	CONSTRAINT Activite_pkey PRIMARY KEY (idActivite), 
    CONSTRAINT UC_Activite UNIQUE (nomActivite)
);

Drop Table If Exists Adherent Cascade ;
CREATE TABLE Adherent (
	idAdherent serial NOT NULL,
	nomAdherent varchar(50) NOT NULL,
	prenomAdherent varchar(50) NOT NULL,
	dateNaissanceAdherent date,
	licenseAdherent varchar(50),
	adresseAdherent varchar(50),
	adresse2Adherent varchar(50),
	mailAdherent varchar(50) /*NOT*/ NULL,
	telAdherent varchar(20) /*NOT*/ NULL,
	CPAdherent varchar(5),
	villeAdherent varchar(50),
	paysAdherent varchar(50),
	nationaliteAdherent varchar(50),
	sexeAdherent varchar(1),
	mdpAdherent varchar(255) Null,
	droitImageAdherent bool,
	refusMailAdherent bool,
	commentaireAdherent text,
	prenomContact varchar(50),
	nomContact varchar(50),
	adresseContact varchar(50),
	adresse2Contact varchar(50),
	villeContact varchar(50),
	CPContact varchar(5),
	mailContact varchar(50),
	telContact varchar(20),
	idTypeCertif int,
	certifNomMedecin varchar(50),
	certifAlpi bool,
	SAEAdherent bool,
	dateDerniereInscriptionAdherent date,
        idAutreaffiliation int,
        CONSTRAINT Adherent_Uniq_NomPrenomNaissance UNIQUE (nomadherent, prenomadherent, datenaissanceadherent),
        CONSTRAINT Adherent_Uniq_Mail UNIQUE (mailAdherent),
	CONSTRAINT Adherent_pkey PRIMARY KEY (idAdherent) 
);
-- LG 20210117 début
-- Ajouter les droits "Adherent" lors de la création d'un adhérent
Create Or Replace function fTrigAdherentAI() returns trigger as $$
Declare
	liNb BigInt ;
Begin
	With cte AS (
		Insert Into Etre (idAdherent, idRole) 
			Select A.idAdherent, 5
				From Adherent A
				where A.idAdherent = new.idAdherent
					And not A.idAdherent In (
						select A.idAdherent
							from etre E
							Where E.idAdherent = new.idAdherent
								AND E.idRole = 5
						)
				returning idAdherent
				)
			Select Count(*) Into liNb From cte ;
		return new ;
End ; $$ Language plpgSQL ;
Drop Trigger If exists trigAdherentAI On Adherent ;
Create Trigger trigAdherentAI
	After Insert On Adherent
	for each row
	Execute Procedure fTrigAdherentAI() ;

Drop Table If Exists Affilier Cascade ;
CREATE TABLE Affilier (
        idaffiliation serial NOT NULL,
	idAdherent int NOT NULL,
	idFederation int NOT NULL,
	idTypeLicense int NULL,
	idTypeReglementAdherent int,
	montantReglementAdherent float(24),
        dateAffiliation date,
        dateFinAffiliation Date Null Default Null ,
	numeroLicense varchar(50),	-- NULL
	CONSTRAINT Affilier_pkey PRIMARY KEY (idAffiliation) 
	CONSTRAINT Affilier_Unique_ad_Fed_Date UNIQUE (idAdherent, idFederation, dateAffiliation) 
);
-- LG 20210117 fin

Drop Table If Exists Avoir Cascade ;
CREATE TABLE Avoir (
	idAdherent int NOT NULL,
	idTag int NOT NULL,
	CONSTRAINT Avoir_pkey PRIMARY KEY (idAdherent, idTag) 
);

Drop Table If Exists Concerner Cascade ;
CREATE TABLE Concerner (
	idSortie int NOT NULL,
	idActivite int NOT NULL,
	CONSTRAINT Concerner_pkey PRIMARY KEY (idSortie, idActivite) 
);

Drop Table If Exists Difficulte Cascade ;
CREATE TABLE Difficulte (
	idDifficulte serial NOT NULL,
	nomDifficulte varchar(50) NOT NULL,
	CONSTRAINT Difficulte_pkey PRIMARY KEY (idDifficulte) ,
    CONSTRAINT UC_Difficulte UNIQUE (nomDifficulte)
);

Drop Table If Exists Etre Cascade ;
CREATE TABLE Etre (
	idAdherent int NOT NULL,
	idRole int NOT NULL,
	CONSTRAINT Etre_pkey PRIMARY KEY (idAdherent, idRole) 
);

Drop Table If Exists Federation Cascade ;
CREATE TABLE Federation (
	idFederation serial NOT NULL,
	nomFederation varchar(50) NOT NULL,
        nbJoursValiditeAffiliation Integer Default -1,
	CONSTRAINT Federation_pkey PRIMARY KEY (idFederation),
    CONSTRAINT UC_Federation UNIQUE (nomFederation)
);

Drop Table If Exists Fonction Cascade ;
CREATE TABLE Fonction (
	idFonction serial NOT NULL,
	nomFonction varchar(50) NOT NULL,
	CONSTRAINT Fonction_pkey PRIMARY KEY (idFonction),
    CONSTRAINT UC_Fonction UNIQUE (nomFonction) 
);

Drop Table If Exists Mail Cascade ;
CREATE TABLE Mail (
	idMail serial NOT NULL,
	datemail timestamp NOT NULL Default Current_Timestamp,
	sujetMail text NOT NULL,
	contenuMail text NOT NULL,
	idAdherentExpediteur int NOT NULL,
    destinatairesVisibles Boolean Default false,
    mailExpediteur varchar(50),
	CONSTRAINT Mail_pkey PRIMARY KEY (idMail) 
);

Drop Table If Exists Obtenir Cascade ;
CREATE TABLE Obtenir (
	idAdherent int NOT NULL,
	idPasseport int NOT NULL,
	CONSTRAINT Obtenir_pkey PRIMARY KEY (idAdherent, idPasseport) 
);

Drop Table If Exists Participer Cascade ;
CREATE TABLE Participer (
    idAdherent int NOT NULL,
    idSortie int NOT NULL,
    dateInscription timestamp DEFAULT CURRENT_TIMESTAMP,
    montantArrhes float(24) NOT NULL Default 0,
    montantTotal float(24) NOT NULL Default 0,
    commentaireParticipation text,
    idFonction int NOT NULL,
    idInscripteur int,
    idRaisonAnnulation int,
    idTypeReglementArrhes int,
    idTypeReglementMontantTotal int,
    arrhesRemboursees Boolean ;
    validationParticipation BOOLEAN,
    nbPlaceVoiture int,
    excluDuCovoiturage BOOLEAN,     -- "Ne participe pas au covoiturage" permettant de ne pas compter certains participants dans le calcul des frais de route (venant pas ses propres moyens ou voiture de fonction par exemple
    dva BOOLEAN Default false,
    pellesonde BOOLEAN Default false,
    debutant Boolean Default false,
    loueMateriel Boolean Default false,
    CONSTRAINT Participer_pkey PRIMARY KEY (idAdherent, idSortie) 
);

Drop Table If Exists Passeport Cascade ;
CREATE TABLE Passeport (
	idPasseport serial NOT NULL,
	nomPasseport varchar(50) NOT NULL,
	CONSTRAINT Passeport_pkey PRIMARY KEY (idPasseport),
    CONSTRAINT UC_Passeport UNIQUE (nomPasseport)
);

Drop Table If Exists RaisonAnnulation Cascade ;
CREATE TABLE RaisonAnnulation (
	idAnnulation serial NOT NULL,
	libelleAnnulation varchar(100) NOT NULL,
	CONSTRAINT RaisonAnnulation_pkey PRIMARY KEY (idAnnulation),
    CONSTRAINT UC_RaisonAnnulation UNIQUE (libelleAnnulation) 
);

Drop Table If Exists Recevoir Cascade ;
CREATE TABLE Recevoir (
	idAdherent int NOT NULL,
	idMail int NOT NULL,
	CONSTRAINT Recevoir_pkey PRIMARY KEY (idAdherent, idMail) 
);

Drop Table If Exists Role Cascade ;
CREATE TABLE Role (
	idRole serial NOT NULL,
	nomRole varchar(50) NOT NULL,
	CONSTRAINT Role_pkey PRIMARY KEY (idRole),
    CONSTRAINT UC_nomRole UNIQUE (nomRole)
);

Drop Table If Exists Sortie Cascade ;
CREATE TABLE Sortie (
	idSortie serial NOT NULL,
	nomSortie varchar(50) NOT NULL,
	lieuSortie varchar(100), --NULL
	dateDebutSortie date,
	dateFinSortie date,
	distanceSortie float(24),	-- NULL
	denivelleSortie float(24),			-- NULL
	altitudeMaxSortie float(24),		-- NULL
    heuredepart varchar(5),
	commentairesSortie text,
	tarifSortie float(24),		-- NULL
	coutAutoroute float(20),
	nombreParticipantsMaxSortie int,	-- NULL
 	nbVoituresSortie int,
	idDifficulte int,			-- NULL
	idOrganisateur int NOT NULL,
	idRaisonAnnulation int,
    inscriptionenligne boolean,
    fraisAnnexe float(20), 
    fraisAnnexeDetails Text,
	CONSTRAINT Sortie_pkey PRIMARY KEY (idSortie), 
    CONSTRAINT UC_Sortie UNIQUE (nomSortie)
);

Drop Table If Exists Tags Cascade ;
Drop Table If Exists Tag Cascade ;
CREATE TABLE Tag (
	idTag serial NOT NULL,
	nomTag varchar(50) NOT NULL,
	CONSTRAINT Tag_pkey PRIMARY KEY (idTag),
    CONSTRAINT UC_Tag UNIQUE (nomTag)
);

Drop Table If Exists TypeReglement Cascade ;
CREATE TABLE TypeReglement (
	idTypeReglement serial NOT NULL,
	nomTypeReglement varchar(50) NOT NULL,
	CONSTRAINT TypeReglement_pkey PRIMARY KEY (idTypeReglement) 
);

Drop Table If Exists TypeCertificat Cascade ;
CREATE TABLE TypeCertificat (
	idTypeCertificat serial NOT NULL,
	nomTypeCertificat varchar(50) NOT NULL,
	PRIMARY KEY (idTypeCertificat),
        CONSTRAINT UC_TypeCertificat UNIQUE (nomTypeCertificat)
);

Drop Table If Exists TypeLicense Cascade ;
CREATE TABLE TypeLicense (
	idLicense serial NOT NULL,
	nomTypeLicense varchar(50) NOT NULL,
	PRIMARY KEY (idLicense) 
);

ALTER TABLE Affilier ADD CONSTRAINT fk_Affilier_TypeReglement_1 FOREIGN KEY (idTypeReglementAdherent) REFERENCES TypeReglement (idTypeReglement) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Affilier ADD CONSTRAINT fk_Affilier_License_1 FOREIGN KEY (idTypeLicense) REFERENCES TypeLicense (idLicense);
ALTER TABLE Affilier ADD CONSTRAINT fk_Affilier_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Affilier ADD CONSTRAINT fk_Affilier_Federation_1 FOREIGN KEY (idFederation) REFERENCES Federation (idFederation) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Avoir ADD CONSTRAINT fk_Avoir_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Avoir ADD CONSTRAINT fk_Avoir_Tag_1 FOREIGN KEY (idTag) REFERENCES Tag (idTag) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Concerner ADD CONSTRAINT fk_Concerner_Activite_1 FOREIGN KEY (idActivite) REFERENCES Activite (idActivite) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE Concerner ADD CONSTRAINT fk_Concerner_Sortie_1 FOREIGN KEY (idSortie) REFERENCES Sortie (idSortie) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE Etre ADD CONSTRAINT fk_Etre_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Etre ADD CONSTRAINT fk_Etre_Role_1 FOREIGN KEY (idRole) REFERENCES Role (idRole) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Obtenir ADD CONSTRAINT fk_Obtenir_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Obtenir ADD CONSTRAINT fk_Obtenir_Passeport_1 FOREIGN KEY (idPasseport) REFERENCES Passeport (idPasseport) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE RESTRICT ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_Fonction_1 FOREIGN KEY (idFonction) REFERENCES Fonction (idFonction) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_Inscripteur FOREIGN KEY (idInscripteur) REFERENCES Adherent (idAdherent) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_Sortie_1 FOREIGN KEY (idSortie) REFERENCES Sortie (idSortie) ON DELETE RESTRICT ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_TypeReglement_1 FOREIGN KEY (idTypeReglementArrhes) REFERENCES TypeReglement (idTypeReglement) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Participer ADD CONSTRAINT fk_Participer_TypeReglement_2 FOREIGN KEY (idTypeReglementMontantTotal) REFERENCES TypeReglement (idTypeReglement) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Recevoir ADD CONSTRAINT fk_Recevoir_Adherent_1 FOREIGN KEY (idAdherent) REFERENCES Adherent (idAdherent) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Recevoir ADD CONSTRAINT fk_Recevoir_Mail_1 FOREIGN KEY (idMail) REFERENCES Mail (idMail) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE Sortie ADD CONSTRAINT fk_Sortie_Difficulte_1 FOREIGN KEY (idDifficulte) REFERENCES Difficulte (idDifficulte) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Sortie ADD CONSTRAINT fk_Sortie_RaisonAnnulation_1 FOREIGN KEY (idRaisonAnnulation) REFERENCES RaisonAnnulation (idAnnulation) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Adherent ADD FOREIGN KEY (idTypeCertif) REFERENCES TypeCertificat (idTypeCertificat) ON DELETE SET NULL ON UPDATE NO ACTION;
ALTER TABLE Adherent ADD FOREIGN KEY (idAutreaffiliation) REFERENCES Autreaffiliation (idAutreaffiliation) ON DELETE SET NULL ON UPDATE NO ACTION;


