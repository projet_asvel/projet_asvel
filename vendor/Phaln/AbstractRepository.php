<?php

namespace Phaln;

use Phaln\BDD;
use Phaln\Exceptions\RepositoryException;
use \App\Lib\Functions;

/**
 * Description of AbstractRepository
 *
 * @author phaln
 */
abstract class AbstractRepository {

	protected $db = NULL;			// la base de donnée (PDO ou mysqli)
	protected $table = '';			// le nom de la table manipulée
	protected $classMapped = '';	// le nom de la classe mappée
	protected $idFieldName = 'id';  // le nom du champ clé primaire. id par défaut.
	protected $notFieldProps = [];	// tableau des propriétés qui ne sont pas des champs de la table sous-jacente
	protected $mandatoryProps = [];	// tableau des propriétés indispensables (qui ne doivent pas être NULL)
	protected $checkableProps = [];	// tableau des vérifications à faire sur les propriétées (ex. ["field" => "dateNaissanceAdherent", "check" => "date"])
	protected $uniqueProps = [];	// Tableau des combinaisons de champs qui doivent être uniques (ex. [["nomActivite"]], [["nomAdherent", "prenomAdherent", "dateNaissanceAdherent"], ["mailAdherent"]])
	
	/**
	 * Constructeur.
	 * Récupère la bdd.
	 * @throws RepositoryException
	 */
	public function __construct() {
		$this->db = BDD::getConnexion();
		if (!$this->db)
			throw new RepositoryException('Pb db dans AbstractRepository::_construct()');
	}
	
	/**
	 * Récupère tous les enregistrements de la table.
	 * @return \Phaln\AbstractEntity un tableau d'objet de classe classMapped dérivant AbstractEntity
	 * @throws RepositoryException
	 */
	public function getAll() {
		$query = 'SELECT * FROM ' . $this->table;
dump_var($query, DUMP, 'Requête SQL:');
		$reqPrep = $this->db->prepare($query);
		$resultSet = $this->getAllByReqPrep($reqPrep);
		return $resultSet;
	}
	
	/**
	 * Récupère tous les enregistrements de la table.
	 * @param type $reqPrep une requête préparée (mais pas encore exécutée)
	 * @return \Phaln\AbstractEntity un tableau d'objet de classe classMapped dérivant AbstractEntity
	 * @throws RepositoryException
	 */
	public function getAllByReqPrep($reqPrep) {
		$resultSet = [];
		$rqtResult = $reqPrep->execute();
dump_var($rqtResult, DUMP, 'AbstractRepository, getAllByReqPrep');
		if ($rqtResult) {
			while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
				$resultSet[] = $this->getEntityByRow($row);
			}
		} else {
dump_var($this->db->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getAll(): ' . $reqPrep->errorInfo()[2]);
		}
		return $resultSet;
	}

	/**
	 * Récupère l'enregistrement n° $id dans la table
	 * @param type $id la valeur de la pk à récupérer
	 * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
	 */
	public function getEntityId($id) {
		// Fonction conservée pour compatibilité
		return $this->getEntityById($id);
	}
	public function getEntityById($id) {
		if (is_array($this->idFieldName)) {
			// On a une clé primaire multiple
			$where = "" ;
			foreach($this->idFieldName as $keyField) {
				if ($where) $where .= " And " ;
				$where .= $keyField . ' = :' . $keyField ;
			}
			$query = 'SELECT * FROM ' . $this->table .
					' WHERE ' . $where ;
			dump_var($query, DUMP, 'Requête SQL:');
			$reqPrep = $this->db->prepare($query);
			
			foreach($this->idFieldName as $keyField) {
				$reqPrep->bindValue(':' . $keyField, $id[$keyField]);
			}

			return $this->getEntityByReqPrep($reqPrep) ;
			
		}
		
		$query = 'SELECT * FROM ' . $this->table .
				' WHERE ' . $this->idFieldName . ' = :idFieldName';
		dump_var($query, DUMP, 'Requête SQL:');

		$reqPrep = $this->db->prepare($query);
		$reqPrep->bindValue(':idFieldName', $id);
		
		return $this->getEntityByReqPrep($reqPrep) ;
	}
	
	/**
	 * Récupère l'enregistrement en utilisant le résultat d'une clause Where SQL
	 * @param type $Where : la clause where
	 * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
	 */
	protected function getEntityByWhere($psWhere) {
		$query = 'SELECT * FROM ' . $this->table . " WHERE " . $psWhere;
dump_var($query, DUMP, 'Requête SQL:');
		$reqPrep = $this->db->prepare($query);
		$resultSet = $this->getAllByReqPrep($reqPrep);
		if (count($resultSet) <= 0) {
			// Aucune entité trouvée
			return null ;
		} else if (count($resultSet) > 1) {
			// Plus d'une entité trouvée
			throw new Exception("Plusieurs entités correspondent à ce filtre") ;
		} else {
			// Une entité trouvée et une seule
			return $resultSet[0];
		}
	}
	
	/**
	 * Récupère l'enregistrement en utilisant le résultat d'une requête préparée
	 * @param type $reqPrep une requête préparée (mais pas encore exécutée)
	 * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
	 */
	public function getEntityByReqPrep($reqPrep) {
// dump_var($reqPrep, DUMP, "Requête préparée") ;
// var_dump($reqPrep) ;
		$rqtResult = $reqPrep->execute();
		dump_var($rqtResult, DUMP, 'AbstractRepository, getEntityId');
		$entity = NULL;
		if ($rqtResult) {
			while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
				$entity = $this->getEntityByRow($row) ;
				break;
			}
		} else {
			dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getEntityId(): ' . $reqPrep->errorInfo()[2]);
		}
		return $entity;
	}
	/**
	 * Récupère l'enregistrement en utilisant la ligne de résultat d'une requête préparée
	 * @param type $reqPrep une requête préparée (mais pas encore exécutée)
	 * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
	 */
	public function getEntityByRow($row) {
		// Générer l'entité demandée
		$entity = new $this->classMapped($row);
		// Renvoyer l'entité 
		return $entity;
	}

	/**
	 * Efface l'enregistrement n° $id dans la table
	 * @param type $id la valeur de la pk à récupérer
	 * @return True si ok, false sinon
	 */
	public function deleteEntityId($id) {
		$resultset = FALSE;
		$laKeyFields = $this->getTabKeyFields()	;
		$query = "DELETE FROM " . $this->table ;
		$where = "" ;
		$laPropsTest = [] ;
		if (is_array($this->idFieldName)) {
			foreach($laKeyFields as $keyField) {
				if ($where) $where .= " And " ;
				$getter = "get" . $keyField ;
				$where .= $keyField . ' = :' . $keyField ;
				$laPropsTest[$keyField] = $id[$keyField] ;
			}
		} else {
			$where .= $this->idFieldName . ' = :' . $this->idFieldName ;
			$laPropsTest[$this->idFieldName] = $id ;
		}
		$query .= ' WHERE ' . $where ;
		$reqPrep = $this->db->prepare($query);
		$rqtResult = $reqPrep->execute($laPropsTest);
dump_var($rqtResult, DUMP, 'AbstractRepository, getEntityId');

		if ($rqtResult) {
			$resultset = true;
		} else {
dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::deleteEntityId(): ' . $reqPrep->errorInfo()[2]);
		}
		return $resultset;
	}

	// Déterminer sir le champ fourni en paramètre fait partie de la clé de la table sous-jacente
	private function isKeyField($field) {
		$laKeyFields = $this->getTabKeyFields() ;		
		foreach($laKeyFields as $keyField) {
			if (strtolower($keyField) == strtolower($field)) {
				return true ;
			}
		}
		return false ;
	}
	// Renvoyer un tablau des champs clés de la table sous-jacente
	private function getTabKeyFields() {
		if (is_string($this->idFieldName)) {
			$laKeyFields[] = $this->idFieldName ;
		} else if (is_array($this->idFieldName)) {
			$laKeyFields = $this->idFieldName ;
		} else {
			throw new \Exception("La propriété idFieldName doit être soit un string soit un tableau de strings");
		}
		return $laKeyFields ;
	}
	
	/**
	 * Vérifier la validité d'une entité et la sauvegarder en BDD
	 * @param Adherent $entity	: l'entité à vérifier
	 * @param type $tabErr		: pour retour d'un tableau de textes d'érreurs éventuelles
	 * @return boolean			: true si la vérification est OK, false s'il y a un problème
	 */
	public function vérifierEtSauver(\Phaln\AbstractEntity $entity, &$tabErr) {
		$lbKO = false ;
		if($entity == NULL) {
			$tabErr[] = "L'entité fournie est NULL" ;
			$lbKO = true ;
			
		} else if (!$this->vérifier($entity, $tabErr)) {
			// Echec de la vérification
			$lbKO = true ;
			
		} else {
			// Succès de la vérification : enregistrer
			$entitySauvée = !$this->sauver($entity) ;
			if (!$entitySauvée) {
				// Echec de l'enregistrement
				$tabErr[] = "Echec de l'enregistrement" ;
				$entitySauvée = NULL ;
				$lbKO = true ;
			}
		}

		// Valeur de retour
		return !$lbKO ;
	}

	/**
	 * Vérifier la validité d'une entité
	 * @param Adherent $entity	: l'entité à vérifier
	 * @param type $tabErr		: pour retour d'un tableau de textes d'érreurs éventuelles
         * @param boolean               : true pour ne pas vérifier les contraintes d'unicité
	 * @return boolean		: true si la vérification est OK, false s'il y a un problème
	 */
	public function vérifier(\Phaln\AbstractEntity $entity, &$tabErr, $pbSansVérifierUnicité = false) {
		$tabErr = [];
                
		// Vérifier la présence d'information dans les champs obligatoires
		foreach ($this->mandatoryProps as $mandatoryProp) {
			$getter = "get" . $mandatoryProp;
			if (!$entity->$getter()) {
				// Propriété non renseignée
				$tabErr[] = "Le champ " . $mandatoryProp . " n'est pas renseigné.";
			}
		}

		// Vérifier la validité des informations dans les champs pour lesquels on a fourni la contrainte		
		foreach ($this->checkableProps as $checkableProp) {
			$getter = "get" . $checkableProp["field"] ;
			$value = $entity->$getter() ;
// LG 20200408 old : on accepte les date NULL si le champ n'est pas mandatory			if ($checkableProp["check"] == "date" && ($value && !Functions::string_is_date($value))) {
			if ($checkableProp["check"] == "date" && ($value && !Functions::string_is_date($value))) {
                            // Date invalide
                            $tabErr[] = "Le champ " . $checkableProp["field"] . " n'est pas une date valide : " . $value ;
			}
		}

		// Vérifier l'unicité dans la table des combinaisons de champs
                if (!$pbSansVérifierUnicité) {
                    foreach ($this->uniqueProps as $uniques) {
// var_dump($uniques) ;
                            $where = "" ;
                            $lst = "" ;
                            $lstVal = "" ;
                            foreach($uniques as $unique) {
                                    $getter = "get" . $unique;
                                    $val = $entity->$getter() ;
                                    if ($where) $where .= " And " ;
//                                    if (is_null($entity->$getter())) {
                                    if ($entity->$getter()) {
                                        $where .= $unique . " = '" . $val . "'" ;
                                   } else if(count($uniques)==1) {
                                        // Inutile de vérifier dans ce cas
                                        continue ;
                                    } else {
                                        $where .= $unique . " Is NULL" ;
                                        $val = "[vide]" ;
                                    }
                                    if ($lst) $lst .= ", " ;
                                    $lst .= $unique ;
                                    if ($lstVal) $lstVal .= ", " ;
                                    $lstVal .= $val ;
                            }
                            if (!$where) {
                                // Le where resultant est vide : ignorer cet item
                                continue ;
                            }
                            if (!$this->estNouvelleEntité($entity)) {
                                    $whereAutres = "" ;
                                    foreach($this->getTabKeyFields() as $keyField) {
                                            if ($whereAutres) $whereAutres .= " And " ;
                                            $getter = "get" . $keyField ;
                                            $whereAutres .= $keyField . ' = ' . $entity->$getter() ;
                                    }
                                    $where .= " And Not (" . $whereAutres . ")" ;
                            }

                            $query = "Select Count(*) iNb From " . $this->table . " WHERE " . $where ;
// echo $query ;
                            $reqPrep = $this->db->prepare($query);
                            $resultSet = $this->getAllByReqPrep($reqPrep);
                            $result = $reqPrep->execute();

                            $nb = 0 ;
                            if ($result) {
                                    while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
                                            $nb = $row["inb"] ;
                                            break;
                                    }
                            } else {
                                    dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
                                    throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::vérifier(): ' . $reqPrep->errorInfo()[2]);
                            }
                            if ($nb > 0) {
                                    // Défaut d'unicité
                                    $tabErr[] = "La valeur de " . $lst . " (" . $lstVal . ") n'est pas unique." ;
                            }
                    }
                }

		return count($tabErr) ? false : true;
	}

	/**
	 * Détermine si l'entité fournie est une nouvelle (à insérer) ou une pré-existante (à updater)
	 * @param \Phaln\AbstractEntity $entity
	 * @throws RepositoryException
	 * @return true si c'est une nouvelle (à insérer), false si c'est une pré-existante (à updater)
	 */
	public function estNouvelleEntité(\Phaln\AbstractEntity $entity){
// LG 20200323 début
		$laPropsToutes = $entity->__toArray();
// LG 20200323 fin
		$laKeyFields = $this->getTabKeyFields() ;
		if (count($laKeyFields) == 1) {
			// Un seul champ clé : si elle est Null c'est que la ligne est nouvelle
			$getter = "get" . $laKeyFields[0] ;
			$nouveau = ($entity->$getter() == NULL) ;
		} else {
			// Clé composée de plusieurs champs : pour savoir si la ligne est nouvelle, il faut faire la requête
			$nouveau = true ;
			$query = "Select Count(*) as iNb From " . $this->table ;
			$where = "" ;
			$laPropsTest = [] ;
			foreach($laKeyFields as $keyField) {
				if ($where) $where .= " And " ;
				$getter = "get" . $keyField ;
// LG 20200323 début
				if (isset($laPropsToutes[$keyField . "_Initial"])) {
                                    $getter .= "_Initial" ;
// echo $getter . "=>" . $entity->$getter() . "<br>" ;
				}
// LG 20200323 fin
				$where .= $keyField . ' = :' . $keyField ;
				$laPropsTest[$keyField] = $entity->$getter() ;
			}
			$query .= ' WHERE ' . $where ;
dump_var($query, DUMP, "Requête de test pour savoir s'il faut insérer ou updater") ;
			$reqPrep = $this->db->prepare($query);
			$result = $reqPrep->execute($laPropsTest);
			if ($result) {
				while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
					$nouveau = ($row["inb"] == 0) ;
					break;
				}
			} else {
dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
				throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::sauver(): ' . $reqPrep->errorInfo()[2]);
			}			
		}
		return $nouveau ;
	}

	
	/**
	 * Enregistrer l'entité fournie
	 * Code selon Sonny Chaprier
	 * @param \Phaln\AbstractEntity $entity
	 * @return \Phaln\AbstractEntity : l'entité enregistrée (avec l'Id renseignée en cas d'insertion et de BDD postgreSQL)
	 *								   ou null si échec
	 */
    public function sauver(\Phaln\AbstractEntity $entity) {
		if($entity == NULL) {
			return NULL ;
		}
		$result = null ;
// var_dump($entity) ;		
// var_dump($entity->getDateNaissanceAdherent()) ;		
		// Construire les listes de champs et de valeurs pour la requête
		$columnsSelect = "";
		$columnsToBind = "";
		$laPropsToutes = $entity->__toArray();
		$laProps = $entity->__toArray();
		foreach($laProps as $key => $values) {
			if ($this->isKeyField($key) && !is_array($this->idFieldName)) {
				// Cette propriété est la clé de la table : l'enlever du tableau
				unset($laProps[$key]);
			} else if (in_array($key, $this->notFieldProps)) {
				// Cette propriété ne mappe pas un champ de la table : l'enlever du tableau
				unset($laProps[$key]);
			} else if ($laProps[$key] === NULL) {
				// La valeur de cette propriété est NULL : ne pas l'inclure
// echo $key ." est NULL : ne pas l'inclure<br>" ;
// LG 20200603 deac : empêche l'écrasement d'une valeur par un Null (cas du choix dans les zones de listes : par exemple raison d'annulation d'une participation)				unset($laProps[$key]);
			} else {
				// Gestion des booleens : il faut explicitement traduire les true et false
				if (is_bool($laProps[$key])===true) {
					$laProps[$key] = $laProps[$key]?"true":"false" ;
				}
			}
		}

		$nouveau = $this->estNouvelleEntité($entity) ;		
		$laKeyFields = $this->getTabKeyFields() ;

		if($nouveau) {
			// Cette entité ne mossède pas d'identifiant : c'est une nouvelle entité, à insérer
// echo "Nouveau" . "<br>" ;                   
			// Construire la requête
			foreach($laProps as $key => $values) {
				$columnsSelect .= $key . ",";
				$columnsToBind .= ':' . $key . ",";
			}
			$columnsSelect = substr($columnsSelect, 0, -1);
			$columnsToBind = substr($columnsToBind, 0, -1);
                        
			$query = 'INSERT INTO ' . $this->table
				. '('. $columnsSelect .')'
				. ' VALUES ('. $columnsToBind .')';
                       
		} else {
// echo "Ancien" . "<br>" ;                   
			// Entité existante à Updater
			foreach($laProps as $key => $values) {
				$columnsSelect .= $key . " = :" . $key . ",";
			}
			$columnsSelect = substr($columnsSelect, 0, -1);

			$query = 'UPDATE ' . $this->table
				. ' SET ' . $columnsSelect ;
			$where = "" ;
//var_dump($laProps) ;
//echo "<br>" ;
//var_dump($entity) ;
//echo "<br>" ;
                        foreach($laKeyFields as $keyField) {
				if ($where) $where .= " And " ;
				if (isset($laPropsToutes[$keyField . "_Initial"])) {
// echo $keyField . "_Initial<br>" ;
                                    $where .= $keyField . ' = :' . $keyField . "_Initial" ;
				} else {
                                    $where .= $keyField . ' = :' . $keyField ;
// echo $keyField . "<br>" ;
				}
			}
			$query .= ' WHERE ' . $where ;
dump_var($query, DUMP, 'Requête SQL:');
			$reqPrep = $this->db->prepare($query);

			foreach($laKeyFields as $keyField) {
				if (isset($laPropsToutes[$keyField . "_Initial"])) {
					$getter = "get" . $keyField . "_Initial" ;
					$laProps[$keyField . "_Initial"] = $entity->$getter() ;
				} else {
					$getter = "get" . $keyField ;
                                        $value = $entity->$getter() ;
					$laProps[$keyField] = $value ;					
				}
			}
		}
		if ($this->db->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'pgsql') {
			// sous Postgres, on peut utiliser la clause "Returning" pour renvoyer le dernier Id donnée par Postgres
			$returning = "" ;
			foreach($laKeyFields as $keyField) {
				if ($returning) $returning .= ", " ;
				$returning .= $keyField ;
			}
			$query .= " Returning " . $returning ;
		}
dump_var($query, DUMP, "Requête d'enregistrement") ;
dump_var($laProps, DUMP, "Données d'enregistrement") ;
		$reqPrep = $this->db->prepare($query);
		$result = $reqPrep->execute($laProps);
dump_var($result, DUMP, "Résultat de la requête d'enregistrement") ;
		if ($result) {
			while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
				foreach($laKeyFields as $keyField) {
					$setter = "set" . $keyField ;
					$entity->$setter($row[strtolower($keyField)]) ;
				}
				break;
			}
		} else {
dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::sauver(): ' . $reqPrep->errorInfo()[2]);
		}
		
		// Renvoyer l'entité enregistrée (avec éventuellement l'Id renseignée)
        return $entity;
    }
// LG 20200208 fin

}
