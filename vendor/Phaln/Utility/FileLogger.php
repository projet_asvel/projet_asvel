<?php

namespace Phaln\Utility;

/**
 * Description of FileLogger
 *
 * @author phaln
 */
class FileLogger {

    static $defaultFileName = LOG_DIR . 'phaln.log';

    static function Trace(array $datas) {
	$str = '';

	if (isset($datas['logFileName'])) {
	    $fileName = $datas['logFileName'];
	    FileLogger::$defaultFileName = $fileName;
	} else
	    $fileName = FileLogger::$defaultFileName;

	$logDatas['date'] = date('Y-m-d H:i:s');
	$logDatas['fileName'] = (isset($datas['fileName'])) ? $datas['fileName'] : '';
	$logDatas['line'] = (isset($datas['line'])) ? $datas['line'] : '';
	$logDatas['message'] = (isset($datas['message'])) ? $datas['message'] : '';
	$logDatas['infos'] = (isset($datas['infos'])) ? $datas['infos'] : null;

	foreach ($logDatas as $key => $value) {
	    if ($key != 'infos')
		$str .= $value . ';';
	    else {
		foreach ($value as $key => $info) {
		    $str .= $key . '=' . $info . ';';
		}
	    }
	}

	if ($fd = fopen($fileName, 'a+')) {
	    if (flock($fd, LOCK_EX)) {
		$strf = md5($str) . ';' . $str . PHP_EOL;
		if (FALSE === fwrite($fd, $strf))
		    throw new Exception("Erreur d'écriture dans le fichier de log $fileName");
		flock($fd, LOCK_UN);    // Enlève le verrou
	    } else {
		throw new Exception("Impossible de verrouiller le fichier $fileName");
	    }

	    fclose($fd);
	} else {
	    throw new Exception("Erreur d'ouverture du fichier de log $fileName");
	}

	return $str;
    }

}
