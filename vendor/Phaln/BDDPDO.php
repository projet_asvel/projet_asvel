<?php
namespace Phaln;

use \PDO;

/* 
 * Surcharge de la classe PDO pour générer des exceptions lors de l'échec des requêtes
 */
class BDDPDO Extends \PDO {
	
	// Surcharge de PDO::query, pour lancer une exception si échec de la requête
	function query(string $statement) {
		$rqtResult = PDO::query($statement) ;
		if (!$rqtResult) {
			// Echec de la requête : relancer l'exception
			dump_var($this->db->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getAll(): ' . $reqPrep->errorInfo()[2]);
		}
		return $rqtResult;
	}
	
	// Surcharge de PDO::query, pour lancer une exception si échec de la requête
	public function prepare($statement, $driver_options = array()){
		$rqtResult = PDO::prepare($statement, $driver_options) ;
		if (!$rqtResult) {
			// Echec de la requête : relancer l'exception
			dump_var($this->db->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getAll(): ' . $reqPrep->errorInfo()[2]);
		}
		return $rqtResult;
	}
}