<?php
namespace Phaln;
/**
 * Moteur de template HTML simpliste.
 * Dans le code HTML, les tags doivent être entre cochets et leur identifiant précédé d'un @.
 * Exemple du tag 'designation' inséré dans une célulle d'un tableau HTML: <td>[@designation]</td>
 * Lors de l'affichage du template, le moteur remplacera le tag 'designation' par la valeur
 * qui lui aura été affecté au préalable avec set('identifiant', "Vive les IRIS"); ici la chaine
 * de caractères "Vive les IRIS"
 */
class Template
{
    /**
     * Le nom du fichier HTML
     * @var string
     */
    protected $fichierTemplate = '';

    /**
     * Le tableau des tags contenus dans le fichier html.
     * @var array
     */
    protected $templateTags = array();

    /**
     * Constructeur.
     * @param string $fichierTemplate Nom du fichier template html
     */
    public function __construct($fichierTemplate)
    {
	//  Vérification de l'existence du fichier de template
	if (!file_exists($fichierTemplate))
	    throw new \Phaln\Exceptions\TemplateException("Erreur lors du chargement du fichier template ($fichierTemplate).");	
	else
	    $this->fichierTemplate = $fichierTemplate;
    }

    /**
     * Insère dans le tableau des tags, un identifiant de tag et sa valeur.
     * @param string $tag identifiant du tag dans le template html (juste le nom, sans @ ni [ ] )
     * @param string $valeur la valeur du tag passé en premier paramètre
     * @return \Template $this pour le chaînage de plusieurs set(...)
     */
    public function set($tag, $valeur)
    {
	$this->templateTags[$tag] = $valeur;
	return $this;
    }

    /**
     * Compose le texte HTML à afficher en lisant le fichier template et en y remplaçant les tags par leur valeur.
     * @return string le texte html à afficher
     */
    public function Affiche()
    {
	//  Vérification de l'existence du fichier de template
	if (!file_exists($this->fichierTemplate))
	    throw new TemplateException("Erreur lors du chargement du fichier template ($this->fichierTemplate).");
	
	//  Lecture du contenu du fichier (normalement du code HTML et des tags...) dans une variable $affichage
	$affichage = file_get_contents($this->fichierTemplate);

	//  Parcours du tableau des tags
	foreach ($this->templateTags as $tag => $valeur)
	{
	    //  Mise en forme du tag selon la forme présente dans le fichier de template avec @ et [ ]
	    $tagRemplace = "[@$tag]";

	    //  Remplacement du tag mis en forme par sa valeur dans la variable $affichage
	    $affichage = str_replace($tagRemplace, $valeur, $affichage);
	}

	//  On retourne la variable $affichage dans laquelle les tags ont été remplacés
	return $affichage;
    }   

    /**
     * Pour faciliter l'affichage, un echo de l'objet suffit
     * @return string
     */
    public function __toString()
    {
	return $this->Affiche();
    }
}