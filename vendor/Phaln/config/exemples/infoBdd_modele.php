<?php

BDD::$infoBdd = array(
		'interface' => 'pdo',		// ou 'mysqli'
		'type'   => 'mysql',		// ou 'pgsql'
		'host'   => 'monServeurBdd',	// Nom du serveur
		'port'   =>  3306,		// 5432 pour postgreSQL, 3306 pour MySQL
		'dbname' => 'maBaseDeDonnées',	// Nom de la base de données
		'charset' => 'monCharset',	// UTF8 par exemple
		'user'   => 'monUserBdd',	// Nom d'utilisateur
		'pass'   => 'monPasswordBdd',	// Password utilisateur
	);
