<?php

namespace Phaln;

/**
 * Description of AbstractViewTwig
 * Classe abstraite encapsulant le chargement du template twig.
 *
 * @author phaln
 */
abstract class AbstractViewTwig {

    protected $loader;
    protected $twig;
    protected $template;

    /**
     * Initialise le loader et le template à rendre
     * @param string $templateFile
     */
    public function __construct($templateFile) {
	dump_var($templateFile, DUMP, "TemplateFile dans AbstractVueTwig:");
	$this->loader = new \Twig\Loader\FilesystemLoader(TEMPLATE_PATH);
	$this->twig = new \Twig\Environment($this->loader);
	$this->twig->addGlobal('urlBase', URL_BASE);
	$this->template = $this->twig->load($templateFile);
    }

    /**
     * Effectue le rendu du template.
     */
    abstract public function render(array $datas);

}
