<?php
namespace Phaln;

/**
 * Description of IController
 *
 * @author phaln
 */
interface IController 
{
    /**
     * L'action par défaut.
     * @param array $param les paramètres reçus
     */
    public function DefaultAction(array $param = null);  
}
