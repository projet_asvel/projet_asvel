<?php
namespace Phaln;

/**
 * Description of AbsractController
 *
 * @author phaln
 */
abstract class AbstractController 
{
    protected $page = null;	//  La page
    protected $template = TEMPLATE_PATH;
    
    /**
     * Constructeur
     */
    public function __construct($template = null)
    {
	$this->template .= (isset($template))?$template:'basePage.tpl';
	if(DUMP) var_dump ($this->template);
    }
    
    /**
     * L'action par défaut.
     * @param array $param les paramètres reçus
     */
    public abstract function DefaultAction(array $param = null);  
}
