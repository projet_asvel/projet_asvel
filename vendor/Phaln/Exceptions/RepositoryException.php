<?php
namespace Phaln\Exceptions;

/**
 * Description of RepositoryException
 *
 * @author phaln
 */
class RepositoryException extends PhalnBaseException {
}
