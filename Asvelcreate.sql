
begin ;

CREATE OR REPLACE FUNCTION public.datederniereinscriptionadherent() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
BEGIN
        UPDATE adherent 
            SET dateDerniereInscriptionAdherent = (SELECT MAX(dateaffiliation) FROM affilier WHERE new.idAdherent= affilier.idAdherent)
			Where new.idAdherent= adherent.idAdherent;
    RETURN new;
END;
$$;

CREATE OR REPLACE FUNCTION public.dedoublonne(piadherentaconserver integer, piadherentavirer integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin
	Update Participer set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;
	Update Participer set idInscripteur = piAdherentAConserver Where idInscripteur = piAdherentAVirer ;

	Delete from Affilier where (idAdherent, idFederation, dateaffiliation) In (Select piAdherentAConserver, idFederation, dateAffiliation From Affilier Where idAdherent = piAdherentAVirer) ;
	Update Affilier set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Avoir where (idAdherent, idTag) In (Select piAdherentAConserver, idTag From Avoir Where idAdherent = piAdherentAVirer) ;
	Update Avoir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Update Recevoir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;
	Update Obtenir set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Etre where (idAdherent, idRole) In (Select piAdherentAConserver, idRole From Etre Where idAdherent = piAdherentAVirer) ;
	Update Etre set idAdherent = piAdherentAConserver Where idAdherent = piAdherentAVirer ;

	Delete from Adherent Where idAdherent = piAdherentAVirer ;

	return true ;
End ; $$;

CREATE OR REPLACE FUNCTION public.ftrigadherentai() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
Declare
	liNb BigInt ;
Begin
	With cte AS (
		Insert Into Etre (idAdherent, idRole) 
			Select A.idAdherent, 5
				From Adherent A
				where A.idAdherent = new.idAdherent
					And not A.idAdherent In (
						select A.idAdherent
							from etre E
							Where E.idAdherent = new.idAdherent
								AND E.idRole = 5
						)
				returning idAdherent
				)
			Select Count(*) Into liNb From cte ;
		return new ;
End ; $$;

CREATE OR REPLACE FUNCTION public.getdatefinaffiliation(pdfinaffiliation date, pddebutaffiliation date, pnnbjoursvaliditeaffiliation integer) RETURNS date
    LANGUAGE plpgsql IMMUTABLE
    AS $$
Begin
	if not pdFinAffiliation is Null Then
		return pdFinAffiliation ;
	end if ;

	if pnNbJoursValiditeAffiliation = -1 Then
		-- "1 an à partir du 1er septembre"
		if (To_Char(pdDebutAffiliation, 'MM'))::Integer >= 9 Then
			-- Inscription à partir de septembre : fin l'année suivante
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer + 1 || '/08/31', 'YYYY/MM/DD') ;
		else
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer || '/08/31', 'YYYY/MM/DD') ;
		end if ;

	elseif pnNbJoursValiditeAffiliation = -2 Then
		-- "1 an à partir du 1er novembre"
		if (To_Char(pdDebutAffiliation, 'MM'))::Integer >= 11 Then
			-- Inscription à partir de septembre : fin l'année suivante
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer + 1 || '/10/31', 'YYYY/MM/DD') ;
		else
			return to_date((To_Char(pdDebutAffiliation, 'YYYY'))::Integer || '/10/31', 'YYYY/MM/DD') ;
		end if ;
		
	else
		-- Validité un certain nb de jours
		return pdDebutAffiliation + pnNbJoursValiditeAffiliation ;
	end if ;

End ; $$;

CREATE OR REPLACE FUNCTION public.miseajourparticipationencadrants() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
DECLARE
ancienencadrant int;
BEGIN
	if TG_OP = 'UPDATE' then
		UPDATE Participer 
			SET idfonction = 1
			WHERE Participer.idadherent = OLD.idorganisateur
				AND Participer.idsortie = New.idSortie ;
	End If ;
	UPDATE Participer 
		SET idfonction = 2
                    , validationParticipation = true
		WHERE idadherent = New.idorganisateur
			AND idsortie = New.idSortie
		Returning idadherent into ancienencadrant ;
	if ancienencadrant is null then
		INSERT INTO Participer (idsortie,idadherent,idfonction, validationParticipation) values (New.idSortie, New.idorganisateur, 2, true);
	end if;
	RETURN New;
END;
$$;

CREATE OR REPLACE FUNCTION public.normalise(pstext character varying, pbtrim boolean DEFAULT true, pbtoupper boolean DEFAULT true) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
	select translate(v_str, '-', '') into v_str; 		-- LG 20220730 : tiret
	select translate(v_str, '''"', '  ') into v_str; 	-- LG 20220730 : quotes et double-quotes
    select translate(v_str, 'Ç', 'C') into v_str;  	    -- LG 20220730
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
-- LG 20220730 old		select translate(v_str, 'Çç', 'Cc') into v_str; 
		select translate(v_str, 'ç', 'c') into v_str; 
	end if ;
	if pbTrim Then
		select translate(v_str, ' ', '') into v_str; 
	End If ;
return v_str; 
end;$$;

CREATE OR REPLACE FUNCTION public.normalize(pstext character varying, pbtrim boolean DEFAULT true, pbtoupper boolean DEFAULT true) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
declare 
		v_str    text; 
begin 

	if pbToUpper Then
		psText = Upper(psText) ;
	end if ;
	if pbTrim Then
		select translate(psText, ' ', '') into psText; 
	End If ;
	select translate(psText, 'ÀÁÂÃÄÅ', 'AAAAAA') into v_str; 
	select translate(v_str, 'ÉÈËÊ', 'EEEE') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÌÍÎÏ', 'IIII') into v_str; 
	select translate(v_str, 'ÒÓÔÕÖ', 'OOOOO') into v_str; 
	select translate(v_str, 'ÙÚÛÜ', 'UUUU') into v_str; 
	if not pbToUpper Then
		select translate(v_str, 'àáâãäå', 'aaaaaa') into v_str; 
		select translate(v_str, 'èéêë', 'eeee') into v_str; 
		select translate(v_str, 'ìíîï', 'iiii') into v_str; 
		select translate(v_str, 'òóôõö', 'ooooo') into v_str; 
		select translate(v_str, 'ùúûü', 'uuuu') into v_str; 
		select translate(v_str, 'Çç', 'Cc') into v_str; 
	end if ;
return v_str; 
end;$$;

DROP TABLE IF EXISTS public.activite CASCADE ;
CREATE TABLE public.activite (
    idactivite integer NOT NULL,
    nomactivite character varying(50)
);

CREATE SEQUENCE public.activite_idactivite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.activite_idactivite_seq OWNED BY public.activite.idactivite;

DROP TABLE IF EXISTS public.adherent CASCADE ;
CREATE TABLE public.adherent (
    idadherent integer NOT NULL,
    nomadherent character varying(50) NOT NULL,
    prenomadherent character varying(50) NOT NULL,
    datenaissanceadherent date,
    licenseadherent character varying(50),
    adresseadherent character varying(50),
    adresse2adherent character varying(50),
    mailadherent character varying(50),
    teladherent character varying(20),
    cpadherent character varying(5),
    villeadherent character varying(50),
    paysadherent character varying(50),
    nationaliteadherent character varying(50),
    sexeadherent character varying(1),
    mdpadherent character varying(255),
    droitimageadherent boolean,
    refusmailadherent boolean,
    commentaireadherent text,
    prenomcontact character varying(50),
    nomcontact character varying(50),
    adressecontact character varying(50),
    adresse2contact character varying(50),
    villecontact character varying(50),
    cpcontact character varying(5),
    mailcontact character varying(50),
    telcontact character varying(20),
    idtypecertif integer,
    certifnommedecin character varying(50),
    certifalpi boolean,
    saeadherent boolean,
    datederniereinscriptionadherent date,
    idautreaffiliation integer
);

CREATE SEQUENCE public.adherent_idadherent_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.adherent_idadherent_seq OWNED BY public.adherent.idadherent;

DROP TABLE IF EXISTS public.affilier CASCADE ;
CREATE TABLE public.affilier (
    idadherent integer NOT NULL,
    idfederation integer NOT NULL,
    idtypelicense integer,
    idtypereglementadherent integer,
    montantreglementadherent real,
    dateaffiliation date NOT NULL,
    numerolicense character varying(50),
    idaffiliation integer NOT NULL,
    datefinaffiliation date
);

CREATE SEQUENCE public.affilier_idaffiliation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.affilier_idaffiliation_seq OWNED BY public.affilier.idaffiliation;

DROP TABLE IF EXISTS public.autreaffiliation CASCADE ;
CREATE TABLE public.autreaffiliation (
    idautreaffiliation integer NOT NULL,
    nomautreaffiliation character varying(50)
);

CREATE SEQUENCE public.autreaffiliation_idautreaffiliation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.autreaffiliation_idautreaffiliation_seq OWNED BY public.autreaffiliation.idautreaffiliation;

DROP TABLE IF EXISTS public.avoir CASCADE ;
CREATE TABLE public.avoir (
    idadherent integer NOT NULL,
    idtag integer NOT NULL
);

DROP TABLE IF EXISTS public.concerner CASCADE ;
CREATE TABLE public.concerner (
    idsortie integer NOT NULL,
    idactivite integer NOT NULL
);

DROP TABLE IF EXISTS public.difficulte CASCADE ;
CREATE TABLE public.difficulte (
    iddifficulte integer NOT NULL,
    nomdifficulte character varying(50) NOT NULL
);

CREATE SEQUENCE public.difficulte_iddifficulte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.difficulte_iddifficulte_seq OWNED BY public.difficulte.iddifficulte;

DROP TABLE IF EXISTS public.etre CASCADE ;
CREATE TABLE public.etre (
    idadherent integer NOT NULL,
    idrole integer NOT NULL
);

DROP TABLE IF EXISTS public.federation CASCADE ;
CREATE TABLE public.federation (
    idfederation integer NOT NULL,
    nomfederation character varying(50) NOT NULL,
    nbjoursvaliditeaffiliation integer DEFAULT '-1'::integer
);

CREATE SEQUENCE public.federation_idfederation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.federation_idfederation_seq OWNED BY public.federation.idfederation;

DROP TABLE IF EXISTS public.fonction CASCADE ;
CREATE TABLE public.fonction (
    idfonction integer NOT NULL,
    nomfonction character varying(50) NOT NULL
);

CREATE SEQUENCE public.fonction_idfonction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.fonction_idfonction_seq OWNED BY public.fonction.idfonction;

DROP TABLE IF EXISTS public.mail CASCADE ;
CREATE TABLE public.mail (
    idmail integer NOT NULL,
    datemail timestamp without time zone DEFAULT now() NOT NULL,
    sujetmail text NOT NULL,
    contenumail text NOT NULL,
    idadherentexpediteur integer NOT NULL,
    destinatairesvisibles boolean DEFAULT false,
    mailexpediteur character varying(50)
);

CREATE SEQUENCE public.mail_idmail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.mail_idmail_seq OWNED BY public.mail.idmail;

DROP TABLE IF EXISTS public.obtenir CASCADE ;
CREATE TABLE public.obtenir (
    idadherent integer NOT NULL,
    idpasseport integer NOT NULL
);

DROP TABLE IF EXISTS public.participer CASCADE ;
CREATE TABLE public.participer (
    idadherent integer NOT NULL,
    idsortie integer NOT NULL,
    dateinscription timestamp without time zone DEFAULT now(),
    montantarrhes real DEFAULT 0 NOT NULL,
    montanttotal real DEFAULT 0 NOT NULL,
    commentaireparticipation text,
    idfonction integer NOT NULL,
    idraisonannulation integer,
    idtypereglementarrhes integer,
    idtypereglementmontanttotal integer,
    validationparticipation boolean,
    nbplacevoiture integer,
    dva boolean,
    pellesonde boolean,
    idinscripteur integer,
    arrhesremboursees boolean,
    excluducovoiturage boolean,
    debutant boolean DEFAULT false,
    louemateriel boolean DEFAULT false
);

DROP TABLE IF EXISTS public.passeport CASCADE ;
CREATE TABLE public.passeport (
    idpasseport integer NOT NULL,
    nompasseport character varying(50) NOT NULL
);

CREATE SEQUENCE public.passeport_idpasseport_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.passeport_idpasseport_seq OWNED BY public.passeport.idpasseport;

DROP TABLE IF EXISTS public.raisonannulation CASCADE ;
CREATE TABLE public.raisonannulation (
    idannulation integer NOT NULL,
    libelleannulation character varying(100) NOT NULL
);

CREATE SEQUENCE public.raisonannulation_idannulation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.raisonannulation_idannulation_seq OWNED BY public.raisonannulation.idannulation;

DROP TABLE IF EXISTS public.recevoir CASCADE ;
CREATE TABLE public.recevoir (
    idadherent integer NOT NULL,
    idmail integer NOT NULL
);

DROP TABLE IF EXISTS public.role CASCADE ;
CREATE TABLE public.role (
    idrole integer NOT NULL,
    nomrole character varying(50) NOT NULL
);

CREATE SEQUENCE public.role_idrole_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.role_idrole_seq OWNED BY public.role.idrole;

DROP TABLE IF EXISTS public.sortie CASCADE ;
CREATE TABLE public.sortie (
    idsortie integer NOT NULL,
    nomsortie character varying(50) NOT NULL,
    lieusortie character varying(100),
    datedebutsortie date,
    datefinsortie date,
    distancesortie real,
    denivellesortie real,
    altitudemaxsortie real,
    heuredepart character varying(5),
    commentairessortie text,
    tarifsortie real,
    coutautoroute real,
    nombreparticipantsmaxsortie integer,
    nbvoituressortie integer,
    iddifficulte integer,
    idorganisateur integer NOT NULL,
    idraisonannulation integer,
    inscriptionenligne boolean,
    fraisannexe real,
    fraisannexedetails text
);

CREATE SEQUENCE public.sortie_idsortie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.sortie_idsortie_seq OWNED BY public.sortie.idsortie;

DROP TABLE IF EXISTS public.tag CASCADE ;
CREATE TABLE public.tag (
    idtag integer NOT NULL,
    nomtag character varying(50) NOT NULL
);

CREATE SEQUENCE public.tag_idtag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.tag_idtag_seq OWNED BY public.tag.idtag;

DROP TABLE IF EXISTS public.typecertificat CASCADE ;
CREATE TABLE public.typecertificat (
    idtypecertificat integer NOT NULL,
    nomtypecertificat character varying(50) NOT NULL
);

CREATE SEQUENCE public.typecertificat_idtypecertificat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.typecertificat_idtypecertificat_seq OWNED BY public.typecertificat.idtypecertificat;

DROP TABLE IF EXISTS public.typelicense CASCADE ;
CREATE TABLE public.typelicense (
    idlicense integer NOT NULL,
    nomtypelicense character varying(50) NOT NULL
);

CREATE SEQUENCE public.typelicense_idlicense_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.typelicense_idlicense_seq OWNED BY public.typelicense.idlicense;

DROP TABLE IF EXISTS public.typereglement CASCADE ;
CREATE TABLE public.typereglement (
    idtypereglement integer NOT NULL,
    nomtypereglement character varying(50) NOT NULL
);

CREATE SEQUENCE public.typereglement_idtypereglement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.typereglement_idtypereglement_seq OWNED BY public.typereglement.idtypereglement;

ALTER TABLE ONLY public.activite ALTER COLUMN idactivite SET DEFAULT nextval('public.activite_idactivite_seq'::regclass);

ALTER TABLE ONLY public.adherent ALTER COLUMN idadherent SET DEFAULT nextval('public.adherent_idadherent_seq'::regclass);

ALTER TABLE ONLY public.affilier ALTER COLUMN idaffiliation SET DEFAULT nextval('public.affilier_idaffiliation_seq'::regclass);

ALTER TABLE ONLY public.autreaffiliation ALTER COLUMN idautreaffiliation SET DEFAULT nextval('public.autreaffiliation_idautreaffiliation_seq'::regclass);

ALTER TABLE ONLY public.difficulte ALTER COLUMN iddifficulte SET DEFAULT nextval('public.difficulte_iddifficulte_seq'::regclass);

ALTER TABLE ONLY public.federation ALTER COLUMN idfederation SET DEFAULT nextval('public.federation_idfederation_seq'::regclass);

ALTER TABLE ONLY public.fonction ALTER COLUMN idfonction SET DEFAULT nextval('public.fonction_idfonction_seq'::regclass);

ALTER TABLE ONLY public.mail ALTER COLUMN idmail SET DEFAULT nextval('public.mail_idmail_seq'::regclass);

ALTER TABLE ONLY public.passeport ALTER COLUMN idpasseport SET DEFAULT nextval('public.passeport_idpasseport_seq'::regclass);

ALTER TABLE ONLY public.raisonannulation ALTER COLUMN idannulation SET DEFAULT nextval('public.raisonannulation_idannulation_seq'::regclass);

ALTER TABLE ONLY public.role ALTER COLUMN idrole SET DEFAULT nextval('public.role_idrole_seq'::regclass);

ALTER TABLE ONLY public.sortie ALTER COLUMN idsortie SET DEFAULT nextval('public.sortie_idsortie_seq'::regclass);

ALTER TABLE ONLY public.tag ALTER COLUMN idtag SET DEFAULT nextval('public.tag_idtag_seq'::regclass);

ALTER TABLE ONLY public.typecertificat ALTER COLUMN idtypecertificat SET DEFAULT nextval('public.typecertificat_idtypecertificat_seq'::regclass);

ALTER TABLE ONLY public.typelicense ALTER COLUMN idlicense SET DEFAULT nextval('public.typelicense_idlicense_seq'::regclass);

ALTER TABLE ONLY public.typereglement ALTER COLUMN idtypereglement SET DEFAULT nextval('public.typereglement_idtypereglement_seq'::regclass);

ALTER TABLE ONLY public.activite
    ADD CONSTRAINT activite_pkey PRIMARY KEY (idactivite);

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_pkey PRIMARY KEY (idadherent);

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_uniq_mail UNIQUE (mailadherent);

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_uniq_nomprenomnaissance UNIQUE (nomadherent, prenomadherent, datenaissanceadherent);

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT affilier_pkey PRIMARY KEY (idaffiliation);

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT affilier_unique_ad_fed_date UNIQUE (idadherent, idfederation, dateaffiliation);

ALTER TABLE ONLY public.autreaffiliation
    ADD CONSTRAINT autreaffiliation_pkey PRIMARY KEY (idautreaffiliation);

ALTER TABLE ONLY public.avoir
    ADD CONSTRAINT avoir_pkey PRIMARY KEY (idadherent, idtag);

ALTER TABLE ONLY public.concerner
    ADD CONSTRAINT concerner_pkey PRIMARY KEY (idsortie, idactivite);

ALTER TABLE ONLY public.difficulte
    ADD CONSTRAINT difficulte_pkey PRIMARY KEY (iddifficulte);

ALTER TABLE ONLY public.etre
    ADD CONSTRAINT etre_pkey PRIMARY KEY (idadherent, idrole);

ALTER TABLE ONLY public.federation
    ADD CONSTRAINT federation_pkey PRIMARY KEY (idfederation);

ALTER TABLE ONLY public.fonction
    ADD CONSTRAINT fonction_pkey PRIMARY KEY (idfonction);

ALTER TABLE ONLY public.mail
    ADD CONSTRAINT mail_pkey PRIMARY KEY (idmail);

ALTER TABLE ONLY public.obtenir
    ADD CONSTRAINT obtenir_pkey PRIMARY KEY (idadherent, idpasseport);

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT participer_pkey PRIMARY KEY (idadherent, idsortie);

ALTER TABLE ONLY public.passeport
    ADD CONSTRAINT passeport_pkey PRIMARY KEY (idpasseport);

ALTER TABLE ONLY public.raisonannulation
    ADD CONSTRAINT raisonannulation_pkey PRIMARY KEY (idannulation);

ALTER TABLE ONLY public.recevoir
    ADD CONSTRAINT recevoir_pkey PRIMARY KEY (idadherent, idmail);

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (idrole);

ALTER TABLE ONLY public.sortie
    ADD CONSTRAINT sortie_pkey PRIMARY KEY (idsortie);

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (idtag);

ALTER TABLE ONLY public.typecertificat
    ADD CONSTRAINT typecertificat_pkey PRIMARY KEY (idtypecertificat);

ALTER TABLE ONLY public.typelicense
    ADD CONSTRAINT typelicense_pkey PRIMARY KEY (idlicense);

ALTER TABLE ONLY public.typereglement
    ADD CONSTRAINT typereglement_pkey PRIMARY KEY (idtypereglement);

ALTER TABLE ONLY public.activite
    ADD CONSTRAINT uc_activite UNIQUE (nomactivite);

ALTER TABLE ONLY public.difficulte
    ADD CONSTRAINT uc_difficulte UNIQUE (nomdifficulte);

ALTER TABLE ONLY public.federation
    ADD CONSTRAINT uc_federation UNIQUE (nomfederation);

ALTER TABLE ONLY public.fonction
    ADD CONSTRAINT uc_fonction UNIQUE (nomfonction);

ALTER TABLE ONLY public.role
    ADD CONSTRAINT uc_nomrole UNIQUE (nomrole);

ALTER TABLE ONLY public.passeport
    ADD CONSTRAINT uc_passeport UNIQUE (nompasseport);

ALTER TABLE ONLY public.raisonannulation
    ADD CONSTRAINT uc_raisonannulation UNIQUE (libelleannulation);

ALTER TABLE ONLY public.sortie
    ADD CONSTRAINT uc_sortie UNIQUE (nomsortie);

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT uc_tag UNIQUE (nomtag);

ALTER TABLE ONLY public.typecertificat
    ADD CONSTRAINT uc_typecertificat UNIQUE (nomtypecertificat);

CREATE TRIGGER miseajourencadrant AFTER INSERT OR UPDATE ON public.sortie FOR EACH ROW EXECUTE FUNCTION public.miseajourparticipationencadrants();

CREATE TRIGGER trigadherentai AFTER INSERT ON public.adherent FOR EACH ROW EXECUTE FUNCTION public.ftrigadherentai();

CREATE TRIGGER updateinscriptionadherent AFTER INSERT OR UPDATE ON public.affilier FOR EACH ROW EXECUTE FUNCTION public.datederniereinscriptionadherent();

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_idautreaffiliation_fkey FOREIGN KEY (idautreaffiliation) REFERENCES public.autreaffiliation(idautreaffiliation) ON DELETE SET NULL;

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_idtypecertif_fkey FOREIGN KEY (idtypecertif) REFERENCES public.typecertificat(idtypecertificat) ON DELETE SET NULL;

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT fk_affilier_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE CASCADE;

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT fk_affilier_federation_1 FOREIGN KEY (idfederation) REFERENCES public.federation(idfederation) ON DELETE CASCADE;

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT fk_affilier_license_1 FOREIGN KEY (idtypelicense) REFERENCES public.typelicense(idlicense);

ALTER TABLE ONLY public.affilier
    ADD CONSTRAINT fk_affilier_typereglement_1 FOREIGN KEY (idtypereglementadherent) REFERENCES public.typereglement(idtypereglement) ON DELETE CASCADE;

ALTER TABLE ONLY public.avoir
    ADD CONSTRAINT fk_avoir_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE CASCADE;

ALTER TABLE ONLY public.avoir
    ADD CONSTRAINT fk_avoir_tag_1 FOREIGN KEY (idtag) REFERENCES public.tag(idtag) ON DELETE CASCADE;

ALTER TABLE ONLY public.concerner
    ADD CONSTRAINT fk_concerner_activite_1 FOREIGN KEY (idactivite) REFERENCES public.activite(idactivite);

ALTER TABLE ONLY public.concerner
    ADD CONSTRAINT fk_concerner_sortie_1 FOREIGN KEY (idsortie) REFERENCES public.sortie(idsortie);

ALTER TABLE ONLY public.etre
    ADD CONSTRAINT fk_etre_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE CASCADE;

ALTER TABLE ONLY public.etre
    ADD CONSTRAINT fk_etre_role_1 FOREIGN KEY (idrole) REFERENCES public.role(idrole) ON DELETE CASCADE;

ALTER TABLE ONLY public.obtenir
    ADD CONSTRAINT fk_obtenir_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE CASCADE;

ALTER TABLE ONLY public.obtenir
    ADD CONSTRAINT fk_obtenir_passeport_1 FOREIGN KEY (idpasseport) REFERENCES public.passeport(idpasseport) ON DELETE CASCADE;

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE RESTRICT;

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_fonction_1 FOREIGN KEY (idfonction) REFERENCES public.fonction(idfonction) ON DELETE SET NULL;

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_inscripteur FOREIGN KEY (idinscripteur) REFERENCES public.adherent(idadherent);

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_sortie_1 FOREIGN KEY (idsortie) REFERENCES public.sortie(idsortie) ON DELETE RESTRICT;

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_typereglement_1 FOREIGN KEY (idtypereglementarrhes) REFERENCES public.typereglement(idtypereglement) ON DELETE SET NULL;

ALTER TABLE ONLY public.participer
    ADD CONSTRAINT fk_participer_typereglement_2 FOREIGN KEY (idtypereglementmontanttotal) REFERENCES public.typereglement(idtypereglement) ON DELETE SET NULL;

ALTER TABLE ONLY public.recevoir
    ADD CONSTRAINT fk_recevoir_adherent_1 FOREIGN KEY (idadherent) REFERENCES public.adherent(idadherent) ON DELETE CASCADE;

ALTER TABLE ONLY public.recevoir
    ADD CONSTRAINT fk_recevoir_mail_1 FOREIGN KEY (idmail) REFERENCES public.mail(idmail) ON DELETE CASCADE;

ALTER TABLE ONLY public.sortie
    ADD CONSTRAINT fk_sortie_difficulte_1 FOREIGN KEY (iddifficulte) REFERENCES public.difficulte(iddifficulte) ON DELETE SET NULL;

ALTER TABLE ONLY public.sortie
    ADD CONSTRAINT fk_sortie_raisonannulation_1 FOREIGN KEY (idraisonannulation) REFERENCES public.raisonannulation(idannulation) ON DELETE SET NULL;
