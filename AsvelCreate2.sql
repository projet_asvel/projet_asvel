DROP TABLE IF EXISTS Materiel CASCADE;
CREATE TABLE Materiel(
   idMateriel serial,
   Taille numeric NULL,
   Pointure numeric NULL,
   Modele VARCHAR(50) NOT NULL,
   Longitude1 numeric NULL,
   Longitude2 numeric NULL,
   Latitude1 numeric NULL,
   Latitude2 numeric NULL,
   dateAchat TIMESTAMP,
   Commentaire VARCHAR(50) NULL,
   Pays VARCHAR(50) NULL,
   idTypeMateriel INT NOT NULL,
   idTypeMiseAdispo INT NOT NULL,
   idAdherent INT NOT NULL,
   CONSTRAINT taille_min CHECK (Taille> 0 and Taille<250),
   CONSTRAINT pointure_min CHECK (Pointure> 0 and Pointure<50),
   CONSTRAINT pk_materiel PRIMARY KEY(idMateriel),
   CONSTRAINT fk_typemateriel FOREIGN KEY(idTypeMateriel) REFERENCES TypeMateriel(idTypeMateriel),
   CONSTRAINT fk_typemiseadispo FOREIGN KEY(idTypeMiseAdispo) REFERENCES TypeMiseAdispo(idTypeMiseAdispo),
   CONSTRAINT fk_adherent FOREIGN KEY(idAdherent) REFERENCES Adherent(idAdherent)
);

DROP TABLE IF EXISTS TypeMateriel CASCADE;
CREATE TABLE TypeMateriel(
   idTypeMateriel serial,
   nomType VARCHAR(50) NOT NULL UNIQUE,
   CONSTRAINT pk_typemateriel PRIMARY KEY(idTypeMateriel)
);

DROP TABLE IF EXISTS TypeMiseAdispo CASCADE;
CREATE TABLE TypeMiseAdispo(
   idTypeMiseAdispo serial,
   nomMiseAdispo VARCHAR(50) NOT NULL UNIQUE,
   CONSTRAINT pk_typemiseadispo PRIMARY KEY(idTypeMiseAdispo)
);

DROP TABLE IF EXISTS InformationMateriel CASCADE;
CREATE TABLE InformationMateriel(
   idInfoMateriel serial,
   nomInfo VARCHAR(50) NOT NULL UNIQUE,
   CONSTRAINT pk_informationmateriel PRIMARY KEY(idInfoMateriel)
);

DROP TABLE IF EXISTS Photo CASCADE;
CREATE TABLE Photo(
   idPhoto serial,
   cheminPhoto VARCHAR(50) NOT NULL,
   dateHeurePhoto TIMESTAMP,
   idSortie INT NOT NULL,
   idAdherent INT NOT NULL,
   CONSTRAINT pk_photo PRIMARY KEY(idPhoto),
   CONSTRAINT fk_sortie FOREIGN KEY(idSortie) REFERENCES Sortie(idSortie),
   CONSTRAINT fk_adherent FOREIGN KEY(idAdherent) REFERENCES Adherent(idAdherent)
);

DROP TABLE IF EXISTS Decrire; 
CREATE TABLE Decrire(
   idTypeMateriel INT NOT NULL,
   idInfoMateriel INT NOT NULL,
   CONSTRAINT pk_decrire PRIMARY KEY(idTypeMateriel, idInfoMateriel), 
   CONSTRAINT fk_typemateriel FOREIGN KEY(idTypeMateriel) REFERENCES TypeMateriel(idTypeMateriel),
   CONSTRAINT fk_infomateriel FOREIGN KEY(idInfoMateriel) REFERENCES InformationMateriel(idInfoMateriel) 
);


