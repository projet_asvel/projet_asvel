$(document).ready(function () {
    var modal = $('.login-container');
    var login = $('.login-form');
    $('#login-button').click(function () {
        // Demande de connexion
        if (modal.is(":visible")) {
// LG 20200207 : ajouté mais inutile car ce n'est pas ce code qui est appellé			$('.login-error').css('display', 'none');
// L'affichage du modal se fait grâce à data-toggle="modal" data-target="#modalLogin"
            modal.hide();
        } else {
            modal.show();
        }
    });

    modal.click(function () {
        if (modal.is(':visible') && !login.is(event.target) && login.has(event.target).length === 0) {
            modal.hide();
        }
    });

    login.submit(function (event) {
// LG 20210124 début
//        $.post("traitement-login.php", {mail: login.find('#username').val(), password: login.find('#password').val()})
        var lsLogin = login.find('#username').val() ;
        var lsMdp = login.find('#password').val() ;
        login.find('#username').val("") ;
        login.find('#password').val("") ;
        $.post("traitement-login.php", {mail: lsLogin, password: lsMdp})
// LG 20210124 fin
        .done(function (data) {
            if (data && data === 'true') {
                $('.login-error').css('display', 'none');       // LG 20210704
                window.location.reload();
            } else {
                $('.login-error').css('display', 'flex');
            }
        });
        event.preventDefault();
    });

    $('#logout-button').click(function (event) {
        // Déconnexion
        window.location.href = "src/App/Call/logout.php";
        event.preventDefault();
    });
    
    $('#login-form-newpwd').click(function () {
        // Demande d'envoi de nouveau mdp
        window.location.href = "envoi-MDP-demande.php?mail=" + login.find('#username').val() ;
        event.preventDefault();
    })

});