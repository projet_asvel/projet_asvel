$(document).ready(function () {
    var spinner = $('.sk-container');
    var form = $('#formimport');

    form.submit(function (event) {
	$("#résultat").html("Importation en cours...");
        event.preventDefault();
        spinner.show();
        var formdata = new FormData();
        var file = $('input[type=file]').prop('files')[0];
        formdata.append('file', file);
        formdata.append('typeImport', $('#typeImport').val());

        $.ajax({
            url: '../../public/pages/traitement-import.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: formdata,
            type: 'post'
        })
        .done(function(response){
			var toasterOptionCloseButton = toastr.options.closeButton ;
//			var toasterOptionTimeout = toastr.options.timeOut ;
//			toastr.options.timeOut = 0 ;
			toastr.options.closeButton = true ;
			if(response && response.substring(0, 5) !== "Echec") {
				toastr["success"]("L'importation est terminée.");
			} else {
				toastr["error"]("Une erreur est survenue au cours de l'importation");
			}
			$("#résultat").html(response);
//			toastr.options.timeOut = toasterOptionTimeout ;
			toastr.options.closeButton = toasterOptionCloseButton ;
        })
        .fail(function (xhr, status, error) {
            toastr["error"]("Une erreur est survenue : " + error);
        })
        .always(function () {
            spinner.hide();
        });
    });
});

// Supprimer un adhérent tout juste importé
// Doit renvoyer false pour éviter l'action de l'hyperlien sur lequel on clique pour supprimer
function effacerAdherentImporté(event, piIdAdherent) {
    if ($("#id" + piIdAdherent).css("text-decoration").indexOf("line-through") >= 0) {
        // L'adhérent a déja été supprimé    
        return false ;
    }
    supprimerAdherent(event, piIdAdherent, false, aprèsEffacementAdherentImporté) ;
    return false ;
}
// Fonction callback une fois que l'adhérent a été supprimé
function aprèsEffacementAdherentImporté(piIdAdherent1, piIdAdherent2) {
    $("#id" + piIdAdherent1).css("text-decoration", "line-through") ;
    $("#id" + piIdAdherent2).css("text-decoration", "line-through") ;
    return false ;
}

// Fusionner deux adhérents suite à confusion dans l'importation
// LG 20220910
function fusionnerAdherentsImportés(event, piIdAdherentAConserver, piIdAdherentAVirer) {
    if ($("#id" + piIdAdherentAConserver).length && $("#id" + piIdAdherentAConserver).css("text-decoration").indexOf("line-through") >= 0) {
        // L'adhérent a déja été supprimé    
        return false ;
    }
    if ($("#id" + piIdAdherentAVirer).length && $("#id" + piIdAdherentAVirer).css("text-decoration").indexOf("line-through") >= 0) {
        // L'adhérent a déja été supprimé    
        return false ;
    }
    fusionnerAdherents(event, piIdAdherentAConserver, piIdAdherentAVirer, false, aprèsEffacementAdherentImporté) ;
    return false ;
}
