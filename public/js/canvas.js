// Partie 1
// window.onload = function () {
//     var canvas=document.getElementById('mon_canvas');
//     var context=canvas.getContext('2d');
//     var x=0;
//     var y=0;
//     var w=10;
//     var h=10;
//
//     while(y + w < canvas.width)
//     {
//         if(x + w > canvas.width){
//             x = 0;
//             if(y % (h*2) == 0){
//                 x = w
//             }
//             y += h;
//         }
//         context.fillRect(x, y, w, h);
//         x += w + 10
//     }
// }


// Partie 2
// Global variables
var canvas;
var context;
var nombreBalle = 9;
var balles = [];
var width = 450;
var height = 250;

function rand(a, b)
{
    min = Math.ceil(a);
    max = Math.floor(b);
    return Math.floor(Math.random() * (max - min)) + min;
}

const Balle = class {
    constructor(x, y, rayon, vitessex, vitessey, couleur) {
        this.rayon = rayon;
        this.posX = x;
        this.posY = y;
        this.vitesseX = vitessex;
        this.vitesseY = vitessey;
        this.couleur = couleur;
    }
    move() {
        if(this.posX + this.rayon >= canvas.width || this.posX - this.rayon <= 0){
            this.vitesseX *= -1;
        }
        if(this.posY + this.rayon >= canvas.height || this.posY - this.rayon <= 0) {
            this.vitesseY *= -1;
        }
        this.posX += this.vitesseX;
        this.posY += this.vitesseY;
    }
};

window.onload = function() {
    //init our canvas and graphic context
    canvas = document.getElementById("mon_canvas");
    context = canvas.getContext("2d");  
    for(var i=1; i<=nombreBalle; i++){
        
        var vitx = rand(-10, 10); 
        var vity = rand(-10, 10);
        while (vitx === 0 || vity === 0){
            vitx = rand(-10, 10);
            vity = rand(-10, 10);
        }
        balles.push(new Balle(rand(50, width), rand(50, height), 20, vitx, vity, 'rgb('+rand(0,255)+','+rand(0,255)+','+rand(0,255)+')'));
    }

    // Render the scene
    setInterval(draw, 20);
};

function draw() {
    // Clear
    context.clearRect(0, 0, canvas.width, canvas.height);
    // Draw
    balles.forEach(function(balle){
        balle.move();
        context.beginPath();
        context.arc(balle.posX, balle.posY, balle.rayon, 0, 2 * Math.PI);
        context.closePath();
        context.fillStyle = balle.couleur;
        context.fill();
    });
};