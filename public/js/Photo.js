function redir(){
    //    variable de redirection
    var obj = window.location.replace("info-photo.php?id=" + $("#idsortie").val());
}
        var form = $('#formphoto');
    
        form.submit(function (event) {
            event.preventDefault();
            // spinner.show();
            var formdata = new FormData();
            formdata.append('file', $('input[type=\'file\']').prop('files')[0]);
            formdata.append('idsortie', $('#idsortie').val());
            formdata.append('idadherent', $('#idadherent').val());
    
            $.ajax({
                url: '../../public/pages/traitement-photo.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: formdata,
                type: 'post',
                success: function (response) { 
                    // alert(response);
                    // window.location.replace("info-photo.php?id=" + $("#idsortie").val());
                }
            })
            .done(function(response){
                var toasterOptionCloseButton = toastr.options.closeButton ;
                toastr.options.closeButton = true ;
                if(response && response.substring(0, 11) == "Duplication") {
                    // alert(response);
                    $('#myModalphoto').modal('hide');
                    $('.modal-backdrop').remove();
                    toastr["error"]("L'image existe déjà");
                }
                else  if(response && response.substring(0, 4) == "Size") {
                    // alert(response);
                    $('#myModalphoto').modal('hide');
                    $('.modal-backdrop').remove();
                    toastr["error"]("L'image est trop grande");
                }else  if(response && response.substring(0, 4) == "Type") {
                    // alert(response);
                    $('#myModalphoto').modal('hide');
                    $('.modal-backdrop').remove();
                    toastr["error"]("Ce type d'image n'est pas accepté");
                } else {
                    
                    toastr["success"]("L'importation est terminée.");
                    $('#myModalphoto').modal('hide');
                    $('.modal-backdrop').remove();
                    window.location.replace("info-photo.php?id=" + $("#idsortie").val());
                }
                $("#résultat").html(response);
                toastr.options.closeButton = toasterOptionCloseButton ;
            })
            .fail(function (xhr, status, error) {
                toastr["error"]("Une erreur est survenue : " + error);
            })
            .always(function () {
                // spinner.hide();
            });
        });
    
    
    