var formAffilier = $('#formAffilier');
formAffilier.submit(function (event) {
    var data = formAffilier.serialize();
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "traitement-affiliation.php";
    $.post(urlDest, data)
            .done(function (data) {
                if (data && parseInt(data)) {
                    // On a reçu l'Id de l'item enregistré
                    $("#idAffiliation").val(parseInt(data)) ;
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
                    toastr.options.closeButton = true;
                    toastr["success"]("L'affiliation a été enrengistrée");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();

});
