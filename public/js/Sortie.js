function redir(){
//    variable de redirection
var obj = window.location.replace("info-sortie.php?id=" + $("#idsortie").val());
}

var form = $('#formsortie');
form.submit(function (event) {
    var data = form.serialize();
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "traitement-sortie.php";
    $.post(urlDest, data)   
            .done(function (data) {
//                if (data && data === 'true') {
                if (data && parseInt(data)) {
                    // On a reçu l'Id de l'item enregistré
                    $("#idsortie").val(parseInt(data)) ;
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
// LG 20200419 deac                    toastr.options.timeOut = 0;
                    toastr.options.closeButton = true;
                    toastr["success"]("La sortie a été enrengistrée");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
//                    fonction de redirection (temps en millisecondes)
                    setTimeout(redir,800);
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
//						debugger ;
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();

});
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


