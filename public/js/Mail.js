function redirToReferer(psURL){
    //    variable de redirection
    var obj = window.location.replace(window.urlReferer);
}

var form = $('#formmail');
form.submit(function (event) {
// LG 20220909 début
    var laTo = $("#destinataire").val() ;
    var lsErr = "" ;
    var lbAuMoinsUnOK = false ;
    laTo.forEach(function(lsItem){
        let lsMail = lsItem.split(":")[0] ;
        if (!lsMail) {
            // Cet item n'a pas de mail
            if (lsErr) lsErr += ", " ;
            lsErr += lsItem.split(":")[1] + "\n" ;
        } else {
            lbAuMoinsUnOK = true ;
        }
    }) ;
    if (lsErr) {
        // Certains destinataires n'ont pas de mail
        if (!lbAuMoinsUnOK) {
            alert("Aucun des destinataires demandés ne possède d'adresse mail, il n'est pas possible d'envoyer ce mail.") ;
            event.preventDefault ;       
            return false ;
        } else if ($('input[name="destinatairesVisibles"]').is(":checked")) {
            // Mode "un seul mail"
            alert("Certains destinataires n'ont pas d'adresse mail : " + lsErr + "\nSi vous voulez envoyer ce mail sans corriger la liste, vous devez décocher \"La liste des destinataires est visible à tous\"") ;
            event.preventDefault ;       
            return false ;
        } else {
            if (!confirm("Certains destinataires n'ont pas d'adresse mail : " + lsErr + "\nSouhaitez-vous tout de même envoyer cet e-mail (certains destinataires ne recevront pas le mail) ?")) {
                event.preventDefault ;       
                return false ;
            }
        }
    }
// LG 20220909 fin

    var data = form.serialize();
    var urlSource = window.location.href;
    window.urlReferer = $("#URLreferer").val();
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('?'));
    urlDest = urlDest.substring(0, urlDest.lastIndexOf('/') + 1);
    urlDest += "traitement-mail.php";
    $.post(urlDest, data)
        .done(function (data) {
//                if (data && data === 'true') {
            if (data && parseInt(data)) {
                // On a reçu l'Id de l'item enregistré
                var toasterOptionCloseButton = toastr.options.closeButton;
                var toasterOptionTimeout = toastr.options.timeOut;
                toastr.options.closeButton = true;
                toastr["success"]("Le mail a été envoyé");
                toastr.options.timeOut = toasterOptionTimeout;
                toastr.options.closeButton = toasterOptionCloseButton;
                
                if (window.urlReferer) {
                    setTimeout(redirToReferer, 2000);     // Rediriger vers la page d'appel (temps en millisecondes)
                }
                
            } else {
                toastr.options.timeOut = 0;         // LG 20220909
                toastr["error"]("Une erreur est survenue : " + data);
            }
        })
        .fail(function (error) {
            toastr.options.timeOut = 0;         // LG 20220909
            toastr["error"]("Une erreur est survenue : " + error.responseText);
        });
    event.preventDefault();

});