var canvas;
var context;
var largeurCarre = 20;
var posX = 0;
var posY = 50;
var deplacement = 5;
var deplacementy = 5;

let balle1 = { posX: 0, posY: 10, width:largeurCarre, height: largeurCarre };
let balle2 = { posX: 30, posY: 50, width:largeurCarre, height: largeurCarre };
let balle3 = { posX: 50, posY: 90, width:largeurCarre, height: largeurCarre };
let balle4 = { posX: 70, posY: 110, width:largeurCarre, height: largeurCarre };
// faire une boucle for pour les balles
window.onload = function ()
{ //init our canvas and graphic context 
    canvas = document.getElementById("mon_canvas");
    context = canvas.getContext("2d");
    //Render the scene 
    draw();
    //launch animation 
    setInterval(move, 20);
}
function move() {
    
    if (deplacement == 5) {
        if (posX < (canvas.width - (largeurCarre))) {
            posX += deplacement;
        } 
        else
        {
            posX -= deplacement;
            deplacement = -5;
        }
    } else {
        if (posX > 0) {
            posX += deplacement;
        } else {
            posX -= deplacement;
            deplacement = 5;
        }

    }
        if (deplacementy == 5) {
        if (posY < (canvas.height - (largeurCarre))) {
            posY += deplacementy;
        } 
        else
        {
            posY -= deplacementy;
            deplacementy = -5;
        }
    } else {
        if (posY > 0) {
            posY += deplacementy;
        } else {
            posY -= deplacementy;
            deplacementy = 5;
        }

    }

    draw();
}
function draw() { // 
    //Clear 
    context.clearRect(0, 0, canvas.width, canvas.height);
    //Draw
    context.beginPath();
    context.rect(posX, posY, largeurCarre, largeurCarre);
    context.closePath();
    context.fill();
}
 