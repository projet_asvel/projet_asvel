var form = $('#formfonction');
form.submit(function (event) {
    var data = form.serialize();
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "traitement-fonction.php";
    $.post(urlDest, data)
            .done(function (data) {
//                if (data && data === 'true') {
                if (data && parseInt(data)) {
                    // On a reçu l'Id de l'item enregistré
                    $("#idfonction").val(parseInt(data)) ;
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
// LG 20200419 deac                    toastr.options.timeOut = 0;
                    toastr.options.closeButton = true;
                    toastr["success"]("La fonction a été enrengistrée");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
//						debugger ;
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();

});
        