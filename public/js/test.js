            var form = $('#formadherent');            
             form.submit(function (event) {
             var idAdherent = $("#idAdherent").val();
             var nomAdherent = $("#nomAdherent").val();
             var prenomAdherent = $("#prenomAdherent").val();
             var dateNaissanceAdherent = $("#dateNaissanceAdherent").val();
             var adresseAdherent = $("#adresseAdherent").val();
             var adresse2Adherent = $("#adresse2Adherent").val();
             var mailAdherent = $("#mailAdherent").val();
             var telAdherent = $("#telAdherent").val();
             var cpAdherent = $("#cpAdherent").val();
             var villeAdherent = $("#villeAdherent").val();
             var paysAdherent = $("#paysAdherent").val();
             var nationaliteAdherent = $("#nationaliteAdherent").val();
             var sexeAdherent = $("#sexeAdherent").val();
             var droitImageAdherent = $("#droitImageAdherent").val();
             var licenseAdherent = $("#licenseAdherent").val();
             var refusMailAdherent = $("#refusMailAdherent").val();
             var commentaireAdherent =  $("#commentaireAdherent").val();
             var prenomContact =  $("#prenomContact").val();
             var nomContact =  $("#nomContact").val();
             var adresseContact =  $("#adresseContact").val();
             var adresse2Contact =  $("#adresse2Contact").val();
             var villeContact = $("#villeContact").val();
             var cpContact =  $("#cpContact").val();
             var mailContact =  $("#mailContact").val();
             var telContact =  $("#telContact").val();
             var certifAlpi =  $("#certifAlpi").val();
             var idTypeCertif =  $("#idTypeCertif").val();
             var certifNomMedecin =  $("#certifNomMedecin").val();
             var roles =  $("#roles").val();
             var tags =  $("#tags").val();
             $.post("traitement-test.php", {
                 idAdherent: idAdherent, 
                 nomAdherent : nomAdherent,
                 prenomAdherent : prenomAdherent,
                 dateNaissanceAdherent : dateNaissanceAdherent,
                 adresseAdherent : adresseAdherent,
                 adresse2Adherent : adresse2Adherent,
                 mailAdherent : mailAdherent,
                 telAdherent : telAdherent,
                 cpAdherent : cpAdherent,
                 villeAdherent :villeAdherent,
                 paysAdherent : paysAdherent,
                 nationaliteAdherent : nationaliteAdherent,
                 sexeAdherent : sexeAdherent,
                 droitImageAdherent : droitImageAdherent,
                 licenseAdherent : licenseAdherent,
                 refusMailAdherent : refusMailAdherent,
                 villeContact : villeContact,
                 commentaireAdherent : commentaireAdherent,
                 prenomContact : prenomContact,
                 nomContact : nomContact,
                 adresseContact : adresseContact,
                 adresse2Contact : adresse2Contact,
                 cpContact : cpContact,
                 mailContact : mailContact,
                 telContact : telContact,
                 certifAlpi : certifAlpi,
                 idTypeCertif : idTypeCertif,
                 certifNomMedecin : certifNomMedecin,                
                 roles : roles,
                 tags : tags
                 
                 
             })
                        .done(function (data) {
                            if (data && data === 'true') {
                                var toasterOptionCloseButton = toastr.options.closeButton;
                                var toasterOptionTimeout = toastr.options.timeOut;
// LG 20200419 deac                                toastr.options.timeOut = 0;
                                toastr.options.closeButton = true;
                                toastr["success"]("L'adhérent a bien été ajouté");
                                toastr.options.timeOut = toasterOptionTimeout;
                                toastr.options.closeButton = toasterOptionCloseButton;
                            } else {
                                toastr["error"]("Une erreur est survenue : " + data);
                            }
                        })
                        .fail(function (error) {
//						debugger ;
                            toastr["error"]("Une erreur est survenue : " + error.responseText);
                        });
                event.preventDefault();

            });
        