<?php
// Page d'édition d'un adhérent avec uniquement les informations de base, pour saisie rapide par un encadrant pour inscription à une sortie
// Page qui s'ouvre dans une autre fenêtre : pas besoin d'ent-têtes
require_once 'info-adherent-prepare.php';
?>

<div class="modal fade" id="modalInfoAdherent" tabindex="-1" role="dialog" aria-labelledby="modalInfoAdherentLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLoginLabel">Ajouter un adhérent pour l'inscrire à la sortie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="float-left col-12" style="color: red; padding-top: 15px; padding-bottom: 15px;">
                Attention, cette fonctionnalité est destinée à créer un nouvel adhérent dans l'urgence afin de pouvoir l'inscrire à une sortie.<br>
                <strong>Elle ne doit être urilisée qu'à titre exceptionnel.</strong>
            </div>
            <form action="" method="POST" id ="formAdherentModal">
                <div class="card">
                    <main id="main" role="main" class="flex-item-fluid pam card-body">
<!--                        <table class="table table-striped">-->
                            <input type="hidden" name="idAdherent" id="idAdherent" value="<?= $idAdherent ?>">
                            <fieldset style="margin-bottom: 40px;">
                                <?php
                                // Imformations générales : dans un fichier à part pour pouvoir être utitlisé ailleurs
                                require_once 'info-adherent-général.php';
                                ?>
                            </fieldset>
                            <div class="card-footer text-center">
                                <input type="submit" class="btn btn-primary"/>
                            </div>
<!--                        </table>-->
                    </main>
                    <aside class="mod w20 pam aside">
                    </aside>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="../js/Adherent.js?v=<?=VERSION_APP ?>"></script>


