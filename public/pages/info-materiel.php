<?php
require_once '../../config/globalConfig.php';

use App\Materiel;
use App\Repository\MaterielRepository;

use App\TypeMiseAdispo;
use \App\Repository\TypeMiseAdispoRepository;
// \App\Entity\TypeMiseAdispo

use App\TypeMateriel;
use \App\Repository\TypeMaterielRepository;


$idAdherent = $_SESSION['idadherent'];

$idMateriel = filter_input(INPUT_GET, 'idMateriel', FILTER_SANITIZE_STRING);

$materiel = null;

if($idMateriel != "")
{
    $modele_Materiel = new MaterielRepository();

    $materiel = $modele_Materiel->getEntityId($idMateriel);
}



$modele_TypeMiseAdispo = new TypeMiseAdispoRepository();

$allMiseADispo = $modele_TypeMiseAdispo->getAll();

$modele_TypeMateriel = new TypeMaterielRepository();

$allTypeMateriel = $modele_TypeMateriel->getAll();

if($materiel != null)
{
    $datetime = new DateTime($materiel->getDateAchat());

    $date = $datetime->format('Y-m-d');

    $idMateriel = $materiel->getIdMateriel();

    $modele = $materiel->getModele();

    $iIdTypeMateriel = $materiel->getIdTypeMateriel();

    $iIdTypeMiseAdispo = $materiel->getIdTypeMiseAdispo();

    $taille = $materiel->getTaille();

    $pointure = $materiel->getPointure();

    $pays = $materiel->getPays();

    $longitude1 = $materiel->getLongitude1();

    $latitude1 = $materiel->getLatitude1();

    $longitude2 = $materiel->getLongitude2();

    $latitude2 = $materiel->getLatitude2();

    $commentaire = $materiel->getCommentaire();
    

    if ($materiel->getIdTypeMateriel() == "-1")
    {
        $ski = "visibility:hidden; height:0px";

        $chaussure = "visibility:hidden; height:0px";

        $carte = "visibility:visible; height:100%";
    }
    else if ($materiel->getIdTypeMateriel() == "1" || $materiel->getIdTypeMateriel() == "3" || $materiel->getIdTypeMateriel() == "5")
    {
        $ski = "visibility:visible; height:100%";

        $chaussure = "visibility:hidden; height:0px";

        $carte = "visibility:hidden; height:0px";
    }
    else if ($materiel->getIdTypeMateriel() == "2" || $materiel->getIdTypeMateriel() == "4" || $materiel->getIdTypeMateriel() == "6" || $materiel->getIdTypeMateriel() == "7" || $materiel->getIdTypeMateriel() == "8")
    {
        $ski = "visibility:hidden; height:0px";

        $chaussure = "visibility:visible; height:100%";

        $carte = "visibility:hidden; height:0px";
    }
}
else 
{
    $date = "";

    $idMateriel = "";

    $modele = "";

    $iIdTypeMateriel = "";

    $iIdTypeMiseAdispo = "";

    $taille = "";

    $pointure = "";

    $pays = "";

    $longitude1 = "";

    $latitude1 = "";

    $longitude2 = "";

    $latitude2 = "";

    $commentaire = "";

    $ski = "visibility:hidden; height:0px";

    $chaussure = "visibility:hidden; height:0px";

    $carte = "visibility:hidden; height:0px";
}

// var_dump($allMiseADispo);

$valider = true;

?>

<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action="traitement-materiel.php" method="POST" id="form_materiel">
            <input type="hidden" name="idAdherent" value="<?= $idAdherent ?>">
            <input type="hidden" name="idMateriel" value="<?= $idMateriel ?>">
            <div class="container col-10 py-4">
                <div class="row py-2">
                    <div class="col-1">
                        <a class="" href="liste-materiel_perso.php"  >
                            <button type='button' class='btn btn-secondary'><i class='fa fa-backward'></i> Retour</button>
                        </a>
                    </div>
                </div>   
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Matériel</h4>
                    </div>
                    <div class="card-body">
                    
                        <div class="row py-2">
                            <div class="col-6">
                                <label for="modele">Modèle :</label>
                                <input type="text" class="form-control" required="true" id="modele" name="modele" placeholder="Modèle" value="<?= $modele ?>">
                            </div>
                            <div class="col-6">
                                <label for="dateAchat">Date d'achat</label>
                                <input type="date" class="form-control" id="dateAchat" name="dateAchat" max="<?php echo date("Y-m-d") ?>" value="<?= $date ?>">
                            </div>
                        </div>   

                        <div class="row py-2">
                            <div class="col-6">
                                <label for="modele">Type de matériel :</label>
                                <select class="custom-select" name="typeMateriel" id="typeMateriel" onchange="changerType(this.value)" required>
                                    <option value="">Séléctionner un type de matériel</option>
                                    <?php
                                        foreach($allTypeMateriel as $typeMateriel)
                                        {
                                            $selectType = "";
                                            if($iIdTypeMateriel == $typeMateriel->getIdTypeMateriel())
                                            {
                                                $selectType = "selected";
                                            }
                                            ?>
                                                <option value="<?php echo $typeMateriel->getIdTypeMateriel() ?>" <?= $selectType ?>><?php echo $typeMateriel->getNomType() ?></option>
                                            <?php
                                        }
                                    ?>
                                    
                                </select>
                            </div>
                            <div class="col-6">
                                <label for="modele">Disponibilité :</label>
                                <select class="custom-select" name="typeMiseAdispo" required id="typeMiseAdispo" >
                                    <option value="">Séléctionner une disponibilité</option>
                                    <?php
                                        foreach($allMiseADispo as $miseADispo)
                                        {
                                            if($miseADispo->getIdTypeMiseAdispo() != 2 and $idMateriel == "")
                                            {
                                                ?>
                                                    <option value="<?php echo $miseADispo->getIdTypeMiseAdispo() ?>"><?php echo $miseADispo->getNomMiseAdispo() ?></option>
                                                <?php
                                            }
                                            else if($idMateriel != "")
                                            {
                                                $selectDispo = "";
                                                if($iIdTypeMiseAdispo == $miseADispo->getIdTypeMiseAdispo())
                                                {
                                                    $selectDispo = "selected";
                                                }

                                                ?>
                                                    <option <?= $selectDispo ?> value="<?php echo $miseADispo->getIdTypeMiseAdispo() ?>"><?php echo $miseADispo->getNomMiseAdispo() ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                    
                                </select>
                            </div>
                        </div>  

                        <div id="ski" style="<?= $ski ?>">
                            <div class="row py-2" >
                                <div class="col-12">
                                    <label for="taille">Taille :</label>
                                    <input type="number" step="any" class="form-control" id="taille" name="taille" value="<?= $taille ?>">
                                </div>
                            </div>
                        </div>

                        <div id="chaussure" style="<?= $chaussure ?>">
                            <div class="row py-2" >
                                <div class="col-12">
                                    <label for="pointure">Pointure :</label>
                                    <input type="text" class="form-control" id="pointure" name="pointure" value="<?= $pointure ?>">
                                </div>
                            </div>
                        </div>

                        <div id="carte" style="<?= $carte ?>">
                            <div class="row py-2" >
                                <div class="col-12">
                                    <label for="pays">Pays :</label>
                                    <input type="text" class="form-control" id="pays" name="pays" value="<?= $pays ?>">
                                </div>
                            </div>
                            <div class="row py-2" >
                                <div class="col-6">
                                    <label for="longitude1">Longitude1 :</label>
                                    <input type="number" step="any" class="form-control" id="longitude1" name="longitude1" value="<?= $longitude1 ?>">
                                </div>
                                <div class="col-6">
                                    <label for="latitude1">Latitude1</label>
                                    <input type="number" step="any" class="form-control" id="latitude1" name="latitude1" value="<?= $latitude1 ?>">
                                </div>
                            </div>
                            <div class="row py-2" >
                                <div class="col-6">
                                    <label for="longitude2">Longitude2 :</label>
                                    <input type="number" step="any" class="form-control" id="longitude2" name="longitude2" value="<?= $longitude2 ?>">
                                </div>
                                <div class="col-6">
                                    <label for="latitude2">Latitude2</label>
                                    <input type="number" step="any" class="form-control" id="latitude2" name="latitude2" value="<?= $latitude2 ?>">
                                </div>
                            </div>
                        </div>


                        <div class="row py-2">
                            <div class="col-12">
                                <label for="modele">Commentaire :</label>
                                <textarea class="form-control" id="commentaire" name="commentaire" ><?= $commentaire ?></textarea>
                            </div>
                        </div>   

                           
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script>
        function changerType(val)
		{
			if (val == "-1")
			{
				document.getElementById("ski").style.visibility = "hidden";
				document.getElementById("ski").style.height = "0px";

                document.getElementById("chaussure").style.visibility = "hidden";
				document.getElementById("chaussure").style.height = "0px";

                document.getElementById("carte").style.visibility = "visible";
				document.getElementById("carte").style.height = "100%";

                document.getElementById("taille").value = "";
                document.getElementById("pointure").value = "";
			}
			else if (val == "1" || val == "3" || val == "5")
			{
				document.getElementById("ski").style.visibility = "visible";
				document.getElementById("ski").style.height = "100%";

                document.getElementById("chaussure").style.visibility = "hidden";
				document.getElementById("chaussure").style.height = "0px";

                document.getElementById("carte").style.visibility = "hidden";
				document.getElementById("carte").style.height = "0px";

                document.getElementById("pays").value = "";
                document.getElementById("longitude1").value = "";
                document.getElementById("latitude1").value = "";
                document.getElementById("longitude2").value = "";
                document.getElementById("latitude2").value = "";
                document.getElementById("pointure").value = "";
			}
            else if (val == "2" || val == "4" || val == "6" || val == "7" || val == "8")
			{
				document.getElementById("ski").style.visibility = "hidden";
				document.getElementById("ski").style.height = "0px";

                document.getElementById("chaussure").style.visibility = "visible";
				document.getElementById("chaussure").style.height = "100%";

                document.getElementById("carte").style.visibility = "hidden";
				document.getElementById("carte").style.height = "0px";

                document.getElementById("pays").value = "";
                document.getElementById("longitude1").value = "";
                document.getElementById("latitude1").value = "";
                document.getElementById("longitude2").value = "";
                document.getElementById("latitude2").value = "";
                document.getElementById("taille").value = "";
			}
			else 
			{
                document.getElementById("ski").style.visibility = "hidden";
				document.getElementById("ski").style.height = "0px";

                document.getElementById("chaussure").style.visibility = "hidden";
				document.getElementById("chaussure").style.height = "0px";

				document.getElementById("carte").style.visibility = "hidden";
				document.getElementById("carte").style.height = "0px";

                document.getElementById("pays").value = "";
                document.getElementById("longitude1").value = "";
                document.getElementById("latitude1").value = "";
                document.getElementById("longitude2").value = "";
                document.getElementById("latitude2").value = "";
                document.getElementById("taille").value = "";
                document.getElementById("pointure").value = "";
			}
		}	

    </script>

</html>
