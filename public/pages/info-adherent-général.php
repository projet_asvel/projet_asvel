<?php
// Partie des informations générales concernant un adhérent
?>

<div class="float-left col-md-6 col-12">
    <div class="form-group">
        <label class="control-label">Nom</label>
        <div class="inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="nomAdherent" name="nomAdherent" placeholder="nom" class="form-control" required="true" value="<?= $adherentedit->getNomAdherent(); ?>" type="text" <?= $disabled ?> maxlength="50">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label">Prénom</label>
        <div class="inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="prenomAdherent" name="prenomAdherent" placeholder="Prenom" class="form-control" required="true" value="<?= $adherentedit->getPrenomAdherent(); ?>" type="text" <?= $disabled ?> maxlength="50"></div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label">Adresse Mail</label>
        <div class="inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input id="mailAdherent" name="mailAdherent" placeholder="Adresse mail" class="form-control" value="<?= $adherentedit->getMailAdherent(); ?>" type="mail" <?= $disabled ?> maxlength="50"></div>
        </div>
    </div>
</div>
<div class="float-right col-md-6 col-12">
    <div class="form-group">
        <label class="control-label">Numéro de Téléphone</label>
        <div class="inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                <input id="telAdherent" name="telAdherent" placeholder="Numéro de téléphone" class="form-control" required="true" value="<?= $adherentedit->getTelAdherent(); ?>" type="tel" <?= $disabled ?> maxlength="50"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label">Tags</label>
        <div class="input-group">
            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
            <select id="select2-TAGS" class="js-example-basic-multiple" name="tags[]" multiple="multiple" style="width: 100%" <?= $TAGSdisabled ?> maxlength="50">
                <?php
                $tags = $adherentedit->getTags();
                foreach ($Tags as $value) {
                    $selected = "";
                    foreach ($tags as $tag) {
                        if ($tag->getIdTag() == $value->getIdTag()) {
                            // Ce role est dans la liste des rôles de l'adhérent en cours de chargement
                            $selected = ' selected = "selected"';
                        }
                    }
                    echo '<option value="' . $value->getIdTag() . '"' . $selected . '>' . $value->getNomTag() . "</option>";
                }
// LG 20200214 fin
                ?>
            </select>
        </div>
    </div>

    <div class="form-group float-left col-md-6 col-12" style="padding-left: 0;">
        <label class="control-label" for="certifAlpi">Certificat médical alpi</label>
        <div class="inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>                                        
                <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" name="certifAlpi" id="certifAlpi" <?= $adherentedit->getCertifAlpi() ? "checked" : "" ?> <?= $disabled ?> maxlength="50">
                <label></label>
            </div>
        </div>
    </div>
    <div class="form-group float-right col-md-6 col-12" style="padding-right: 0;">
        <label class="control-label" for="droitImageAdherent">Droit à l'image</label>
        <div class="inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" name="droitImageAdherent" id="droitImageAdherent"  <?= $adherentedit->getDroitImageAdherent() ? "checked" : "" ?> <?= $disabled ?> maxlength="50">
                <label></label>
            </div>
        </div>
    </div>

</div>
<?PHP
if ($adherentedit->getIdAdherent()) {
    // Adhérent pré-existant : afficher les affiliations actuelles
?>
<div class="form-group float-left col-12">
    <label class="control-label" for="Affiliations">Affiliations</label>
    <div class=" inputGroupContainer">
        <div class="input-group" id="Affiliations">
            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
            <?php
            $listeAffiliations = $adherentedit->getAffiliations();
            echo '<span type="text" class="form-control" name="federations" style="background-color: lightgray;height: auto;max-height: 150px;overflow: auto;">';
            $txtAffiliationsEnCours = "";
            $txtAffiliationsPassées = "";
            foreach ($listeAffiliations as $value) {
                $dateFin = $value->getDateFinAffiliation() ;
                if (strtotime($dateFin) >= strtotime(date("Y-m-d"))) {
                    $variable = "txtAffiliationsEnCours" ;
//                    // Affiliation en cours
//                    if ($txtAffiliationsEnCours)
//                        $txtAffiliationsEnCours .= ", ";
//                    $txtAffiliationsEnCours .= "<a href='info-affiliation.php?idAffiliation=" . $value->getIdAffiliation() . "'>"
//                            . $value->getFederation()->getNomFederation() . "</a>"
//                            . " (depuis le " . date('d/m/Y', strtotime($value->getDateAffiliation()))
//                            . " jusqu'au " . date('d/m/Y', strtotime($dateFin))
//                            . ")";
                } else {
                    $variable = "txtAffiliationsPassées" ;
                }
                                    // Affiliation en cours
                if ($$variable)
                    $$variable .= ", ";
                $$variable .= "<a href='info-affiliation.php?idAffiliation=" . $value->getIdAffiliation() . "'>"
                        . $value->getFederation()->getNomFederation() . "</a>"
                        . " (depuis le " . date('d/m/Y', strtotime($value->getDateAffiliation()))
                        . " jusqu'au " . date('d/m/Y', strtotime($dateFin))
                        . ")";

            }
            if ($txtAffiliationsEnCours) echo "Actives : " . $txtAffiliationsEnCours ;
            if ($txtAffiliationsPassées) {
                if ($txtAffiliationsEnCours) echo "<br>" ;
                echo "<span style='text-decoration: strike'>Passées : " . $txtAffiliationsPassées . "</span>" ;
            }
            echo "</span>";
            ?>
        </div>
    </div>
</div>
<?PHP
} else {
    // Nouvel adhérent : permettre la saisie d'une affiliation
    require_once 'info-affiliation-prepare.php' ;
    echo '<div class="card-body"><div class="float-left col-12" style="border-width: 1px; border-style: solid; border-color: gray; margin-bottom: 10px;">' ;
    $masquedherent = true ;
    require_once 'info-affiliation-formfields.php' ;
    echo '</div></div>' ;
}
?>

<!--    </div>-->

