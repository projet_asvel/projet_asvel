<?php

require_once '../../config/globalConfig.php';

use App\Entity\Photo;
use App\Repository\PhotoRepository;

$idPhoto = filter_input(INPUT_POST, 'idphoto', FILTER_SANITIZE_STRING);

$chemin = filter_input(INPUT_POST, 'chemin', FILTER_SANITIZE_STRING);

$photoRep = new PhotoRepository();

$photoRep->deleteEntityId($idPhoto);

unlink("../photo_sortie/$chemin");