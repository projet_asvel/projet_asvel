<?php
require_once '../../config/globalConfig.php';

// header('Content-Type: text/html; charset=UTF-8');       // LG 20220729


use App\Repository\AdherentRepository;
use App\Entity\Adherent;
use App\Security;

if (!Security::hasRole([Security::ROLE_SECRETAIRE])) {
	header("location: Accessdenied.php");
}

// Vérifications du fichier à importer
$errfile = false;
if (!isset($_FILES['file'])) {
	$errfile = "Aucun fichier n'a été choisi" ;
} else if ($_FILES['file']['error'] > 0 && $_FILES['file']['error'] != 4) {
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_INI_SIZE:
            $errfile = "Le fichier dépasse la taille maximale par PHP (" . ini_get('upload_max_filesize') . ")." ;
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $errfile = "Le fichier dépasse la taille maximale du formulaire(" . ini_get('post_max_size') . ")." ;
            break;
        case UPLOAD_ERR_PARTIAL:
            $errfile = "Le fichier est transféré partiellement." ;
            break;
        case UPLOAD_ERR_EXTENSION:
            $errfile = "Une erreur d'extension s'est produite lors de l'importation du fichier." ;
            break;
		default:
//        case UPLOAD_ERR_NO_TMP_DIR:
//        case UPLOAD_ERR_CANT_WRITE:
            $errfile = "Une erreur inconnue s'est produite lors de l'importation du fichier." ; 			
    }
}

// Récupération du fichier
if (!$errfile) {
//	$filePath = App\Lib\Functions::getPath() . "files/" . $_FILES['file']['name'];
	$filePath = "../../tmp/" . $_FILES['file']['name'];
    if ($_FILES['file']['error'] == 0) {
        $move = move_uploaded_file($_FILES['file']['tmp_name'], $filePath);
        if (!$move) {
            $errfile = "<br>".$filePath."<br>". $_FILES['file']['tmp_name'] ."<br> : Impossible de déplacer le fichier vers le répertoire de destination" ;
        }
    } else {
        $errfile = "Erreur de téléchargement" ;
    }
}

// Ouverture du fichier d'importation téléchargé
if ($errfile) {
} else if (($handle = fopen($filePath, "r")) == FALSE) {
	$errfile = "Erreur d'ouverture du fichier téléchargé" ;
} else if (($handle) == NULL) {
	$errfile = "Erreur d'ouverture du fichier téléchargé : NULL" ;
}
if ($errfile) {
	echo "Echec de l'importation : " . $errfile ;
	return false ;
}

$lsSeparateur = ";" ;
//$lsSeparateur = "\t" ;

// Ligne d'en-tête et détection du séparateur
//$dataEnTetes = fgetcsv($handle, 1000, $lsSeparateur) ;
$ligne = fgets($handle, 1000);
$liNbTabs = substr_count($ligne, "\t") ;
$liNbVirgules = substr_count($ligne, ",") ;
$liNbPointsVirgules = substr_count($ligne, ";") ;
$liNbMax = max([$liNbTabs, $liNbVirgules, $liNbPointsVirgules]) ;
if ($liNbMax == $liNbTabs) {
	$lsSeparateur = "\t" ;
} else if ($liNbMax == $liNbVirgules) {
	$lsSeparateur = "," ;
} else {
	$lsSeparateur = ";" ;
}
$dataEnTetes = explode($lsSeparateur, $ligne) ;
//echo $liNbTabs . "<br>" ;
//echo $liNbVirgules . "<br>" ;
//echo $liNbPointsVirgules . "<br>" ;
//echo $lsSeparateur . "<br>" ;
//var_dump($dataEnTetes) ;
//return false ;

// Ouverture du fichier modèle de stucture
$lsTypeImport = isset($_POST["typeImport"])?$_POST["typeImport"]:"FFMEZZ" ;
$fichStru = "StructureImport" . $lsTypeImport . ".csv" ;
$pathStru = "../../config/" . $fichStru ;
if (($handleStru = fopen($pathStru, "r")) == FALSE) {
	$errfile = "Erreur d'ouverture du fichier de modèle de structure " . $fichStru ;
} else if (($handle) == NULL) {
	$errfile = "Erreur d'ouverture du fichier de modèle de structure : NULL" ;
}
if ($errfile) {
	echo "Echec de l'importation : " . $errfile ;
	return false ;
}

$laStructureImport = [] ;
// En-Tête de colonnes : 1ere ligne
$dataStruEnTetes = fgetcsv($handleStru, 1000, ";") ;
if (count($dataStruEnTetes) !== count($dataEnTetes)) {
	echo "Echec de l'importation : <br>" ;
	echo "Le nombre de colonnes de données (" . count($dataEnTetes) . ")"
                . " est différent du nombre de colonnes du modèle (" . count($dataStruEnTetes) . ")"
                . ": mettez à jour le modèle (" . $fichStru . "), ou choisissez-en un autre.<br>" ;
//	var_dump($dataStruEnTetes) ;
//	var_dump($dataEnTetes) ;
	return false ;
}
// Signification des colonnes : 2eme ligne
$dataStruFlds = fgetcsv($handleStru, 1000, ";") ;
$liCol = 0 ;
foreach ($dataStruEnTetes as $struEnTete) {
	if (trim($struEnTete) !== trim($dataEnTetes[$liCol])) {
		// Cet en-tête de colonne diffère de celle du modèle de structure
		echo "Echec de l'importation : <br>" ;
		echo "Le fichier de données ne correspond pas au modèle : mettez à jour le modèle (" . $fichStru . "), ou choisissez-en un autre.<br>" ;
		echo "La colonne N°" . ($liCol+1) . " contient \"" . $dataEnTetes[$liCol] . "\" et non \"" . $struEnTete . "\" comme dans le modèle.<br>" ;
		return false ;
	}
	$struFld = $dataStruFlds[$liCol] ;
	$laStructureImport[$liCol] = $struFld ;
	$liCol++ ;
}

$repo = new AdherentRepository() ;

$laEntitiesExtraites = [];
$liNbEntitésOK = 0 ;
$liNbEntitésDouteuses = 0 ;
$liNbEntitésKO = 0 ;
$liNbErreurs = 0 ;
$lstErreurs = "" ;
$liRow = 1;
while (($dataCSV = fgetcsv($handle, 1000, $lsSeparateur)) !== FALSE) {	
    $liRow++ ;
    $msgAdherent = "" ;
    $data = [];
    $liCol = -1 ;

    foreach ($laStructureImport as $lsFld) {
            $liCol++;
            $lcErrRow = "" ;
            if ($lsFld) {
                if (!isset($dataCSV[$liCol])) {
                    $lcErrRow = "Ligne " . $liRow . ", colonne " . $liCol . " inexistante<br>" ;
                    $msgAdherent .= $lcErrRow ;
                    break ;
                }
                    // Cette colonne est importable
// LG 20220729 début
//                    $data[$lsFld] = iconv("ISO-8859-1", "UTF-8//TRANSLIT", $dataCSV[$liCol])?:Null ;
                    $data[$lsFld] = $dataCSV[$liCol] ;
                    $lsEncoding = mb_detect_encoding($data[$lsFld], ['UTF-8', 'ISO-8859-1'], false) ;
                    if ($lsEncoding != "UTF-8") {
                        // La détection automatique n'a pas trouvé de l'UTF/8 : transtyper
                        $data[$lsFld] = iconv("ISO-8859-1", "UTF-8//TRANSLIT", $data[$lsFld])?:Null ;
                    }
// LG 20220729 fin

                    $data[$lsFld] = filter_var($data[$lsFld], FILTER_SANITIZE_STRING) ;        // Eviter les caractères spéciaux comme le simple quote
                    if (substr($lsFld, 0, 3) == 'tel') {
                            $data[$lsFld] = str_replace(" ", "", $data[$lsFld]);
                            $data[$lsFld] = str_replace(".", "", $data[$lsFld]);
                            $data[$lsFld] = str_replace(",", "", $data[$lsFld]);
                   }
            }
    }
    if ($lcErrRow) {
        // Erreur en cours de lecture de la ligne
        continue ;
    }

    // Corrections des données
    if ($data["telAdherent"] && $data["telAdherent2"]) {
        // Cet adhérent a deux N°s de téléphone : on devrait avertir qu'un seul sera conservé
        $msgAdherent .= "<li>Cet adhérent possède deux numéros de tel. : un seul a été conservé (" . $data["telAdherent"] . ").</li>" ;
    } else if ($data["telAdherent2"]) {
        // Cet adhérent n'a que ne N° de tel 2 : le mettre en N° de tel ppal
        $data["telAdherent"] = $data["telAdherent2"] ;
    }
    if (!$data["telAdherent"]) {
        $msgAdherent .= "<li>Le numéro de téléphone de l'adhérent est vide.</li>" ;
        $data["telAdherent"] = NULL ;
    } else if (strlen($data["telAdherent"]) > 20) {
        $corr = substr($data["telAdherent"], 0, 20) ;
        $msgAdherent .= "<li>Le numéro de téléphone (" . $data["telAdherent"] . ") est trop long, il a été tronqué (" . $corr . "). </li>" ;
        $data["telAdherent"] = $corr ;
    }
    if (isset($data["telContact"]) && strlen($data["telContact"]) > 20) {
        $corr = substr($data["telContact"], 0, 20) ;
        $msgAdherent .= "<li>Le numéro de téléphone du contact (" . $data["telContact"] . ") est trop long, il a été tronqué (" . $corr . ").</li>" ;
        $data["telContact"] = $corr ;
    }
    if (strlen($data["cpAdherent"]) > 5) {
        $corr = substr($data["cpAdherent"], 0, 5) ;
        $msgAdherent .= "<li>Le code postal (" . $data["cpAdherent"] . ") est trop long, il a été tronqué (" . $corr . ").</li>" ;
        $data["cpAdherent"] = $corr ;
    }
    if (isset($data["cpContact"]) && strlen($data["cpContact"]) > 5) {
        $corr = substr($data["cpContact"], 0, 5) ;
        $msgAdherent .= "<li>Le code postal du contact (" . $data["cpContact"] . ") est trop long, il a été tronqué (" . $corr . ").</li>" ;
        $data["cpContact"] = $corr ;
    }
    if (!$data["mailAdherent"]) {
        $msgAdherent .= "<li>L'adresse mail de l'adhérent est vide, il ne pourra pas se connecter au site.</li>" ;
        $data["mailAdherent"] = NULL ;
    }
    if (!$data["dateNaissanceAdherent"]) {
        $data["dateNaissanceAdherent"] = NULL ;
    } else if (!App\Lib\Functions::string_is_date($data["dateNaissanceAdherent"], $dateNaissanceISO)) {
        $msgAdherent .= "<li>La date de naissance de l'adhérent n'a pas un format correct.</li>" ;
        
    } else {
        // Convertir la date au format ISO
        $data["dateNaissanceAdherent"] = $dateNaissanceISO ; ;
    }
    if (!$data["dateLicense"]) {
        $data["dateLicense"] = NULL ;
    } else if (!App\Lib\Functions::string_is_date($data["dateLicense"], $dateNaissanceISO)) {
        $msgAdherent .= "<li>La date de la license de l'adhérent n'a pas un format correct.</li>" ;
        
    } else {
        // Convertir la date au format ISO
        $data["dateLicense"] = $dateNaissanceISO ; ;
    }
// LG 20220729 début
    if (isset($data["sexeAdherent"]) && strlen($data["sexeAdherent"]) > 1) {
        $corr = substr($data["sexeAdherent"], 0, 1) ;
        $data["sexeAdherent"] = $corr ;
    }
// LG 20220729 fin

// LG 20201010 début
    if (isset($data["groupe"]) && $data["groupe"]) {
		// Ajouter les groupes à la liste des tags (cas FFME)
		$data["groupe"] = "_gpe" . trim($data["groupe"]) ;
		if ($data["tags"]) {
			$data["tags"] .= ", " . $data["groupe"] ;
		} else {
			$data["tags"] = $data["groupe"] ;
		}
    }	
// LG 20220224 old    if ($data["tags"]) {
    if (isset($data["tags"]) && $data["tags"]) {
		// Enlever les caractères spéciaux et en particulier les espaces, qui gênent le filtrage des adhérents par tag
		$data["tags"] = str_replace("&#39;", "'", $data["tags"]) ;	// Cas particulier de l'apostrophe
        $data["tags"] = preg_replace("/[^[:alnum:],']/u", '-', $data["tags"]);
		while (strpos($data["tags"], "--")) {
			$data["tags"] = str_replace("--", "-", $data["tags"]) ;
		}
    }	
// LG 20201010 fin
	
    if ($lsTypeImport==="FFME") {
        // FFME
        $data["nomFederationLicense"] = "FFME" ;
    } else if ($lsTypeImport==="FFSLoisirs") {
        // FFS loisirs
        $data["nomFederationLicense"] = "FFS" ;
        $data["nomTypeLicense"] = "Loisirs" ;
    } else if ($lsTypeImport==="FFSCompDir") {
        // FFS dompétiteurs/dirigeants
        $data["nomFederationLicense"] = "FFS" ;
        $data["nomTypeLicense"] = "Compétition" ;
    } else {
        // Non prévu
    }

    $adherent = new Adherent($data) ;
    $tabAdherentsConfondants = [] ;
    try {
// LG 20220729 début
//        $res = $repo->retrouveAdherentImporté($adherent, $tabAdherentsConfondants);
        $lbAdherentNouveau = false ;
        $res = $repo->retrouveAdherentImporté($adherent, $tabAdherentsConfondants, $lbAdherentNouveau);
// LG 20220729 fin
    } catch (Exception $ex) {
        $liNbErreurs++ ;
        $res = null ;
        $err = "<li>Erreur sur la ligne " .  ($liRow) . " (" . $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() . ") : " . $ex->getMessage() . "</li>" ;
        $lstErreurs .= $err ;
        $msgAdherent .= $err ;
    }

    if ($res && !$tabAdherentsConfondants) {
        // Cet adhérent est ancien, ses informations ont été fusionnées avec celles en cours d'importation
        // ou bien il est nouveau
// LG 20220729 old        $laEntitiesExtraites[] = Array("row"=>$liRow, "statut"=>"OK", "importé"=>$adherent, "confondants"=>[], "messages"=>$msgAdherent);
        $laEntitiesExtraites[] = Array("row"=>$liRow, "nouveau"=>$lbAdherentNouveau, "statut"=>"OK", "importé"=>$adherent, "confondants"=>[], "messages"=>$msgAdherent);
        $liNbEntitésOK++ ;
    } else if ($res) {
        // Cet adhérent peut être mis en relation avec un autre, mais sans certitude
// LG 20220729 old        $laEntitiesExtraites[] = Array("row"=>$liRow, "statut"=>"DOUTEUX", "importé"=>$adherent, "confondants"=>$tabAdherentsConfondants, "messages"=>$msgAdherent);
        $laEntitiesExtraites[] = Array("row"=>$liRow, "nouveau"=>$lbAdherentNouveau, "statut"=>"DOUTEUX", "importé"=>$adherent, "confondants"=>$tabAdherentsConfondants, "messages"=>$msgAdherent);
        $liNbEntitésDouteuses++ ;
    } else {
        // Cet adhérent n'a pas pu être importé
// LG 20220729 old        $laEntitiesExtraites[] = Array("row"=>$liRow, "statut"=>"KO", "importé"=>$adherent, "confondants"=>$tabAdherentsConfondants, "messages"=>$msgAdherent);
        $laEntitiesExtraites[] = Array("row"=>$liRow, "nouveau"=>$lbAdherentNouveau, "statut"=>"KO", "importé"=>$adherent, "confondants"=>$tabAdherentsConfondants, "messages"=>$msgAdherent);
        $liNbEntitésKO++ ;
    }
}
// Fermer le fichier d'importation
fclose($handle);

// Afficher le résultat
$lsImportsOK = "" ;
$lsImportsDouteux = "" ;
$lsImportsKO = "" ;
$liItem = 0 ;
foreach($laEntitiesExtraites as $laEntité) {
    $adherent = $laEntité["importé"] ;
    $lsImportMsg = "" ;
    
    // Parcourir les confondants pour les inclure dans le texte du message
    $laConfondants = $laEntité["confondants"] ;
    $lsConfondants = "" ;
    $laConfondantsTraités = [] ;
    foreach($laConfondants as $laConfondant) {
        $loConfondant = $laConfondant["adherent"] ;
        if ($loConfondant == null) {
            // Ce n'est pas un confondant mais une erreur de saisie de l'adherent
            $tabErr = $laConfondant["commentaire"] ;
//if ($adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() == "PALISSE Françoise") {
//    echo $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() . " : " . $laEntité["messages"] . "<br>";
//}

            foreach($tabErr as $err) {
                $laEntité["messages"] .= "<li>" . $err . "</li>" ;
//if ($adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() == "PALISSE Françoise") {
//    echo $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() . " : " . $laEntité["messages"] . "<br>";
//}
            }
            continue ;
        }
        
        $liIdConfondant = $loConfondant->getIdAdherent() ;
        if (in_array(strval($liIdConfondant), $laConfondantsTraités)) {
            // On a déja traité cet item : passer au suivant
            continue ;
        }
        $laConfondantsTraités[] = strval($liIdConfondant) ;
// LG 20220910 début
//         $lsConfondants .= "<li>" . $loConfondant->getNomAdherent() 
//                         . " " . $loConfondant->getPrenomAdherent() ;
        $lsConfondants .= "<li><a target='_blank' rel='noopener noreferrer' href='info-adherent.php?id=" . $loConfondant->getIdAdherent() . "'>" 
                        . $loConfondant->getNomAdherent() . " " . $loConfondant->getPrenomAdherent() 
                        . "</a>";
// LG 20220910 fin
        // Parcourir tous les confondants identiques
        $lsCommentaires = "" ;
        foreach($laConfondants as $laConfondant2) {
            if ($laConfondant2["adherent"] == null) {
                // Ce n'est pas un confondant mais une erreur de saisie de l'adherent
                continue ;
            }
            if ($laConfondant["adherent"]->getIdAdherent() == $liIdConfondant) {
                // C'est le même adhérent
                if ($lsCommentaires) $lsCommentaires .= ", " ;
                $lsCommentaire = $laConfondant2["commentaire"] ;
                $lsCommentaires .= $lsCommentaire ;
            }
        }
        $lsConfondants .= " (" . $lsCommentaires . ")" ;
// LG 20220910 début
        // Ajouter les liens proposant la fusion
        $lsConfondants .= "<div><span class = \"pseudo_A\" onclick=\"fusionnerAdherentsImportés(event, " . $adherent->getIdAdherent() . ", " . $loConfondant->getIdAdherent() . ")\">"
                        . "Fusionner"
                        . "</span> et conserver l'adhérent nouvellement importé (" . $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() . ")"
                        ."</div>" ;
        $lsConfondants .= "<div><span class = \"pseudo_A\" onclick=\"fusionnerAdherentsImportés(event, " . $loConfondant->getIdAdherent() . ", " . $adherent->getIdAdherent() . ")\">"
                        ."Fusionner"
                        . "</span> et conserver l'adhérent précédemment présent (" . $loConfondant->getNomAdherent() . " " . $loConfondant->getPrenomAdherent(). ")"
                        ."</div>" ;
// LG 20220910 fin        
    }

    $lsNomAdherent = $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() ;
    if ($laEntité["statut"] !== "KO") {
        // Cet adhérent a été importé, séfinir le lien pour l'ouvrir sur un nouvel onglet
        $lsNomAdherent = "<a target='_blank' rel='noopener noreferrer' href='info-adherent.php?id=" . $adherent->getIdAdherent() . "'>" 
                        . $lsNomAdherent
                        . "</a>" ;      
    }
// LG 20220729    if ($laEntité["statut"] === "DOUTEUX") {
    if ($laEntité["statut"] === "DOUTEUX" && $laEntité["nouveau"]) {
        // Cet adhérent EST NOUVEAU et a été importé avec un doute : proposer de le supprimer
        $lsNomAdherent .= ' - <A href="#" onClick="return effacerAdherentImporté(event, ' . $adherent->getIdAdherent() . ');">Annuler l\'importation</A>' ;      
    }
    $lsImportMsg .= $lsNomAdherent ;      
//    $lsImportMsg .= '<span Id = "id' . $adherent->getIdAdherent() . '">' . $lsNomAdherent . "</span>" ;      
//if ($adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() == "BERNARD Johara") {
//    echo $adherent->getNomAdherent() . " " . $adherent->getPrenomAdherent() . " : " . $lsConfondants . "; " . $laEntité["messages"] . "<br>";
//}
    $lsLstMsg = "" ;
    if ($lsConfondants) {
        $lsImportMsg .= ", peut être confondu avec : " ;
        $lsLstMsg .= $lsConfondants ;
    }
    if($laEntité["messages"]) {
        // Il y a eu des messages d'avertissement
// echo $laEntité["messages"] . "<br>" ;
        $lsLstMsg .= $laEntité["messages"] ;
    }
    if ($lsLstMsg) {
        $lsImportMsg .= "<ul>" . $lsLstMsg . "</ul>" ;
    }
    
    $li = '<li Id = "id' . $adherent->getIdAdherent() . '">' ;
// LG 20220730 début
    if ($laEntité["nouveau"]) {
        $lsNouveau = '<span style="border: dotted coral;font-weight: bold;">NOUVEAU</span>' ;
    } else {
        $lsNouveau = "" ;
    }
// LG 20220730 fin

    if ($laEntité["statut"] === "KO") {
// LG 20220730 old        $lsImportsKO .= $li . "Ligne " . $laEntité["row"] . " : " . $lsImportMsg . "</li>" ;
        $lsImportsKO .= $li . "<strong>(non importé)</strong> Ligne " . $laEntité["row"] . " : " . $lsImportMsg . "</li>" ;
        
    } else if ($laEntité["statut"] === "DOUTEUX") {
// LG 20220730 old        $lsImportsDouteux .= $li . "Ligne " . $laEntité["row"] . " : " . $lsImportMsg . "</li>" ;
        $lsImportsDouteux .= $li . $lsNouveau. " douteux, Ligne " . $laEntité["row"] . " : " . $lsImportMsg . "</li>" ;
        
    } else if ($laEntité["statut"] === "OK"){
// LG 20220730 old        $lsImportsOK .= $li . $lsImportMsg . "</li>" ;
        $lsImportsOK .= $li . $lsNouveau . $lsImportMsg . "</li>" ;
        
    } else {
        throw new Exception("Cas non prévu") ;
    }
    
    $liItem++ ;
}

if (!$lsImportsKO) {
    $lsImportsKO = "Aucun" ;
} else {
    $lsImportsKO = "<ul>" . $lsImportsKO . "</ul>" ;
}
if (!$lsImportsDouteux) {
    $lsImportsDouteux = "Aucun" ;
} else {
    $lsImportsDouteux = "<ul>" . $lsImportsDouteux . "</ul>" ;
}
if (!$lsImportsOK) {
    $lsImportsOK = "Aucun" ;
} else {
    $lsImportsOK = "<ul>" . $lsImportsOK . "</ul>" ;
}
echo "<b>Rapport de l'importation : " . ($liNbEntitésKO + $liNbEntitésDouteuses + $liNbEntitésOK) . " adhérents, " 
        . $liNbEntitésOK . " importés, " 
        . $liNbEntitésDouteuses . " importés avec réserve, " 
        . $liNbEntitésKO . " non importés<br></b>" ;
echo '<br>' ;
echo '<div style="display: inline-block;"><div class="ImportésKO">Les ' . $liNbEntitésKO . ' adhérents non importés : </div>' . $lsImportsKO . '</div>' ;
echo '<hr>' ;
echo '<div style="display: inline-block;"><div class="ImportésDouteux">Les ' . $liNbEntitésDouteuses . ' adhérents importés avec réserve : </div>' . $lsImportsDouteux . '</div>' ;
echo '<hr>' ;
echo '<div style="display: inherit;"><div class="ImportésOK">Les ' . $liNbEntitésOK . ' adhérents importés : </div>' . $lsImportsOK . '</div>' ;

return true;
