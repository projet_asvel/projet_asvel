<?php
require_once '../../config/globalConfig.php';

use App\Entity\Activite;
use App\Repository\ActiviteRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
    header("location: Accessdenied.php");
}


if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mapperActivite = new ActiviteRepository();

    if ($id == null) {
        $Activite = new Activite([]);
    } else {
        $mapper = new ActiviteRepository();

        $Activite = $mapper->getEntitesById($id);
    }
}
// LG 20200504 début
//$liste = 'activite.php">Retour à la liste des activités';
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
$valider = true ;
$bouton = '<a href="liste-activite.php"'
        . ' style="margin-left: 1%;" class="btn btn-secondary">'
        . '<i class="fa fa-backward"></i>'
        . ' Retour liste des activités'
        . '</a>';
$footer_listeBoutons = [$bouton];
// LG 20200504 fin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action="" method="POST" id="formactivite">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Activités</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="idactivite" id="idactivite" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="nomActivite">Nom de l'activite :</label>
                                <input type="text" class="form-control" name="nomactivite" id="nomactivite" placeholder="Nom de l'activite..." required="required" value="<?= $Activite->getNomActivite() ?>">
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script src="../js/Activite.js?v=<?=VERSION_APP ?>"></script>

</html>
