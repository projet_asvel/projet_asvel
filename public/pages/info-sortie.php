<?php
require_once '../../config/globalConfig.php';

use App\Entity\Adherent;
use App\Entity\Sortie;
use App\Entity\Difficulte;
use App\Entity\RaisonAnnulation;
use App\Repository\SortieRepository;
use App\Repository\AdherentRepository;
use App\Repository\DifficulteRepository;
use App\Repository\ActiviteRepository;
use App\Repository\ParticiperRepository;
use App\Repository\RaisonAnnulationRepository;
use App\Security;

// use App\Entity\Photo;
// use App\Repository\PhotoRepository;

// LG 20211127 début
// if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header("location: Accessdenied.php");
    return ;
}

$AdheRep = new AdherentRepository();
$AdherentAuthentifié = $AdheRep->getEntitesById($_SESSION['idadherent']);

if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $idActiviteDemandee = (isset($_GET["Activite"]))?$_GET["Activite"]:0;
    $nomActiviteDemandee = "";
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $Adherent = new Adherent([]);
    $mapperAdherent = new AdherentRepository();
    $Adherents = $mapperAdherent->getAll();
    $nomOrganisateur = $mapperAdherent->getNomOrganisateurBySortie($id);
    $Difficulte = new Difficulte([]);
    $mapperDifficulte = new DifficulteRepository();
    $Raisonannulation = new RaisonAnnulation([]);
    $mapperRaisonAnnulation = new \App\Repository\RaisonAnnulationRepository();
    $raison = $mapperRaisonAnnulation->getAll();
    $LaRaison = $mapperRaisonAnnulation->getLibelleRaisonBySortie($id);
    $Difficultes = $mapperDifficulte->getAll();
    $nomDifficultes = $mapperDifficulte->getNomDifficulteBySortie($id);
    $mapperSortie = new SortieRepository();
    if ($id == null) {
        $Sortie = new Sortie([]);
    } else {
        $mapper = new SortieRepository();
        $Sortie = $mapper->getEntityId($id);
        $Info_Adherent = $mapperAdherent->getEntityId($Sortie->getIdOrganisateur());
//$Info_Difficulte = $mapperDifficulte->getEntityId($Sortie->getId_Difficulte());
    }

// LG 20200609 début
//    $disabled = "";
//    $valider = false ;
//    if ($Sortie->getDateDebutSortie() != NULL && $Sortie->getDateDebutSortie() < date("Y-m-d")) {
//        // Sortie dans le passé
//        $disabled = 'disabled="disabled"';
//    } else {
//        $valider = true ;
//    }
	// Ces variables sont prévues pour désactiver la modification/enregistrement d'une sortie passée
	// Mais ce n'est pas possible car les sorties passées doivent pouvoir être enregistrées a posteriori
	// en particulier en ce qui concerne les données comptables, modifier date/heure, participants réels,...
    $disabled = "";
// LG 20211127 début
    if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
        // Les adhérents simples ne peuvent pas modifier
        $disabled = 'disabled="disabled"';
    }
// LG 20211127 fin
    $valider = true ;
// LG 20200609 fin

    $RepoParticiper = new ParticiperRepository();
    $listeParticipant = $RepoParticiper->getAllAdherentByIdSortie($id);
    $listeMailpartcipants = '';
    foreach ($listeParticipant as $participant) {
// LG 20201011 début
        if ($participant->getidRaisonAnnulation()) {
                // Ce participant a annulé sa participation
                continue ;
        }
// LG 20201011 fin
        if ($listeMailpartcipants) {
            $listeMailpartcipants .= ',';
        }
        $listeMailpartcipants .= $participant->getIdAdherent();
    }
}
//Retour à la liste de sortie par rapport à l'activité
if ($Sortie->getDateDebutSortie() != NULL && $Sortie->getDateDebutSortie() < date("Y-m-d")) {
    // Sortie dans le passé
    $lsPassées = 'T';
} else {
    $lsPassées = 'F';
}

if (!$idActiviteDemandee) {
	$ActiviteRep = new ActiviteRepository();
	$lstActivites = $ActiviteRep->getAll();
    foreach ($Sortie->getActivites() as $SortieActivites) {
		$idActiviteDemandee = $SortieActivites->getIdActivite() ;
		break ;
    }
}

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php
        include_once 'inc/header.php';
        ?>

        <form action="" method="POST" id ="formsortie">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">
                            Sortie <?= $Sortie->getTitre() ?>
<?PHP // LG 20201011 début ?>
							<a href="#" class="pull-right" style="font-size: initial;font-weight: initial;" 
							   onmouseenter="$('#tarifs').css('display', 'initial')"
							   onmouseleave="$('#tarifs').css('display', 'none')"
							   >
								tarifs...
							</a>
							<div id="tarifs" style="display: none;font-size: initial;font-weight: initial;position: absolute;background-color: white;border: 1px solid black;box-shadow: 4px 5px 5px grey;border-radius: 0.25rem;z-index:1000;">
								<?php include 'info-tarifs.php' ?>
							</div>
<?PHP // LG 20201011 fin ?>
                        </h4>
                        <?php
                        if (isset($id)) {
                            $NbParticipantsInscrits = $RepoParticiper->getNbParticipantsBySortie($Sortie->getIdSortie(), $NbParticipantsValidés);
                            $NbPlacesVoiture = $Sortie->getNbPlacesVoiture();
                            $alerte = "";
                            if ($NbParticipantsValidés > $Sortie->getNombreParticipantsMaxSortie() || $NbPlacesVoiture < $NbParticipantsValidés) {
                                $alerte = 'style = "color: red; font-weight: bold;"';
                            }
                            echo "<div " . $alerte . ">"
                            . ($NbParticipantsValidés) . " participant validé" . (($NbParticipantsValidés > 1) ? "s" : "")
                            . ", " . ($NbParticipantsInscrits - $NbParticipantsValidés) . " en attente"
                            . ", " . ($Sortie->getNombreParticipantsMaxSortie()?:"-") . " max."
                            . ", " . $NbPlacesVoiture . " places de voiture"
                            . "</div>";
                        }
                        ?>

                    </div>
                    <div class="card-header">
                        <?php
// LG 20211127 début
//                        if (isset($id)) {
                        if (isset($id) && Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
// LG 20211127 fin                       
                            echo "<a class='' href='liste-participant.php?idSortie=" . $id . "'>"
                            . "<button type='button' class='btn btn-primary'>"
                            . "<i class='fa fa-forward'></i>"
                            . " Voir Participants"
                            . "</button>"
                            . "</a>&nbsp;";
                            if (Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ENCADRANT))) {
                                echo '<a href="info-mail.php?pourSortie&adherents=' . $listeMailpartcipants . '&sujet=Sortie ' . $Sortie->getTitre(true) . '"><button type="button" class="btn btn-primary"><i class="fa fa-forward"></i> Envoyer un mail aux participants</button></a>&nbsp;';
                            }
                            if (Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ENCADRANT, Security::ROLE_COMPTABLE))) {
                                echo "<a class='' target='_blank' href='edit-fichesortie.php?idSortie=" . $id . "'><button type='button' class='btn btn-primary'><i class='fa fa-forward'></i> Générer la fiche de sortie</button></a>&nbsp;";
                            }
                            if (Security::hasRole(Array(Security::ROLE_ENCADRANT))) {
                                echo '<a href="info-sortie.php?Activite=' . $idActiviteDemandee . '"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nouvelle sortie</button></a>&nbsp;';
                                echo '<a href="info-participant.php?idSortie=' . $id . '"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nouveau participant</button></a>&nbsp;';
                            }
                        }
                        ?>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="col-2">
                                    <a class="" href="info-photo.php?id=<?= $id ?>"  >
                                        <button type='button' class='btn btn-primary'><i class='fa fa-forward'></i> Mes photos</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="idsortie" id="idsortie" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="nomsortie">Numéro de la sortie :</label>
                                <input type="text" class="form-control" id="nomsortie" name="nomsortie" placeholder="Nom de la sortie..." required="required" value="<?= $Sortie->getNomSortie() ?>"<?= $disabled ?>>
                            </div>
                            <div class="col">
                                <label for="activites">Activités :</label>
                                <select class="js-example-basic-multiple" name="activites[]" id="activites" multiple="multiple" required="required" style="width: 100%" <?= $disabled ?>>
                                    <?php
                                    $repoActivites = new ActiviteRepository();
                                    $listeActivites = $repoActivites->getAll();
                                    $listeActivitesSortie = $Sortie->getActivites();
                                    foreach ($listeActivites as $value) {
                                        $selected = "";
										if ($idActiviteDemandee && $value->getIdActivite() == $idActiviteDemandee) {
											// Mémoriser le nom de cette activité, qui est celle demandée
											$nomActiviteDemandee = $value->getNomActivite() ;
										}
										if ($Sortie->getIdSortie()) {
											// Sortie préexiatante
											foreach ($listeActivitesSortie as $activite) {
												if ($activite->getIdActivite() == $value->getIdActivite()) {
													// Cette activité est dans la liste des activités de la sortie en cours de chargement
													$selected = ' selected = "selected"';
												}
											}
										} else if ($idActiviteDemandee && $value->getIdActivite() == $idActiviteDemandee) {
											// Nouvelle sortie, demandée pour cette activité
											 $selected = ' selected = "selected"';
										}
                                        echo '<option value="' . $value->getIdActivite() . '"' . $selected . '>' . $value->getNomActivite() . "</option>";
                                    }
                                    // LG 20200214 fin
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class = "row py-2">
                            <div class="col">
                                <label for ="organisateur">Organisateur:</label>
                                <select required name="organisateur" id="organisateur" class = "form-control" size="1" <?= $disabled ?>>

                                    <?php
									$hasRoleEncadrant = Security::hasRole(Security::ENCADRANT) ;
                                    if (!$Sortie->getIdOrganisateur() && !$hasRoleEncadrant) {
										// Nouvelle sortie, mais l'utilisateur en cours n'est pas encadrant
										// Sélectionner un élément vide
                                        ?>
                                        <option value="NULL" hidden selected>Choisissez un encadrant</option>   

                                        <?php
                                    }
                                    $r = $mapperAdherent->getAllByRole(Security::IDROLE_ENCADRANT);
                                    foreach ($r as $donnees) {
										if (!$Sortie->getIdOrganisateur() && $hasRoleEncadrant && $donnees->getIdAdherent() == $AdherentAuthentifié->getIdAdherent()) {
											// Nouvelle sortie, l'utilisateur est encadrant, c'est celui de cet item
											$selected = " selected" ;
										} else {
											$selected = ($Sortie->getIdOrganisateur() == $donnees->getIdAdherent()) ? " selected" : "";
										}
                                        ?>
                                        <option value="<?= $donnees->getIdAdherent(); ?>" <?= $selected ?>> <?php echo $donnees->getNomAdherent() . " " . $donnees->getPrenomAdherent(); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class = "col">
                                <label for = "lieusortie">Lieu :</label>
                                <input type = "text" class = "form-control" placeholder = "Lieu..." id = "lieusortie" name = "lieusortie"  value = "<?= $Sortie->getLieuSortie() ?>" <?= $disabled ?>>
                            </div>
                        </div>
                        <div class = "row py-2">
                            <div class = "col">
                                <label for = "datedebutsortie">Date de début :</label>
                                <input type = "date" class = "form-control" id = "datedebutsortie" name = "datedebutsortie" required="required" value = "<?= $Sortie->getDateDebutSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "datefinsortie">Date de fin :</label>
                                <input type = "date" class = "form-control" id = "datefinsortie" name = "datefinsortie" value = "<?= $Sortie->getDateFinSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "heuredepart">Heure de départ :</label>
                                <input type = "time" class = "form-control" id = "heuredepart" name = "heuredepart" value = "<?= $Sortie->getHeureDepart() ?>" <?= $disabled ?>>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col">
                                <label for ="difficulte">Difficulté :</label>
                                <select required name="difficulte" id="difficulte"  class = "form-control" size="1" <?= $disabled ?>>
                                    <?php
                                    if ($Sortie->getIdDifficulte() == NULL) {
                                        ?>                                                    
                                        <option value="NULL" hidden selected>Choisissez un élément</option>  
                                        <?php
                                    } else {
                                        ?>

                                        <option  value="<?php echo $Sortie->getIdDifficulte(); ?>" hidden selected> <?php echo $nomDifficultes->getNomDifficulte() ?></option>
                                        <?php
                                    }
                                    $reponse = $Difficultes;
                                    foreach ($reponse as $donnees) {
                                        $selected = ($Sortie->getIdDifficulte() == $donnees->getIdDifficulte()) ? " selected" : "";
                                        ?>
                                        <option value="<?php echo $donnees->getIdDifficulte(); ?>" <?= $selected ?>> <?php echo $donnees->getNomDifficulte(); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class = "col">
                                <label for = "nombreparticipantsmaxsortie">Nombre max de participant :</label>
                                <input type = "number" class = "form-control" placeholder = "Nombre de participant..." id = "nombreparticipantsmaxsortie" name = "nombreparticipantsmaxsortie"  value = "<?= $Sortie->getNombreParticipantsMaxSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col input-group">
                                <label for = "inscriptionenligne">Inscription en ligne :</label>
                                <input type="checkbox" style="margin-right: 95%;" class="selectpicker form-control"  name="inscriptionenligne" id="inscriptionenligne"<?= $Sortie->getInscriptionenligne() ? "checked" : "" ?> <?= $disabled ?>>
                            </div>
                        </div>
                        <div class = "row py-2">
                            <div class = "col">
                                <label for = "denivellesortie">Dénivelé :</label>
                                <input type = "number" class = "form-control" placeholder = "Dénivelé..." id = "denivellesortie" name = "denivellesortie" value = "<?= $Sortie->getDenivelleSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "altitudemaxsortie">Altitude max :</label>
                                <input type = "number" class = "form-control" placeholder = "Altitude..." id = "altitudemaxsortie" name = "altitudemaxsortie" value = "<?= $Sortie->getAltitudeMaxSortie() ?>" <?= $disabled ?>>
                            </div>
                        </div>
                        <div class = "row py-2">
                            <div class = "col">
                                <label for = "commentairessortie">Commentaire :</label>
                                <textarea style="height: 100px;" class="form-control" id="commentairessortie" name="commentairessortie" placeholder="Commentaire sur la sortie" <?= $disabled ?>><?= $Sortie->getcommentairesSortie() ?></textarea>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class = "col">
                                <label for = "idRaisonAnnulation">Raison d'annulation :</label>
                                <select name="idRaisonAnnulation" id="idRaisonAnnulation" size="1" class = "form-control" <?= $disabled ?>>
                                    <?php
                                    echo '<option value="Null">Non annulé</option>';
                                    foreach ($raison as $donnees) {
                                        $selected = ($Sortie->getIdRaisonAnnulation() == $donnees->getIdAnnulation()) ? " selected" : "";
                                        ?>
                                        <option value="<?= $donnees->getIdAnnulation() ?>" <?= $selected ?>> <?= $donnees->getLibelleAnnulation() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-header">
						<H4>Informations comptables</H4>
                    </div>
                    <div class="card-body">
                        <div class = "row py-2">
                            <div class = "col">
                                <label for = "tarifsortie">Tarif :</label>
                                <input type = "number" step="0.01" min="0.00" max="10000.00" step="0.01" class = "form-control" placeholder = "Tarif..." id = "tarifsortie" name = "tarifsortie"  value = "<?= $Sortie->getTarifSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "nbvoituressortie">Nombre de voitures :</label>
                                <input type = "number" class = "form-control" placeholder = "Nombre de voiture..." id = "nbvoituressortie" name = "nbvoituressortie" value = "<?= $Sortie->getNbVoituresSortie() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "distancesortie">Distance (kms aller-retour) :</label>
                                <input type = "number" class = "form-control" placeholder = "Distance..." id = "distancesortie" name = "distancesortie"  value = "<?= $Sortie->getDistanceSortie() ?>" <?= $disabled ?>>  
                            </div>
                            <div class = "col">
                                <label for = "coutautoroute">Cout de l'autoroute (aller-retour) :</label>
                                <input type="number" step="0.01" min="0.00" max="10000.00" step="0.01" class="form-control" placeholder="Cout de l'autoroute" id="coutautoroute" name="coutautoroute" value="<?= $Sortie->getCoutAutoroute() ?>" <?= $disabled ?>>
                            </div>
                        </div>
                        <div class = "row py-2">
                            <div class = "col">
                                <label for = "fraisAnnexe">Frais annexes (par exemple les frais d'hébergement de l'encadrant sur un week end) :</label>
                                <input type = "number" step="0.01" min="0.00" max="10000.00" step="0.01" class = "form-control" placeholder = "Frais annexe" id = "tarifsortie" name = "fraisAnnexe"  value = "<?= $Sortie->getFraisAnnexe() ?>" <?= $disabled ?>>
                            </div>
                            <div class = "col">
                                <label for = "fraisAnnexeDetails">Détails des frais annexes :</label>
                                <textarea style="height: calc(1.5em + 0.75rem + 2px);" class="form-control" id="commentairessortie" name="fraisAnnexeDetails" placeholder="Détails des frais annexe" <?= $disabled ?>><?= $Sortie->getFraisAnnexeDetails() ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </br> </br>

            <?php 
			if ($nomActiviteDemandee) {
				$bouton = '<a href="liste-sortie.php?Activite=' . $idActiviteDemandee . '"'
						. ' style="margin-left: 1%;" class="btn btn-secondary">'
						. '<i class="fa fa-backward"></i>'
						. ' Retour à la liste des sorties ' . $nomActiviteDemandee
						. '</a>';
				$footer_listeBoutons = [$bouton];
			}
// LG 20211127 début
//                include_once 'inc/footer-valider.php' ;
                if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
                    include_once 'inc/footer-valider.php' ;
                }
// LG 20211127 fin
            ?>
        </form>
    </body>
    <script src="../js/Sortie.js?v=<?=VERSION_APP ?>"></script>

    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2({placeholder: "Choisissez les activités de cette sortie"});
        });
    </script>

</html>
