<?php
require_once '../../config/globalConfig.php';
?>
<input type="hidden" name="idAffiliation" id="idAffiliation" value="<?= $idAffiliation ?>">
<?php
if (!isset($masquedherent) || !$masquedherent) {
    // Afficher le select de choix des adhérents
    ?>
    <div class="row py-2">
        <div class="col">
            <label for ="idAdherent">Affilié :</label>
            <select name="idAdherent" id="idAdherent" class="form-control" required <?= $adherentDisabled ?> >
                <?php
                if (!$affiliation->getIdAdherent()) {
                    // Aucun adhérent sélectionné : mettre un placeholder
                    echo "<option disabled selected value=''>Choisissez un adhérent</option>";
                }

                $reponses = $mapperAdherent->getAll();
                $lbSelected = false;
                foreach ($reponses as $donnees) {
                    $selected = "";
                    if ($donnees->getIdAdherent() == $affiliation->getIdAdherent()) {
                        // Cet adhérent est sélectionné
                        $selected = " selected";
                    }
                    echo '<option value="' . $donnees->getIdAdherent() . '"' . $selected . '>'
                    . $donnees->getNomAdherent() . " " . $donnees->getPrenomAdherent()
                    . '</option>';
                }
                ?>
            </select>
            <?php
            if ($adherentDisabled) {
                // Les comboboxes disabled ne sont pas envoyées dans le submit. Il faut ajouter un input caché pour ça
                echo '<input type="hidden" name="idAdherent" value="' . $affiliation->getIdAdherent() . '">';
            }
            ?>
        </div>
    </div>
    <?php
}
?>
<div class="row py-2">
    <div class = "col">
        <label for ="idFederation">Fédération :</label>
        <select name="idFederation"  id="idFederation" size="1" class = "form-control" required <?= $disabled ?>  >
            <?php
            if (!$affiliation->getIdFederation()) {
                echo "<option disabled selected value=''>Sélectionnez une fédération ou un club</option>";
            }
            $Reponse = $mapperFédération->getAll();
            foreach ($Reponse as $donnees) {
                $selected = ($affiliation->getIdFederation() == $donnees->getIdFederation()) ? "selected" : "";
                ?>
                <option value="<?php echo $donnees->getIdFederation(); ?>"<?= $selected ?> > <?php echo $donnees->getNomFederation(); ?> </option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class = "col">
        <label for = "numeroLicense">N° de license :</label>
        <input type = "text" class = "form-control" id = "numeroLicense" name = "numeroLicense" value = "<?= $affiliation->getNumeroLicense() ?>" <?= $disabled ?> >
    </div>
</div>
<div class="row py-2">
    <div class = "col">
        <label for = "dateAffiliation">Date de début d'affiliation :</label>
        <?php
        echo '<input type = "date" class = "form-control" ' . $disabled
        . ' id = "dateAffiliation" name = "dateAffiliation"  required  value = "';
        if ($affiliation->getDateAffiliation()) {
            echo substr($affiliation->getDateAffiliation(), 0, 10);
        } else {
            echo substr(date('Y-m-d H:i'), 0, 10);
        }
        echo '">';
        ?>
    </div>
    <div class = "col">
        <label for = "dateFinAffiliation">Date de fin d'affiliation (facultatif) :</label>
        <?php
        echo '<input type = "date" class = "form-control" ' . $disabled
        . ' id = "dateFinAffiliation" name = "dateFinAffiliation"  value = "';
        if ($affiliation->getDateFinAffiliation()) {
//                                    echo substr($affiliation->getDateAffiliation(), 0, 10) ;
        }
        echo '">';
        ?>
    </div>
</div>
