<?php

require_once '../../config/globalConfig.php';

use App\Entity\Activite;
use App\Repository\ActiviteRepository;

$idActivite = filter_input(INPUT_POST, 'idactivite', FILTER_SANITIZE_STRING);
$nomActivite = filter_input(INPUT_POST, 'nomactivite', FILTER_SANITIZE_STRING);

//if ($idActivite == NULL) {
$datas = array(
    'idactivite' => $idActivite,
    'nomactivite' => $nomActivite,
);

try {
    $Activite = new Activite($datas);
    $ActiviteRep = new ActiviteRepository();
    if ($ActiviteRep->vérifier($Activite, $tabErr)) {
        $Lactivite = $ActiviteRep->sauver($Activite);
//        echo 'true';
        echo $Lactivite->getIdActivite() ;
    } else {
        echo $tabErr[0];
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
//} else {
//    $datas = array(
//        'nomactivite' => $nomActivite,
//    );
//}


//header('Location: liste-activite.php');
