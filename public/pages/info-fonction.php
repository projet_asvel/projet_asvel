<?php
require_once '../../config/globalConfig.php';

use App\Entity\Fonction;
use App\Repository\FonctionRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
    header("location: Accessdenied.php");
}


if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mapperActivite = new FonctionRepository();

    if ($id == null) {
        $Fonction = new Fonction([]);
    } else {
        $mapper = new FonctionRepository();

        $Fonction = $mapper->getEntityById($id);
    }
}
// LG 20200504 début
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
//$liste = 'fonction.php">Retour à la liste des fonctions';
$valider = true ;
$bouton = '<a href="liste-fonction.php"'
        . ' style="margin-left: 1%;" class="btn btn-secondary">'
        . '<i class="fa fa-backward"></i>'
        . ' Retour liste des fonctions'
        . '</a>';
$footer_listeBoutons = [$bouton];
// LG 20200504 fin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action=""  id="formfonction" method="POST">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Fonction</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="idfonction"  id="idfonction" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="nomFonction">Nom des Fonction :</label>
                                <input type="text" class="form-control" required="true" id="nomfonction" name="nomfonction" placeholder="Nom de Fonction" value="<?= $Fonction->getNomFonction() ?>">
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script src="../js/Fonction.js?v=<?=VERSION_APP ?>"></script>

</html>
