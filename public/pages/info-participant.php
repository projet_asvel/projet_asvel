<?php
require_once '../../config/globalConfig.php';

use App\Entity\Participer;
use App\Repository\ParticiperRepository;
use App\Entity\Adherent;
use App\Repository\AdherentRepository;
use App\Entity\Sortie;
use App\Repository\SortieRepository;
use App\Entity\Fonction;
use App\Repository\FonctionRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header("location: Accessdenied.php");
}

if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $idparticipant = filter_input(INPUT_GET, 'idAdherent', FILTER_SANITIZE_STRING);
    $idsortie = filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING);
    $repoParticiper = new ParticiperRepository();
    $repoAdherent = new AdherentRepository();
    $Fonction = new Fonction([]);
    $repoFonction = new FonctionRepository();
    $Participer = NULL;
    if ($idparticipant && $idsortie) {
        $Participer = $repoParticiper->getEntityById(["idAdherent" => $idparticipant, "idSortie" => $idsortie]);
    }
    if ($Participer == NULL) {
        $Participer = new Participer();
//		if (isset($_SESSION['idadherent'])) {
//			$Participer->setIdInscripteur($_SESSION['idadherent']) ;
//		}
    }
    if (!$Participer->getIdInscripteur() && isset($_SESSION['idadherent'])) {
        $Participer->setIdInscripteur($_SESSION['idadherent']);
    }
    if ($idsortie == null) {
        $Sortie = new Sortie([]);
    } else {
        $mapperSortie = new SortieRepository();
        $Sortie = $mapperSortie->getEntityById($idsortie);
    }
}
if ($Sortie->getInscriptionenligne() == FALSE && !Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
if (isset($Sortie->getActivites()[0])) {
    $idActivite = $Sortie->getActivites()[0]->getIdActivite();
    $nomActivite = $Sortie->getActivites()[0]->getNomActivite();
} else {
    $idActivite = null;
    $nomActivite = "";
}

$checked = "";

$date = substr($Participer->getdateInscription(), 0, 10);
$heure = substr($Participer->getdateInscription(), 11);
$datetest = date('Y-m-d H:i');
$defaultdate = substr($datetest, 0, 10);
$defaultheure = substr($datetest, 11);
$defaultdatecomplet = $defaultdate . "T" . $defaultheure;
$datecomplet = $date . "T" . $heure;

$disabled = "";
$disabledPlacesVoitures = "";
$lbCacheParticipants = false ;
if (!Security::hasRole(Array(Security::ROLE_ENCADRANT))) {
    $disabled = 'disabled="disabled"';
    $disabledPlacesVoitures = 'disabled="disabled"';
    $lbCacheParticipants = true ;
}
if (Security::hasRole(Array(Security::ROLE_ADHERENT)) && isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $idparticipant) {
    // Si l'utilisateur possède le role d'adherent et que ce dernier est sur sa propre participation
    // Il a le droit d'indiquer le nb de places de voiture
    $disabledPlacesVoitures = "";
}

if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    // L'utilisateur en cours a les droits sur les données
} else if (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] != $idparticipant) {
    // L'utilisateur en cours lui-même est en train de visualiser ses propres informations : RAS
} else {
    // Pas les droits
    header("location: Accessdenied.php");
}
$lbAfficheExplicationsAdhérent = false ;
if (!Security::hasRole(Array(Security::ROLE_ENCADRANT)) && isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $idparticipant) {
    $lbAfficheExplicationsAdhérent = true ;
}

$valider = true;
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php'; ?>

        <form action="" method="POST" id="formparticiper">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">
                            Participant à la sortie <?= $Sortie->getTitre() ?>
                            <a href="#" class="pull-right" style="font-size: initial;font-weight: initial;" 
                               onmouseenter="$('#tarifs').css('display', 'initial')"
                               onmouseleave="$('#tarifs').css('display', 'none')"
                               >
                                tarifs...
                            </a>
                            <div id="tarifs" style="display: none;font-size: initial;font-weight: initial;position: absolute;background-color: white;border: 1px solid black;box-shadow: 4px 5px 5px grey;border-radius: 0.25rem;z-index:1000;">
                                <?php include 'info-tarifs.php' ?>
                            </div>
                        </h4>
                        <?php
                        $NbParticipantsInscrits = $repoParticiper->getNbParticipantsBySortie($Sortie->getIdSortie(), $NbParticipantsValidés);
                        $NbPlacesVoiture = $Sortie->getNbPlacesVoiture();
                        $alerte = "";
                        if ($NbParticipantsValidés > $Sortie->getNombreParticipantsMaxSortie() || $NbPlacesVoiture < $NbParticipantsValidés) {
                            $alerte = 'style = "color: red; font-weight: bold;"';
                        }
                        if ($NbParticipantsValidés >= $Sortie->getNombreParticipantsMaxSortie() && !$Participer->getIdAdherent()) {
                            // Ajout d'une nouvelle participation, alors que la sortie est déja complète
                            // Avertir de la liste d'attente
                            $Script = "$(document).ready(function () {
									alert('Ce nouveau participant sera sur liste d\'attente') ;
								});";
                            echo "<script>" . $Script . "</script>";
                        }
                        echo "<div " . $alerte . ">"
                        . ($NbParticipantsValidés) . " participant validé" . (($NbParticipantsValidés > 1) ? "s" : "")
                        . ", " . ($NbParticipantsInscrits - $NbParticipantsValidés) . " en attente"
                        . ", " . ($Sortie->getNombreParticipantsMaxSortie() ?: "-") . " max."
                        . ", " . $NbPlacesVoiture . " places de voiture"
                        . "</div>";
                        
                        if ($lbAfficheExplicationsAdhérent) {
                            ?>
                            <div style='text-align: left ;'>
                            <br>
                            Bienvenue sur la page d'inscription à la sortie.<br>
                            Une fois que vous aurez validé sotre saisie, vous ne pourrez pas vous désisncrire, mais vous pourrez revenir sur cette page pour modifier le commentaire de votre participation pour y expliquer que vous êtes désolé(e), mais bon voilà ça n'est plus possible finalement...<BR>
                            Une fois inscrit, il ne vous reste plus qu'à attendre la validation de la part de l'encadrant, qui intervient généralement au début de la permanence du jeudi : la validation de votre participation apparaît immédiatement dans la liste des sorties.<br>
                            Merci de bien penser à renseigner les cases "Débutant", "Loue son matériel", "Emprunte DVA", "Emprunte Pelle + sonde" là où c'est utile, et de mettre un petit mot dans la zone "commentaires" si vous avez des choses à noujs dire.
                            </div>
                        <?PHP
                        }
                        ?>
                    </div>

                    <div class="card-header <?=($Participer->getIdRaisonAnnulation())?"participation-annulee":"" ?>">
                        <?php
                        if ($Participer->getIdAdherent()) {
                            // Ce n'est pas un nouveau
                            if (Security::hasRole(Array(Security::ROLE_ENCADRANT))) {
                                // on peut proposer la création d'un nouveau, et la suppression
                                echo '<button type="button" class="btn btn-primary" onClick="supprimerParticipant(event, ' . $Participer->getIdAdherent() . ',' . $Sortie->getIdSortie() . ')"><i class="fa fa-minus"></i> Supprimer la participation</button>&nbsp';
                                echo '<a href=info-participant.php?idSortie=' . $Sortie->getIdSortie() . '><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un participant</button></a>&nbsp';
                            }
                            if (Security::hasRole(Array(Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
                                // On peut proposer l'envoi de mail
                                echo ' <a href="info-mail.php?adherents=' . $Participer->getIdAdherent() . '"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Envoyer un mail au participant</button></a>';
                            }
//						}
                            ?>
                            <div class="pull-right" <?php if ($lbCacheParticipants) echo 'style="display: none;"'; ?>>
                                <?php
                                $adherentSélectionnéTrouvé = false;
                                $adherentSélectionnéEstLePrécédent = false;
                                $idAdherentSelectionné = "";
                                $idAdherentPrécédent = "";
                                $idAdherentSuivant = "";
                                $lblAdherentPrécédent = "";
                                $lblAdherentSuivant = "";
                                if ($Participer->getIdAdherent()) {
                                    // Un adhérent est déja défini pour cette participation
                                    $idAdherentSelectionné = $Participer->getIdAdherent();
                                }

                                $listeParticipant = $repoParticiper->getAllAdherentByIdSortie($Sortie->getIdSortie());
                                $lbSelected = false;
                                $options = "";
                                foreach ($listeParticipant as $donnees) {
                                    $selected = "";
                                    $Adherent = $repoAdherent->getEntityById($donnees->getIdAdherent());
                                    if ($adherentSélectionnéEstLePrécédent) {
                                        // Cet adhérent est celui qui suit celui affiché
                                        $idAdherentSuivant = $donnees->getIdAdherent();
// LG 20201010 old										$lblAdherentSuivant = $Adherent->getNomAdherent() . " " . $Adherent->getPrenomAdherent() ;
                                        $lblAdherentSuivant = $Adherent->getNomUsuel();
                                        $adherentSélectionnéEstLePrécédent = false;
                                    }
                                    if ($donnees->getIdAdherent() == $idAdherentSelectionné) {
                                        // Cet adhérent est sélectionné
                                        $selected = " selected";
                                        $adherentSélectionnéTrouvé = true;
                                        $adherentSélectionnéEstLePrécédent = true;
                                    }
                                    if (!$adherentSélectionnéTrouvé) {
                                        // Cet adhérent est celui qui précède celui affiché
                                        $idAdherentPrécédent = $donnees->getIdAdherent();
// LG 20201010 old										$lblAdherentPrécédent = $Adherent->getNomAdherent() . " " . $Adherent->getPrenomAdherent() ;
                                        $lblAdherentPrécédent = $Adherent->getNomUsuel();
                                    }
// LG 20201011 début
//									$options .= "<option value=" . $Adherent->getIdAdherent() . $selected . ">"
//											. $Adherent->getNomAdherent() . " " . $Adherent->getPrenomAdherent() . "</option>" ;
                                    $options .= "<option value=" . $Adherent->getIdAdherent() . $selected . ">"
                                            . $Adherent->getNomUsuel() . "</option>";
// LG 20201011 fin
                                }
                                ?>
                                <div class="input-group mb-3">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-primary" 
                                                title="Passer au participant précédent : <?= $lblAdherentPrécédent ?>" 
                                                onClick="allerAuParticipant(<?= $idAdherentPrécédent ?>, <?= $Sortie->getIdSortie() ?>);"
                                                <?= ($idAdherentPrécédent === "") ? " disabled = 'disabled'" : "" ?>
                                                >
                                            <i class="fa fa-backward"></i>
                                        </button>
                                    </div>
                                    <select id="cboGoToParticipant" class="form-control" onChange="allerAuParticipant('cboGoToParticipant', <?= $Sortie->getIdSortie() ?>)">
                                        <?= $options ?>
                                    </select>
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-primary" 
                                                title="Passer au participant suivant : <?= $lblAdherentSuivant ?>" 
                                                onClick="allerAuParticipant(<?= $idAdherentSuivant ?>, <?= $Sortie->getIdSortie() ?>);"
                                                <?= ($idAdherentSuivant === "") ? " disabled = 'disabled'" : "" ?>
                                                >
                                            <i class="fa fa-forward"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?PHP
                            // Fin du si ce n'est pas un nouveau participant
                        }
                        ?>
                        <input type="hidden" name="idParticipantInitial" value="<?= $idparticipant ?>">
                        <input type="hidden" name="idSortieInitial" value="<?= $idsortie ?>">
                        <input type="hidden" name="idsortie" id="idsortie" value="<?= $idsortie ?>">
                        <div class="card-body">
                            <div class="row py-2">
                                <div class="col">
                                    <label for ="idAdherent">Participant :</label>
                                    <div class="input-group mb-3">
                                        <select name="idadherent" id="idadherent" class="form-control" required <?= $disabled ?> >
                                            <?php
                                            $idAdherentSelectionné = "";
                                            if ($Participer->getIdAdherent()) {
                                                // Un adhérent est déja défini pour cette participation
                                                // filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING)
                                                $idAdherentSelectionné = $Participer->getIdAdherent();
                                                // IG 28012021 Debut
                                                //  } else if (isset($_GET['idAdherent']) && isset($_SESSION['idadherent']) && $_GET['idAdherent'] == $_SESSION['idadherent']) {
                                                // IG 28012021 Fin
                                            } else if (isset($_GET['idAdherent']) && isset($_SESSION['idadherent']) && filter_input(INPUT_GET, 'idAdherent', FILTER_SANITIZE_STRING) == $_SESSION['idadherent']) {

                                                filter_input(INPUT_GET, 'idAdherent', FILTER_SANITIZE_STRING);
                                                // Un adhérent a été demandé et c'est celui qui est actuellement loggé
                                                // IG 28012021 Debut
                                                // $idAdherentSelectionné = $_GET['idAdherent'];
                                                // IG 28012021 Fin

                                                $idAdherentSelectionné = filter_input(INPUT_GET, 'idAdherent', FILTER_SANITIZE_STRING);
                                            }
                                            if (!$idAdherentSelectionné) {
                                                // Aucun adhérent sélectionné : mettre un placeholder
                                                echo "<option disabled selected>Choisissez un adhérent</option>";
                                            }

// LG 20201011 old										$reponses = $repoAdherent->getAll();
                                            $reponses = $repoAdherent->getAll(true);
                                            $lbSelected = false;
                                            foreach ($reponses as $donnees) {
                                                $selected = "";
                                                if ($donnees->getIdAdherent() == $idAdherentSelectionné) {
                                                    // Cet adhérent est sélectionné
                                                    $selected = " selected";
                                                }
                                                ?>
                                                <option value="<?php echo $donnees->getIdAdherent(); ?>"<?= $selected ?>>
                                                    <?php echo $donnees->getNomUsuel(true); ?> </option>
                                                    <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalInfoAdherent" title="Créer un nouvel adhérent">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary" data-toggle="modal" title="Voir les informations de cet adhérent" onClick="voirInfosAdherent()">
                                                <i>V</i>
                                            </button>
                                        </div>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary" data-toggle="modal" title="Voir les sorties de cet adhérent" onClick="voirSortiesAdherent()">
                                                <i>S</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class = "col">
                                    <label for ="fonction">Fonction :</label>
                                    <select name="fonction"  id="fonction" size="1" class = "form-control" <?= $disabled ?>  >
                                        <?php
                                        if ($Participer->getIdAdherent() == NULL) {
                                            ?>                                                    
                                            <option value="1" hidden selected>Participant</option>   
                                            <?php
                                        }

                                        $Reponse = $repoFonction->getAll();
                                        foreach ($Reponse as $donnees) {
                                            $selected = ($Participer->getIdFonction() == $donnees->getIdFonction()) ? "selected" : "";
                                            ?>
                                            <option value="<?php echo $donnees->getIdFonction(); ?>"<?= $selected ?> > <?php echo $donnees->getnomfonction(); ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col">
                                    <div class="row py-2">
                                        <div class = "col">
                                            <label for = "dateinscription">Date d'inscription :</label><?php
                                        if ($Participer->getdateInscription()) {
                                            echo "  <input type = datetime-local class = form-control " . $disabled . " id = dateinscription name = dateinscription  value = " . $datecomplet . ">";
                                        } else {
                                            echo "  <input type = datetime-local class = form-control " . $disabled . "  id = dateinscription name = dateinscription  value = " . $defaultdatecomplet . ">";
                                        }
                                        ?>
                                        </div>
                                        <div class = "col">
                                            <label for = "inscripteur">Inscrit par :</label><?php
                                            echo "  <input type=hidden name=idinscripteur id=idinscripteur value='" . $Participer->getIdInscripteur() . "'>";
                                            echo "  <input type=text id=inscripteur class=form-control disabled  value='" . ($Participer->getIdInscripteur() ? $Participer->getInscripteur()->getNomUsuel() : "Inconnu") . "'>";
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class = "col">
                                    <div class="row py-2">
                                        <div class = "col">
                                            <label class="control-label">Participation validée :</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">

                                                    <?php
                                                    if ($_SESSION['idadherent'] == $Participer->getIdAdherent() || $Participer->getIdAdherent() == NULL) {
                                                        
                                                    }
                                                    if (isset($_SERVER["HTTP_REFERER"]) && (strpos($_SERVER["HTTP_REFERER"], 'liste-participant') || strpos($_SERVER["HTTP_REFERER"], 'info-participant')
                                                            ) && $Participer->getIdAdherent() == NULL) {
                                                        // si l'encadrant provient de la page liste-participant et qu'il inscrit un nouveau participant
                                                        $checked = "checked";
                                                    } else {
                                                        // si l'encadrant ou l'adhérent consulte une participation pré-remplie
                                                        $checked = $Participer->getValidationParticipation() ? "checked" : "";
                                                    }
                                                    ?>
                                                    <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="validationparticipation" name="validationparticipation" <?= $checked ?>  <?= $disabled ?>  maxlength="50">
                                                    <label></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class = "col">
                                    <label class="control-label">Débutant :</label>
                                    <div class="input-group">
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="debutant" name="debutant" <?= $Participer->getDebutant() ? "checked" : ""; ?> maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                                <div class = "col">
                                    <label class="control-label">Loue son matériel :</label>
                                    <div class="input-group">
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="louemateriel" name="louemateriel" <?= $Participer->getLoueMateriel() ? "checked" : ""; ?> maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                                <div class = "col">
                                    <label class="control-label">Emprunte DVA :</label>
                                    <div class="input-group">
                                        <!--<input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="dva" name="dva" <?= $Participer->getDVA() ? "checked" : ""; ?>  <?= $disabled ?>  maxlength="50">-->
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="dva" name="dva" <?= $Participer->getDVA() ? "checked" : ""; ?> maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                                <div class = "col">
                                    <label class="control-label">Emprunte Pelle + Sonde :</label>
                                    <div class="input-group">
    <!--                                            <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="pellesonde" name="pellesonde" <?= $Participer->getPelleSonde() ? "checked" : ""; ?> <?= $disabled ?>  maxlength="50">-->
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="pellesonde" name="pellesonde" <?= $Participer->getPelleSonde() ? "checked" : ""; ?> maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class = "col">
                                    <label for = "nbPlaceVoiture">Nombre de place voiture (0 pas de voiture) :</label>
                                    <input type = "number" class = "form-control" id = "nbPlaceVoiture" name = "nbPlaceVoiture" value = "<?= $Participer->getnbPlaceVoiture() ?>" <?= $disabledPlacesVoitures ?> placeholder="Nombre de places dans la voiture">
                                </div>  
                                <div class = "col">
                                    <label class="control-label">Exclus du covoiturage :</label>
                                    <div class="input-group">
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="ExcluDuCovoiturage" name="ExcluDuCovoiturage" <?= $Participer->getExcluDuCovoiturage() ? "checked" : ""; ?>  <?= $disabled ?>  maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                                <div class = "col raison-annulation">
                                    <label for = "idraisonannulation">Raison d'annulation :</label>
                                    <select name="idraisonannulation" size="1" id="idraisonannulation" class = "form-control" <?= $disabled ?>  >
                                        <?php
                                        $mapperRaisonAnnulation = new \App\Repository\RaisonAnnulationRepository();
                                        $reponse = $mapperRaisonAnnulation->getAll();
                                        echo '<option value="Null">Non annulé</option>';
                                        foreach ($reponse as $donnees) {
                                            $selected = ($Participer->getIdRaisonAnnulation() == $donnees->getIdAnnulation()) ? " selected" : "";
                                            ?>
                                            <option value="<?= $donnees->getIdAnnulation() ?>" <?= $selected ?>> <?= $donnees->getLibelleAnnulation() ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class = "col">
                                    <label for = "montantarrhes">Montant des arrhes (Euros):</label>
                                    <input type="number" placeholder="Montant des arrhes" step="0.01" class="form-control" id = "montantarrhes" name = "montantarrhes" value = "<?= $Participer->getMontantarrhes() ?>" <?= $disabled ?> >
                                </div>
                                <div class = "col">
                                    <label for ="idTypeReglementArrhes">Type de règlement arrhes :</label>
                                    <select name="idtypereglementarrhes" id="idtypereglementarrhes" size="1" class = "form-control" <?= $disabled ?> >
                                        <?php
                                        $mapperTypeRèglement = new \App\Repository\TypeReglementRepository;
                                        $reponse = $mapperTypeRèglement->getAll();
                                        echo '<option value="Null">Aucun</option>';
                                        foreach ($reponse as $donnees) {
                                            $selected = ($Participer->getIdTypeReglementArrhes() == $donnees->getIdTypeReglement()) ? " selected" : "";
                                            ?>
                                            <option value="<?= $donnees->getIdTypeReglement() ?>" <?= $selected ?>> <?= $donnees->getNomTypeReglement() ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class = "col">
                                    <label class="control-label">Les arhhres ont éré remboursées :</label>
                                    <div class="input-group">
                                        <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" id="arrhesRemboursees" name="arrhesRemboursees" <?= $Participer->getArrhesRemboursees() ? "checked" : ""; ?>  <?= $disabled ?>  maxlength="50">
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class = "col">
                                    <label for = "montanttotal">Solde des frais de sortie (Euros, s'ajoute aux arrhes si non remboursées) :</label>
                                    <input type = "number" placeholder="Solde des frais de sortie" step="0.01" class = "form-control" id = "montanttotal" name = "montanttotal" value = "<?= $Participer->getmontantTotal() ?>" <?= $disabled ?> >
                                </div>
                                <div class = "col">
                                    <label for ="idtypereglementmontanttotal">Type de règlement :</label>
                                    <select name="idtypereglementmontanttotal" id="idtypereglementmontanttotal" size="1" class = "form-control" <?= $disabled ?>  >
                                        <?php
                                        $mapperTypeRèglement = new \App\Repository\TypeReglementRepository;
                                        $reponse = $mapperTypeRèglement->getAll();
                                        echo '<option value="Null">Aucun</option>';
                                        foreach ($reponse as $donnees) {
                                            $selected = ($Participer->getIdTypeReglementMontantTotal() == $donnees->getIdTypeReglement()) ? " selected" : "";
                                            ?>
                                            <option value="<?= $donnees->getIdTypeReglement() ?>" <?= $selected ?>> <?= $donnees->getNomTypeReglement() ?> <?= $disabled ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class = "col">
                                    <label for = "commentaireparticipation">Commentaire:</label>
    <!--                                <textarea style="height: 210px;" id="commentaireParticipation" name="commentaireParticipation" placeholder="Commentaire sur la participation" class="form-control" <?= $disabled ?>  ><?= $Participer->getCommentaireParticipation(); ?></textarea>-->
                                    <textarea style="height: 210px;" id="commentaireParticipation" name="commentaireParticipation" placeholder="Commentaire sur la participation" class="form-control"><?= $Participer->getCommentaireParticipation(); ?></textarea>
                                </div>

                                <br>
                            </div>
                            <!-- FIN CARD -->
                        </div>
                    </div>
                </div>
                <?php
                $bouton1 = '<a href="liste-participant.php?idSortie=' . $idsortie . '"'
                        . ' style="margin-left: 1%;" class="btn btn-secondary">'
                        . '<i class="fa fa-backward"></i>'
                        . ' Retour à la liste des participants'
                        . '</a>';
                $bouton2 = '<a href="info-sortie.php?id=' . $idsortie . '"'
                        . ' style="margin-left: 1%;" class="btn btn-secondary">'
                        . '<i class="fa fa-backward"></i>'
                        . ' Retour à la sortie'
                        . '</a>';
                $footer_listeBoutons = [$bouton1, $bouton2];
                if ($idActivite) {
                    $bouton3 = '<a href="liste-sortie.php?Activite=' . $idActivite . '"'
                            . ' style="margin-left: 1%;" class="btn btn-secondary">'
                            . '<i class="fa fa-backward"></i>'
                            . ' Retour à la liste des sorties ' . $nomActivite
                            . '</a>';
                    $footer_listeBoutons[] = $bouton3;
                }
                include_once 'inc/footer-valider.php'
                ?>
        </form>
        <?php include_once 'info-adherent-modal.php' ?>

    </body>
    <script src ="../js/participant.js?v=<?= VERSION_APP ?>"></script>
</html>
