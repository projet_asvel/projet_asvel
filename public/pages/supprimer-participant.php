<?php

require_once '../../config/globalConfig.php';

use App\Repository\SortieRepository;
use App\Repository\ParticiperRepository;
use App\Security;

// if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
if (!Security::hasRole([Security::ROLE_ENCADRANT])) {
//    header("location: Accessdenied.php");
    echo "Merci de vous authentifier en tant que l'encadrant organisateur de la sortie" ;
    return false ;
}

// Seul le responsable de la sortie peut supprimer la participation
$mapperSortie = new SortieRepository();
// IG 28012021 Debut
// $Sortie = $mapperSortie->getEntityById($_POST['idSortie']);
// IG 28012021 Fin
$Sortie = $mapperSortie->getEntityById(filter_input(INPUT_POST, 'idSortie', FILTER_SANITIZE_STRING)
);

// LG 20200603 déac : tous les encadrants peuvent supprimer une participation
//if ($Sortie->getIdOrganisateur() !== $_SESSION['idadherent']) {
//    echo "Désolé, seul le responsable de la sortie peut effacer une participation" ;
//    return false ;
//}

//$mapperParticiper->deleteEntity($_GET['idAdherent'], $_GET['idSortie']);
//header("location:index.php");

$mapperParticiper = new ParticiperRepository();
// IG 28012021 Debut
// if ($mapperParticiper->deleteEntity($_POST['idAdherent'], $_POST['idSortie'])) {
// IG 28012021 Fin

if ($mapperParticiper->deleteEntity(filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'idSortie', FILTER_SANITIZE_STRING))) {
    echo "OK" ;
    return true ;
} else {
    echo "Echec de la suppression" ;
    return false ;
}

