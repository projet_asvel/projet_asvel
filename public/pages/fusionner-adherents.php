<?php
// LG 20220910

require_once '../../config/globalConfig.php';

use App\Repository\AdherentRepository;
use App\Repository\SortieRepository;
use App\Security;

if (!Security::hasRole([Security::ROLE_SECRETAIRE])) {
    echo "Merci de vous authentifier en tant qu'utilisateur avec droits de suppression" ;
    return false ;
}

// $repoSortie = new SortieRepository() ;
// $sorties = $repoSortie->getAllByAdherent(filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING)) ;

// if (count($sorties) > 0) {
//     echo "Cet adhérent ne peut pas être supprimé car il a participé ou est inscrit à des sorties." ;
// }

$repoAdherent = new AdherentRepository();
$liAdherentAConserver = filter_input(INPUT_POST, 'idAdherentAConserver', FILTER_SANITIZE_STRING) ;
$liAdherentAVirer = filter_input(INPUT_POST, 'idAdherentAVirer', FILTER_SANITIZE_STRING) ;
$loAdherentAConserver = $repoAdherent->getEntityById($liAdherentAConserver) ;
if (!$loAdherentAConserver) {
    // L'adhérent n'existe plus
    echo "L'adhérent à conserver n'existe plus" ;
    return false ;
}
$loAdherentAVirer = $repoAdherent->getEntityById($liAdherentAVirer) ;
if (!$loAdherentAVirer) {
    // L'adhérent n'existe plus
    echo "L'adhérent à qui aurait du être éliminé n'existe plus" ;
    return false ;
}

$lsMsgErr = "" ;
if ($repoAdherent->fusionnerAdhérents($loAdherentAConserver, $loAdherentAVirer, $lsMsgErr)) {
    echo "OK : Les données de " . $loAdherentAConserver->getNomUsuel() . " ont été conservées, et celles de " . $loAdherentAVirer->getNomUsuel() . " y ont été ajoutées." ;
    return true ;
} else {
    echo "Une erreur est survenue : " . $lsMsgErr ;
    return false ;
    
}

// if ($repoAdherent->deleteEntityId()) {
//     echo "OK" ;
//     return true ;
// } else {
//     echo "Echec de la suppression" ;
//     return false ;
// }

