<?php
require_once '../../config/globalConfig.php';
use App\Security;


if (!Security::hasRole([Security::ROLE_SECRETAIRE])) {
	header("location: Accessdenied.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>ASVEL Ski Montagne</title>
        <?php //<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script> ?>
        <?php include_once 'inc/head.php'; ?>
    </head>
    <body>
        <header>
            <?php include_once 'inc/header.php'; ?>
            <script src="../js/Adherent.js?v=<?= VERSION_APP ?>"></script>
        </header>
        <div class="flex-container" style="padding: 10px;">
			<div id="explications" style="display: inline-block;">
				L'importation permet de récupérer des données saisies sur les sites de la FFME et de la FFS.<br>
				Les deux sites fournissent des fichiers de type CSV, qui peuvent s'ouvrir avec Excel, mais dont la structure est différente.<br>
				La FFME fournit un seul type de fichier, qui contient tous les adhérents inscrits.<br>
				La FFS fournit deux types de fchiers : l'un pour les "adhérents simples", l'autre pour les "compétiteurs-dirigeants".<br>
				<br>
				Pour chaque ligne du fichier à importer, l'importation va rechercher parmi les données actuelles si l'adhérent existe déja : ily a 3 cas de figure : 
				<ul>
					<li>
						Certains adhérents sont retrouvés sans ambiguité : les noms, prénoms et adresse mail correspondent parfaitement, ou alors les noms, prénoms et date de naissance correspondent parfaitement.<br>
						Ceux-là sont simplement mis à jour.
					</li>
					<li>
						D'autres sont retrouvés avec ambiguité : 
						<ul>
							<li>
								les nom et prénom correspondent, mais pas l'adresse mail ni la date de naissance ;
							</li>
							<li>
								les nom et date de naissance correspondent mais pas le reste ;
							</li>
							<li>
								l'adresse mail correspond mais pas le reste;
							</li>
							<li>
								le numéro de téléphone correspond mais pas le reste.
							</li>
						</ul>
						Ceux-là seront importés mais il sera possible de les effacer à partir du rapport d'importation.
					</li>
					<li>
						Tous les autres seront simplement insérés dans la liste des adhérents.
					</li>
				</ul>
			</div>
			<div id="form">
				<br>
				<form method="POST" action="" id="formimport">
					<label for="typeImport">Séléctionnez le type de fichier à importer</label>
					<select name="typeImport" id="typeImport">
						<option value="FFME">FFME</option>
						<option value="FFSLoisirs">FFS loisir</option>
						<option value="FFSCompDir">FFS compétiteurs-dirigeants</option>
					</select>
					<br>
					<label for="avatar">Séléctionnez le fichier à importer</label>
					<input type="file" id="avatar" name="avatar" accept=".csv" style="width: 100%;"/>
					<br>
					<input type="submit" value="importer" style="width: 120px;"/>
					<br>
				</form>
			</div>
			<div id="résultat" style="overflow: auto;border-style: groove;padding: 5px;max-height: 500px;">
			</div>
		</div>
		<script src="../js/import.js?v=<?=VERSION_APP ?>"></script>
        <script src ="../js/adherent.js?v=<?=VERSION_APP ?>"></script>

        <?php include_once 'inc/footer.php'; ?>
    </body>
</html>
