<?php

require_once '../../config/globalConfig.php';

use App\Repository\AdherentRepository;
use App\Repository\SortieRepository;
use App\Security;

if (!Security::hasRole([Security::ROLE_SECRETAIRE])) {
//    header("location: Accessdenied.php");
    echo "Merci de vous authentifier en tant qu'utilisateur avec droits de suppression" ;
    return false ;
}

$repoSortie = new SortieRepository() ;
// IG 28012021 Debut
// $sorties = $repoSortie->getAllByAdherent($_POST['idAdherent']) ;
// IG 28012021 Fin

$sorties = $repoSortie->getAllByAdherent(filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING)) ;

if (count($sorties) > 0) {
    echo "Cet adhérent ne peut pas être supprimé car il a participé ou est inscrit à des sorties." ;
}

$repoAdherent = new AdherentRepository();
// IG28012021
// if ($repoAdherent->deleteEntityId($_POST['idAdherent'])) {
if ($repoAdherent->deleteEntityId(filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING))) {
    echo "OK" ;
    return true ;
} else {
    echo "Echec de la suppression" ;
    return false ;
}

