<?php
require_once '../../config/globalConfig.php';

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">

<?php 

    $valider = true ;
    if (isset($_GET["mail"])) {
        // IG 28012021 Debut
        // $mail = $_GET["mail"] ; 
        // IG 28012021 Fin

        $mail = filter_input(INPUT_GET, 'mail', FILTER_SANITIZE_STRING);
    } else {
        $mail = "" ;
    }
?>

<html>
    <body>

        <?php include_once 'inc/header.php'; ?>

        <h1 style='text-align: center;text-decoration: underline;'></h1>
    
        <form action="" method="POST" id="envoi-MDP-demande">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Demander un nouveau mot de passe</h4>
                    </div>
                    <div class="card-body">
                        <div class="row py-2">
                            <div class="col">
                                Vous pouvez désormais gérer vos demandes d'inscriptions sur l'application de l'ASVEL.<BR>
                                Les demandes que vous ferez seront prises en compte par les encadrants avec les règles de priorité suivantes : <BR> 
                                <UL>
                                    <LI>
                                        Les débutants sont prioritaires sur les sorties initiation
                                    </LI>
                                    <LI>
                                        Les adhérents qui ont fait une sortie avec l'ASVEL récemment ne sont pas prioritaires
                                    </LI>
                                    <LI>
                                        Les adhérents qui n'ont pas été retenus lors de leurs précédentes demandes sont prioritaires
                                    </LI>
                                </UL>
                            </div>    
                        </div>    
                        <div class="row py-2"></div>    
                        <div class="row py-2"></div>    
                        <div class="row py-2"></div>    
                        <div class="row py-2">
                            <div class="col">
                                <label for="mail">
                                    Indiquez ici l'adresse mail que vous avez indiqué lors de votre inscription à l'ASVEL Ski Montagne
                                </label>
                                <input type="email" class="form-control" name="mail" id="mail" placeholder="votre mail..." required="required" value="<?= $mail ?>">
                            </div>
                        </div>    
                        <div class="row py-2">
                            <div class="col">
                                <label for="mail">
                                    Pour prouver que vous n'êtes pas un robot, écrivez ici le nom de votre association (5 lettres majuscules qui commence par un A et se termine par un L)
                                </label>
                                <input type="text" class="form-control" name="captcha" id="captcha" placeholder="votre association..." required="required"">
                            </div>
                        </div>    
                        <div class="row py-2"></div>    
                        <div class="row py-2"></div>    
                        <div class="row py-2">
                            <div class="col">
                                Enfin validez avec le bouton en bas de la page.<br>
                                Nous vous enverrons un mail contenant votre nouveau mot de passe valable pour cette adresse mail.
                            </div>    
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    
        <?php //include_once 'inc/footer.php' ?>
    </body>

</html>
<script src="../js/envoi-MDP-demande.js?v=<?=VERSION_APP ?>"></script>
