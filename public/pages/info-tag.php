<?php
require_once '../../config/globalConfig.php';

use App\Entity\Tag;
use App\Repository\TagsRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
    header("location: Accessdenied.php");
}


if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mapperActivite = new TagsRepository();

    if ($id == null) {
        $Tag = new Tag([]);
    } else {
        $mapper = new TagsRepository();

        $Tag = $mapper->getEntityById($id);
    }
}
// LG 20200504 début
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
//$liste = 'tag.php">Retour à la liste des tags';
$valider = true ;
$bouton = '<a href="liste-tag.php"'
        . ' style="margin-left: 1%;" class="btn btn-secondary">'
        . '<i class="fa fa-backward"></i>'
        . ' Retour liste des tags'
        . '</a>';
$footer_listeBoutons = [$bouton];
// LG 20200504 fin

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action="" method="POST" id="formtag">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Tag</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="idtag"  id="idtag" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="nomTag">Nom du Tag :</label>
                                <input type="text" class="form-control" required="true" id="nomTag" name="nomTag" placeholder="Nom de l'activite..." value="<?= $Tag->getNomTag() ?>">
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script src="../js/Tag.js?v=<?=VERSION_APP ?>" ></script>

</html>
