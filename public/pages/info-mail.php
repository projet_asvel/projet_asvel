<?php
require_once '../../config/globalConfig.php';
require_once '../../src/App/Repository/AdherentRepository.php';

use App\Repository\AdherentRepository;
use App\Security;

// Gestion de la sécurité
if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}

// Mail demandé
$idMailDemandé = "";

if (isset($_GET["id"])) {
    // On a fourni un Id
    $repo = new App\Repository\MailRepository();
    $idMailDemandé = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mail = $repo->getEntityById($idMailDemandé);
} else {
    $mail = new App\Entity\Mail();
    $mail->setMailExpediteur(Lib\olitsMailer::$_infoMail['Sender']);
    if (isset($_GET["pourSortie"])) {
        // Pour un mail sur une sortie, les destinataires sont visibles par défaut
        $mail->setDestinatairesVisibles(true);
    } else {
        // Pour un autre mail, par défaut les destinataires ne sont pas visibles
        $mail->setDestinatairesVisibles(false);
    }
}

$adherents = filter_input(INPUT_GET, 'adherents', FILTER_SANITIZE_STRING);
$loExpéditeur = NULL;
if (isset($_GET["adherents"])) {
    $repo = new App\Repository\MailRepository();
    $lst = explode(",", $adherents);
} else {
    // On a fourni un Id de mail : mail préexistant
    $laDestinataires = $mail->getDestinataires();
    $lst = [];
    foreach ($laDestinataires as $loDestinataire) {
        $lst[] = $loDestinataire->getIdAdherent();
    }
    $loExpéditeur = $mail->getExpéditeur($mail->getIdMail());
}
if (isset($_GET["sujet"]) && !$mail->getSujetMail()) {
    $mail->setSujetMail(filter_input(INPUT_GET, 'sujet', FILTER_SANITIZE_STRING));
// IG 28012021 Debut
// $mail->setSujetMail($_GET["sujet"]);
// IG 28012021 Fin
}
$disabled = "";
if ($mail->getIdMail()) {
    // Il s'agit d'un mail ancien : lecture seule
    $disabled = 'disabled="disabled"';
}
// LG 20201010 début
else {
    // Mail nouveau : insérer la signature par défaut
    $mail->setContenuMail(SIGNATURE_MAIL);
}
// LG 20201010 fin

$valider = true;
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <body>

        <?php include_once 'inc/header.php';
        ?>
        <!--<section  style="padding: 10px;" id="contact" class="contact">-->
        <!--<h2 class="contact__heading title">Envoyer un e-mail :</h2>-->
        <!--<div class="contact__wrapper wrapper--large">-->
        <form class="contact__form" action="" id="formmail" method="post">

            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <?php
                        if ($idMailDemandé) {
                        ?>
                        <h4 class="mb-0">E-mail envoyé le <?= $mail->getDateMail() ?></h4>
                        <?php
                        } else {
                        ?>
                        <h4 class="mb-0">Envoyer un e-mail</h4>
                        <?php
                        }
                        ?>
                        
                    </div>

                    <input type="hidden" name="idmail" id="idmail" value="<?= $mail->getIdMail() ?>">
                    <input type="hidden" name="URLreferer" id="URLreferer" value="<?= $_SERVER['HTTP_REFERER'] ?>">
                    <input type="hidden" name="validation" value="OK">

                    <div class="card-body">

                        <div class="row py-2">
                            <?php if ($loExpéditeur) { ?>
                                <div class="col">
                                    <label for="Expediteur">Expéditeur :</label>
                                    <input name="Expediteur"
                                           class="form-control" type="text"  spellcheck="false"
                                           value="<?= $loExpéditeur->getPrenomAdherent() . " " . $loExpéditeur->getNomAdherent() ?>"
                                           disabled="disabled"
                                           >
                                </div>
                            <?php } ?>
                            <div class="col">
                                <label for="mailExpediteur">Répondre à :</label>
                                <input type="text" spellcheck="false" name="mailExpediteur" class="form-control"
                                       value="<?= $mail->getMailExpediteur() ?>"
                                       <?php
                                       echo $disabled;
                                       ?>
                                       >
                            </div>
                            <div class="col">
                                <label for="destinatairesVisibles">La liste des destinataires est visible à tous :</label>
                                <input type="checkbox" style="margin-right: calc(100% - 38px);" class="selectpicker form-control" name="destinatairesVisibles" 
                                       <?= $mail->getDestinatairesVisibles() ? "checked" : ""; ?>  <?= $disabled ?>  maxlength="50"
                                       >
                            </div>
                        </div>

                        <div class="row py-2">
                            <div class="col">
                                <label for="obj">Destinataire(s) : (en rouge ceux pour qui l'adresse mail n'est pas renseignée)</label>
                                <select id="destinataire" name="destinataires[]" class="form-control js-example-basic-multiple" name="states[]" multiple="multiple" style="width:100%;"
                                <?php
                                if ($mail->getIdMail()) {
                                    echo 'disabled="disabled"';
                                }
                                $Connexion = new AdherentRepository();
                                $tabAdherent = $Connexion->getAll();

                                foreach ($tabAdherent as $item) {
                                    $selected = "";
                                    foreach ($lst as $id) {
                                        echo $id;
                                        if ($item->getIdAdherent() == $id) {
                                            // Cet adhérent fait partie des destinataires demandés
                                            $selected = ' selected = "selected"';
                                        }
                                    }
                                    if (!$item->getMailAdherent()) {
                                        // Pas d'adresse mail
                                        $selected .= ' style="color: #F00" data-color="#F00" ' ;
                                    }
                                    echo '<option value="' . $item->getMailAdherent() . ":" . $item->getNomUsuel() . '"' . $selected . '>' . $item->getNomUsuel() . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col">
                                <label for="obj">Objet :</label>
                                <input type="text" spellcheck="false" name="obj" class="form-control"
                                       value="<?= $mail->getSujetMail() ?>"
                                       <?php
                                       echo $disabled;
                                       ?>
                                       >
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col">
                                <label for="contenu">Contenu :</label>
                                <textarea style="height: 200px;" id="contenuMail" class="form-control" name="contenu"
                                          <?php
//									  if ($mail->getIdMail()) {
//										  echo 'disabled="disabled"';
//									  }
                                          echo $disabled;
                                          ?>><?= $mail->getContenuMail() ?></textarea>
                                          <?php
                                          if ($mail->getIdMail() != NULL) {
//                        $liste = 'mail.php">Retour à la liste des mails';
                                              $bouton = '<a href="liste-mail.php"'
                                                      . ' style="margin-left: 1%;" class="btn btn-secondary">'
                                                      . '<i class="fa fa-backward"></i>'
                                                      . ' Retour liste des mails'
                                                      . '</a>';
                                              $footer_listeBoutons = [$bouton];
                                          }
                                          include_once 'inc/footer-valider.php';
                                          ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--</div>-->   
        <!--</section>-->

        <script src ="../js/login.js?v=<?= VERSION_APP ?>"></script>
        <script src="../js/Mail.js?v=<?= VERSION_APP ?>"></script>
        <script>
            $(document).ready(function () {
// LG 20220909 début, pour les couleurs si pas adresse, selon https://stackoverflow.com/questions/49801609/how-to-use-different-colors-for-each-select2-option
                // $('.js-example-basic-multiple').select2({placeholder: "Choisissez les destinataires"});
                function formatState(state) {
                    const option = $(state.element);
                    const color = option.data("color");

                    if (!color) {
                        return state.text;
                    }

                    return $(`<span style="color: ${color}">${state.text}</span>`);
                };

                $('.js-example-basic-multiple').select2({
                    placeholder: "Choisissez les destinataires",
                    templateResult: formatState,
                    templateSelection: formatState,
                      });
// LG 20220909 fin
            });
        </script>


    </body>

</html>






