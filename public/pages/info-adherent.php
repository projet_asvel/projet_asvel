<?php
require_once '../../config/globalConfig.php';
require_once 'info-adherent-prepare.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>ASVEL Ski Montagne</title>
        <?php include_once 'inc/head.php'; ?>
    </head>
    <body>

        <header>
            <?php include_once 'inc/header.php'; ?>
        </header>
        <div class="card">
            <main id="main" role="main" class="flex-item-fluid pam card-body">
                <table class="table table-striped">
                    <form action="" method="POST" id ="formadherent">
                        <input type="hidden" name="idAdherent" id="idAdherent" value="<?= $idAdherent ?>">
                        <fieldset style="margin-bottom: 40px;">
                            <div class="pull-left col-6" style="height: 50px;">
                                <button id="b1" class="pull-left" style="height: 30px; width: 30px;">-</button> 
                                <h2 id="t1" style="margin-left: 40px; cursor: pointer;">Infos Adherent <?= $adherentedit->getNomUsuel(); ?></h2>
                            </div>
                            <div class="float-right col-md-6 col-12" style="height: 50px;">
                                <div class="form-group">
                                    <a href="info-mail.php?adherents= <?= $adherentedit->getIdAdherent(); ?>"
                                       style="" class="btn btn-primary">
                                        <i class="fa fa-forward"></i>
                                        Envoyer un mail à l'adherent
                                    </a>
<?php // LG 20211127 début ?>
                                    <a href="liste-sortie.php?Adhérent= <?= $adherentedit->getIdAdherent(); ?>"
                                       style="" class="btn btn-primary">
                                        <i class="fa fa-forward"></i>
                                        Voir les sorties de l'adherent
                                    </a>
<?php // LG 20211127 fin ?>
                                </div>
                            </div>

                            <div id="principale">
                                <?php
                                // Imformations générales : dans un fichier à part pour pouvoir être utitlisé ailleurs
                                require_once 'info-adherent-général.php';
                                ?>
                            </div>

                            <?php
                            if ($lbAutoriseDataEtContacts) {
                                ?>
                                <div class="supp pull-left col-12">
                                    <button id="b2" class="pull-left" style="height: 30px; width: 30px;">+</button> 
                                    <h2 id="t2" style="margin-left: 40px; cursor: pointer;">Informations supplémentaires</h2>
                                </div>
                                <div id="supp" style="display: none;">
                                    <div class="float-left  col-md-6 col-12">                           
                                        <div class="form-group">
                                            <label class="control-label">Sexe</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <select class="selectpicker form-control" name="sexeAdherent" id="sexeAdherent" <?= $disabled ?>>
                                                        <?php
                                                        if ($adherentedit->getSexeAdherent() == NULL) {
                                                            ?>                                                    
                                                            <option value="NULL" hidden selected>Choisissez un élément</option>   
                                                            <?php
                                                        }

                                                        if ($adherentedit->getSexeAdherent() == "H") {
                                                            ?>                                                    
                                                            <option value="<?php echo $adherentedit->getIdTypeCertif(); ?>" hidden selected>Homme</option>   
                                                            <?php
                                                        }
                                                        if ($adherentedit->getSexeAdherent() == "F") {
                                                            ?>                                                    
                                                            <option value="<?php echo $adherentedit->getIdTypeCertif(); ?>" hidden selected>Femme</option>   
                                                            <?php
                                                        }
                                                        ?>  
                                                        <?php
                                                        if ($adherentedit->getSexeAdherent() == "") {
                                                            ?>                                                    
                                                            <option value="<?php echo $adherentedit->getIdTypeCertif(); ?>" hidden selected>Autre</option>   
                                                            <?php
                                                        }
                                                        ?> 
                                                        <option value="H"<?= $adherentedit->getSexeAdherent() == "H" ? "selected" : ""; ?>>Homme</option>
                                                        <option value="F"<?= $adherentedit->getSexeAdherent() == "F" ? "selected" : ""; ?>>Femme</option>
                                                        <option value=""<?= $adherentedit->getSexeAdherent() == "" ? "selected" : ""; ?>>Autre</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Nationalite</label>
                                            <div class="inputGroupContainer">
                                                <select  name="nationaliteAdherent" id="nationaliteAdherent" placeholder="Nationalite" class="form-control"  value="<?= $adherentedit->getNationaliteAdherent(); ?>" type="text" maxlength="50" <?= $disabled ?>>
                                                    <?= '<option value="' . $adherentedit->getNationaliteAdherent() . '"' . $selected . '>' . $adherentedit->getNationaliteAdherent() . "</option>"; ?>
                                                    <?php include_once 'nationalités.php' ?>                                                   
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Date de naissance</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="dateNaissanceAdherent" name="dateNaissanceAdherent" placeholder="" class="form-control"  value="<?= $adherentedit->getDateNaissanceAdherent(); ?>" type="date" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Adresse</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="adresseAdherent" name="adresseAdherent" placeholder="Adresse" class="form-control"  value="<?= $adherentedit->getAdresseAdherent(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Adresse 2</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="adresse2Adherent" name="adresse2Adherent" placeholder="Adresse2" class="form-control" value="<?= $adherentedit->getAdresse2Adherent(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Ville</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="villeAdherent" name="villeAdherent" placeholder="Ville" class="form-control"  value="<?= $adherentedit->getVilleAdherent(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Pays</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="paysAdherent" name="paysAdherent" placeholder="Pays" class="form-control"  value="<?= $adherentedit->getPaysAdherent(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Code Postal</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="cpAdherent" name="cpAdherent" placeholder="Code Postal" class="form-control"  value="<?= $adherentedit->getCpAdherent(); ?>" type="text" maxlength ="5" <?= $disabled ?>></div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-right  col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="control-label">Licence</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="licenseAdherent" name="licenseAdherent" placeholder="License" class="form-control"  value="<?= $adherentedit->getLicenseAdherent(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Certificat nom medecin</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="certifNomMedecin" name="certifNomMedecin" placeholder="Nom du medecin" class="form-control"  value="<?= $adherentedit->getCertifNomMedecin(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Type de certificat</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <select class="selectpicker form-control" name="idTypeCertif" id="idTypeCertif" <?= $disabled ?>>

                                                        <?php
                                                        if ($adherentedit->getIdTypeCertif() == NULL) {
                                                            ?>                                                    
                                                            <option value="NULL" disabled selected>Choisissez un élément</option>   
                                                            <?php
                                                        }


                                                        if ($adherentedit->getIdTypeCertif() == 1) {
                                                            ?>                                                    
                                                            <option value="<?php echo $adherentedit->getIdTypeCertif(); ?>" hidden selected>Loisir</option>   
                                                            <?php
                                                        }
                                                        if ($adherentedit->getIdTypeCertif() == 2) {
                                                            ?>                                                    
                                                            <option value="<?php echo $adherentedit->getIdTypeCertif(); ?>" hidden selected>Compétition</option>   
                                                            <?php
                                                        }
                                                        foreach ($Certificats as $donnees) {
                                                            ?>
                                                            <option value="<?php echo $donnees->getIdTypeCertificat(); ?>"> <?php echo $donnees->getNomTypeCertificat(); ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                        <!--                                                        <option value="1">competition</option>
                                                                                                                <option value="2">loisir</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Ne souhaite pas recevoir les mails d'avertissement ?</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <input type="checkbox" style="margin-right: 95%;" class="selectpicker form-control"  name="refusMailAdherent" id="refusMailAdherent"<?= $adherentedit->getRefusMailAdherent() ? "checked" : "" ?> <?= $disabled ?>>
                                                    <label></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Passeport</label>
                                            <div class=" inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <input type="text" name="passeports" id=""  class="form-control" style="width: 100%" disabled="disabled" value="<?php
                                                    $listePasseport = $adherentedit->getPasseports();
                                                    foreach ($listePasseport as $value) {
                                                        echo $value->getNomPasseport();
                                                    }
                                                    ?>">


                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Commentaire sur l'adherent</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i>
                                                    </span>
                                                    <textarea style="height: 210px;" id="commentaireAdherent" name="commentaireAdherent" placeholder="Commentaire sur l'adherent" class="form-control" <?= $disabled ?>><?= $adherentedit->getCommentaireAdherent(); ?></textarea>

                                                    <?php // 												<textarea id="certitype" name="commentaireAdherent" placeholder="Commentaire sur l'adherent" class="form-control" required="true"><?= $adherentedit->getCommentaireAdherent(); ? >"</textarea>
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <br>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            if ($lbAutoriseDataEtContacts) {
                                ?>

                                <div class="contact pull-left col-12"">

                                    <button id="b3" class="pull-left" style="height: 30px; width: 30px;">+</button>
                                    <h2 id="t3" style="margin-left: 40px; cursor: pointer;">Infos Contact</h2>
                                </div>
                                <div id="contact" style="display: none;">
                                    <div class="float-left  col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="control-label">Nom</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="nomContact" name="nomContact" placeholder="Nom du contact" class="form-control"  value="<?= $adherentedit->getNomContact(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Prénom</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="prenomContact" name="prenomContact" placeholder="Prenom du contact" class="form-control"  value="<?= $adherentedit->getPrenomContact(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Adresse</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="adresseContact" name="adresseContact" placeholder="Adresse du contact" class="form-control" value="<?= $adherentedit->getAdresseContact(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Adresse 2</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="adresse2Contact" name="adresse2Contact" placeholder="Adresse secondaire du contact" class="form-control" value="<?= $adherentedit->getAdresse2Contact(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="float-right  col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="control-label">Adresse Mail</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="mailContact" name="mailContact" placeholder="Adresse mail du contact" class="form-control"  value="<?= $adherentedit->getMailContact(); ?>" type="email" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Ville</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="villeContact" name="villeContact" placeholder="Ville du contact" class="form-control"  value="<?= $adherentedit->getVilleContact(); ?>" type="text" maxlength="50" <?= $disabled ?>></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Code Postal</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="cpContact" name="cpContact" placeholder="Code Postal du contact" class="form-control"  value="<?= $adherentedit->getCpContact(); ?>" type="text" maxlength ="5" <?= $disabled ?>></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Numéro de Téléphone</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="telContact" name="telContact" placeholder="Numéro de téléphone" class="form-control"  value="<?= $adherentedit->getTelContact(); ?>" type="tel" maxlength ="10" <?= $disabled ?>></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            if ($lbAutoriseAdministrationDesDroits) {
                                ?>
                                <div class="Roles pull-left col-12">
                                    <button id="b4" class="pull-left" style="height: 30px; width: 30px;">+</button>
                                    <h2 id="t4" style="margin-left: 40px; cursor: pointer;">Administration des droits</h2>
                                </div>

                                <div id="Roles" style="display: none;">
                                    <div class="float-left  col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="control-label">Rôles</label>
                                            <div class="inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <select id="select2-ROLES" class="js-example-basic-multiple" name="roles[]" id="roles" multiple="" style="width: 49%" <?= $AdminDisabled ?>>
                                                        <?php
                                                        $roles = $adherentedit->getRoles();
                                                        foreach ($listeRoles as $value) {
                                                            $selected = "";
                                                            foreach ($roles as $role) {
                                                                if ($role->getIdRole() == $value->getIdRole()) {
                                                                    // Ce role est dans la liste des rôles de l'adhérent en cours de chargement
                                                                    $selected = ' selected = "selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $value->getIdRole() . '"' . $selected . '>' . $value->getNomRole() . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="float-right col-md-6 col-12">
										<div class="float-left col-md-6 col-12">
											<label class="control-label">Mot de passe</label>
											<div class="form-group">
												<input type="button" class="btn btn-primary" value="Envoyer un nouveau mot de passe par mail" onclick="envoyerMDP(event)">
											</div>
										</div>
										<?php
											if ($lbEstAdherentConnecté) {
												// L'utilisateur en cours est l'adhérent édite : il peut changer son mdp
										?>
										<div class="float-right col-md-6 col-12">
											<div class="form-group">
												<label class="control-label">Changer mon mot de passe</label>
												<div class="inputGroupContainer">
													<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="mdpAdherent" name="mdpAdherent" placeholder="mot de passe" class="form-control" value="" type="password" maxlength="50"></div>
												</div>
											</div>
										</div> 
										<?php
											}
										?>
                                    </div> 
									

                                </div>
                                <?php
                            }   // fin de $lbAutoriseAdministrationDesDroits;
                            ?>
                            <div class="form-group ">
                                <div class="pull-left col-12">

                                    <?php
                                    if ($lbAutoriseDataEtContacts || $lbAutoriseAdministrationDesDroits) {
                                        ?>


                                    </div>
                                </div>
                                <?php
                            }   // fin de $lbAutoriseDataEtContacts || $lbAutoriseAdministrationDesDroits
                            ?>
                        </fieldset>
                        <?php
                        // Pied de page avec les boutons
                        $bouton = '<a href="liste-adherent.php"'
                                . ' style="margin-left: 1%;" class="btn btn-secondary">'
                                . '<i class="fa fa-backward"></i>'
                                . ' Retour liste des adherents'
                                . '</a>';
                        $footer_listeBoutons = [$bouton];
                        $valider = true;
                        include_once 'inc/footer-valider.php'
                        ?>
                    </form>
                </table>
            </main>
            <aside class="mod w20 pam aside">
            </aside>
        </div>
        <script src="../js/Adherent.js?v=<?= VERSION_APP ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                function toggleDiv(psIdBouton, psIdDiv, event) {
                    event.preventDefault();
                    if ($(psIdBouton).text() == "+") {
                        $(psIdBouton).text("-");
                    } else {
                        $(psIdBouton).text("+");
                    }
                    $(psIdDiv).toggle("slow");
                }
                $("#b1").click(function (event) {
                    toggleDiv("#b1", "#principale", event)
                });
                $("#b2").click(function (event) {
                    toggleDiv("#b2", "#supp", event)
                });
                $("#b3").click(function (event) {
                    toggleDiv("#b3", "#contact", event)
                });
                $("#b4").click(function (event) {
                    toggleDiv("#b4", "#Roles", event)
                });
                $("#t1").click(function (event) {
                    toggleDiv("#b1", "#principale", event)
                });
                $("#t2").click(function (event) {
                    toggleDiv("#b2", "#supp", event)
                });
                $("#t3").click(function (event) {
                    toggleDiv("#b3", "#contact", event)
                });
                $("#t4").click(function (event) {
                    toggleDiv("#b4", "#Roles", event)
                });
            });
        </script>


    </body>
</html>
