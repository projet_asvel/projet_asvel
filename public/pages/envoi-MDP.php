<?php
require_once '../../src/Lib/olitsMailer.php';
require_once '../../config/globalConfig.php';

use App\Repository\AdherentRepository;
use Lib\olitsMailer;
use App\Lib\Functions;

$repo = new AdherentRepository() ;

if (isset($_POST["idAdherent"])) {
    // On a fourni un id d'adhérent
    $idAdherent = filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_NUMBER_INT);
    $adherent = $repo->getEntityById($idAdherent) ;
    if (!$adherent) {
        echo "Adhérent non retrouvé" ;
        return false ;        
    }
} else if (isset($_POST["mail"])) {
    $mailAdherent = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL);
    $captcha = filter_input(INPUT_POST, 'captcha', FILTER_SANITIZE_STRING);
    if ($captcha !== "ASVEL") {
        echo "Le nom de votre association n'a pas été saisi correctement, nous ne pouvons pas vous distinguer d'un robot" ;
        return false ;        
    }
    
    $adherent = $repo->getEntityByLogin($mailAdherent) ;
    if (!$adherent) {
        echo "Cette adresse mail n'existe pas parmi nos adhérents" ;
        return false ;        
    }

}
    
if (!$adherent) {
    echo "Aucun adhérent n'a été passé en paramètre" ;
    return false ;
}

// LG 20220513 début : il faut ajouter le nom de l'adhérent pour que le mail ne passe pas en spam
// En fait, ça ne suffit pas pour éviter le spam
// $mail = $adherent->getMailAdherent() ;
$mail = $adherent->getMailAdherent()
        . ":" . $adherent->getNomUsuel() ;
// LG 20220513 fin
$subject = "Renouvellement du mot de passe ASVEL" ;
if ($mail === "luc.gilot@sfr.fr") {
    $MDP = "osemi262" ;
} else {
    $MDP = Functions::chaine_aleatoire() ;
}
// $MDP = "123" ;
$actual_link = "http://$_SERVER[HTTP_HOST]";
$body = "Votre nouveau mot de passe pour ". $actual_link ." est : " . $MDP ;

//$res = olitsMailer::EnvoyerMail($adresseExpediteur, $mail, $subject, $body);
$res = olitsMailer::EnvoyerMail($mail, $subject, $body);
if (is_bool($res)) {
    if (MDPCRYPTES) {
        $adherent->setMdpAdherent(password_hash($MDP, PASSWORD_DEFAULT));
    } else {
	$adherent->setMdpAdherent($MDP) ;
    }

// Pour pallier aux pb d'envoi de mail : ajouter temporairement le mdp au nom pour le visualiser et l'envoyer    
// $adherent->setNomAdherent($adherent->getNomAdherent() . " " . $MDP) ;
    
    $adherent = $repo->sauver($adherent) ;
    if (!$adherent) {
        // Echec de l'enregistrement
        echo "Echec de l'enregistrement du nouveau mot de passe" ;
        return false;
    }
} else {
    echo 'echec de l\'envoi du mail';
    return false ;
}

echo "true" ;
return true ;
