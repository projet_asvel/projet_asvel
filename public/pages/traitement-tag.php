<?php

require_once '../../config/globalConfig.php';

use App\Entity\Tag;
use App\Repository\TagsRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
$idTag = filter_input(INPUT_POST, 'idtag', FILTER_SANITIZE_STRING);
$nomTag = filter_input(INPUT_POST, 'nomTag', FILTER_SANITIZE_STRING);

//if ($idActivite == NULL) {
$datas = array(
    'idTag' => $idTag,
    'nomTag' => $nomTag,
);

try {
    $Tag = new Tag($datas);
    $TagRep = new TagsRepository();
    if ($TagRep->vérifier($Tag, $tabErr)) {
        $LTag = $TagRep->sauver($Tag);
//        echo 'true';
        echo $LTag->getIdTag() ;
    } else {
        echo $tabErr[0];
    }

    return true;
} catch (PDOException $e) {
    echo $e->getMessage();
}
//} else {
//    $datas = array(
//        'nomactivite' => $nomActivite,
//    );
//}


//header('Location: liste-tag.php');
