<?php
    require_once '../../config/globalConfig.php';

    use App\Repository\AdherentRepository ;
    use App\Security;

    // Gestion de la sécurité
    if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
	header("location: Accessdenied.php");
    }

    // Il faut être certain que le flag "Validé" n'est pas posé par erreur sur les participants en liste d'attente
    
    $mapper = new AdherentRepository() ;
    $lbActifsUniquement = true ;
    $listeAdherent = $mapper->getAll(false, $lbActifsUniquement) ;
?>

<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

	<body>
            <?php include_once 'inc/header.php'; ?>

		<div class="container col-11 py-3">
			<div class="row justify-content-center">
				<h4>Liste des Adhérents, affichage des priorités sur les sorties</h4>
			</div>
			<div class="row py-2"></div>
			<table class="table table-hover" id="table_adherent">
				<thead class="thead-dark">
					<tr>
						<th style="display: none">Id</th>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Dernières sorties dans les 30 derniers jours</th>
						<th title="Nombre de fois où l'adhérent n'a pas pu sortir bien qu'il en aie fait la demande">
                                                    Sorties refusées depuis
                                                </th>
						<th>Prochaine demande de participation</th>
						<th title="est débutant dans l'activité, doit louer son matériel à l'avance">Divers</th>
					</tr>
				<tbody>
					<?php
                                        $liNbJoursPériodeAConsidérer = 30 ;
					foreach ($listeAdherent as $loAdherent) {
						echo "<tr>";
						echo "<td style='display: none'>" . $loAdherent->getIdAdherent() . "</td>";
						echo "<td>" . $loAdherent->getNomAdherent() . "</td>";
						echo "<td>" . $loAdherent->getPrenomAdherent() . "</td>";
                                                
//                                                $ldDateLimite = date("Y-m-d", time() - $liNbJoursPériodeAConsidérer * 24 * 60 * 60) ;
//                                                if ($loDernièreSortie = $loAdherent->getDernièreSortie($ldDateLimite)) {
                                                if ($loDernièreSortie = $loAdherent->getDernièreSortie($liNbJoursPériodeAConsidérer)) {
                                                    // Une sortie a été trouvée dans les 30 jours précédents
                                                    $loNbJours = date_diff(date_create($loDernièreSortie->getDateFinSortie()), date_create(date('Y/m/d'))) ;
                                                    $liNbJours = $loNbJours->format('%a');
                                                    $lsDernièreSortie = "il y a " . $liNbJours . " jours (" . $loDernièreSortie->getTitre(true) . ")" ;
// LG 20210123 début
                                                    $liNb = $loAdherent->getNbDernièresSorties($liNbJoursPériodeAConsidérer) ;
                                                    if ($liNb <= 1) {
                                                        $lsDernièreSortie = "1 sortie, " . $lsDernièreSortie ;
                                                    } else {
                                                        $lsDernièreSortie = $liNb . " sorties, la dernière " . $lsDernièreSortie ;                                                      
                                                    }
// LG 20210123 fin
                                                } else {
                                                    // Pas de dernière sortie depuis 1 mois
                                                    $lsDernièreSortie = "aucune" ;
                                                    $liNbJours = 9999 ;
                                                }
						echo '<td>' . $lsDernièreSortie . "</td>";

                                                echo "<td>" . $loAdherent->getInfosSortiesRefusées(min([$liNbJours, $liNbJoursPériodeAConsidérer])) . "</td>";

                                                $loProchaineDemande = $loAdherent->getProchaineDemandeDeParticipation() ;
                                                if ($loProchaineDemande) {
                                                    $lsProchaineDemande = "<div style='display:none'>" . $loProchaineDemande->getDateDebutSortie() . "</div>"
                                                            . "<a href='' onclick='modifierParticipation(event, " . $loAdherent->getIdAdherent() . ", " .$loProchaineDemande->getIdSortie() . ")'>"
                                                            . $loProchaineDemande->getTitre(true)
                                                            . "</a> / " ;
                                                    if ($loProchaineDemande->nomFonction) {
                                                        $lsProchaineDemande .= " (" . $loProchaineDemande->nomFonction . ")" ;
                                                    } else if ($loProchaineDemande->validationParticipation) {
                                                        $lsProchaineDemande .= " <a href='' onclick='déValiderParticipation(event, " . $loAdherent->getIdAdherent() . ", " .$loProchaineDemande->getIdSortie() . ")'>dévalider</a>" ;                                                        
                                                    } else {
                                                        $lsProchaineDemande .= " <a href='' onclick='validerParticipation(event, " . $loAdherent->getIdAdherent() . ", " . $loProchaineDemande->getIdSortie() . ")'>valider</a>" ;
                                                    }
// LG 20210123 début                                                    
//                                                    $lsDebutant = $loProchaineDemande->debutant?"oui":"" ;
//                                                    $lsLoueMateriel = $loProchaineDemande->loueMateriel?"oui":"" ;
                                                    $lsDivers = "" ;
                                                    if ($loProchaineDemande->debutant) {
                                                        if ($lsDivers) $lsDivers .= ", " ;
                                                        $lsDivers .= "débutant" ;
                                                    }
                                                    if ($loProchaineDemande->loueMateriel) {
                                                        if ($lsDivers) $lsDivers .= ", " ;
                                                        $lsDivers .= "loue" ;
                                                    }
// LG 20210123 fin                                                    
                                                } else {
                                                    $lsProchaineDemande = "aucune" ;
// LG 20210123 début                                                    
//                                                    $lsDebutant = "" ;
//                                                    $lsLoueMateriel = "" ;
                                                    $lsDivers = "" ;
// LG 20210123 fin                                                    
                                                }
                                                echo "<td>" . $lsProchaineDemande . "</td>";
// LG 20210123 début                                                    
//                                                echo "<td>" . $lsDebutant . "</td>";
//                                                echo "<td>" . $lsLoueMateriel . "</td>";
                                                echo "<td>" . $lsDivers . "</td>";
// LG 20210123 fin                                                    
                                                
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
<?php include_once 'inc/footer.php' ?>
	</body>
	<script type="text/javascript" src="../js/datatables.min.js"></script>
	<script>
        $(document).ready(function () {
            var options = {"order": [[1, "asc"]],
                "language": {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }};
            var table = $('#table_adherent').DataTable(options);
        });

	</script>

</html>
<script src="../js/liste-adherent-priorite.js?v=<?=VERSION_APP ?>"></script>