<?php

require_once '../../config/globalConfig.php';

use App\Entity\Affilier;
use App\Repository\AffilierRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ENCADRANT))) {
    header("location: Accessdenied.php");
}
//var_dump($_POST) ;
$idAffiliation = filter_input(INPUT_POST, 'idAffiliation', FILTER_SANITIZE_STRING);
$idAdherent = filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING);
$idFederation = filter_input(INPUT_POST, 'idFederation', FILTER_SANITIZE_STRING);
$dateAffiliation = filter_input(INPUT_POST, 'dateAffiliation', FILTER_SANITIZE_STRING);
$dateFinAffiliation = filter_input(INPUT_POST, 'dateFinAffiliation', FILTER_SANITIZE_STRING);
$numeroLicense = filter_input(INPUT_POST, 'numeroLicense', FILTER_SANITIZE_STRING);
if (!$dateFinAffiliation) $dateFinAffiliation = null ;
if (isset($traitement_affiliation_IdAdherent)) {
    // L'appellant a fourni l'Id de l'adhérent (cas de l'enregistrement de l'affiliation directement depuis le formulaire de saisie d'un nouvel adhérent)
    $idAdherent = $traitement_affiliation_IdAdherent ;
}

//if ($idActivite == NULL) {
$datas = array(
    'idAffiliation' => $idAffiliation,
    'idAdherent' => $idAdherent,
    'idFederation' => $idFederation,
    'dateAffiliation' => $dateAffiliation,
    'dateFinAffiliation' => $dateFinAffiliation,
    'numeroLicense' => $numeroLicense,
);
try {
    $Affiliation = new Affilier($datas);
//var_dump($datas) ;
//var_dump($Affiliation) ;
    $AffiliationRep = new AffilierRepository();
    if ($AffiliationRep->vérifier($Affiliation, $tabErr)) {
        $LAffiliation = $AffiliationRep->sauver($Affiliation);
        echo $LAffiliation->getIdAffiliation() ;
    } else {
        echo $tabErr[0];
    }

    return true;
} catch (PDOException $e) {
    echo $e->getMessage();
}
