<?php
require_once '../../config/globalConfig.php';

use App\Entity\Difficulte;
use App\Repository\DifficulteRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
    header("location: Accessdenied.php");
}


if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mapperActivite = new DifficulteRepository();

    if ($id == null) {
        $Tag = new Difficulte([]);
    } else {
        $mapper = new DifficulteRepository();

        $Tag = $mapper->getEntityById($id);
    }
}
// LG 20200504 début
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
//$liste = 'difficulte.php">Retour à la liste des difficultés';
$valider = true ;
$bouton = '<a href="liste-difficulte.php"'
        . ' style="margin-left: 1%;" class="btn btn-secondary">'
        . '<i class="fa fa-backward"></i>'
        . ' Retour liste des difficultés'
        . '</a>';
$footer_listeBoutons = [$bouton];
// LG 20200504 fin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action="" method="POST" id="formdifficulte">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Difficulté</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="iddifficulte" id="idDifficulte" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="nomDifficulte">Nom de Difficulté :</label>
                                <input type="text" class="form-control" id="nomDifficulte" required="true" name="nomdifficulte" placeholder="Nom de Difficulte" value="<?= $Tag->getNomDifficulte() ?>">
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->

            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script src="../js/Difficulte.js?v=<?=VERSION_APP ?>"></script>

</html>
