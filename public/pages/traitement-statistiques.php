<?php require_once '../../config/globalConfig.php';?>
<link href="projet_asvel\public\css\style.css" rel="stylesheet">

<?php require_once '../../config/globalConfig.php';?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

<?php
//require('dates_activite_statistiques.php');

// Récupération des valeurs des champs datedebut et datefinsortie du formulaire HTML
$datedebutsortie = filter_input(INPUT_POST, 'datedebutsortie', FILTER_SANITIZE_STRING);
$datefinsortie = filter_input(INPUT_POST, 'datefinsortie', FILTER_SANITIZE_STRING);
$nomactivite = filter_input(INPUT_POST, 'nomactivite', FILTER_SANITIZE_STRING);
// Vérification si les champs datedebut et datefinsortie ont été remplis
if (!empty($datedebutsortie) && !empty($datefinsortie) and !empty($nomactivite) and isset($_POST['VALIDER'])) {

//pour test rapide en local sans formulaire de saisie de date et d'activite
// $datedebutsortie = "2020-06-14";
// $datefinsortie = "2020-06-14";
// $db = new PDO('pgsql:host=localhost;dbname=ASVEL','postgres','root');

//connexion BDD
$dsn = $infoBdd['type'] . ':host=' . $infoBdd['host'] . ';port=' . $infoBdd['port'] . ';dbname=' . $infoBdd['dbname'];
$db = new PDO($dsn, $infoBdd['user'], $infoBdd['pass']);

  // Préparation des requête SQL
    // 1 nombre adherent total
    $query = $db->prepare("
        select count(*) from adherent
    ");
    // 2 nombre adherent par activite
    $query2 = $db->prepare("
        select nomactivite,count(*) as nombre_adherent
        from adherent a
        join Participer p on p.idadherent = a.idadherent
        join Sortie s on s.idsortie = p.idsortie
        join Concerner c on c.idsortie = s.idsortie
        join Activite ac on ac.idactivite = c.idactivite
        where nomactivite = :nomactivite
        group by nomactivite
    ");
    // 3 nombre adherent entre deux dates
    $query3 = $db->prepare("
        select count(*) 
        from adherent as a
        join participer p on p.idadherent = a.idadherent
        join sortie s on s.idsortie = p.idsortie
        where datedebutsortie >= :datedebutsortie 
        and datefinsortie <= :datefinsortie
    ");
    // 4 nombre sortie total
    $query4 = $db->prepare("
        select count(*) from sortie
    ");
    // 5 nombre de sortie total par activite
    $query5 = $db->prepare("
        select nomactivite,count(*) as nbsortie 
        from sortie s
        join concerner c on s.idsortie = c.idsortie
        join activite a on c.idactivite = a.idactivite
        where nomactivite = :nomactivite
        group by a.nomactivite
    ");
    // 6 nombre de sortie entre deux dates
    $query6 = $db->prepare("
        select count(*) as nbsortie from sortie s
        where datedebutsortie >= :datedebutsortie 
        and datefinsortie <= :datefinsortie
    ");
    // 7 nombre d'adherents par sortie total
    $query7 = $db->prepare("
        select nomsortie,count(*) as nbadherent 
        from adherent a
        join participer p on a.idadherent = p.idadherent
        join sortie s on s.idsortie = p.idsortie
        group by nomsortie
    ");
    // 8 nombre d'adherents par sortie par activite
    $query8 = $db->prepare("
        select nomsortie,nomactivite,count(*) as nbadherent 
        from adherent a
        join participer p on a.idadherent = p.idadherent
        join sortie s on s.idsortie = p.idsortie
        join concerner c on c.idsortie = s.idsortie
        join activite ac on ac.idactivite = c.idactivite
        where nomactivite = :nomactivite
        group by nomsortie, nomactivite
    ");
    // 9 nombre adhérents par sortie de date à date
    $query9 = $db->prepare("
        select nomsortie,count(*) as nbadherent 
        from adherent a
        join participer p on a.idadherent = p.idadherent
        join sortie s on s.idsortie = p.idsortie
        join concerner c on c.idsortie = s.idsortie
        join activite ac on ac.idactivite = c.idactivite
        where s.datedebutsortie >= :datedebutsortie
        and s.datedebutsortie <= :datefinsortie
        group by nomsortie
        ");
    // 10 nombre de participations par adherents total ceux qui ont 0 participation sont pas affiches
    $query10 = $db->prepare("
        select nomadherent, count(*) as nbparticipation
        from participer p
        left join adherent a on a.idadherent = p.idadherent
        group by nomadherent
        ");
    // 11 nombre de participations par adherents par activite ceux qui ont 0 participation sont pas affiches
    $query11 = $db->prepare("
        select nomadherent,nomactivite, count(*) as nbpart_activite_adherent
        from participer p
        join adherent a on a.idadherent = p.idadherent
        join sortie s on s.idsortie = p.idsortie
        join concerner c on s.idsortie =  c.idsortie 
        join activite ac on ac.idactivite = c.idactivite
        where nomactivite = :nomactivite
        group by nomadherent,nomactivite
    ");
    // 12 nombre de participations par adherents entre deux dates ceux qui ont 0 participation sont pas affiches
    $query12 = $db->prepare("
        select nomadherent, count(*) as nbpart_adherent_date
        from participer p
        join adherent a on a.idadherent = p.idadherent
        join sortie s on s.idsortie = p.idsortie
        join concerner c on s.idsortie =  c.idsortie 
        join activite ac on ac.idactivite = c.idactivite
        where s.datedebutsortie >= :datedebutsortie 
        and s.datefinsortie <= :datefinsortie
        group by nomadherent
    ");
    // 13 nombre de sortie annules total
    $query13 = $db->prepare("
        select count(*)
        from sortie
        where idRaisonAnnulation notnull
    ");
    // 14 nombre de sortie annules par activite
    $query14 = $db->prepare("
        select nomactivite,count(*) as nbsortie_annule
        from sortie s
        join concerner c on s.idsortie = c.idsortie
        join activite a on a.idactivite = c.idactivite
        where idRaisonAnnulation notnull and nomactivite = :nomactivite
        group by nomactivite
    ");
    // 15 nombre de sortie annules entre deux dates
    $query15 = $db->prepare("
        select count(*) as nbsortie_annule_date
        from sortie s
        join concerner c on s.idsortie = c.idsortie
        join activite a on a.idactivite = c.idactivite
        where idRaisonAnnulation notnull
        and s.datedebutsortie >= :datedebutsortie
        and s.datefinsortie <= :datefinsortie
    ");
    // 16  nombre desistements aux sorties totales
    $query16 = $db->prepare("
        select count(*) 
        from participer p
        join sortie s on p.idsortie = s.idsortie
        join concerner c on c.idsortie = s.idsortie
        join activite a on a.idactivite = c.idactivite
        where p.idRaisonAnnulation is not null
    ");
    // 17  nombre desistements par activite
    $query17 = $db->prepare("
        select nomactivite, count(*) as nb_desistement 
        from participer p
        join sortie s on p.idsortie = s.idsortie
        join concerner c on c.idsortie = s.idsortie
        join activite a on a.idactivite = c.idactivite
        where p.idRaisonAnnulation is not null and nomactivite = :nomactivite
        group by nomactivite
    ");
    // 18  nombre desistements entre deux dates
    $query18 = $db->prepare("
        select count(*) as nb_desistement 
        from participer p
        join sortie s on p.idsortie = s.idsortie
        join concerner c on c.idsortie = s.idsortie
        join activite a on a.idactivite = c.idactivite
        where p.idRaisonAnnulation is not null
        and s.datedebutsortie >= :datedebutsortie
        and s.datefinsortie <= :datefinsortie
    ");
    // 19 nombre moyen de sortie par adherent
    $query19 = $db->prepare("
        SELECT AVG(nombre_sorties) FROM
        (SELECT COUNT(idSortie) as nombre_sorties FROM Participer GROUP BY idAdherent) as sous_requete;
    ");
    // 20 nombre moyen de sortie entre deux dates
    $query20 = $db->prepare("
        SELECT AVG(nombre_sorties) FROM
        (SELECT COUNT(participer.idSortie) as nombre_sorties FROM Participer
        INNER JOIN Sortie ON Sortie.idSortie = Participer.idSortie
        WHERE Sortie.dateDebutSortie  >= :datedebutsortie
        and Sortie.datefinSortie <= :datefinsortie
        GROUP BY idAdherent) as sous_requete;
    ");

    //echec requete benefices
//     SELECT SUM(benefice) AS total, AVG(benefice) AS moyenne
// FROM (
//   SELECT 
//     S.idsortie, 
//     (CASE 
//       WHEN COALESCE(S.tarifsortie, 0) = 0 THEN SUM(P.montanttotal) 
//       ELSE COALESCE(S.tarifsortie, 0) * (COUNT(*) - 1) 
//     END - (COALESCE(S.coutautoroute, 0) * COALESCE(S.nbVoituresSortie, 0) 
//     + :fraisKilometrique * COALESCE(S.distanceSortie, 0) * COALESCE(S.nbVoituresSortie, 0) 
//     + COALESCE(s.fraisannexe, 0))) / COUNT(*)::double precision AS benefice
//   FROM participer p 
//   JOIN sortie s ON p.idsortie = s.idsortie
//   JOIN Concerner C ON C.idSortie = S.idSortie
//   JOIN Activite A ON A.idActivite = C.idActivite
//   WHERE A.nomactivite = '$nomActivite' 
//   AND TO_DATE('2020-05-16', 'YYYY-MM-DD') <= dateDebutSortie 
//   AND TO_DATE('2023-01-05', 'YYYY-MM-DD') >= dateFinSortie 
//   GROUP BY s.idsortie
// ) AS beneficesparsortie;


  //echo $datedebutsortie . $datefinsortie;
  // Liaison des valeurs des champs datedebut et datefinsortie pour les requetes date à date
    $query3->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query3->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query6->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query6->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query6->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query6->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query9->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query9->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query12->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query12->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query15->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query15->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query18->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query18->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);
    $query20->bindValue(':datedebutsortie', $datedebutsortie, PDO::PARAM_STR);
    $query20->bindValue(':datefinsortie', $datefinsortie, PDO::PARAM_STR);

    //Liaison des valeurs du champ nomactivite pour les requetes l'utilisant
    $query2->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);
    $query5->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);
    $query8->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);
    $query11->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);
    $query14->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);
    $query17->bindValue(':nomactivite', $nomactivite, PDO::PARAM_STR);


    // echo ":nomactivite = " . $nomactivite."<br>";

    // Exécution des requêtes
    $query->execute();
    $query2->execute();
    $query3->execute();
    $query4->execute();
    $query5->execute();
    $query6->execute();
    $query7->execute();
    $query8->execute();
    $query9->execute();
    $query10->execute();
    $query11->execute();
    $query12->execute();
    $query13->execute();
    $query14->execute();
    $query15->execute();
    $query16->execute();
    $query17->execute();
    $query18->execute();
    $query19->execute();
    $query20->execute();


    // Récupération des résultats des requêtes
    $result = $query->fetch();
    $result3 = $query3->fetch(PDO::FETCH_NUM);
    $result4 = $query4->fetch();
    $result6 = $query6->fetch(PDO::FETCH_NUM);
    //exploitation des résultat
    echo "<h1 style='margin-top : 150px'>Affichage des statistiques</h1>";
    //echo '<class="navbar">';

        //query 1
        echo '<div class="navbar"></div>';
        echo "<h2>Nombre d'adhérents total:"."</h2>";
        echo "<h5>"."$result[0]" . "</h5>";

        //query 2
        echo '<div class="navbar"></div>';
        echo "<h2>nombre d'adherents par activité :</h2>";
        while ($row = $query2->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] . "</h5>"."<br>";
        }

        //query 3
        echo '<div class="navbar"></div>';
        echo "<h2>nombre d'adherent entre le $datedebutsortie et le $datefinsortie est de :" ."</h2>";
        echo "<h5>"."$result3[0]"."</h5>";

        //query 4
        echo '<div class="navbar"></div>';
        echo "<h2>Nombre de sorties totale: "."</h2>";
        echo "<h5>".$result4[0]."</h5>";

        //query 5
        echo "<h2>nombre de sorties par activité :</h2>";
        while ($row = $query5->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] . "</h5>"."<br>";
        }

        //query 6
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de sortie entre le $datedebutsortie et le $datefinsortie est de :</h2>";
        echo "<h5>".$result4[0]."</h5>";
        //query 7

        echo '<div class="navbar"></div>';
        echo "<h2>nombre d'adherents par sorties totale :</h2>";
        while ($row = $query7->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] ."</h5>". "<br>";
        }

        //query 8
        echo '<div class="navbar"></div>';
        echo "<h2>nombre d'adherents par sorties par activite :</h2>";
        while ($row = $query8->fetch(PDO::FETCH_NUM)) {
            echo "<h5>". $row[0] . ": " . $row[1] . ": " . $row[2]. "</h5>"."<br>";
        }

        //query 9
        echo '<div class="navbar"></div>';
        echo "<h2>nombre d'adherents par sorties entre le $datedebutsortie et le $datefinsortie est de :</h2>";
        while ($row = $query9->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] ."</h5>"."<br>";
        }

        //query 10
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de participations par adherents total :</h2>";
        while ($row = $query10->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] ."</h5>"."<br>";
        }

        //query 11
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de participations par adherents par activite :</h2>";
        while ($row = $query11->fetch(PDO::FETCH_NUM)) {
        echo "<h5>".$row[0] . ": " . $row[1] . ": " . $row[2]. "</h5>"."<br>";
        }

        //query 12
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de participations par adherents entre le $datedebutsortie et le $datefinsortie est de :</h2>";
        while ($row = $query12->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0] . ": " . $row[1] ."</h5>"."<br>";
        }

        //query 13
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de sortie annulees total :</h2>";
        while ($row = $query13->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

        //query 14
        echo '<div class="navbar"></div>';
        echo "<h2>nombre de sortie annulees par activite </h2>";
        while ($row = $query14->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0].": " . $row[1] ."</h5>"."<br>";
        }

        //query 15
        echo '<div class="navbar"></div>';
        echo "<h2>le nombre de sortie annulees entre le $datedebutsortie et le $datefinsortie est de </h2>";
        while ($row = $query15->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

        //query 16
        echo '<div class="navbar"></div>';
        echo "<h2>le nombre de désistements totales est de :</h2>";
        while ($row = $query16->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

        //query 17
        echo '<div class="navbar"></div>';
        echo "<h2>le nombre de désistements par activite est de :</h2>";
        while ($row = $query17->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0].": " . $row[1] ."</h5>"."<br>";
        }

        //query 18
        echo '<div class="navbar"></div>';
        echo "<h2>le nombre de désistements entre le $datedebutsortie et le $datefinsortie est de :</h2>";
        while ($row = $query18->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

        //query 19
        echo '<div class="navbar"></div>';
        echo "<h2>les nombre moyen de sortie par adherent est de :</h2>";
        while ($row = $query19->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

        //query 20
        echo '<div class="navbar"></div>';
        echo "<h2>les nombre moyen de sortie par adherent entre le $datedebutsortie et le $datefinsortie est de  :</h2>";
        while ($row = $query20->fetch(PDO::FETCH_NUM)) {
            echo "<h5>".$row[0]."</h5>"."<br>";
        }

    }
    else{echo "merci de saisir les dates";}
    ?>


    <br><br>        </div>
        <!-- LG 20211127 déac <script src ="../js/login.js?v=1.6">-->

        </script>
        
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!-- Footer -->
<footer class="pt-5 pb-4" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">ASVEL SKI MONTAGNE</h5>
                <ul class="f-address"style="margin-left: -17%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href='https://www.asvelskimontagne.fr/'>
                                        <img src="..\img\logo-asvel.jpg" alt="Logo asvel" width="150" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-3">
                            <div class="col-1"><i class="fas fa-map"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Addresse :</h6>
                                <p>245, cours Emile Zola 69100 Villeurbanne</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-1"><i class="fas fa-phone"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Téléphone :</h6>
                                <p>04 78 84 91 21</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PROCHAINES SORTIES</h5>
                <ul class="recent-post"style="margin-left: -15%">               
                                    </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">FÉDÉRATIONS</h5>
                <ul class="f-address">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\logoffme.png" alt="logo Omnisport" width="75">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\ffs-asvel.png"  width="75">
                            </div>
                        </div>
                    </li>
                </ul>
                <h5 class="mb-4 font-weight-bold" style="margin-top: 50px;">RÉSEAUX SOCIAUX</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li><a href="https://www.facebook.com/asvel.skimontagne/" title="facebook" style="display: block;")><i class="fab fa-facebook-f"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PARTENAIRES</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href="http://www.decathlon.fr/fr/store?store_id=PS_98">
                                    <img src="..\img\logodecathlon.png" alt="logo decathlon" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="https://www.omnisport-lyon.fr">
                                    <img src="..\img\omnisport-logo.png" alt="logo Omnisport" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="Contributeur.php"><img src="..\img\LogoOlits.png"  width="150"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="http://www.limoog.net">
                                    <img src="https://www.limoog.net/wp-content/uploads/2021/12/cropped-cropped-cropped-cropped-cropped-Logo-Limoog.png" width="150" style="background-color: white;" alt="logiciel planning médico social et alternance">
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright -->
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="text-center text-white">
                    &copy; Association loi de 1901 agrée jeunesse et sports, affiliée FFS - FFME | Tous droits réservés  |<a href='https://www.asvelskimontagne.fr/'>   ASVEL Ski Montagne</a> 
                </div>
            </div>
        </div>
    </div>
</section>
    </body>

</html>

     <style>
        h1 {;
            background-color: #AEA79F;
        }
        h2 {;
            color: black;
            background-color: #AEA79F;
            position: relative;
            left: 0;
            top: -21;
        }
        .navbar {
            height: 5px;
            min-width: -200px;
            max-width: 4000px;
            background-color: orange;
    }
    </style>