<?php
use App\Security;
// ob_start() ;
require_once '../../config/globalConfig.php';
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <body>

<?php
include_once 'inc/header.php';
?>

        <h1 style='text-align: center;font-size: 30px;'> Application de planning des sorties ASVEL et des inscriptions en ligne </h1>
        <h2 style='text-align: center;font-weight: normal;font-size: 25px;'> Bienvenue à l'ASVEL SKI MONTAGNE ! </h1>
        <div class="container">
            <div id="carousel_index" class="carousel slide" data-ride="carousel" style="">
                <div class="carousel-inner" style="display: block;
                     margin-left: auto;
                     margin-right: auto">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="../img/skieur.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/escalade.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/alpinisme.jpg" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/randonne.jpg" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/raquette.jpg" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/ski-de-fond.jpg" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../img/skirando.jpg" alt="Third slide">
                    </div>
                </div>
            </div>
            <br>
            <h3 style="font-size: 20px;">Découverte, perfectionnement, convivialité, conseils. Formation sécurité montagne.</h3>
            <p>Pour les débutants et les initiés :<br>
                - Toutes nos activités en montagne, hiver comme été, à la journée, en week-end, en séjour.<br>
                - Escalade en salle tous les soirs de la semaine. Encadrement par les moniteurs FEDEREAUX bénévoles FFS ET FFME de l'ASVEL Ski Montagne.</p>
            <br>
            <?php 
                if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                    include_once 'inc/explications.php' ;
                    echo "<br><br>" ;
                }
            ?>
        </div>
        <!-- LG 20211127 déac <script src ="../js/login.js?v=<?= VERSION_APP ?>">-->

        </script>
        <?php include_once 'inc/footer.php' ?>
    </body>

</html>
