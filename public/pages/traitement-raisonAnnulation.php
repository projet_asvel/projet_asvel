<?php

require_once '../../config/globalConfig.php';

use App\Entity\RaisonAnnulation;
use App\Repository\RaisonAnnulationRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
$idAnnulation = filter_input(INPUT_POST, 'idannulation', FILTER_SANITIZE_STRING);
$libelleAnnulation = filter_input(INPUT_POST, 'libelleannulation', FILTER_SANITIZE_STRING);

//if ($idActivite == NULL) {
$datas = array(
    'idannulation' => $idAnnulation,
    'libelleannulation' => $libelleAnnulation,
);


try {
    $RaisonAnnulation = new RaisonAnnulation($datas);
    $RaisonAnnulationRep = new RaisonAnnulationRepository();
    if ($RaisonAnnulationRep->vérifier($RaisonAnnulation, $tabErr)) {
        $LRaisonAnnulation = $RaisonAnnulationRep->sauver($RaisonAnnulation);
//        echo 'true';
        echo $LRaisonAnnulation->getIdAnnulation() ;
    } else {
        echo $tabErr[0];
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
//} else {
//    $datas = array(
//        'nomactivite' => $nomActivite,
//    );
//}


//header('Location: liste-raisonAnnulation.php');
