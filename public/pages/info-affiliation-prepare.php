<?php
require_once '../../config/globalConfig.php';

use App\Entity\Affilier;
use App\Repository\AffilierRepository;
use App\Entity\Adherent;
use App\Repository\AdherentRepository;
use App\Entity\Federation;
use App\Repository\FederationRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header("location: Accessdenied.php");
}

$mapperAffiliation = new AffilierRepository();
$mapperAdherent = new AdherentRepository();
$mapperFédération = new FederationRepository();
$idAffiliation = null ;
$idAdherent = null;
$idFederation = null;
$adherentDisabled = "";
if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $idAffiliation = filter_input(INPUT_GET, 'idAffiliation', FILTER_SANITIZE_STRING);
if (isset($_GET["idAdherent"]) || $idAffiliation) {

        // Adhérent imposé
        $adherentDisabled = 'disabled="disabled"';
    }
    if ($idAffiliation) {
        $affiliation = $mapperAffiliation->getEntityById($idAffiliation);
        $idAdherent = $affiliation->getIdAdherent();
        $idFederation = $affiliation->getIdFederation();
    } else {
        $idAdherent = filter_input(INPUT_GET, 'idAdherent', FILTER_SANITIZE_STRING);
        $idFederation = filter_input(INPUT_GET, 'idFederation', FILTER_SANITIZE_STRING);
    }
}
if (!isset($affiliation)) {
    $affiliation = new Affilier() ;
    $affiliation->setIdAdherent($idAdherent) ;
    $affiliation->setIdFederation($idFederation) ;
}

$disabled = "";
if (!(Security::hasRole(Array(Security::ROLE_SECRETAIRE)) || (Security::hasRole(Array(Security::ROLE_ENCADRANT)) && !($idAdherent && $idFederation)))) {
    // Seuls les secrétaires peuvent modifier une affiliation
    // Les encadrants peuvent en créer une nouvelle
    $disabled = 'disabled="disabled"';
    $adherentDisabled = $disabled ;
    
}

if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    // L'utilisateur en cours a les droits sur les données
} else {
    // Pas les droits
    header("location: Accessdenied.php");
}
?>
