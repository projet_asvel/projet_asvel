<?php
require_once '../../config/globalConfig.php';


use App\Materiel;
use App\Repository\MaterielRepository;

use App\Adherent;
use App\Repository\AdherentRepository;

use App\Security;

$loRepo = new MaterielRepository();

$idActivite = isset($_GET['Activite']) ? $_GET['Activite'] : Null;

$model_adherent = new AdherentRepository();



//$liAdherent = $_SESSION['idadherent'] ;
if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {

    $listeMateriel = $loRepo->getAll();
}
else
{
    $listeMateriel = $loRepo->getAllMaterielDispo();

}

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>
    <body>
        <?php include_once 'inc/header.php'; ?>

        <div class="container col-11 py-3">
            <div class="row justify-content-center">
                <h1 style="font-size: 25px;">
                <?php 
                    echo "Listes des matériels";
                ?>
                </h1>
            </div>
 
            <div class="row py-2"></div>
            <table class="table table-hover" id="table_sortie">
                <thead class="thead-dark">
                    <tr>
                        <th style="display: none">Id</th>
                        <th>Nom</th>
                        <th>Type de matériel</th>
                        <th>Pointure</th>
                        <th>Taille</th>
                        <th>Longitude 1</th>
                        <th>Latitude 1</th>
                        <th>Longitude 2</th>
                        <th>Latitude 2</th>
                        <th>Pays</th>
                        <th>Mise à dispo</th>
                        <th>Date d'achat</th>
                        <th>Contact</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($listeMateriel != null)
                    {
                        foreach ($listeMateriel as $materiel)
                        {
                            $adherent = $model_adherent->getEntityId($materiel->getIdAdherent());
                        ?>  
                            <tr >
                            <td style="display: none"><?= $materiel->getIdMateriel() ?></td>
                                <td><?= $materiel->getModele() ?></td>
                                <td><?= $loRepo->getTypeMat($materiel->getIdTypeMateriel()) ?></td>
                                <td><?= $materiel->getPointure() ?></td>
                                <td><?= $materiel->getTaille() ?></td>

                                <td><?= $materiel->getLongitude1() ?></td>
                                <td><?= $materiel->getLatitude1() ?></td>
                                <td><?= $materiel->getLongitude2() ?></td>
                                <td><?= $materiel->getLatitude2() ?></td>
                                <td><?= $materiel->getPays() ?></td>
                                
                                <td><?= $loRepo->getTypeDispo($materiel->getIdTypeMiseAdispo()) ?></td>
                                <td><?= $materiel->getDateAchat() ?></td>
                                <td><?= $adherent->getMailAdherent() ?></td>
                            </tr>
                
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php include_once 'inc/footer.php' ?>
    </body>
    <script type="text/javascript" src="../js/datatables.min.js"></script>

                    <?PHP
                    if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                        $location = '"liste-materiel_dispo.php?id=" + data[0]';
                    } else {
                        $location = '';
                    }
                    ?>
    <script>

        $(document).ready(function () {
            var options = {"order": [[0, "asc"]],
                "language": {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }};
            var table = $('#table_sortie').DataTable(options);
            $('#table_sortie tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                // window.location.assign(<?= $location ?>);
            });
        });

    </script>
</html>
