<?php
require_once '../../config/globalConfig.php';

use App\Entity\Adherent;
use App\Repository\AdherentRepository;
use App\Security;


if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
	header("location: Accessdenied.php");
}
// echo($_POST['nomAdherent']);
//if ($idActivite == NULL) {
$datas = array(
    'idAdherent' => filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING),
    'nomAdherent' => filter_input(INPUT_POST, 'nomAdherent', FILTER_SANITIZE_STRING),
    'prenomAdherent' => filter_input(INPUT_POST, 'prenomAdherent', FILTER_SANITIZE_STRING),
    'dateNaissanceAdherent' => filter_input(INPUT_POST, 'dateNaissanceAdherent', FILTER_SANITIZE_STRING),
    'adresseAdherent' => filter_input(INPUT_POST, 'adresseAdherent', FILTER_SANITIZE_STRING),
    'adresse2Adherent' => filter_input(INPUT_POST, 'adresse2Adherent', FILTER_SANITIZE_STRING),
    'mailAdherent' => filter_input(INPUT_POST, 'mailAdherent', FILTER_SANITIZE_STRING),
    'telAdherent' => filter_input(INPUT_POST, 'telAdherent', FILTER_SANITIZE_STRING),
    'cpAdherent' => filter_input(INPUT_POST, 'cpAdherent', FILTER_SANITIZE_STRING),
    'villeAdherent' => filter_input(INPUT_POST, 'villeAdherent', FILTER_SANITIZE_STRING),
    'paysAdherent' => filter_input(INPUT_POST, 'paysAdherent', FILTER_SANITIZE_STRING),
    'nationaliteAdherent' => filter_input(INPUT_POST, 'nationaliteAdherent', FILTER_SANITIZE_STRING),
    'sexeAdherent' => filter_input(INPUT_POST, 'sexeAdherent', FILTER_SANITIZE_STRING),
    'droitImageAdherent' => filter_input(INPUT_POST, 'droitImageAdherent', FILTER_SANITIZE_STRING),
    'refusMailAdherent' => filter_input(INPUT_POST, 'refusMailAdherent', FILTER_SANITIZE_STRING),
    'idTypeReglementAdherent' => filter_input(INPUT_POST, 'idTypeReglementAdherent', FILTER_SANITIZE_STRING),
    'montantReglementAdherent' => filter_input(INPUT_POST, 'montantReglementAdherent', FILTER_SANITIZE_STRING),
    'commentaireAdherent' => filter_input(INPUT_POST, 'commentaireAdherent', FILTER_SANITIZE_STRING),
    'prenomContact' => filter_input(INPUT_POST, 'prenomContact', FILTER_SANITIZE_STRING),
    'nomContact' => filter_input(INPUT_POST, 'nomContact', FILTER_SANITIZE_STRING),
    'adresseContact' => filter_input(INPUT_POST, 'adresseContact', FILTER_SANITIZE_STRING),
    'adresse2Contact' => filter_input(INPUT_POST, 'adresse2Contact', FILTER_SANITIZE_STRING),
    'villeContact' => filter_input(INPUT_POST, 'villeContact', FILTER_SANITIZE_STRING),
    'cpContact' => filter_input(INPUT_POST, 'cpContact', FILTER_SANITIZE_STRING),
    'mailContact' => filter_input(INPUT_POST, 'mailContact', FILTER_SANITIZE_STRING),
    'telContact' => filter_input(INPUT_POST, 'telContact', FILTER_SANITIZE_STRING),
    'idTypeCertif' => filter_input(INPUT_POST, 'idTypeCertif', FILTER_SANITIZE_STRING),
    'certifNomMedecin' => filter_input(INPUT_POST, 'certifNomMedecin', FILTER_SANITIZE_STRING),
    'certifAlpi' => filter_input(INPUT_POST, 'certifAlpi', FILTER_SANITIZE_STRING),
    'saeAdherent' => filter_input(INPUT_POST, 'saeAdherent', FILTER_SANITIZE_STRING),
    'roles' => implode(",", filter_var_array(isset($_POST["roles"])?$_POST["roles"]:[])),
    'tags' => implode(",", filter_var_array(isset($_POST["tags"])?$_POST["tags"]:[])),
    
    
// LG 20200214 : réactiver quand les passeports seront inclus dans EditAdherent.php    'passeports' => implode(",", filter_var_array ($_POST["passeports"])),
);


// LG 20200224 début
// Gestion des booleens : ils ne sont pas passés quand "False"
if (!$datas["certifAlpi"]) {
	$datas["certifAlpi"] = false ;
}
if (!$datas["refusMailAdherent"]) {
	$datas["refusMailAdherent"] = false ;
}
if (!$datas["droitImageAdherent"]) {
	$datas["droitImageAdherent"] = false ;
}

// LG 20200224 fin
try{
$Adherent = new Adherent($datas);
$AdherentRep = new AdherentRepository();
$Ladherent = $AdherentRep->sauver($Adherent);
echo 'true';
} catch(PDOException $e){

    $AdherentRep->vérifier($Adherent, $tabErr);
    echo $tabErr[0];    

}
//if (!$Ladherent) {
//	// Erreur
//	    echo 'Échec de l\'enrengistrement : ' . $e->getMessage();
//}
// LG 20200214 old header('Location: index.php');
//header('Location: info-adherent.php?id=' . $Ladherent->getIdAdherent());

    
