<?php
require_once '../../config/globalConfig.php';
// LG 20211127 deac require_once 'inc/head.php';
require '../../vendor/PDF/autoload.php';

use App\Entity\Adherent;
use App\Entity\Sortie;
use App\Entity\Difficulte;
use App\Entity\RaisonAnnulation;
use App\Repository\SortieRepository;
use App\Repository\AdherentRepository;
use App\Repository\DifficulteRepository;
use App\Repository\ActiviteRepository;
use App\Repository\ParticiperRepository;
use App\Repository\RaisonAnnulationRepository;
use App\Security;
use Spipu\Html2Pdf\Html2Pdf;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header('Location: Accessdenied.php');
//	echo "Merci de vous authentifier pour accéder à cette fonctionnalité" ;
    return;
}

ob_start();     // LG 20211127

$position = 'L';
$format = 'A4';
$langue = 'fr';
$encodage = 'UTF-8';
$font = 'Arial';
// LG 20220910 déac, passé + bas $nomFichier = 'ficheSortie.pdf';

//Variables Tarifs Sortie :
$FraisKilometriques = FRAIS_KILOMETRIQUES;

// LG 20201011 déac début : passé dans info-tarifs
//$TarifJournéeRandonée = TARIF_SORTIE_AUTRE_1J . "€";
//$TarifJournéeRaquettes = TARIF_SORTIE_AUTRE_1J . "€";
//$TarifJournéeSkiRandonnée = TARIF_SORTIE_AUTRE_1J . "€";
//$TarifJournéeAlpinisme = TARIF_SORTIE_ALPI_1J . "€";
//$TarifJournéeEscalade = TARIF_SORTIE_ESCALADE_1J . "€";
//
//$TarifWeekEndRandonée = TARIF_SORTIE_AUTRE_2J . "€";
//$TarifWeekEndRaquettes = TARIF_SORTIE_AUTRE_2J . "€";
//$TarifWeekEndSkiRandonnée = TARIF_SORTIE_AUTRE_2J . "€";
//$TarifWeekEndAlpinisme = TARIF_SORTIE_ALPI_2J . "€";
//$TarifWeekEndEscalade = TARIF_SORTIE_ESCALADE_2J . "€";
//
//$TarifWeekEnd3JoursRandonée = TARIF_SORTIE_AUTRE_3J . "€";
//$TarifWeekEnd3JoursRaquettes = TARIF_SORTIE_AUTRE_3J . "€";
//$TarifWeekEnd3JoursSkiRandonnée = TARIF_SORTIE_AUTRE_3J . "€";
//$TarifWeekEnd3JoursAlpinisme = TARIF_SORTIE_ALPI_3J . "€";
//$TarifWeekEnd3JoursEscalade = TARIF_SORTIE_ESCALADE_3J . "€";
//
//$TarifJournéeSkiPiste = TARIF_SORTIE_SKIPISTE_1J;
//$TarifWeekEndSkiPiste = TARIF_SORTIE_SKIPISTE_2J . "€";
//$TarifWeekEnd3JoursSkiPiste = TARIF_SORTIE_SKIPISTE_3J . "€";
// LG 20201011 déac fin

$RepoActivites = new ActiviteRepository();
$ListeActivites = $RepoActivites->getAll();

$RepoDifficulte = new DifficulteRepository();
$RepoAdherents = new AdherentRepository();
$RepoParticipants = new ParticiperRepository();
$RepoTypeRèglement = new \App\Repository\TypeReglementRepository;

if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING);
    $Adherent = new Adherent([]);
    $mapperAdherent = new AdherentRepository();
    $Adherents = $mapperAdherent->getAll();
    $nomOrganisateur = $mapperAdherent->getNomOrganisateurBySortie($id);
    $Difficulte = new Difficulte([]);
    $mapperDifficulte = new DifficulteRepository();
    $Raisonannulation = new RaisonAnnulation([]);
    $mapperRaisonAnnulation = new \App\Repository\RaisonAnnulationRepository();
    $raison = $mapperRaisonAnnulation->getAll();
    $LaRaison = $mapperRaisonAnnulation->getLibelleRaisonBySortie($id);
    $Difficultes = $mapperDifficulte->getAll();
    $nomDifficultes = $mapperDifficulte->getNomDifficulteBySortie($id);
    $mapperSortie = new SortieRepository();
    if ($id == null) {
        $Sortie = new Sortie([]);
    } else {
        $mapper = new SortieRepository();
        $Sortie = $mapper->getEntityId($id);
        $Info_Adherent = $mapperAdherent->getEntityId($Sortie->getIdOrganisateur());
//$Info_Difficulte = $mapperDifficulte->getEntityId($Sortie->getId_Difficulte());
    }
}

$nomFichier = 'ficheSortie ' . $Sortie->getNomSortie() . '.pdf';


$difficulte = $RepoDifficulte->getEntityById($Sortie->getIdDifficulte());
if ($difficulte) {
    $difficultename = $difficulte->getNomDifficulte();
} else {
    $difficultename = "non précisé";
}
$activiténame = $Sortie->getNomsActivites();

$DébutSortie = new DateTime($Sortie->getDateDebutSortie());
$FinSortie = new DateTime($Sortie->getDateFinSortie());
$Différence = date_diff($DébutSortie, $FinSortie);
$NombreJour = $Différence->format('%a jours') + 1;

$nbEncadrants = 1;
$NomEncadrant = "<strong>" . $Sortie->getOrganisateur()->getNomUsuel() . "</strong>";
$coEncadrants = $RepoParticipants->getAllCoEncadrantsByIdSortie($id);
foreach ($coEncadrants as $value) {
    $nbEncadrants++;
    $Adherent = $mapperAdherent->getEntityById($value->getIdAdherent());
    $NomEncadrant .= "<br>" . $Adherent->getNomUsuel();
}
$NbParticipantsMax = $Sortie->getNombreParticipantsMaxSortie();
if (!$NbParticipantsMax) {
    $NbParticipantsMax = "non précisé";
}

$listeParticipant = $RepoParticipants->getAllAdherentByIdSortie($id);
$effectif = 0;
$numéroParticipant = 0;
$tableParticipant = '';
$TotantMontantTotal = 0;
$TotantMontantArrhes = 0;
$NbDVAPS = 0;
$TotantMontantLocation = 0;
$TotalMontantEspèces = 0;
$TotauxParTypeRèglement = [];
$TotauxParTypeRèglement[TYPEREGELEMENT_ESPECES] = 0;
$TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUE] = 0;
$TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUEVACANCES] = 0;
$ParticipantsAnnulés = "";
$ParticipantsNonValidés = "";
// LG 20201011 déac $NbEncadrants = 0 ;
$nbExclusCovoiturage = 0; // LG 20201011

foreach ($listeParticipant as $Participant) {

    $styleArrhes = "";
    if ($Participant->getMontantArrhes() == 0 || $Participant->getMontantArrhes() == NULL) {
        $montantArrhes = " ";
        $typeReglementArrhes = "";
    } else {
        $montantArrhes = $Participant->getMontantArrhes() . " €";
        if (!$Participant->getIdTypeReglementArrhes()) {
            $typeReglementArrhes = "non réglé";
        } else {
            $Type = $RepoTypeRèglement->getEntityById($Participant->getIdTypeReglementArrhes());
            $typeReglementArrhes = $Type->getNomTypeReglement();
        }
        if ($Participant->getArrhesRemboursees()) {
            $styleArrhes = "text-decoration: line-through;";
            $typeReglementArrhes = "remboursé";
        }
    }

// LG 20211127 déplacé + bas début
//    if ($Participant->getmontantTotal() == 0 || $Participant->getmontantTotal() == NULL) {
//        $montantTotal = " ";
//        $typeReglement = "";
//    } else {
//        $montantTotal = number_format($Participant->getmontantTotal(), 2) . " €";
//        if (!$Participant->getIdTypeReglementMontantTotal()) {
//            $typeReglement = "non réglé";
//        } else {
//            $Type = $RepoTypeRèglement->getEntityById($Participant->getIdTypeReglementMontantTotal());
//            $typeReglement = $Type->getNomTypeReglement();
//            $TotauxParTypeRèglement[$Participant->getIdTypeReglementMontantTotal()] += $Participant->getmontantTotal();
//        }
//    }
// LG 20211127 déplacé + bas fin

    if ($Participant->getIdRaisonAnnulation()) {
        // Ce participant est annulé
        if (trim($montantArrhes . $montantTotal)) {
            // Ce participant a payé des arrhes ou le tarif de sortie
            $reglementParticipantAnnulé = "";
            if (trim($montantArrhes)) {
                $reglementParticipantAnnulé .= "arrhes : " . $montantArrhes . " payé " . $typeReglementArrhes;
            }
            if (trim($montantTotal)) {
                if ($reglementParticipantAnnulé)
                    $reglementParticipantAnnulé .= ", ";
                $reglementParticipantAnnulé .= "solde : " . $montantTotal . " payé " . $typeReglement;
            }
        } else {
            // Ce participant n'a rien payé
            $reglementParticipantAnnulé = "aucun paiement en cours";
        }
        if ($ParticipantsAnnulés)
            $ParticipantsAnnulés .= ", ";
        $ParticipantsAnnulés .= $Participant->getAdherent()->getNomUsuel()
                . " (" . $reglementParticipantAnnulé . ")";

        continue;
    }

// LG 20211127 déplacé de + haut début
    if ($Participant->getmontantTotal() == 0 || $Participant->getmontantTotal() == NULL) {
        $montantTotal = " ";
        $typeReglement = "";
    } else {
        $montantTotal = number_format($Participant->getmontantTotal(), 2) . " €";
        if (!$Participant->getIdTypeReglementMontantTotal()) {
            $typeReglement = "non réglé";
        } else {
            $Type = $RepoTypeRèglement->getEntityById($Participant->getIdTypeReglementMontantTotal());
            $typeReglement = $Type->getNomTypeReglement();
            $TotauxParTypeRèglement[$Participant->getIdTypeReglementMontantTotal()] += $Participant->getmontantTotal();
        }
    }
// LG 20211127 déplacé de + haut fin
    
    if (!$Participant->getValidationParticipation()) {
        // Ce participant n'est pas validé
        if ($ParticipantsNonValidés)
            $ParticipantsNonValidés .= ", ";
        $ParticipantsNonValidés .= $Participant->getAdherent()->getNomUsuel();
        continue;
    }
    $effectif++;
    if ($Participant->getIdAdherent() == $Sortie->getIdOrganisateur()) {
        continue;
    }
// LG 20211127 début
//	$TotantMontantArrhes = $TotantMontantArrhes + $Participant->getMontantArrhes();
    if (!$Participant->getArrhesRemboursees()) {
        $TotantMontantArrhes = $TotantMontantArrhes + $Participant->getMontantArrhes();
    }
// LG 20211127 fin
    $TotantMontantTotal = $TotantMontantTotal + $Participant->getmontantTotal();

    $Adherent = $RepoAdherents->getEntityById($Participant->getIdAdherent());
    $numéroParticipant++;
    $montantLocation = "";
    if ($Participant->getDVA() == TRUE) {
        $montantLocation = number_format(TARIF_LOCATION_DVA * $NombreJour, 2) . " €";
        $TotantMontantLocation += TARIF_LOCATION_DVA * $NombreJour;
        $NbDVAPS++;
        $DVA = "DVA";
    } else {
        $DVA = "";
    }
    if ($Participant->getPelleSonde() == TRUE) {
        $PelleSonde = "PelleSonde";
    } else {
        $PelleSonde = "";
    }

    if ($Participant->getNbPlaceVoiture() == NULL) {
        $PlaceVoiture = " ";
    } else {
        $PlaceVoiture = $Participant->getNbPlaceVoiture() . ' Places';
    }

// LG 20201011 début
//	$tableParticipant .= '<tr style="' . ($Participant->getIdRaisonAnnulation()?"text-decoration: line-through;":"") . ';">'
//			. '<td>' . $numéroParticipant . '</td>'
//			. '<td>' . $Adherent->getPrenomAdherent() . " " . $Adherent->getNomAdherent() . '</td> '
//			. '<td>' . $PlaceVoiture . ' </td>'
//			. '<td>' . $DVA . ' ' . $PelleSonde . '</td>'
//			. '<td>' . $Adherent->getTelAdherent() . '</td>'
////			. '<td>' . $Adherent->getMailAdherent() . '</td>'
//			. '<td style="' . $styleArrhes . '">' . $montantArrhes . ' ' . $typeReglementArrhes . ' </td>'
//			. '<td>' . number_format($Sortie->getTarifSortie(), 2) . ' </td>'
//			. '<td>' . $montantLocation . ' </td>'
//			. '<td>' . $montantTotal . ' ' . $typeReglement . ' </td>'
//			. '<td>' . substr($Participant->getCommentaireParticipation(), 0, 20) . '</td>'
//			. '</tr>';
    $CommentaireParticipation = $Participant->getCommentaireParticipation();
    if ($Participant->getExcluDuCovoiturage()) {
        if ($CommentaireParticipation) {
            $CommentaireParticipation = ", " . $CommentaireParticipation;
        }
        $CommentaireParticipation = "Exclus du covoiturage" . $CommentaireParticipation;
        $nbExclusCovoiturage++;
    }
    if (strlen($CommentaireParticipation) > 55) {
        $CommentaireParticipation = substr($CommentaireParticipation, 0, 52) . "...";
    }
    $tableParticipant .= '<tr style="' . ($Participant->getIdRaisonAnnulation() ? "text-decoration: line-through;" : "") . ';">'
            . '<td>' . $numéroParticipant . '</td>'
            . '<td>' . $Adherent->getPrenomAdherent() . " " . $Adherent->getNomAdherent() . '</td> '
            . '<td>' . $PlaceVoiture . ' </td>'
            . '<td>' . $DVA . ' ' . $PelleSonde . '</td>'
            . '<td>' . $Adherent->getTelAdherent() . '</td>'
//			. '<td>' . $Adherent->getMailAdherent() . '</td>'
            . '<td style="' . $styleArrhes . '">' . $montantArrhes . ' ' . $typeReglementArrhes . ' </td>'
            . '<td>' . number_format($Sortie->getTarifSortie(), 2) . ' </td>'
            . '<td>' . $montantLocation . ' </td>'
            . '<td>' . $montantTotal . ' ' . $typeReglement . ' </td>'
            . '<td>' . $CommentaireParticipation . '</td>'
            . '</tr>';
// LG 20201011 fin
}

$nbCar = 50;
$commentaire = $Sortie->getCommentairesSortie();
if (strlen($commentaire) > 254) {
    $commentaire = substr($commentaire, 0, 250) . "...";
}

if ($ParticipantsNonValidés) {
    $commentaire .= "<br>Participants non validés : " . $ParticipantsNonValidés .= ", ";
}
if ($ParticipantsAnnulés) {
    $commentaire .= "<br>Participants annulés : " . $ParticipantsAnnulés .= ", ";
}
// $commentaire = "" ;

try {
//    foreach ($Sortie as $s){
    $html2pdf = new Html2Pdf($position, $format, $langue, $encodage);
    $html2pdf->setDefaultFont($font);

    ob_clean();
    ob_start();
    ?>
    <html>
        <head>
            <title>ASVEL Ski Montagne</title>
            <style>
                table, th, td {
                    font-size: 11px;
                    border: 1px solid black;
                    border-collapse: collapse;
                    padding: 5px;
                }
            </style>
        </head>
        <body>
            <div>
                <div style="position: absolute; top: 20px; padding-left: 65px;">
                    <table>
                        <tr>
                            <th>Date</th><td><?= $Sortie->getDateDebutSortie() ?></td><th>Heure Depart</th>
                            <td><?= $Sortie->getHeureDepart() ?></td>
                        </tr>
                        <tr>
                            <td>Activite</td>
                            <td colspan="3"><?= $activiténame ?></td>
                        </tr>
                        <tr>
                            <td>Niveau</td>
                            <td colspan="3"><?= $difficultename ?></td>
                        </tr>
                        <tr>
                            <td>Encadrant</td>
                            <td colspan="3"><?= $NomEncadrant ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">Nb participants maximum</td>
                            <td colspan="2"><?= $NbParticipantsMax ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">N° de sortie</td>
                            <td colspan="2" style="text-align: center;"><?= $Sortie->getNomSortie() ?></td>
                        </tr>
                    </table>
                </div>
    <?php // border a été mis à none car je ne sais pas comment régler la hauteur du cadre du commentaire (bug de html2pdf?)  ?>
                <div style="position: absolute; top: 20px; padding-left: 335px; height: 96px; max-height: 96px; border: none;">
                    <table>
                        <tr>
                            <th>DESTINATION, EQUIPEMENTS et OBSERVATIONS</th>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <?php 
// LG 20220910 old                                echo $Sortie->getLieuSortie() ;
                                if ($liIdRaisonAnnulation = $Sortie->getIdRaisonAnnulation()) {
                                    $loRaisonRepository = new RaisonAnnulationRepository() ;
                                    $loRaisonAnnulation = $loRaisonRepository->getEntityById($liIdRaisonAnnulation) ;
                                    echo "SORTIE ANNULEE : " . $loRaisonAnnulation->getLibelleAnnulation() ;
                                } else {
                                    echo $Sortie->getLieuSortie() ;
                                }
                                ?><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 86px; max-height: 86px; width: 100px; border: none;">
    <?= trim($commentaire) ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="position: absolute; top: 20px; padding-left: 630px;">
                    <table>
                        <tr>
                            <th colspan="4">Nombre de jours</th>
                        </tr>
                        <tr>
                            <td colspan="4"><?= $NombreJour ?></td>
                        </tr>
                    </table>
                </div>
                <div style="position: absolute; top: 20px; padding-left: 779px;">
    <?php include 'info-tarifs.php' ?>
                </div>
            </div>
            <div style="padding-top: 160px; padding-left: 65px;">
                <table>
                    <tr>
                        <th colspan="5">PARTICIPANTS</th>
                        <th colspan="4">Règlement</th>
                        <th>Divers</th>
                    </tr>
                    <tr>
                        <th>N°</th>
                        <th>Nom et prénoms</th>
                        <th>Voiture</th>
                        <th>DVA,PelleSonde</th>
                        <th>Téléphone</th>
    <?php //                    <th>Adresse Mail</th>	 ?>
                        <th>Arrhes</th>
                        <th>Tarif sortie</th>
                        <th>Location</th>
                        <th>Solde</th>
                        <th>Commentaires</th>
                    </tr>
                    <?= $tableParticipant ?>                            
                    <?php
                    $NbDVAPS = $NbDVAPS ?: "";
                    $TotantMontantArrhes = $TotantMontantArrhes ? number_format($TotantMontantArrhes, 2) . "€" : "";
                    $TarifSortie = number_format($Sortie->getTarifSortie() * ($effectif - $nbEncadrants), 2) . "€";
                    $TotantMontantLocation = $TotantMontantLocation ? number_format($TotantMontantLocation, 2) . "€" : "";
                    $TotantMontantTotal = $TotantMontantTotal ? number_format($TotantMontantTotal, 2) . "€" : "";
                    ?>
                    <tr style="font-weight: bold ;">
                        <td><?= $effectif ?></td>	<?php //N°  ?>
                        <td>Totaux</td>				<?php //Nom-prenom  ?>
                        <td></td>					<?php //Voiture  ?>
                        <td><?= $NbDVAPS ?></td>		<?php //DVA  ?>
                        <td></td>					<?php //Tel  ?>
                        <td><?= $TotantMontantArrhes ?></td>						<?php //Arrhes  ?>
                        <td><?= $TarifSortie ?></td>								<?php //Tarif sortie  ?>
                        <td><?= $TotantMontantLocation ?></td>					<?php //Location  ?>
                        <td><?= $TotantMontantTotal ?></td>						<?php //Solde ?>
                        <td></td>												<?php //Commentaire ?>
                    </tr> 
                </table>
                <?php
                $totalRecettes = $TotauxParTypeRèglement[TYPEREGELEMENT_ESPECES] + $TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUE] + $TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUEVACANCES];
                $coutKms = $Sortie->getDistanceSortie() * $Sortie->getNbVoituresSortie() * $FraisKilometriques;
                $coutAutoroute = $Sortie->getCoutAutoroute() * $Sortie->getNbVoituresSortie();
                $coutTotalSortie = $coutKms + $coutAutoroute;
// LG 20201011 début
//				if ($effectif) {
//					$coutParPersonne = $coutTotalSortie / $effectif ;
//				} else {
//					$coutParPersonne = $coutTotalSortie ;
//				}
                if ($effectif - $nbExclusCovoiturage) {
                    $coutParPersonne = $coutTotalSortie / ($effectif - $nbExclusCovoiturage);
                } else {
                    $coutParPersonne = $coutTotalSortie;
                }
// LG 20201011 fin
                $fraisDivers = $Sortie->getFraisAnnexe();  // Il faut prévoir la saisie dans la fiche de sortie
                $fraisRouteCadres = $coutParPersonne * $nbEncadrants;
                $fraisTotal = $fraisRouteCadres + $fraisDivers;
                $bilanPartiel = $totalRecettes - $fraisTotal;
                $location = 0;
                $bilanSortie = $bilanPartiel - $location;
                ?>
                <div style="position: absolute; padding-left: 80px; padding-top: 570px;">
                    <table>
                        <tr>
                            <th colspan="3">RECAPITULATIF RECETTES</th>
                        </tr>
                        <tr>
                            <td>Especes</td>
                            <td><?= $TotauxParTypeRèglement[TYPEREGELEMENT_ESPECES] ?></td>
                            <td>caisse le</td>
                        </tr>
                        <tr>
                            <td>Chèques</td>
                            <td><?= $TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUE] ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Chèques vacances</td>
                            <td><?= $TotauxParTypeRèglement[TYPEREGELEMENT_CHEQUEVACANCES] ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>TOTAL DES RECETTES</td>
                            <td><?= $totalRecettes ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th colspan="3">REMARQUES</th>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 1px;"><?= $Sortie->getFraisAnnexeDetails() ?></td>
                        </tr>
                    </table>
                </div>
                <div style="position: absolute; padding-left: 300px; padding-top: 570px;">
                    <table>
                        <tr>
                            <th colspan="3">RECAPITULATIF DEPENSES</th>
                        </tr>
                        <tr>
                            <td>Frais de route cadres</td>
                            <td><?= number_format($fraisRouteCadres, 2) ?></td>
                            <td>caisse le</td>
                        </tr>
                        <tr>
                            <td>Divers</td>
                            <td><?= number_format($fraisDivers, 2) ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>TOTAL</td>
                            <td><?= number_format($fraisTotal, 2) ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Bilan partiel</td>
                            <td><?= number_format($bilanPartiel, 2) ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td><?= number_format($location, 2) ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Bilan sortie</td>
                            <td><?= number_format($bilanSortie, 2) ?></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div style="position: absolute; padding-left: 525px; padding-top: 570px;">
                    <table>
                        <tr>
                            <th colspan="4">CALCUL DES FRAIS DE ROUTE</th>
                        </tr>
                        <tr>
                            <td>Nombre de cadres</td>
                            <td><?= $nbEncadrants ?></td>
                            <td>Nombre de covoitureurs</td>
                            <td><?= $effectif - $nbExclusCovoiturage ?></td>
                        </tr>
                        <tr>
                            <td>Nombre de participants</td>
                            <td><?= $effectif - $nbEncadrants ?></td>
                            <td>Nb km * nb voiture</td>
                            <td><?= $Sortie->getDistanceSortie() ?></td>
                        </tr>
                        <tr>
                            <td>Nb exclus covoiturage</td>
                            <td><?= $nbExclusCovoiturage ?></td>
                            <td>Autoroute AR * nb voitures</td>
                            <td><?= $coutAutoroute ?></td>
                        </tr>
                        <tr>
                            <td>Kilomètres (AR)</td>
                            <td><?= $Sortie->getDistanceSortie() ?></td>
                            <td>Nb km * <?= number_format($FraisKilometriques, 2) ?></td>
                            <td><?= $coutKms ?></td>
                        </tr>
                        <tr>
                            <td>Nombre de voitures</td>
                            <td><?= $Sortie->getNbVoituresSortie() ?></td>
                            <td>+ autoroute</td>
                            <td><?= $coutTotalSortie ?></td>
                        </tr>
                        <tr>
                            <td>Autoroute AR</td>
                            <td><?= number_format($Sortie->getCoutAutoroute() ?: 0, 2) ?></td>
                            <td>Frais de route/covoitureur</td>
                            <td><?= number_format($coutParPersonne, 2) ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </body>
    </html>    
    <?php
    $contenu = ob_get_clean();
    ob_clean();
//        $contenu = "toto" ;
    error_reporting(E_ERROR);
    $html2pdf->writeHTML($contenu);
    $html2pdf->output($nomFichier);
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
