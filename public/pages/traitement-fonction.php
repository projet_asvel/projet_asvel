<?php

require_once '../../config/globalConfig.php';

use App\Entity\Fonction;
use App\Repository\FonctionRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
$idFonction = filter_input(INPUT_POST, 'idfonction', FILTER_SANITIZE_STRING);
$nomFonction = filter_input(INPUT_POST, 'nomfonction', FILTER_SANITIZE_STRING);

//if ($idActivite == NULL) {
$datas = array(
    'idFonction' => $idFonction,
    'nomFonction' => $nomFonction,
);
try {
    $Fonction = new Fonction($datas);
    $FonctionRep = new FonctionRepository();
    if ($FonctionRep->vérifier($Fonction, $tabErr)) {
        $LFonction = $FonctionRep->sauver($Fonction);
//        echo 'true';
        echo $LFonction->getIdFonction() ;
    } else {
        echo $tabErr[0];
    }

    return true;
} catch (PDOException $e) {
    echo $e->getMessage();
}
//} else {
//    $datas = array(
//        'nomactivite' => $nomActivite,
//    );
//}

//header('Location: liste-fonction.php');
