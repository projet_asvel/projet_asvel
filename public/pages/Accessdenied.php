<?php
header('HTTP/1.0 403 Forbidden');

require_once '../../config/globalConfig.php';
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <body>

    </div>
    <?php include_once 'inc/header.php'; ?>
    <div class="container col-11 py-3">
        <br>
        <br>
        <h1 style='text-align: center;text-decoration: underline;'> Désolé, cette page ne vous est pas accessible</h1>
        <br>
        <h3>Vous n'êtes actuellement pas auhtentifié, ou vous avez été déconnecté après une période d'inactivité sur cette page.<br>Merci de vous authentifier à nouveau.</h3>
        <br>
        <br>
        <?php include_once 'inc/explications.php' ?>
        <br>
        <a href= "index.php">Retourner à l'accueil</a>
        <br>
        <br>
    </div>
</body>
<?php include_once 'inc/footer.php' ?>


</html>
