<?php

require_once '../../config/globalConfig.php';

use App\Entity\Difficulte;
use App\Repository\DifficulteRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
$idDifficulte = filter_input(INPUT_POST, 'iddifficulte', FILTER_SANITIZE_STRING);
$nomDifficulte = filter_input(INPUT_POST, 'nomdifficulte', FILTER_SANITIZE_STRING);

//if ($idActivite == NULL) {
$datas = array(
    'idDifficulte' => $idDifficulte,
    'nomDifficulte' => $nomDifficulte,
);

try {
    $Difficulte = new Difficulte($datas);
    $DifficulteRep = new DifficulteRepository();
    if ($DifficulteRep->vérifier($Difficulte, $tabErr)) {
        $LDifficulte = $DifficulteRep->sauver($Difficulte);
//        echo 'true';
        echo $LDifficulte->getIdDifficulte() ;
    } else {
        echo $tabErr[0];
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
//} else {
//    $datas = array(
//        'nomactivite' => $nomActivite,
//    );
//}


//header('Location: index.php');
