<?php
require_once '../../config/globalConfig.php';

use App\Repository\ParticiperRepository;
use App\Repository\AdherentRepository;
use App\Repository\FonctionRepository;
use App\Repository\SortieRepository;
use App\Security;

$RepoSortie = new SortieRepository();
$RepoAdherents = new AdherentRepository();
$RepoFonction = new FonctionRepository();
$RepoParticiper = new ParticiperRepository();
$RepoTypeRèglement = new \App\Repository\TypeReglementRepository;

// Gestion de la sécurité
if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ENCADRANT))) {
    header("location: Accessdenied.php");
}

$listeFonction = $RepoParticiper->getAll();
// IG 28012021 Debut
// $listeParticipant = $RepoParticiper->getAllAdherentByIdSortie($_GET["idSortie"]);
// IG 28012021 Fin
$listeParticipant = $RepoParticiper->getAllAdherentByIdSortie(filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING));
// IG 28012021 Debut
// $compteparticipant = $RepoParticiper->CompteParticipant($_GET["idSortie"]);
// IG 28012021 Fin

$compteparticipant = $RepoParticiper->CompteParticipant(filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING));

//IG 28012021 Debut
// $Sortie = $RepoSortie->getEntityId($_GET['idSortie']);
// IG 28012021 Fin

$Sortie = $RepoSortie->getEntityId(filter_input(INPUT_GET, 'idSortie', FILTER_SANITIZE_STRING));

$nbmax = $Sortie->getNombreParticipantsMaxSortie();
$listeMailpartcipants = '';
foreach ($listeParticipant as $Participant) {
// LG 20201011 début
    if ($Participant->getidRaisonAnnulation()) {
        // Ce participant a annulé sa participation
        continue;
    }
// LG 20201011 fin
    if ($listeMailpartcipants) {
        $listeMailpartcipants .= ',';
    }
    $listeMailpartcipants .= $Participant->getIdAdherent();
}

//dump_var($listeSortie, DUMP, 'Liste des sorties: ');
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>
    <body>
        <?php include_once 'inc/header.php'; ?>
        <div class="container col-11 py-3">
            <div class="text-center">
                <h4>Liste des Participants de la sortie <a href='info-sortie.php?id=<?= $Sortie->getIdSortie() ?>'><?= $Sortie->getTitre() ?></a></h4>
                <?PHP
                $NbParticipantsInscrits = $RepoParticiper->getNbParticipantsBySortie($Sortie->getIdSortie(), $NbParticipantsValidés);
                $NbPlacesVoiture = $Sortie->getNbPlacesVoiture();
                $alerte = "";
                if ($NbParticipantsValidés > $Sortie->getNombreParticipantsMaxSortie() || $NbPlacesVoiture < $NbParticipantsValidés) {
                    $alerte = 'style = "color: red; font-weight: bold;"';
                }
                echo "<div " . $alerte . ">"
                . ($NbParticipantsValidés) . " participant validé" . (($NbParticipantsValidés > 1) ? "s" : "")
                . ", " . ($NbParticipantsInscrits - $NbParticipantsValidés) . " en attente"
                . ", " . ($Sortie->getNombreParticipantsMaxSortie() ?: "-") . " max."
                . ", " . $NbPlacesVoiture . " places de voiture"
                . "</div>";
                ?>
                <br>
            </div>
            <?php
//            $Rolesdemendée = array('Administrateur');
//            $Protected = $Security::hasRole($Rolesdemendée);
            if (Security::hasRole(Array(Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
                echo '<a href="info-sortie.php?id=' . $_GET["idSortie"] . '"><button type="button" class="btn btn-secondary"><i class="fa fa-backward"></i> Retourner à la sortie</button></a>&nbsp';
                echo '<a href=info-participant.php?idSortie=' . $_GET["idSortie"] . '><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un participant</button></a>&nbsp';
                echo '<a href="info-mail.php?pourSortie&adherents=' . $listeMailpartcipants . '&sujet=Sortie ' . $Sortie->getTitre(true) . '"><button type="button" class="btn btn-primary"><i class="fa fa-forward"></i> Envoyer un mail aux participants</button></a>';
            }


            $attribut = "";     // attribut confié au nom d'un participant (encadrant, participation validée...)
            $styleOut = "";     // attribut confié aux participants dépassant la limite maximum de participation disponible
            $texteOut = ""; // Texte "liste d'attente"
            $annulé = "";       // attribut confié aux participants ayant annulée leurs participations
            ?>
            <input type="hidden" name="idSortie" id="idsortie" value="<?= $_GET["idSortie"] ?>">
            <div class="row py-2"></div>            

            <table class="table table-hover" id="table_sortie">
                <thead class="thead-dark">
                    <tr>
                        <th style='display: none'>idAdherent</th>
                        <th style='display: none'>idSortie</th>
                        <th>N°</th>
                        <th>Participant</th>                        
                        <th>Fonction</th>
                        <th title="Certificat médical alpînisme">Alpi</th>
                        <th title="Nombre de places disponibles dans la voiture">Voiture</th>
                        <th>Etat Participation</th>
                        <?PHP
//	                        <th>Adresse Mail</th> 
                        ?>
                        <th>Téléphone</th>
                        <th>Matériel</th>
                        <th>Arrhes</th>
                        <th>Tarif</th>
                        <th>Commentaire</th>
                    </tr>
                <tbody>
                    <?php
                    $nb = 1;
                    foreach ($listeParticipant as $Participant) {
                        $nb = $nb + 1;

                        $Fonction = $RepoFonction->getEntityById($Participant->getIdFonction());
                        $Adherent = $RepoAdherents->getEntityById($Participant->getIdAdherent());

                        if ($Fonction->getNomFonction() == "Encadrant" || $Fonction->getNomFonction() == "Co-encadrant") {
                            $FonctionAdherent = "<b>";
                        } else {
                            $FonctionAdherent = "";
                        }
                        if ($Participant->getValidationParticipation() == 1) {
                            $validation = "validée";
                            $attribut = "";
                        } else {
                            $validation = 'non validée';
                            $attribut = "background-color:lightgray;";
                        }
                        echo "<tr style='" . $styleOut . ($Participant->getIdRaisonAnnulation() ? "text-decoration: line-through;" : "") . "'>";
                        echo "<td style='display: none;'>" . $Participant->getIdAdherent() . "</td>";
                        echo "<td style='display: none;'>" . $_GET["idSortie"] . "</td>";
                        echo "<td style='" . $attribut . "'>" . ($nb - 1) . "</td>";
                        echo "<td style='" . $attribut . "'><a href='info-adherent.php?id=" . $Participant->getIdAdherent() . "'>" . $annulé . "" . $FonctionAdherent . $Adherent->getNomUsuel() . "</a></td>";
                        echo "<td style='" . $attribut . "'>" . $annulé . "" . $FonctionAdherent . $Fonction->getNomFonction() . "</td>";
                        //echo "<td>" . $Adherent->getCertifAlpi() . "</td>";
                        $certif = $Adherent->getCertifAlpi();
                        echo "<td style='" . $attribut . "'>";
                        if ($certif == TRUE) {
                            echo '<img src="../img/checkBox.jpg" style="width:20px; height:20px;">';
                        }
                        echo "</td>";
                        echo "<td style='" . $attribut . "'>" . $FonctionAdherent . $Participant->getNbPlaceVoiture() . "</td>";
                        echo "<td style='" . $attribut . "'>" . $annulé . "" . $FonctionAdherent . $validation . $texteOut . "</td>";
//                        echo "<td style='".$attribut."'>". $FonctionAdherent .$Adherent->getMailAdherent()."</td>";
                        echo "<td style='" . $attribut . "'>" . $FonctionAdherent . $Adherent->getTelAdherent() . "</td>";

                        if ($Participant->getDVA() == TRUE) {
                            $DVA = "DVA";
                        } else {
                            $DVA = "";
                        }
                        if ($Participant->getPelleSonde() == TRUE) {
                            $PelleSonde = "PelleSonde";
                        } else {
                            $PelleSonde = "";
                        }
                        echo "<td style='" . $attribut . "'>" . $DVA . "  " . $PelleSonde . "</td>";

                        $styleArrhes = "";
                        if ($Participant->getMontantArrhes() == 0 || $Participant->getMontantArrhes() == NULL) {
                            $montantArrhes = " ";
                            $typereglementArrhes = "";
                        } else {
                            $montantArrhes = $Participant->getMontantArrhes() . " €";
                            if (!$Participant->getIdTypeReglementArrhes()) {
                                $typereglementArrhes = "non réglé";
                            } else {
                                $Type = $RepoTypeRèglement->getEntityById($Participant->getIdTypeReglementArrhes());
                                $typereglementArrhes = $Type->getNomTypeReglement();
                            }
                            if ($Participant->getArrhesRemboursees()) {
                                $styleArrhes = "text-decoration: line-through;";
                                $typereglementArrhes = "remboursé";
                            }
                        }
                        echo "<td style='" . $attribut . $styleArrhes . "'>" . $montantArrhes . " " . $typereglementArrhes . "</td>";

                        $styleTarif = "";
                        if ($Participant->getMontantTotal() == 0 || $Participant->getMontantTotal() == NULL) {
                            $montantTotal = " ";
                            $typereglementMontantTotal = "";
                        } else {
                            $montantTotal = $Participant->getMontantTotal() . " €";
                            if (!$Participant->getIdTypeReglementMontantTotal()) {
                                $typereglementMontantTotal = "non réglé";
                            } else {
                                $Type = $RepoTypeRèglement->getEntityById($Participant->getIdTypeReglementMontantTotal());
                                $typereglementMontantTotal = $Type->getNomTypeReglement();
                            }
// LG 20211127 début
//                            if ($Participant->getArrhesRemboursees()) {
//                                $styleTarif = "text-decoration: line-through;";
//                                $typereglementMontantTotal = "remboursé";
//                            }
// LG 20211127 fin
                        }
                        echo "<td style='" . $attribut . $styleTarif . "'>" . $montantTotal . " " . $typereglementMontantTotal . "</td>";

                        echo "<td style='" . $attribut . "width:20;overflow: hidden; white-space:nowrap;text-overflow:ellipsis;'> " . substr($Participant->getCommentaireParticipation(), 0, 20) . "</td>";
                        echo "</tr>";

                        // pour chaque participant, verifie si le nombre maximum est dépassé ou non. si oui, ajoute un fond gris sur la ligne du participant.
                        if ($nb > $nbmax) {
                            $styleOut = "background-color:#707070;";
                            $texteOut = ", liste d'attente";
                        } else {
                            $styleOut = "";
                            $texteOut = "";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
<?php include_once 'inc/footer.php' ?>
    </body>
    <script type="text/javascript" src="../js/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            var options = {"order": [[2, "asc"]],
                pageLength: 20,
                "language": {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }};
            var table = $('#table_sortie').DataTable(options);
            var data = table.row(this).data();
// console.log(data);
            $('#table_sortie tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
//                console.log(data)
                window.location.assign("info-participant.php?idAdherent=" + data[0] + "&idSortie=" + $("#idsortie").val());
            });
        });
    </script>
</html>


