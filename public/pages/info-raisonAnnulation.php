<?php
require_once '../../config/globalConfig.php';

use App\Entity\RaisonAnnulation;
use App\Repository\RaisonAnnulationRepository;
use App\Security;

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
    header("location: Accessdenied.php");
}


if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $mapperActivite = new RaisonAnnulationRepository();

    if ($id == null) {
        $Annulation = new RaisonAnnulation([]);
    } else {
        $mapper = new RaisonAnnulationRepository();

        $Annulation = $mapper->getEntityById($id);
    }
}
// LG 20200504 début
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
//$liste = 'raisonAnnulation.php">Retour à la liste des raison d\'annulation ';
$valider = true ;
$bouton = '<a href="liste-raisonAnnulation.php"'
        . ' style="margin-left: 1%;" class="btn btn-secondary">'
        . '<i class="fa fa-backward"></i>'
        . ' Retour liste des raisons d\'annulation'
        . '</a>';
$footer_listeBoutons = [$bouton];
// LG 20200504 fin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>

        <form action="" method="POST" id="formannulation">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Raison d'annulation</h4>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="idannulation"  id="idannulation" value="<?= $id ?>">
                        <div class="row py-2">
                            <div class="col">
                                <label for="libelleannulation">Nom de l'Annulation :</label>
                                <input type="text" class="form-control"  required="true" name="libelleannulation" id="libelleannulation" placeholder="Nom de l'Annulation" value="<?= $Annulation->getlibelleAnnulation() ?>">
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
            <!-- FIN CARD -->
            <?php include_once 'inc/footer-valider.php' ?>
        </form>
    </body>
    <script src="../js/RaisonAnnulation.js?v=<?=VERSION_APP ?>"></script>

</html>
