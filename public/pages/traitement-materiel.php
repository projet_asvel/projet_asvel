<?php

require_once '../../config/globalConfig.php';

use App\Entity\Materiel;
use App\Repository\MaterielRepository;

$idMateriel = filter_input(INPUT_POST, 'idMateriel', FILTER_SANITIZE_NUMBER_INT);
$taille = filter_input(INPUT_POST, 'taille', FILTER_SANITIZE_STRING);
$pointure = filter_input(INPUT_POST, 'pointure', FILTER_SANITIZE_STRING);
$modele = filter_input(INPUT_POST, 'modele', FILTER_SANITIZE_STRING);
$longitude1 = filter_input(INPUT_POST, 'longitude1', FILTER_SANITIZE_STRING);
$latitude1 = filter_input(INPUT_POST, 'latitude1', FILTER_SANITIZE_STRING);
$longitude2 = filter_input(INPUT_POST, 'longitude2', FILTER_SANITIZE_STRING);
$latitude2 = filter_input(INPUT_POST, 'latitude2', FILTER_SANITIZE_STRING);
$dateAchat = filter_input(INPUT_POST, 'dateAchat', FILTER_SANITIZE_STRING);
$commentaire = filter_input(INPUT_POST, 'commentaire', FILTER_SANITIZE_STRING);
$pays = filter_input(INPUT_POST, 'pays', FILTER_SANITIZE_STRING);
$typeMiseADispo = filter_input(INPUT_POST, 'typeMiseAdispo', FILTER_SANITIZE_NUMBER_INT);
$typeMateriel = filter_input(INPUT_POST, 'typeMateriel', FILTER_SANITIZE_NUMBER_INT);
$idAdherent = filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_NUMBER_INT);

$datas = array(
    'idMateriel' => $idMateriel ?: null,
    'taille' => $taille ?: null,
    'pointure' => $pointure ?: null,
    'modele' => $modele,
    'longitude1' => $longitude1 ?: null,
    'latitude1' => $latitude1 ?: null,
    'longitude2' => $longitude2 ?: null,
    'latitude2' => $latitude2 ?: null,
    'dateAchat' => $dateAchat." 00:00:00" ?: "0000-00-00 00:00:00",
    'commentaire' => $commentaire ,
    'pays' => $pays ,
    'idTypeMiseAdispo' => $typeMiseADispo,
    'idTypeMateriel' => $typeMateriel,
    'idAdherent' => $idAdherent,
);
// $Materiel = new Materiel($datas);
// var_dump($Materiel);
try {
    $Materiel = new Materiel($datas);
    $MaterielRepository = new MaterielRepository();
    if ($MaterielRepository->vérifier($Materiel, $tabErr)) {
        $LeMateriel = $MaterielRepository->sauver($Materiel);
    } else {
        echo $tabErr[0];
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}

header('Location: liste-materiel_perso.php?idadherent=' . $idAdherent);
