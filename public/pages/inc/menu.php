<?php

use App\Repository\AdherentRepository;
use App\Repository\ActiviteRepository;
use App\Security;

$AdherentAuthentifié = null;
$Security = new Security();

if (isset($_SESSION['idadherent'])) {
    // Un adhérent est déja authentifié
    // Récupérer l'entité que le décrit (ou Null si le login est invalide)
    $AdheRep = new AdherentRepository();
    $AdherentAuthentifié = $AdheRep->getEntitesById($_SESSION['idadherent']);
}
// LG 20200221 déac if ($AdherentAuthentifié) {
// LG 20200221 déac 	// Un adhérent est authentifié
?>
<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item active">
        <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
    </li>
    <?php
    $ActiviteRep = new ActiviteRepository();
    $lstActivites = $ActiviteRep->getAll();
    ?>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="sortiesAVenir" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sorties à venir
        </a>
        <div class="dropdown-menu" aria-labelledby="sortiesAVenir">
            <?php
            foreach ($lstActivites As $activite) {
                echo '<a class="dropdown-item" href="liste-sortie.php?Passées=F&Activite=' . $activite->getIdActivite() . '">' . $activite->getNomActivite() . '</a>';
            }
            ?>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="sortiesPassées" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sorties passées222
        </a>
        <div class="dropdown-menu" aria-labelledby="sortiesPassées">
            <?php
            foreach ($lstActivites As $activite) {
                echo '<a class="dropdown-item" href="liste-sortie.php?Passées=T&Activite=' . $activite->getIdActivite() . '">' . $activite->getNomActivite() . '</a>';
            }
            ?>
        </div>
    </li>

    <?php
// LG 20211106 début    
if ($AdherentAuthentifié) {
    ?>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="matériels" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Matériels
        </a>
        <div class="dropdown-menu" aria-labelledby="matériels">
            <a class="dropdown-item" href="liste-materiel_dispo.php">Matériels Disponibles</a>
            <a class="dropdown-item" href="liste-materiel_perso.php?idadherent=<?= $AdherentAuthentifié->getIdAdherent()?>">Mes matériels</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link" id="sortiesPassées" href="liste-sortie.php?Adhérent=<?= $AdherentAuthentifié->getIdAdherent()?>">
            Mes sorties
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link" id="statistiques" href="dates_activite_statistiques.php">
            Mes statistiques
        </a>
    </li>
    
    <?php
}
// LG 20211106 fin    
    ?>
    <?php
    if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
        ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="Adhérents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Adhérents
            </a>
            <div class="dropdown-menu" aria-labelledby="Adhérents">
                <a class="dropdown-item" href="liste-adherent.php?actifs=true">Adhérents actifs</a>
                <a class="dropdown-item" href="liste-adherent.php?actifs=false">Tous les adhérents</a>
                <a class="dropdown-item" href="liste-adherent-priorite.php">Priorités sur les sorties</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="mails" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Mails
            </a>
            <div class="dropdown-menu" aria-labelledby="mails">
                <a class="dropdown-item" href="info-mail.php">Envoyer un mail</a>
                <a class="dropdown-item" href="liste-mail.php">Mails envoyés</a>
            </div>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="mails" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Listes
            </a>
            <div class="dropdown-menu" aria-labelledby="Listes">
                <a class="dropdown-item"  href="liste-activite.php">Activités</a>
                <a class="dropdown-item" href="liste-difficulte.php">Difficultés</a>
                <a class="dropdown-item" href="liste-fonction.php">Fonctions</a>
                <a class="dropdown-item" href="liste-raisonAnnulation.php">Raisons d'annulation</a>
                <a class="dropdown-item" href="liste-tag.php">Tags</a>  
            </div>
        </li>
       <?php
    }
// LG 20200221 fin
// LG 20210119 début
    if (Security::hasRole([Security::ROLE_ADMIN, Security::ROLE_COMPTABLE, Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE])) {
    ?>
    <li class="nav-item">
        <a class="nav-link" target="_blank"  href="doc/tutoAppliASVEL.pdf">Mode d'emploi <span class="sr-only">(current)</span></a>
    </li>
        <?php
        } else {
    ?>
    <li class="nav-item">
        <a class="nav-link" target="_blank"  href="doc/tutoInscriptionAsvel.pdf">Mode d'emploi <span class="sr-only">(current)</span></a>
    </li>
        <?php
    }
// LG 20210119 fin
    ?>
    <?php
    if (Security::hasRole([Security::ROLE_ADMIN, Security::ROLE_COMPTABLE, Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE])) {
    ?>
    <!-- <li class="nav-item">
        <a class="nav-link"  href="manageFacebook">Facebook <span class="sr-only"></span></a>
    </li> -->
        <?php
        } else {}
    ?>


</ul>

<?php
if ($AdherentAuthentifié) {
    // Un adhérent est authentifié
    ?>
    <div class="text-white mx-3">
        Bonjour, <?= $AdherentAuthentifié->getNomAdherent() . " " . $AdherentAuthentifié->getPrenomAdherent() ?>
    </div>
    <?php
// LG 20200323 déac    if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
        ?>    
        <ul class="navbar-nav">
            <li class="nav-item ">
                <a class="nav-link" style="margin-left:-12%;" href="info-adherent.php?id=<?= $_SESSION['idadherent'] ?>">Mon Profil</a>
            </li>   
        </ul>
        <?php
// LG 20200323 déac    }
    ?>

    <form class="form-inline my-2 my-lg-0" action="Deconnexion.php">
        <button class="btn btn-primary my-2 my-sm-0" type="submit">Déconnexion</button>
    </form>

    <?php
} else {
    ?>
    <div class="form-inline my-2 my-lg-0">
        <button type="button" class="btn btn-primary my-2 my-sm-0" data-toggle="modal" data-target="#modalLogin">Connexion</button>
    </div>
    <?php
}
?>
<?php

include_once 'modal_connexion.php';
