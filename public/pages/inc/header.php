
<nav class="navbar navbar-expand-lg navbar-dark bg-primary" style="border-bottom:1px solid #933300; border-top:4px solid #933300;">
  <a class="navbar-brand" href="index.php" style="margin-top:-10px;margin-bottom:-10px;">
    <img src="../img/logo-asvel.jpg" width="50" height="50" alt="">
  </a>
  <div class="collapse navbar-collapse" id="navbarColor01">
    <?php include_once 'menu.php' ?>
  </div>
</nav>


<!-- 1er lien de confirmation lorsqu'on quitte un site sans avoir enregistré
https://stackoverflow.com/questions/7317273/warn-user-before-leaving-web-page-with-unsaved-changes
2eme lien: https://stackoverflow.com/questions/11844256/alert-for-unsaved-changes-in-form/34899446-->
