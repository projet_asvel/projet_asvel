<?php

use App\Security;
?>

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!-- Footer -->
<footer class="pt-5 pb-4" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">ASVEL SKI MONTAGNE</h5>
                <ul class="f-address"style="margin-left: -17%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href='https://www.asvelskimontagne.fr/'>
                                        <img src="..\img\logo-asvel.jpg" alt="Logo asvel" width="150" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-3">
                            <div class="col-1"><i class="fas fa-map"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Addresse :</h6>
                                <p>245, cours Emile Zola 69100 Villeurbanne</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-1"><i class="fas fa-phone"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Téléphone :</h6>
                                <p>04 78 84 91 21</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PROCHAINES SORTIES</h5>
                <ul class="recent-post"style="margin-left: -15%">               
                    <?php

                    use App\Repository\SortieRepository;

$SortieRep = new SortieRepository;
                    $LesSorties = $SortieRep->getProchainesSorties();
                    Foreach ($LesSorties as $Sortie) {
                        echo "<li>";
                        echo "<label>";
                        $conv_date = date('d/m/Y', strtotime($Sortie->getDateDebutSortie()));
                        if ($Sortie->getDateDebutSortie() == $Sortie->getDateFinSortie()) {
//                                echo $Sortie->getDateDebutSortie() ;
                            echo $conv_date;
                        } else {
                            echo "du " . $conv_date;
                            $conv_date = date('d/m/Y', strtotime($Sortie->getDateFinSortie()));
                            echo " au " . $conv_date;
                        }
                        echo "<br><span></span>";
                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                            echo "<a href= info-sortie.php?id=" . $Sortie->getIdSortie() . " class='font-weight-bold mb-0'>";
                        }
                        echo $Sortie->getNomSortie() . " : " . $Sortie->getNomsActivites();
                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                            echo "</a>";
                        }
                        echo "</label><br>";
                        echo "<span></span>";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">FÉDÉRATIONS</h5>
                <ul class="f-address">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\logoffme.png" alt="logo Omnisport" width="75">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\ffs-asvel.png"  width="75">
                            </div>
                        </div>
                    </li>
                </ul>
                <h5 class="mb-4 font-weight-bold" style="margin-top: 50px;">RÉSEAUX SOCIAUX</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li><a href="https://www.facebook.com/asvel.skimontagne/" title="facebook" style="display: block;")><i class="fab fa-facebook-f"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PARTENAIRES</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href="http://www.decathlon.fr/fr/store?store_id=PS_98">
                                    <img src="..\img\logodecathlon.png" alt="logo decathlon" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="https://www.omnisport-lyon.fr">
                                    <img src="..\img\omnisport-logo.png" alt="logo Omnisport" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="Contributeur.php"><img src="..\img\LogoOlits.png"  width="150"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="http://www.limoog.net">
                                    <img src="https://www.limoog.net/wp-content/uploads/2021/12/cropped-cropped-cropped-cropped-cropped-Logo-Limoog.png" width="150" style="background-color: white;" alt="logiciel planning médico social et alternance">
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright -->
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="text-center text-white">
                    &copy; Association loi de 1901 agrée jeunesse et sports, affiliée FFS - FFME | Tous droits réservés  |<a href='https://www.asvelskimontagne.fr/'>   ASVEL Ski Montagne</a> 
                </div>
            </div>
        </div>
    </div>
<!--    <div class="modal login-container" >
        <div class="tonmodal">
            <form class="form-inline my-2 my-lg-0" method="post" action="identification.php" style="  background-image: linear-gradient(orangered, black);   border: 3px solid black !important; width: 33%; margin-left: 34%; margin-top: 20% !important">
                <div style="margin-left: 10%">
                    <label for="mailadherent" >Login: </label>
                    <input class="form-control mr-sm-2" type="text" name="mailadherent" style="display:block; display:inline-block; margin: 0 auto;" maxlength="50" required/><br/> 

                </div>

                <div>
                    <label for="mdpadherent">Password: </label>
                    <input class="form-control mr-sm-2" type="password"  name="mdpadherent" maxlength="50" style="display:block; display:inline-block; " required/><br/> 
                </div>
                <input class="form-control mr-sm-2" type="submit" value="Envoyer" style="display:block; display:inline-block; margin-left: 43% !important;"/>
            </form>
        </div>
    </div>-->
</section>
