<title><?php if (defined("TITRE_APP")) {echo TITRE_APP ;} else {echo "ASVEL Ski Montagne" ;} ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description" content="<?php if (defined("DESCRIPION_APP")) {echo DESCRIPION_APP ;} else {echo "Visualisez les plannings des sorties montagne de l'ASVEL Ski Montagne et inscrivez-vous en ligne" ;} ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../img/logo-asvel.jpg" />

<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/elementFacebook.css">
<link rel="stylesheet" href="../css/fonts/font-awesome-4.2.0/css/font-awesome.css">
<link rel="stylesheet" href="../css/style.css?v=<?=VERSION_APP ?>">
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.js"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

<link href="../css/toastr.css" rel="stylesheet"/>
<script src="../js/toastr.js"></script>
<script src="../js/toastr-options.js"></script>

<script src="../js/login.js?v=<?=VERSION_APP ?>"></script>
