<?php ?>
        En tant que visiteur non authentifié, vous pouvez visiter les pages de liste des sorties, en simple consultation.<br>
        Pour aller plus loin, il vous faut vous authentifier, en utilisant le bouton "Connexion", en haut à droite.<br>
        <br>
        Le Email demandé est celui que vous avez fourni lors de votre inscription à l'ASVEL (attention, si vous êtes plusieurs à avoir donné la même adresse mail, celle-ci ne sera utilisable comme identifiant ici que pour l'un d'entre vous).<br>
        Si vous avez déja votre mot de passe, c'est parfait. Sinon, vous pouvez utiliser "M'envoyer un noueau mot de passe" : le nouveau mot de passe sera envoyé par mail à l'adresse que vous avez saisie, pourvu que celle-ci corresponde à celle fournie lors de votre adhésion à l'ASVEL.<br>     
        Si vous n'êtes pas encore adhérent à l'ASVEL, n'hésitez pas à passer nous voir le jeudi entre 19h30 et 20h30 : par vidéo en temps de COVID (<a href="https://meet.jit.si/ASVELSkiMontagnePermanenceVirtuelle">ici</a>), et sinon au 245, cours Emile Zola 69100 à Villeurbanne.
        <br><br>
        Une fois authentifié, vous pouvez toujours consulter la liste des sorties, mais vous pouvez maintenant vous y inscrire. Les inscriptions sont ouvertes du mardi précédent la sortie à 19h jusqu'au jeudi précédent la sortie à 19h pour les sorties d'une journée. Pour les sorties de deux jours les inscriptions ouvrent le mardi précédent.<br>
        <br>
        Une fois inscrit sur une sortie, vous ne pouvez pas vous désisncrire, mais vous pouvez modifier le commentaire de votre participation pour y expliquer que vous êtes désolé(e), mais bon voilà ça n'est plus possible finalement...<BR>
        Une fois inscrit, il ne vous reste plus qu'à attendre la validation de la part de l'encadrant, qui intervient généralement au début de la permanence du jeudi : la validation de votre participation apparaît immédiatement dans la liste des sorties.<br>

