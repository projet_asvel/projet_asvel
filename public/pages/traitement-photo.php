<?php

require_once '../../config/globalConfig.php';

use App\Entity\Photo;
use App\Repository\PhotoRepository;
use App\Security;

// if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
//     header("location: Accessdenied.php");
// }

$idSortie = filter_input(INPUT_POST, 'idsortie', FILTER_SANITIZE_STRING);
$idAdherent = filter_input(INPUT_POST, 'idadherent', FILTER_SANITIZE_STRING);

$target_file = '../photo_sortie/' . basename($_FILES["file"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Vérifie le faite que ce soit bien une image et pas un fake
$check = getimagesize($_FILES["file"]["tmp_name"]);
if($check !== false) {
    $uploadOk = 1;
}else {
    $uploadOk = 0;
    echo 'Not an image';
}

// regarde si le fichier existe
if (file_exists($target_file)) {
    $uploadOk = 0;
    echo 'Duplication';
}

// regarde la taille de l'image
if ($_FILES["file"]["size"] > 500000) {
    $uploadOk = 0;
    echo 'Size';
}

// vérifie le type d'image
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
    $uploadOk = 0;
    echo 'Type';
}

// Regarde si notere variable est ok ou non
if ($uploadOk != 0) {
    // si tout est ok tente d'upload le fichier
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
    }else {
        // Error uploading file
        echo 'Error uploading file';
    }

    $datas = array(
        'id' => null,
        'cheminPhoto' => $_FILES['file']['name'],
        'dateHeurePhoto' => date('Y-m-d H:i:s'),
        'idSortie' => $idSortie,
        'idAdherent' => $idAdherent,
    );
    
    try {
        $Photo = new Photo($datas);
        $PhotoRep = new PhotoRepository();
        if ($PhotoRep->vérifier($Photo, $tabErr)) {
            $LaPhoto = $PhotoRep->sauver($Photo);
            echo $LaPhoto->getIdSortie() ;
        } else {
            echo $tabErr[0];
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}



