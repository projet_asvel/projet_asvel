<?php
    require_once '../../config/globalConfig.php';

    use App\Repository\AdherentRepository ;
    use App\Security;

    // Gestion de la sécurité
    if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
	header("location: Accessdenied.php");
    }

    $mapper = new AdherentRepository() ;
    $lbActifsUniquement = false ;
    if (isset($_GET["actifs"])) {
        $lbActifsUniquement = ($_GET["actifs"] === "true") ;
    }
    $listeAdherent = $mapper->getAll(false, $lbActifsUniquement) ;
?>

<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

	<body>
            <?php include_once 'inc/header.php'; ?>

		<div class="container col-11 py-3">
			<div class="row justify-content-center">
				<h4>Liste des Adhérents (<?= $lbActifsUniquement?"uniquement les adhérents actifs actuellement":"tous les adhérents" ?>)</h4>
			</div>
			<?php
			if (isset($_SESSION['idadherent'])) {
				if (Security::hasRole(Array(Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE))) {
					echo '<a href="info-adherent.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</button></a>';
				}
				if (Security::hasRole(Array(Security::ROLE_SECRETAIRE))) {
					echo ' <a href="import.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Importer</button></a>';
				}
				if (Security::hasRole(Array(Security::ROLE_ENCADRANT, Security::ROLE_SECRETAIRE))) {
					echo '&nbsp;<button type="button" class="btn btn-primary" onClick="envoyerMailAuxFiltrés(event)"><i class="fa fa-forward"></i> Envoyer un mail aux adhérents filtrés</button>&nbsp';
				}
			}
			?>
			<div class="row py-2"></div>
			<table class="table table-hover" id="table_adherent">
				<thead class="thead-dark">
					<tr>
						<th style="display: none">Id</th>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Tags</th>
						<th>Fédérations</th>
					</tr>
				<tbody>
					<?php
					foreach ($listeAdherent as $value) {
						echo "<tr>";
						echo "<td style='display: none'>" . $value->getIdAdherent() . "</td>";
						echo "<td>" . $value->getNomAdherent() . "</td>";
						echo "<td>" . $value->getPrenomAdherent() . "</td>";
//          echo "<td>";
						$listeTags = $value->getTags();
						$listNomsTags = "";
						$i = 0;
						foreach ($listeTags as $valeur) {
							$i++;
//              if ($i > 1) {
//            echo ", ";
//              }
//            echo $valeur->getNomTag();
						if ($listNomsTags)
							$listNomsTags .= ", ";
							$listNomsTags .= $valeur->getNomTag();
						}
						if (strlen($listNomsTags) > 105) {
							echo '<td title="' . $listNomsTags . '">' . substr($listNomsTags, 0, 103) . "...";
						} else {
							echo '<td>' . $listNomsTags;
						}
						echo "</td>";

						echo "<td>";
						$listeFederation = $value->getFederations();
						$lst = "";
						foreach ($listeFederation as $value) {
							if ($lst)
								$lst .= ", ";
							$lst .= $value->getNomFederation();
						}
						echo $lst;
						echo "</td>";
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
<?php include_once 'inc/footer.php' ?>
	</body>
	<script type="text/javascript" src="../js/datatables.min.js"></script>
	<script>
        $(document).ready(function () {
            var options = {"order": [[1, "asc"]],
                "language": {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }};
            var table = $('#table_adherent').DataTable(options);

            $('#table_adherent tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                window.location.assign("info-adherent.php?id=" + data[0]);
            });
        });


        function envoyerMailAuxFiltrés(event) {
            var urlSource = window.location.href;
            var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
            urlDest += "info-mail.php";
            // Récupérer la liste des Id des adhérents correspondant au filtre
            // debugger ;
            lcLst = "";
            $("#table_adherent").DataTable().rows({'search': 'applied'}).data().each(function (value, index) {
                if (lcLst)
                    lcLst += ","
                //        lcLst += $("#table_adherent").DataTable().data()[value][0] ;
                lcLst += value[0];
            });
            urlDest += "?adherents=" + lcLst;
            window.location = urlDest;
            event.preventDefault();
        }

	</script>

</html>
