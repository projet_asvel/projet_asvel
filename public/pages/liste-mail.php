<?php
require_once '../../config/globalConfig.php';

use App\Repository\MailRepository;
use App\Security;

// Gestion de la sécurité
if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
	header("location: Accessdenied.php");
}
// LG 20200221 fin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
		<?php include_once 'inc/header.php' ?>

        <div class="container col-11 py-3">
            <div class="row justify-content-center">
                <h4>Historique des mails</h4>
            </div>
            <a href="info-mail.php">
				<button type="button" class="btn btn-primary">
					<i class="fa fa-plus"></i>
					Envoyer un nouveau mail
				</button>
			</a>
            <div class="row py-2"></div>
            <table class="table table-hover" id="table_sortie">
                <thead class="thead-dark">
                    <tr>
                        <th style="display: none;">Id</th>
                        <th>Date</th>
                        <th>Expéditeur</th>
                        <th>Sujet</th>
                        <th>Contenu</th>
                    </tr>
                <tbody>
					<?php
					$repo = new MailRepository();
					$listeMails = $repo->getAll();
// var_dump($listeMails);
					foreach ($listeMails as $item) {
						$loExpéditeur = $item->getExpéditeur();
						echo "<tr>";
						echo '<td style="display: none">' . $item->getIdMail() . "</td>";
						echo "<td>" . substr($item->getDateMail(), 0, 19) . "</td>";
						echo '<td>' . $loExpéditeur->getPrenomAdherent() . " " . $loExpéditeur->getNomAdherent() . "</td>";
						echo "<td>" . $item->getSujetmail() . "</td>";
						echo "<td>" . substr($item->getContenumail(), 0, 50) . "</td>";
						echo "</tr>";
					}
					?>
                </tbody>
            </table>
        </div>
		<?php include_once 'inc/footer.php' ?>
    </body>
    <script type="text/javascript" src="../js/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
			var options = {"order": [[1, "desc"]]};
            var table = $('#table_sortie').DataTable(options);

            $('#table_sortie tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                window.location.assign("info-mail.php?id=" + data[0]);
            });
        });
    </script>

</html>
