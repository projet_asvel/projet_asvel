<?php
require_once '../../config/globalConfig.php';

use App\Entity\Adherent;
use App\Entity\Sortie;
use App\Entity\Difficulte;
use App\Entity\RaisonAnnulation;
use App\Repository\SortieRepository;
use App\Repository\AdherentRepository;
use App\Repository\DifficulteRepository;
use App\Repository\ActiviteRepository;
use App\Repository\ParticiperRepository;
use App\Repository\RaisonAnnulationRepository;
use App\Security;
use App\Photo;
use App\Repository\PhotoRepository;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header("location: Accessdenied.php");
    return ;
}

$AdheRep = new AdherentRepository();
$AdherentAuthentifié = $AdheRep->getEntitesById($_SESSION['idadherent']);

// if ('GET' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) {
    $idActiviteDemandee = (isset($_GET["Activite"]))?$_GET["Activite"]:0;
    $nomActiviteDemandee = "";
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $Adherent = new Adherent([]);
    $mapperAdherent = new AdherentRepository();
    $Adherents = $mapperAdherent->getAll();
    $nomOrganisateur = $mapperAdherent->getNomOrganisateurBySortie($id);
    $Difficulte = new Difficulte([]);
    $mapperDifficulte = new DifficulteRepository();
    $Raisonannulation = new RaisonAnnulation([]);
    $mapperRaisonAnnulation = new \App\Repository\RaisonAnnulationRepository();
    $raison = $mapperRaisonAnnulation->getAll();
    $LaRaison = $mapperRaisonAnnulation->getLibelleRaisonBySortie($id);
    $Difficultes = $mapperDifficulte->getAll();
    $nomDifficultes = $mapperDifficulte->getNomDifficulteBySortie($id);
    $mapperSortie = new SortieRepository();
    if ($id == null) {
        $Sortie = new Sortie([]);
    } else {
        $mapper = new SortieRepository();
        $Sortie = $mapper->getEntityId($id);
        $Info_Adherent = $mapperAdherent->getEntityId($Sortie->getIdOrganisateur());
        //$Info_Difficulte = $mapperDifficulte->getEntityId($Sortie->getId_Difficulte());
    }

	// Ces variables sont prévues pour désactiver la modification/enregistrement d'une sortie passée
	// Mais ce n'est pas possible car les sorties passées doivent pouvoir être enregistrées a posteriori
	// en particulier en ce qui concerne les données comptables, modifier date/heure, participants réels,...
    $disabled = "";
    
    if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
        // Les adhérents simples ne peuvent pas modifier
        $disabled = 'disabled="disabled"';
    }
    
    $valider = true ;
    

    /*
    $RepoParticiper = new ParticiperRepository();
    $listeParticipant = $RepoParticiper->getAllAdherentByIdSortie($id);
    $listeMailpartcipants = '';
    foreach ($listeParticipant as $participant) {
        
        if ($participant->getidRaisonAnnulation()) {
                // Ce participant a annulé sa participation
                continue ;
        }
        
        if ($listeMailpartcipants) {
            $listeMailpartcipants .= ',';
        }
        $listeMailpartcipants .= $participant->getIdAdherent();
    }*/
// }

    $photoRep = new PhotoRepository();

    $allPhoto = $photoRep->getBySorties($id);

    $idAllCheckbox = "";

    if($allPhoto != "")
    {
        foreach($allPhoto as $photo)
        {
            if($idAllCheckbox == "" )
            {
                $idAllCheckbox = $photo->getIdPhoto();
            }
            else
            {
                $idAllCheckbox .= ",".$photo->getIdPhoto();
            }
            
        }
    }
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php
        include_once 'inc/header.php';
        ?>

        <!-- <form action="" method="POST" id ="formphoto"> -->
        <form action="" enctype="multipart/form-data" method="POST" id ="formphoto">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">
                            Sortie <?= $Sortie->getTitre() ?>
                        </h4>

                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-1">
                                <a class="" href="info-sortie.php?id=<?= $id ?>"  >
                                    <button type='button' class='btn btn-secondary'><i class='fa fa-backward'></i> Retour</button>
                                </a>
                            </div>
                            <div class="col-4">
                                <button type='button' class='btn btn-primary' data-toggle="modal" data-target="#myModalphoto" ><i class='fa fa-forward'></i> Ajouter photo</button>
                                <button type='button' style="background-color:#1977f3" onclick="envoi_image()" class='btn'><i class='fa fa-forward'></i> Envoie Facebook</button>
                            </div>
                        </div>

                        <!-- Modal Création Photo -->
                        <div class="modal fade mymodalhoraire" id="myModalphoto"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                            <div class="modal-dialog modal-lg" role="document">

                                <div class="modal-content">

                                    <div class="modal-header">

                                        <h4 class="modal-title">Photo</h4>

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    </div>

                                    <!-- <form class="form-actions"  enctype="multipart/form-data" action="#" method="post" > -->
                                    

                                    <div class="modal-body">

                                        <!-- <div class="row">

                                            <div class="col-md-4">

                                                <b>Libelle</b>

                                            </div>
                                            <div class="col-md-8">

                                                <input type="text" required name="libellePhoto" placeholder="Titre de la photo" id="libellePhoto" class="form-control"/>

                                            </div>
                                        </div><br/> -->

                                        <div class="row">

                                            <div class="col-md-4">

                                                <b>Photo</b>

                                            </div>
                                            <div class="col-md-8 custom-file">

                                                <input required type="file"  name="fichier_input" accept=".png,.jpg,.jpeg" id="fichier_input" class="custom-file-input "/>

                                                <label class="custom-file-label" for="validatedCustomFile">Choisir un fichier...</label>

                                            </div>
                                        </div><br/>

                                        <input type="hidden" name="idsortie" id="idsortie" value="<?= $id ?>">

                                        <input type="hidden" name="idadherent" id="idadherent" value="<?= $_SESSION['idadherent'] ?>">

                                    </div>
                                
                                    <div class="modal-footer">

                                        <div class="row col-lg-12">

                                            <div class="col-10">
                                                <button type="submit" name="bouton_photo_save" class="btn btn-block btn-primary"><i class="fa fa-save"></i> Valider</button>
                                            </div>

                                            <div class="col-2">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                            </div>

                                        </div>

                                    </div>
                                <!-- </form> -->

                                </div><!-- /.modal-content -->

                            </div><!-- /.modal-dialog -->
                        </div><br/><br/>

                        <div class="row">
                            <?php 

                            

                            if($allPhoto != "")
                            {
                                foreach($allPhoto as $photo)
                                {
                                    // margin-left:80%;width:20%;overflow: hidden;
                                    ?>
                                    
                                        <div class="card" style="width:300px;height:300px;overflow: hidden;">
                                            <div class="row col-auto ">
                                                <a onclick="supp_image(<?= $photo->getIdPhoto() ?>,<?php echo '\''.$photo->getCheminPhoto().'\'' ?>)" class="btn btn-danger"><i class="fa fa-times" style="color:white" aria-hidden="true"></i></a>
                                                <!-- <a onclick="supp_image(<?= $photo->getIdPhoto() ?>,<?php echo '\''.$photo->getCheminPhoto().'\'' ?>)" class="btn btn-secondary"><i class="fa fa-times" style="color:white" aria-hidden="true"></i></a><br/> -->
                                                <input style="width:50px" type="checkbox" id="<?= $photo->getIdPhoto() ?>" name="checkPhoto<?= $photo->getIdPhoto() ?>">
                                            </div>
                                            <a  style="margin-left: auto;margin-right: auto;overflow: hidden;" download="<?= $photo->getCheminPhoto() ?>" href="../photo_sortie/<?= $photo->getCheminPhoto() ?>" title="ImageName">
                                                <img  alt="ImageName" width="100%" height="100%" style="overflow: hidden;" src="../photo_sortie/<?= $photo->getCheminPhoto() ?>"><br/>
                                            </a>
                                            <p style="overflow: hidden;" class="card-text" style="text-align:center"><?= $photo->getCheminPhoto() ?></p>

                                        </div>
                                
                                    <?php 
                                }
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
            </br> </br>

        </form>
        <form action="manageFacebook.php" enctype="multipart/form-data" method="POST" id ="formEnvoiePhoto">
            <input  type="hidden"  name="checkList" id="checkList" />
        </form>
    </body>
    <script src="../js/Photo.js?v=<?=VERSION_APP ?>"></script>
    <script type="text/javascript" src="../js/jquery-3.6.0.js"></script>

    <script>
        var j = jQuery.noConflict();

        function supp_image(idphoto,chemin)
        {
            if (confirm("Souhaitez-vous réellement supprimer cette photo ?")) {
                $.ajax({

                    type: 'post',
                    url: '../../public/pages/supprimer-photo.php',
                    data: {
                        idphoto : idphoto,
                        chemin : chemin
                    },

                    success: function (response) { 

                        window.location.replace("info-photo.php?id=" + $("#idsortie").val());
                    }

                });
            }
            
        }

     

        function envoi_image()
        {

            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            var checkList = '';
            for (var i=0; i<checkboxes.length; i++) {
                if (checkboxes[i].checked == true){
                    
                        checkList += ","+checkboxes[i].id;
                }
            }

            alert(checkList);
            if (checkList != "") {
                const elem = document.getElementById('checkList');
                elem.value = checkList;

                
                document.forms["formEnvoiePhoto"].submit();
            }
            
            
        }
        j(document).ready(function () {
            // var j = jQuery.noConflict();

            j('input[type="file"]').change(function(e){

                var fileName = e.target.files[0].name;

                j('.custom-file-label').html(fileName);

            });
        });
    </script>

</html>
