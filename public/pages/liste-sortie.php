<?php
require_once '../../config/globalConfig.php';

//require_once'../../src/App/Repository/SortieRepository.php';

use App\Repository\SortieRepository;
use App\Repository\DifficulteRepository;
use App\Repository\ParticiperRepository;
use App\Repository\RaisonAnnulationRepository;
use App\Security;

$loRepo = new SortieRepository();
// LG 20211106 début
// $lbSortiePassée = (isset($_GET['Passées']) && $_GET['Passées'] == "T");
// $listeSortie = $loRepo->getAllFiltré($liFiltreDate, $idActivite);
// $idActivite = isset($_GET['Activite']) ? $_GET['Activite'] : 1;
$idActivite = isset($_GET['Activite']) ? $_GET['Activite'] : Null;

// LG 20211127 début
if ($idActivite == 3) {
    // Ski alpin
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php'; ?>
         <div class="container col-11 py-3">
            <h4>
            Sorties de ski alpin
            </h4>
            <br><br><br>
            La liste des sorties de ski alpin, et les inscriptions en ligne se font directement sur le site de l'ASVEL ski montagne : <br><br>
            <a href="https://www.asvelskimontagne.fr/inscription-aux-sorties-de-ski/" target="_blank">https://www.asvelskimontagne.fr/inscription-aux-sorties-de-ski/>https://www.asvelskimontagne.fr/inscription-aux-sorties-de-ski/</a>
            <br><br><br>
         </div>
        <?php include_once 'inc/footer.php' ?>
    </body>
<?php
    return ;
}
// LG 20211127 fin

if (!isset($_GET['Passées'])) {
    $liFiltreDate = FILTRESORTIE_TOUTESDATES ;
} else if ($_GET['Passées'] == "T") {
    $liFiltreDate = FILTRESORTIE_DATESPASSEES ;
} else {
    $liFiltreDate = FILTRESORTIE_DATESAVENIR ;
}
if (isset($_GET['Adhérent'])) {
    $liAdherent = $_GET['Adhérent'] ;
} else {
    $liAdherent = null ;
}
//$liAdherent = $_SESSION['idadherent'] ;
$listeSortie = $loRepo->getAllFiltré($liFiltreDate, $idActivite, $liAdherent);
// LG 20211106 fin

$RepoDifficulte = new DifficulteRepository();
$ListeDifficulte = $RepoDifficulte->getAll();

$repoActivites = new \App\Repository\ActiviteRepository();
$acti = $repoActivites->getEntityById($idActivite);

$PartiRepo = new ParticiperRepository();
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php';
        ?>

        <div class="container col-11 py-3">
            <div class="row justify-content-center">
                <h1 style="font-size: 25px;">
                <?php 
                    if ($liAdherent && isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $liAdherent) {
                        // L'adherent connecté consulte ses propres sorties
                        echo "Mes sorties passées et à venir " ;
                    } else if ($liAdherent) {
                        // Un utilisateur consulte les sorties d'un adherent
                        $RepoAdherents = new \App\Repository\AdherentRepository();
                        $Adherent = $RepoAdherents->getEntityById($liAdherent) ;
                        echo "Les sorties passées et à venir de " . $Adherent->getNomUsuel() ;
                    } else {
                        echo "Les sorties " ;                       
                    }
                    if ($liFiltreDate == FILTRESORTIE_DATESPASSEES) { 
                        echo "passées" ;
                    } else if ($liFiltreDate == FILTRESORTIE_DATESAVENIR) {
                        echo "à venir" ;
                    }
                    if ($idActivite) {
                        echo " : " . $acti->getNomActivite() ;
                    }
                ?>
                </h1>
            </div>
<!-- LG 20211106 fin -->
            <?php
            if (Security::hasRole(Array(Security::ROLE_ENCADRANT))) {
                echo '<a href="info-sortie.php?Activite=' . $idActivite . '"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</button></a>';
            }
            ?>

            <div class="row py-2"></div>
            <table class="table table-hover" id="table_sortie">
                <thead class="thead-dark">
                    <tr>
                        <th style="display: none">Id</th>
                        <th>Nom</th>
                        <th>Encadrant</th>
                        <th>Lieu</th>
                        <?php
                        /* Astuce pour trier les dates correctement tout en afficant la date à la française : 
                         * l'en-tête de colonne date est visible, les données contiennent les dates Y/m/d et sont cachées
                         * l'en-tête de colonne NonTriable est caché, les données contiennent les dates d/m/Y et sont visibles
                         */
                        ?>
                        <th>Date</th>
                        <th style="display: none">DateNonTriable</th>
                        <th>Durée</th>
                        <th>Départ</th>
                        <th>Difficulté</th>
                        <?PHP
                        echo "<th title='Inscrits/Max/(+ en attente car non validés)'>Participants</th>";
                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                            echo "<th>Inscription en ligne</th>";
                            echo "<th>Comm</th>";
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($listeSortie as $sortie) {
                        $NombreParticipants = 0;
                        $NombreParticipantsAttente = 0;
                        if ($sortie->getInscriptionenligne() == 1) {
                            $inscriptionenligne = "Oui";
                        } else {
                            $inscriptionenligne = "Non";
                        }
                        $lbSortiePassée = ($sortie->getDateDebutSortie() <= date("Y-m-d")) ;
                        
                        $difficultename = "";
                        foreach ($ListeDifficulte as $toto) {
                            if ($sortie->getIdDifficulte() == $toto->getIdDifficulte()) {
                                $difficultename = $toto->getNomDifficulte();
                            }
                        }

                        if ($sortie->getidRaisonAnnulation()) {
                            echo "<tr style='text-decoration: line-through;'>";
                        } else {
                            echo "<tr>";
                        }
                        echo "<td style='display: none'>" . $sortie->getIdSortie() . "</td>";
                        echo "<td>" 
// . $sortie->getDateDebutSortie() . "<br>" . date("Y-m-d") . "<br> " . ($lbSortiePassée?"PASSEE ":"A VENIR ") . "<br>"
                                . $sortie->getNomSortie() . " " . $sortie->getNomsActivites() 
                                . "</td>";
                        echo "<td>" . $sortie->getOrganisateur()->getNomUsuel() . "</td>";
                        echo "<td>" . $sortie->getLieuSortie() . "</td>";
                        echo "<td style='display: none'>" . date('Y/m/d', strtotime($sortie->getDateDebutSortie())) . "</td>";
                        echo "<td>" . date('d/m/Y', strtotime($sortie->getDateDebutSortie())) . "</td>";
                        echo "<td>";
                        $nbJours = $sortie->getNbJours();
                        echo $nbJours . " jour";
                        if ($nbJours > 1) {
                            echo "s";
                        }
                        echo "</td>";
                        echo "<td>" . $sortie->getHeureDepart() . "</td>";
                        echo "<td>" . $difficultename . "</td>";

//                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                        // Nb de participants
                        $NombreParticipantsInscrits = $PartiRepo->getNbParticipantsBySortie($sortie->getIdSortie(), $NombreParticipantsValidés);
                        $Surplus = "";
                        if ($NombreParticipantsValidés > $sortie->getNombreParticipantsMaxSortie()) {
                            $Surplus = 'style = "color: red; font-weight: bold;"';
                        }
                        echo "<td " . $Surplus . ">";
                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
                            echo "<a class='' href='liste-participant.php?idSortie=" . $sortie->getIdSortie() . "'>";
                        }
                        echo ($NombreParticipantsValidés)
                        . "/" . ($sortie->getNombreParticipantsMaxSortie() ?: "-")
                        . "/(+" . ($NombreParticipantsInscrits - $NombreParticipantsValidés) . ")";
                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
                            echo "</a>";
                        }
                        echo "</td>";
//                        }

// LG 20210513 début : suspension de l'automatisme des ouvertures d'inscriptions d'après Ismael
//                        if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR) && !$lbSortiePassée) {
//                            // S'inscrire à la sortie
//                            if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR) && $sortie->getInscriptionenligne() == 1) {
//                                //  combien de temps au maximum il faut pour que la sortie soit ouverte : jourmax
//                                //  si sortie de 1j : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_1_J
//                                //  sinon si : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_2_3_J
//                                //  sinon : jourmax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_SUP_3_J
//                                // si $joursDiff< jourmax
//                                // 	si $joursDiff< NB_JOURS_LIMITE_INSCRIPTION_SORTIE jours clos
//                                // 	 sinon 	 ouvert selon heure et jours
//                                // sinon
//                                // pas encore ouvert
//
//                                $TempsActuelle = getdate();
//
//                                $liNoJourDHui = $TempsActuelle['wday'] - 1; //Lundi=0 Dimanche=6
//                                if ($liNoJourDHui < 0) {
//                                    $liNoJourDHui = 6;
//                                }
//                                $JoursNumActuelle = $TempsActuelle['yday'];
//                                $TimeDateSortie = strtotime($sortie->getDateDebutSortie());
//                                $TimestampActuelle = $TempsActuelle[0]; #timetamps actuelle
//                                $HeureActuelle = idate("H", $TimestampActuelle) + $TempsActuelle['minutes'] / 60;
//                                $joursDiff = ceil(($TimeDateSortie - $TimestampActuelle) / 86400);
//
//                                if ($nbJours == 1) {
//                                    $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_1_J;
//                                } elseif ($nbJours == 2 || $nbJours == 3) {
//                                    $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_2_3_J;
//                                } elseif ($nbJours > 3) {
//                                    $jourMax = NB_JOURS_AVANCE_INSCRIPTION_SORTIE_SUP_3_J;
//                                }
//
//                                if ($joursDiff < $jourMax) {
//                                    if ($joursDiff < NB_JOURS_LIMITE_INSCRIPTION_SORTIE) {
//                                        $lsInscription = "<td>Inscription close</td>";
//                                    } else {
//                                        //------------------------------------ Triage par jours
//
//                                        if (($liNoJourDHui <= 1 && $HeureActuelle < HEURE_OUVERTURE_INSCRIPTION_SORTIE)) {
//                                            $lsInscription = "<td>Inscription non ouverte</td>";
//                                        } elseif ($liNoJourDHui >= 3) {
//                                            if ($liNoJourDHui >= 4) {
//                                                $lsInscription = "<td>Inscription closes</td>";
//                                            } else {
//                                                if ($HeureActuelle >= HEURE_FERMETURE_INSCRIPTION_SORTIE) {
//                                                    $lsInscription = "<td>insription closes</td>";
//                                                } else {
//                                                    $lsInscription = "<td> Inscription ouvertes </td>";
//                                                }
//                                            }
//                                        } else {
//                                            $lsInscription = "<td> Inscription pas encore ouverte1</td>";
//                                        }
//                                    }
//                                } else {
//                                    $lsInscription = "<td> Inscription pas encore ouverte2</td>";
//                                }
//
//                                $laLstParticipations = $PartiRepo->$getParticipationByAdherentAndSortie($_SESSION['idadherent'], $sortie->getIdSortie());
//                                foreach ($laLstParticipations as $p) {
//                                    if ($p->getValidationParticipation() == TRUE) {
//                                        $lsInscription = "<td><a href= 'info-participant.php?idAdherent=" . $_SESSION['idadherent'] . "&idSortie=" . $sortie->getIdSortie() . "'>Vous êtes inscrit</a></td>";
//                                    } else if ($p->getValidationParticipation() == FALSE) {
//                                        $lsInscription = "<td>En Attente de validation</td>";
//                                    }
//                                }
//                            } else if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR) && $sortie->getInscriptionenligne() == 0) {
//                                $laLstParticipations = $PartiRepo->$getParticipationByAdherentAndSortie($_SESSION['idadherent'], $sortie->getIdSortie());
//                                foreach ($laLstParticipations as $p) {
//                                    if ($p->getValidationParticipation() == TRUE) {
////					$lsInscription = "<td><a href= 'info-participant.php?idAdherent=" . $_SESSION['idadherent'] . "&idSortie=" . $sortie->getIdSortie() . "'>Vous êtes inscrit</a></td>";
//                                        $lsInscription = "<td>Vous êtes inscrit</td>";
//                                    } else if ($p->getValidationParticipation() == FALSE) {
//                                        $lsInscription = "<td>En Attente de validation</td>";
//                                    }
//                                }
//                                if ($laLstParticipations == NULL) {
//                                    $lsInscription = "<td style=color:grey;>Non disponible </td>";
//                                }
//                            } else if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
//                                //$lsInscription = "<td><a href=info-participant.php?idSortie=" . $sortie->getIdSortie() . "&idAdherent=" . $_SESSION['idadherent'] . ">S'inscrire</button></a></td>";
//                            } else {
//                                $lsInscription = "";
//                            }
//                            echo $lsInscription;
//                        }

                        
                        // Déterminer le statut de l'utilisateur pour cette sortie
                        if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                            // L'utilisateur en cours n'est pas authentifié
                            // RAS
                            $lsInscription = "" ;
                        } else {
                            $lsInscription = "<td></td>";       // Valeur par défaut
                            // Utilisateur authentifé : on peut déterminer son statut
                            if ($lbSortiePassée) {
                                // Sortie passée : pas de lien vers l'inscription
                                $lsInscription_LienVersParticipation = "" ;
                            } else {
                                $lsInscription_LienVersParticipation = "<a href='info-participant.php?idAdherent=" . $_SESSION['idadherent'] . "&idSortie=" . $sortie->getIdSortie() . "'>" ;
                            }
                            
                            $laLstParticipations = $PartiRepo->getParticipationByAdherentAndSortie($_SESSION['idadherent'], $sortie->getIdSortie());
                            $loParticipation = null ;
                            foreach ($laLstParticipations as $loParticipation) {
                                // Cette boucle permet de renseigner $loParticipation si la liste des participations contient un élément
                            }
                            
                            if ($loParticipation) {
                                if ($lbSortiePassée) {
                                    // L'utilisateur a été inscrit sur cette sortie dans le passé
                                    if ($loParticipation->getIdFonction() == 2) {
                                        // Encadrant
                                        $lsInscription = "<td>J'ai encadré</td>";
                                    } else if ($loParticipation->getIdFonction() == 3) {
                                        // Co-encadrant
                                        $lsInscription = "<td>J'ai co-encadré</td>";
                                    } else if (!$loParticipation->getIdRaisonAnnulation() && $loParticipation->getValidationParticipation()) {
                                        // La participation a été effective
                                        $lsInscription = "<td>J'ai participé</td>";
                                    } else {
                                        // La participation a été annulée ou non validée
                                    }
                                } else {
                                    if ($loParticipation->getIdRaisonAnnulation()) {
                                        // La participation est annulée
                                        $loRepoRaisons = new RaisonAnnulationRepository() ;
                                        $loRaison = $loRepoRaisons->getEntityById($loParticipation->getIdRaisonAnnulation()) ;
                                       $lsInscription = '<td title="Votre participation est annulée' . chr(13) . $loRaison->getLibelleAnnulation() . chr(13) . chr(13) . $loParticipation->getCommentaireParticipation() . '">' . $lsInscription_LienVersParticipation . "Refusée ou annulée</td>";
                                    } else if ($loParticipation->getIdFonction() == 2) {
                                        // Encadrant
                                        $lsInscription = "<td>J'encadre</td>";
                                    } else if ($loParticipation->getIdFonction() == 3) {
                                        // Co-encadrant
                                        $lsInscription = "<td>Je co-encadre</td>";
                                    } else if ($loParticipation->getValidationParticipation()) {
                                        // La participation est validée
                                        $lsInscription = '<td title="Votre participation à la sortie est validée' . chr(13) . chr(13) . $loParticipation->getCommentaireParticipation() . '">' . $lsInscription_LienVersParticipation . "Validée</a></td>";
                                    } else {
                                        $lsInscription = "<td>" . $lsInscription_LienVersParticipation . "En attente de validation</td>";
                                    }
                                }
                            } else {
                                if ($lbSortiePassée) {
                                    // Sortie passée : RAS
                                } else if ($sortie->estOuvertePourInscription($lsTexteStatut, $lsDétailsStatut)) {
                                    // La sortie est ouverte aux inscriptions
                                    $lsInscription = "<td>" . $lsInscription_LienVersParticipation . "M'inscrire</td>";
                                } else {
                                   $lsInscription = '<td title="' . $lsDétailsStatut .'">' . $lsTexteStatut . "</td>";
                                }
                            }
                        }
                        echo $lsInscription ;
// LG 20210513 fin
// LG 20211128 début
                        if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                            // L'utilisateur en cours n'est pas authentifié, RAS
                            $lsCommentaires = "" ;
                        } else if ($lsCommentaires = $sortie->getCommentairesSortie()) {
                            // Il y a des commentaires pour cette sortie
                            $lsCommentaires = "<td title='" . $lsCommentaires . "'>" . substr($lsCommentaires, 0, 5) . "...</td>" ;
                        } else {
                            $lsCommentaires = "<td></td>" ;
                        }
                        echo $lsCommentaires ;
// LG 20211128 fin
                        echo "</tr>";
// break;
                    }
                    // [ fin de à tester]
                    ?>
                </tbody>
            </table>
        </div>
        <?php include_once 'inc/footer.php' ?>
    </body>
    <script type="text/javascript" src="../js/datatables.min.js"></script>

                    <?PHP
// LG 20211127 old                   if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
                    if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
                        $location = '"info-sortie.php?id=" + data[0]';
                    } else {
                        $location = '';
                    }
                    ?>
    <script>

        $(document).ready(function () {
            var options = {"order": [[4, "<?php
// LG 20211106 old                    if ($lbSortiePassée) {
                    if ($liFiltreDate == FILTRESORTIE_DATESPASSEES || $liFiltreDate == FILTRESORTIE_TOUTESDATES) {
                        echo "desc";
                    } else {
                        echo "asc";
                    }
                    ?>"]],
                "language": {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }};
            var table = $('#table_sortie').DataTable(options);
            $('#table_sortie tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                window.location.assign(<?= $location ?>);
            });
        });

<?PHP
//if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
//    ? >
//                $('#table_sortie tbody').on('click', 'tr', function () {
//                    var data = table.row(this).data();
//
//                    window.location.assign("info-sortie.php?id=" + data[0]);
//                });
//            });
//    < ?PHP
//} 
//else {
//    ? >
//            $('#table_sortie tbody').on('click', 'tr', function () {
//            var data = table.row(this).data();
//                    // La ligne "window.location.assign()" est vide du fait que ce cette ligne est necessaire pour la génération des données; la laisser vide permet de les diriger sur la page elle meme.
//                    window.location.assign();
//            });
//            }
//            );
//    < ?PHP
?>
        // http://localhost/PHP/ASVEL/Olits/public/css/DataTables-1.10.20/images/sort_both.png
    </script>
</html>
