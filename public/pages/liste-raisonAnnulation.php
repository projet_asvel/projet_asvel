<?php
require_once '../../config/globalConfig.php';

use App\Repository\RaisonAnnulationRepository;
use App\Security;

$loRepo = new RaisonAnnulationRepository();

// Gestion de la sécurité
if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}

$listeRaisonAnnulation = $loRepo->getAll();
//dump_var($listeSortie, DUMP, 'Liste des sorties: ');
?>
<!DOCTYPE html>
        <?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>
    <body>
<?php include_once 'inc/header.php' ?>
        <div class="container col-11 py-3">
            <div class="row justify-content-center">
                <h4>Liste des Raisons d'annulation</h4>
            </div>
            <?php
// LG 20200221 début
//            if (isset($_SESSION['idadherent'])) {
//                $Rolesdemendée = array('Administrateur');
//                $Protected = $Security::hasRole($Rolesdemendée);
//                if ($Protected) {
//                    echo'<a href="info-activite.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</button></a>';
//                }
//            }
                $Rolesdemendée = array('Administrateur');
                $Protected = $Security::hasRole($Rolesdemendée);
                if (Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ADMIN))) {
                    echo'<a href="info-raisonAnnulation.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</button></a>';
                }
// LG 20200221 fin
            ?>
            <div class="row py-2"></div>            

            <table class="table table-hover" id="table_sortie">
                <thead class="thead-dark">
                    <tr>
                        <th style="display: none">Id</th>
                        <th>Nom</th>
                    </tr>
                <tbody>
                    <?php
                    foreach ($listeRaisonAnnulation as $value) {
                        echo "<tr>";
                        echo "<td style='display: none'>" . $value->getIdAnnulation() . "</td>";
                        echo "<td>" . $value->getlibelleAnnulation() . "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
<?php include_once 'inc/footer.php' ?>
    </body>
    <script type="text/javascript" src="../js/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
                            var options = {"order": [[1, "asc"]],
                    "language": {
                        processing: "Traitement en cours...",
                        search: "Rechercher&nbsp;:",
                        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix: "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable: "Aucune donnée disponible dans le tableau",
                        paginate: {
                            first: "Premier",
                            previous: "Pr&eacute;c&eacute;dent",
                            next: "Suivant",
                            last: "Dernier"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    }};
            var table = $('#table_sortie').DataTable(options);

            $('#table_sortie tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                window.location.assign("info-raisonAnnulation.php?id=" + data[0]);
            });
        });
    </script>
</html>


