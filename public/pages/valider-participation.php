<?php
    require_once '../../config/globalConfig.php';

    use App\Repository\ParticiperRepository;

    $repo = new ParticiperRepository() ;

    if (!isset($_POST["idSortie"]) || !isset($_POST["idAdherent"])) {
        echo "Paramètres incorrects" ;
        return false ;        
    }
        
    $idAdherent = filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_NUMBER_INT);
    $idSortie = filter_input(INPUT_POST, 'idSortie', FILTER_SANITIZE_NUMBER_INT);
    $valider = filter_input(INPUT_POST, "dévalider", FILTER_SANITIZE_STRING)==="false" ;
    
//echo $idAdherent . "<br>" . $idSortie . "<br>" . $valider . "<br>" ;
//return false ;        
/*    
$idAdherent = 157 ;
$idSortie = 37 ;
$valider = true ;
 */
    $loParticipation = $repo->getEntityById(["idAdherent" => $idAdherent, "idSortie" => $idSortie]) ;
    if (!$loParticipation) {
        // Participation non retrouvée
        echo "Participation non retrouvée" ;
        return false ;        
    }
// echo filter_input(INPUT_POST, "dévalider", FILTER_SANITIZE_STRING) . "<br>" ;
// echo $valider ? "VALIDER":"DEVALIDER<BR>" ;
    
    $loParticipation->setValidationParticipation($valider) ;
    if ($repo->sauver($loParticipation)) {
        echo "true" ;
        return true ;
    } else {
        echo "L'enregistrement a échoué" ;
        return false ;      
    }
