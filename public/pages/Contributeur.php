<?php
require_once '../../config/globalConfig.php';
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <body>
	</div>
	<?php
	include_once 'inc/header.php';
// LG 20200208 old dump_var($_SESSION);
// LG 20200208 old   $Repo = new App\Repository\AdherentRepository();
// LG 20200208 old $Repo->getEntitesById(1);
	?>

	<h1 style="text-align: center;text-decoration: underline;"> Contributeurs au projet </h1>
	<table style="width: 61%; margin-left: 15%;margin-right: 15%;">
		<tbody>
			<tr>
				<td>
					<a href="https://ort-france.fr/lyon" style="margin-left: 44%"><img src="..\img\logoOrt.png"  width="200"></a>
				</td>
				<td>
				</td>
				<td>
					<a href="https://ort-france.fr/lyon" style="margin-left: 44%"><img src="..\img\LogoOlits.png"  width="200"></a>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<h3 style="text-align: center"> Les membres de la classe 3CSI année scolaire 2019-2020 </h3>
	<h3 style="text-align: center"> En particulier:
        <h3 style="text-align: center">Quentin Baudrant</h3>
        <h3 style="text-align: center">Haris Berramdane</h3>  
        <h3 style="text-align: center">Matthieu Givaudan</h3>
        <h3 style="text-align: center">Yvan Konche Tchuente</h3>
        <h3 style="text-align: center">Antoine Martin</h3>
        <h3 style="text-align: center">Vincent Telouk</h3>
        <h3 style="text-align: center">Guillaume Zapata</h3>
        <h3 style="text-align: center">Et leur encadrant pour cette épreuve : Luc Gilot</h3>

		<canvas id="mon_canvas" width="500" height="300" style="border:1px black solid;margin-left: 34%">
			<script src="../js/canvas.js?v=<?= VERSION_APP ?>"></script>