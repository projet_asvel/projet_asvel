<?php
// Génération du tableau d'information sur les tarifs des sorties
// LG 20201011
require_once '../../config/globalConfig.php';

$TarifJournéeRandonée = TARIF_SORTIE_AUTRE_1J . "€";
$TarifJournéeRaquettes = TARIF_SORTIE_AUTRE_1J . "€";
$TarifJournéeSkiRandonnée = TARIF_SORTIE_AUTRE_1J . "€";
$TarifJournéeAlpinisme = TARIF_SORTIE_ALPI_1J . "€";
$TarifJournéeEscalade = TARIF_SORTIE_ESCALADE_1J . "€";

$TarifWeekEndRandonée = TARIF_SORTIE_AUTRE_2J . "€";
$TarifWeekEndRaquettes = TARIF_SORTIE_AUTRE_2J . "€";
$TarifWeekEndSkiRandonnée = TARIF_SORTIE_AUTRE_2J . "€";
$TarifWeekEndAlpinisme = TARIF_SORTIE_ALPI_2J . "€";
$TarifWeekEndEscalade = TARIF_SORTIE_ESCALADE_2J . "€";

$TarifWeekEnd3JoursRandonée = TARIF_SORTIE_AUTRE_3J . "€";
$TarifWeekEnd3JoursRaquettes = TARIF_SORTIE_AUTRE_3J . "€";
$TarifWeekEnd3JoursSkiRandonnée = TARIF_SORTIE_AUTRE_3J . "€";
$TarifWeekEnd3JoursAlpinisme = TARIF_SORTIE_ALPI_3J . "€";
$TarifWeekEnd3JoursEscalade = TARIF_SORTIE_ESCALADE_3J . "€";

$TarifJournéeSkiPiste = TARIF_SORTIE_SKIPISTE_1J;
$TarifWeekEndSkiPiste = TARIF_SORTIE_SKIPISTE_2J . "€";
$TarifWeekEnd3JoursSkiPiste = TARIF_SORTIE_SKIPISTE_3J . "€";
?>
<style>
	.tableTarifs > table {
	  border-collapse: collapse;
	}

	.tableTarifs > th, td {
	  border: 1px solid black;
	}	
</style>
	
<table class="tableTarifs">
	<tr>
	<th colspan="4">Tarifs sorties</th>
	</tr>
	<tr>
		<th>Activité</th>
		<th>Journée</th>
		<th>WE</th>
		<th>WE 3 jours</th>
	</tr>
	<tr>
		<td>Randonnée<BR>Raquettes<BR>Ski de rando<BR>Ski de fond<BR>Escalade</td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifJournéeRandonée?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEndRandonée?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEnd3JoursRandonée?></td>
	</tr>
	<tr>
		<td>Ski de piste</td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifJournéeSkiPiste?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEndSkiPiste?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEnd3JoursSkiPiste?></td>
	</tr>
	<tr>
		<td>Alpinisme</td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifJournéeAlpinisme?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEndAlpinisme?></td>
		<td style="text-align: center; vertical-align: middle;"><?=$TarifWeekEnd3JoursAlpinisme?></td>
	</tr>
</table>

