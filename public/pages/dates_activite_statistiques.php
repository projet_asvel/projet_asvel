<?php require_once '../../config/globalConfig.php';?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

    <body>
        <?php include_once 'inc/header.php' ?>


<!-- STATISTIQUES DEBUT -->
<link href="..\css\style.css" rel="stylesheet">
<h1 class="onglet">Statistiques</h1>
    
    <form action="traitement-statistiques.php" method="post">
    
    <p class=texte>Date de début sortie</h><br><br/>
    
    <input type="date" id="start" name="datedebutsortie"
           value="2018-07-22"
           min="2010-01-01" max="2023-12-31">

    <p class=texte>Date de fin sortie</h><br><br/>
    
    <input type="date" id="start" name="datefinsortie"
           value="2018-07-22"
           min="2010-01-01" max="2023-12-31">
    
        <br><br><br> </br> </br> </br>  
                <input type="checkbox" id="vehicle1" name="nomactivite" value="Escalade">
                <label for="vehicle1"> Escalade</label>

              <input type="checkbox" id="vehicle1" name="nomactivite" value="Randonnée">
                <label for="vehicle1"> Randonnée</label>

                <input type="checkbox" id="vehicle1" name="nomactivite" value="Ski de Fond">
                <label for="vehicle1"> Ski de Fond</label>

                <input type="checkbox" id="vehicle1" name="nomactivite" value="Canyoning">
                <label for="vehicle1"> Canyoning</label>

                <input type="checkbox" id="vehicle1" name="nomactivite" value="Alpinisme">
                <label for="vehicle1"> Alpinisme</label>

                <input type="checkbox" id="vehicle1" name="nomactivite" value="Ski de randonnée">
                <label for="vehicle1"> Ski de randonnée</label>
                
                <input type="checkbox" id="vehicle1" name="nomactivite" value="Via ferrata">
                <label for="vehicle1"> Via ferrata</label>

                <input type="checkbox" id="vehicle1" name="nomactivite" value="Randonnée raquettes">
                <label for="vehicle1"> Randonnée raquettes</label>


    
    <br><br/><br><br/>
    <div class="button">
            <input type="submit" name="VALIDER" />
        </div>
    
    </form>

<!-- STATISTIQUES FIN-->


<br><br>        </div>
        <!-- LG 20211127 déac <script src ="../js/login.js?v=1.6">-->

        </script>
        
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!-- Footer -->
<footer class="pt-5 pb-4" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">ASVEL SKI MONTAGNE</h5>
                <ul class="f-address"style="margin-left: -17%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href='https://www.asvelskimontagne.fr/'>
                                        <img src="..\img\logo-asvel.jpg" alt="Logo asvel" width="150" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-3">
                            <div class="col-1"><i class="fas fa-map"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Addresse :</h6>
                                <p>245, cours Emile Zola 69100 Villeurbanne</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-1"><i class="fas fa-phone"></i></div>
                            <div class="col-10">
                                <h6 class="font-weight-bold mb-0">Téléphone :</h6>
                                <p>04 78 84 91 21</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PROCHAINES SORTIES</h5>
                <ul class="recent-post"style="margin-left: -15%">               
                                    </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">FÉDÉRATIONS</h5>
                <ul class="f-address">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\logoffme.png" alt="logo Omnisport" width="75">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <img src="..\img\ffs-asvel.png"  width="75">
                            </div>
                        </div>
                    </li>
                </ul>
                <h5 class="mb-4 font-weight-bold" style="margin-top: 50px;">RÉSEAUX SOCIAUX</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li><a href="https://www.facebook.com/asvel.skimontagne/" title="facebook" style="display: block;")><i class="fab fa-facebook-f"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                <h5 class="mb-4 font-weight-bold">PARTENAIRES</h5>
                <ul class="social-pet mt-4"style="margin-left: -15%">
                    <li>
                        <div class="row">
                            <div class="col-10">
                                <a href="http://www.decathlon.fr/fr/store?store_id=PS_98">
                                    <img src="..\img\logodecathlon.png" alt="logo decathlon" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="https://www.omnisport-lyon.fr">
                                    <img src="..\img\omnisport-logo.png" alt="logo Omnisport" width="150">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="Contributeur.php"><img src="..\img\LogoOlits.png"  width="150"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row py-2">
                            <div class="col-10">
                                <a href="http://www.limoog.net">
                                    <img src="https://www.limoog.net/wp-content/uploads/2021/12/cropped-cropped-cropped-cropped-cropped-Logo-Limoog.png" width="150" style="background-color: white;" alt="logiciel planning médico social et alternance">
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright -->
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="text-center text-white">
                    &copy; Association loi de 1901 agrée jeunesse et sports, affiliée FFS - FFME | Tous droits réservés  |<a href='https://www.asvelskimontagne.fr/'>   ASVEL Ski Montagne</a> 
                </div>
            </div>
        </div>
    </div>
</section>
    </body>

</html>
