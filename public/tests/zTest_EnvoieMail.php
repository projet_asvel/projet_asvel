<?php
    require_once '../../config/globalConfig.php';
    
    use Lib\olitsMailer;

    $subject = "test" ;
    $body = "test" ;
    $destinataire = "luc.gilot@sfr.fr,luc.gilot@wanadoo.fr,luc.gilot@limoog.net" ;
    $expediteur = 67 ;      // Luc Gilot
    $mailExpediteur = "luc.gilot@sfr.fr" ;
    $destinatairesVisibles = true ;

    $dataMail = array(
        'sujetMail' => $subject,
        'contenuMail' => $body,
        'idadherentexpediteur' => $expediteur,
        'dateMail' => Date('Y-m-d H:i:s'),
        'mailExpediteur' => $mailExpediteur,
        'destinatairesVisibles' => $destinatairesVisibles
    );

    $res = olitsMailer::EnvoyerMail($destinataire, $subject, $body, $mailExpediteur, $destinatairesVisibles);
    if (is_bool($res)) {
        echo "envoi réussi" ;
    } else {
        echo "échec" ;
    }
