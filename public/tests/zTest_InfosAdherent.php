<?php
require_once '../../config/globalConfig.php';

$repo = new \App\Repository\AdherentRepository ;
$adh = $repo->getEntityById(1) ;
$affiliations = $adh->getAffiliations() ;
echo $adh->getNomAdherent() . "<br>" ;
foreach ($affiliations as $value) {
    echo $value->getFederation()->getNomFederation() . "<br>" ;
    echo "depuis le " . date('d/m/Y', strtotime($value->getDateAffiliation())) . "<br>" ;
    echo "jusqu'au " . date('d/m/Y', strtotime($value->getDateFinAffiliation())) . "<br>" ;
}
