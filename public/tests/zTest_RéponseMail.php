<?php
// Page de réception pour les retours du sondage de test du mail
// LG 20210919

if (isset($_GET["adherent"])) {
    $lsAdherent = $_GET["adherent"] ;
} else {
    $lsAdherent = "inconnu" ;
}

if (isset($_GET["date"])) {
    $lsDate = $_GET["date"] ;
} else {
    $lsDate = "inconnue" ;
}

if (isset($_GET["spam"])) {
    $lsSpam = $_GET["spam"] ;
} else {
    $lsSpam = "inconnu" ;
}

$lsLog = date('Y-m-d H:i:s') . ";Date : " . $lsDate . ";Adhérent : " . $lsAdherent . ";Spam : " . $lsSpam . "\n" ;
$lsFileName = "../../logs/RetourSondageMail.log" ;
if (file_put_contents($lsFileName, $lsLog, FILE_APPEND) === false) {
    // Echec
    echo "L'enregistrement de votre réponse a échoué" ;
} else {
    echo "Votre réponse a été bien reçue : " . $lsLog . "<br>" ;
    echo "Merci de votre participation" ;
}
